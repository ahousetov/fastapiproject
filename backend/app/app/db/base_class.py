from typing import Any
from sqlalchemy.ext.declarative import as_declarative, declared_attr


@as_declarative()
class Base:
    id: Any
    __title: str
    __item_title: str
    __name__: str
    #__prefix__: str = ''

    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        # if cls.__prefix__:
        #     return f'{cls.__prefix__.lower()}.{cls.__name__.lower()}'
        return cls.__name__.lower()

    @staticmethod
    def get_title(cls) -> str:
        return getattr(cls, f"_{cls.__name__}__title", 'Nope')

