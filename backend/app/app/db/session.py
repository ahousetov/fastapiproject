from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import quoted_name
from app.core.config import settings

# import logging
# import pprint
#
# logging.basicConfig(level=logging.INFO)
# logger = logging.getLogger(__name__)

#print(f"settings.SQLALCHEMY_DATABASE_URI : {settings.SQLALCHEMY_DATABASE_URI}")

#logger.info(pprint.pformat(settings, indent=4))
engine = create_engine(settings.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True)
#engine.has_table(quoted_name("federal_districts", True))
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
