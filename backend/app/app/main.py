####################################### Версия Гриши #######################################

from dotenv import load_dotenv
from pathlib import Path

# Setting environment variables
three_dirs_up_env = Path(__file__).parents[3].joinpath('.env')
if three_dirs_up_env.exists():
    load_dotenv(three_dirs_up_env)

from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import JSONResponse
from starlette.middleware.cors import CORSMiddleware

from app.api.api_v1.api import api_router
from app.core.config import settings
from app.exceptions import API_JSON_Exception

# from initial_data import main as init_main
# from backend_pre_start import main as back_pre
# from celeryworker_pre_start import main as celery_pre

import logging

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_V1_STR}/openapi.json"
)


@app.exception_handler(API_JSON_Exception)
async def http_exception_handler(request: Request, exc: API_JSON_Exception):
    resp = JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.detail}
    )
    return resp


# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_router, prefix=settings.API_V1_STR)

if __name__ == '__main__':
    if settings.TESTING:
        import pytest
        import sys

        sys.exit(pytest.main(["-qq"]))
    else:
        import uvicorn

        # init_main()
        # back_pre()
        # celery_pre()
        uvicorn.run("main:app",
                    env_file=three_dirs_up_env,
                    reload=True,
                    log_level="info")

####################################### Версия Гриши #######################################
####################################### Версия базовая #######################################
# from fastapi import FastAPI, HTTPException, Request
# from fastapi.responses import JSONResponse
# from starlette.middleware.cors import CORSMiddleware
# from app.api.api_v1.api import api_router
# from app.core.config import settings
# from app.exceptions import API_JSON_Exception
# # from initial_data import main as init_main
# # from backend_pre_start import main as back_pre
# # from celeryworker_pre_start import main as celery_pre
#
# import logging
#
# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
#
# app = FastAPI(
#     title=settings.PROJECT_NAME, openapi_url=f"{settings.API_V1_STR}/openapi.json"
# )
#
#
# @app.exception_handler(API_JSON_Exception)
# async def http_exception_handler(request: Request, exc: API_JSON_Exception):
#     resp = JSONResponse(
#         status_code=exc.status_code,
#         content={"detail": exc.detail}
#     )
#     return resp
#
#
# # Set all CORS enabled origins
# if settings.BACKEND_CORS_ORIGINS:
#     app.add_middleware(
#         CORSMiddleware,
#         allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
#         allow_credentials=True,
#         allow_methods=["*"],
#         allow_headers=["*"],
#     )
#
# app.include_router(api_router, prefix=settings.API_V1_STR)
#
# if __name__ == '__main__':
#     if settings.TESTING:
#         import pytest
#         import sys
#
#         sys.exit(pytest.main(["-qq"]))
#     else:
#         import uvicorn
#         from pathlib import Path
#
#         # init_main()
#         # back_pre()
#         # celery_pre()
#         three_dirs_up_env = Path.cwd().parents[2] / '.env'
#         uvicorn.run("main:app",
#                     env_file=three_dirs_up_env,
#                     reload=True,
#                     log_level="info")
####################################### Версия базовая #######################################
