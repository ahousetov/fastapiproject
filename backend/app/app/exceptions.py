from typing import Any, List
from fastapi.responses import JSONResponse


class API_JSON_Exception(Exception):
    def __init__(self, name: str, status_code: int, detail: Any):
        self.name = name
        self.status_code = status_code
        self.detail=detail
