from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.declarants import Declarants
from app.schemas.declarants import Declarants_base, Declarants_create, Declarants_update, Declarants_delete


class CRUDDeclarants(CRUDBase[Declarants, Declarants_create, Declarants_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDDeclarants, self).__init__(model)


declarants = CRUDDeclarants(Declarants)
