from app.crud.base import CRUDBase
from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional
from app.models.public.profile_types import Profile_types
from app.schemas.public.profile_types import Profile_types_create, \
    Profile_types_update, \
    Profile_types_blocks, \
    Profile_types_block


class CRUDProfile_types(CRUDBase[Profile_types,
                                 Profile_types_create,
                                 Profile_types_update]):

    def get(self, db: Session, id: Any) -> Optional[Profile_types_blocks]:
        try:
            value = super(type(self), self).get(db=db, id=id)
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)
        block = Profile_types_block(id=value.id, name=value.name)
        profile_types = Profile_types_blocks(title=self.title,
                                             item_title=value.name,
                                             blocks=[block])
        return profile_types


profile_types = CRUDProfile_types(Profile_types)
