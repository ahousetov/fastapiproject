from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.public.stamp_codes import Stamp_codes
from app.models.public.nursery_stamp_codes import Nursery_stamp_codes
from app.schemas.public import Stamp_codes_base, Stamp_codes_create, Stamp_codes_update, Stamp_codes_delete


class CRUDStamp_codes(CRUDBase[Stamp_codes, Stamp_codes_create, Stamp_codes_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDStamp_codes, self).__init__(model)

    def get_object(self,
                   db: Session,
                   id: int = None,
                   id_name: str = 'profile_id',
                   is_deleted: bool = False,
                   omit_exception: bool = False) -> Optional[ModelType]:
        """
        Эта версия get_object возвращает ORM-объект по profile_id в nursery_stamp_codes.


        Возвращаем ORM-объект по id или выбрасываем исключение.
        """
        id_field = getattr(Nursery_stamp_codes, id_name, Nursery_stamp_codes.profile_id) \
            if id_name else Nursery_stamp_codes.profile_id
        query = db.query(self.model).join(Nursery_stamp_codes).filter(id_field == id)
        # TODO: У Stamp_codes и у Nursery_stamp_codes есть is_deleted.
        # Когда его учитывать, а когда нет?
        if is_deleted is not None and hasattr(Nursery_stamp_codes, 'is_deleted'):
            query = query.filter(Nursery_stamp_codes.is_deleted == is_deleted)

        first_obj = query.first()
        if not first_obj:
            if omit_exception:
                return None
            else:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"Не найдено записи по id = {id}"
                )
        return first_obj

    def update(
        self,
        db: Session,
        *,
        db_obj: ModelType,
        obj_in: Union[Stamp_codes_update, Dict[str, Any]],
        commit: bool = True
    ) -> ModelType:
        if db_obj:
            obj_data = jsonable_encoder(db_obj)
            upd_data = obj_in if isinstance(
                obj_in, dict) else obj_in.dict(
                exclude_unset=True)
            for field in obj_data:
                if field in upd_data:
                    setattr(db_obj, field, upd_data[field])
            try:
                db.add(db_obj)
            except Exception as e:
                raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                    detail=str(e))
            if commit:
                try:
                    db.commit()
                    db.refresh(db_obj)
                except Exception as e:
                    raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                        detail=str(e))
            return db_obj


stamp_codes = CRUDStamp_codes(Stamp_codes)
