from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from sqlalchemy.orm import Session, Query

# CRUDs
from app.crud.base import ModelType, CRUDBase, Contact_types as cont_t_enum

# Models
from app.models.public.bank_data import Bank_data as bank_model

# Schemas
from app.schemas import Bank_data, Bank_data_create, Bank_data_update


class CRUDBank_data(CRUDBase[bank_model, Bank_data_create, Bank_data_update]):

    def get_by_profile_id(self,
                          db: Session,
                          profile_id: int,
                          omit_exception: bool = False,
                          as_object: bool = False) -> Any:
        if as_object:
            bank_data = db.query(self.model) \
                .filter(self.model.profile_id == profile_id).first()
        else:
            bank_data = db.query(self.model.id,
                                 self.model.rs_number,
                                 self.model.bank_name,
                                 self.model.is_deleted,
                                 self.model.bic_number,
                                 self.model.profile_id)\
                .filter(self.model.profile_id == profile_id).first()
        if not bank_data and not omit_exception:
            model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
            raise Exception(
                f"Записи в '{model_name}' по profile_id={profile_id} не найдено.")
        return bank_data


crud_bank_data = CRUDBank_data(bank_model)
