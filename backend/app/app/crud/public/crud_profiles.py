from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase
from app.models.public.profiles import Profiles
from app.schemas.public import Profiles_create, Profiles_update
from fastapi import HTTPException, status


class CRUDProfiles(CRUDBase[Profiles, Profiles_create, Profiles_update]):

    def create_not_deleted(
            self,
            db: Session,
            *,
            obj_in: Profiles_create = None,
            obj_in_encoded: dict = None
    ) -> Profiles:
        if obj_in:
            obj_in_data = jsonable_encoder(obj_in, exclude_unset=True)
        elif obj_in_encoded:
            obj_in_data = obj_in_encoded
        else:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Don't have any data in crud.base.create."
            )
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def remove_by_names(self, db: Session, *,
                        generated_names: List[str] = None) -> Profiles:
        query_to_remove = db.query(self.model).filter(self.model.value.in_(generated_names),
                                                      self.model.description.in_(generated_names))
        query_to_remove.delete()
        db.commit()
        return True


profiles = CRUDProfiles(Profiles)
