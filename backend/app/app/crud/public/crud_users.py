from app.crud.base import ModelType, CRUDBase
from app.models.public.users import Users
from app.schemas.public.users import Users_create, \
    Users_update


class CRUDUsers(CRUDBase[Users, Users_create, Users_update]):
    pass


users = CRUDUsers(Users)
