from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from typing import Any, List, Optional, Type
from sqlalchemy import exc, text
from sqlalchemy.orm import Session

import schemas
from app.crud.base import ModelType, CRUDBase
from app.models.public.club_address import Club_addresses
from app.models.catalogs.address_types import Address_types
from app.models.catalogs.street_types import Street_types
from app.models.catalogs.house_types import House_types
from app.models.catalogs.flat_types import Flat_types
from app.models.public.profiles import Profiles
from app.models.public.profile_types import Profile_types
from app.models.catalogs.cities import Cities
from app.models.catalogs.regions import Regions
from app.models.catalogs.city_types import City_types
from app.schemas.public.club_address import Club_address_create, \
    Club_address_update


class CRUDClub_address(CRUDBase[Club_addresses,
                                Club_address_create,
                                Club_address_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDClub_address, self).__init__(model)
        self.address_types_name = f"{self.address_types}.name"
        self.city_types_name = f"{self.city_types}.name"
        self.cities_name = f"{self.cities}.name"
        self.regions_name = f"{self.regions}.name"
        self.street_types_name = f"{self.street_types}.name"
        self.house_types_name = f"{self.house_types}.name"
        self.flat_types_name = f"{self.flat_types}.name"
        self.profiles_id = f"{self.profiles}.id"

    def __query__(self, db: Session):
        club_address = db.query(
            self.model.id.label('id'),
            self.model.profile_id.label('profile_id'),
            self.model.address_type_id.label('address_type_id'),
            self.model.street_name.label('street_name'),
            self.model.house_name.label('house_name'),
            self.model.flat_name.label('flat_name'),
            self.model.postcode.label('postcode'),
            self.model.geo_lat.label('geo_lat'),
            self.model.geo_lon.label('geo_lon'),
            self.model.building_name.label('building_name'),
            Address_types.name.label(self.l_address_types_name),
            City_types.name.label(self.l_city_types_name),
            Cities.name.label(self.l_cities_name),
            Regions.name.label(self.l_regions_name),
            Street_types.name.label(self.l_street_types_name),
            House_types.name.label(self.l_house_types_name),
            Flat_types.name.label(self.l_flat_types_name)
        ).join(Profiles)\
            .join(Profile_types)\
            .join(Address_types, isouter=True) \
            .join(Cities, isouter=True) \
            .join(Regions, isouter=True) \
            .join(City_types, isouter=True) \
            .join(Street_types, isouter=True) \
            .join(House_types, isouter=True) \
            .join(Flat_types, isouter=True)
        return club_address

    def get(self, db: Session, id: int = None, profile_id: int = None) -> dict:
        """
        get - метод позволяющий получать данные об адресах клубов как
        по id самой записи в таблице, так и по profile_id.

        def get(self, db: session, id: int = None, profile_id: int = None) -> dict:
            ...

        Если необходимо получить данные по id то можно вызвать метод так:

        address = crud.club_addresses.get(db=db, id=id)

        Если нужно получить данные по profile_id, то:


        address = crud.club_addresses.get(db=db, profile_id=profile_id)
        """
        club_address = self.__query__(db=db)
        club_address = club_address.filter(self.model.is_deleted == False)
        if id:
            club_address = club_address.filter(self.model.id == id)
        elif profile_id:
            club_address = club_address.filter(self.model.profile_id == profile_id)
        else:
            raise HTTPException(
                status_code=status.HTTP_406_NOT_ACCEPTABLE,
                detail=f"Не задано параметров для поиска"
            )
        club_address = club_address.first()

        if not club_address:
            raise HTTPException(status_code=status.HTTP_,
                                detail=f"Не найдено адресов клуба по id = {id}")
        return club_address

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: dict = None
    ) -> List[ModelType]:
        sort_direction, sort_field = self._sort_params(
            sort_direction=sort_direction, sort_field=sort_field)
        model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
        club_addresses = self.__query__(db=db)
        fil_query = self._make_filter_model(filters_model=filters,
                                            column_descriptors=club_addresses.column_descriptions,
                                            not_deleted=True)
        club_addresses = club_addresses.filter(text(fil_query))\
            .order_by(text(f"{sort_field} {sort_direction}"))\
            .offset(skip).limit(limit)
        result_list = club_addresses.all()
        return result_list

    def delete_multi(self, db: Session, ids: List[int], commit=True):
        if ids:
            removed = db.query(self.model).\
                filter(self.model.id.in_(ids)). \
                delete(synchronize_session=False)
            if commit:
                db.commit()
            return removed

    def create_multi(self, db: Session, create_addresss: List[dict], commit=True):
        create_objs = []
        for address in create_addresss:
            obj = self.model(**address)
            create_objs.append(obj)

        db.add_all(create_objs)
        if commit:
            db.commit()

        return create_objs

    def split_objs_type_id_in_crud(self,
                                   db_objects: List[ModelType],
                                   objs_in: List[dict],
                                   profile_id: int = None
                                   ):
        """
        Функция разделяющая obj_in на
        списки delete_address_ids, update_address, create_address
        """
        # Объекты существующие в базе данных
        db_objects = {obj.id: {obj} for obj in db_objects}

        existing_address_in = {}
        create_address = []

        for address in objs_in:
            if ('id' in address) and (address['id']):
                address = {key: value for key,
                           value in address.items() if value is not None}
                existing_address_in.update({address['id']: address})
            else:
                if 'id' in address:
                    del address['id']
                if profile_id:
                    address['profile_id'] = profile_id
                create_address.append(address)

        # id обновляемых контактов = пересечение входных id и существующих в бд
        update_ids = set(db_objects.keys()) & set(existing_address_in.keys())
        update_address = [
            address for id,
            address in existing_address_in.items() if id in update_ids]

        # id удаляемых = разность существующих в бд и входных id
        delete_address_ids = list(set(db_objects.keys()) -
                                  set(existing_address_in.keys()))

        return delete_address_ids, update_address, create_address

    def _cte(self, db: Session, address_type_id: int = None):
        club_address = self.__query__(db=db)\
            .filter(
            self.model.address_type_id == address_type_id,
            self.model.is_deleted == False
        )
        return club_address.cte()

    def _subquery(self,
                  db: Session,
                  address_type_id: int = None,
                  name: str = "club_addresses"):
        club_address = self.__query__(db=db)\
            .filter(
            self.model.address_type_id == address_type_id,
            self.model.is_deleted == False
        )
        return club_address.subquery(name)

    def profile_check(self,
                      db: Session,
                      profile_id: int = None,
                      address_type_id: int = None) -> bool:

        return db.query(self.model).filter(self.model.profile_id == profile_id,
                                           self.model.address_type_id == address_type_id).first() is not None

    def create(self,
               db: Session,
               *,
               obj_in: Club_address_create = None,
               obj_in_dict: dict = None,
               ignore_check: bool = False) -> ModelType:
        if obj_in:
            obj_in_data = jsonable_encoder(obj_in, exclude_unset=True)
        elif obj_in_dict:
            obj_in_data = obj_in_dict
        else:
            raise HTTPException(
                status_code=status.HTTP_204_NO_CONTENT,
                detail="Нет достаточных данных для создания записи"
            )
        if ignore_check:
            db_obj = self.model(**obj_in_data, is_deleted=False)
        else:
            if self.profile_check(
                    db=db,
                    profile_id=obj_in_data['profile_id'],
                    address_type_id=obj_in_data['address_type_id']
            ):
                db_obj = self.model(**obj_in_data, is_deleted=False)
            else:
                raise HTTPException(
                    status_code=status.HTTP_409_CONFLICT,
                    detail="Контактные данные с этим профилем УЖЕ существуют."
                )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def remove_by_names(self, db: Session, *,
                        generated_names: List[str] = None) -> ModelType:
        query_to_remove = db.query(self.model).filter(self.model.postcode.in_(generated_names),
                                                      self.model.street_name.in_(
                                                          generated_names),
                                                      self.model.house_name.in_(
                                                          generated_names),
                                                      self.model.flat_name.in_(
                                                          generated_names),
                                                      self.model.geo_lat.in_(
                                                          generated_names),
                                                      self.model.geo_lon.in_(
                                                          generated_names),
                                                      self.model.building_name.in_(generated_names))
        query_to_remove.delete()
        db.commit()
        return True


club_address = CRUDClub_address(Club_addresses)
