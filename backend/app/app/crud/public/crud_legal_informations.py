from typing import Any, List, Optional, Dict, Union, Type
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types, ModelType
from app.models.public.legal_informations import Legal_informations
from app.schemas.public.legal_informations import \
    Legal_informations_create, \
    Legal_informations_update


class CRUDLegal_informations(CRUDBase[Legal_informations,
                                      Legal_informations_create,
                                      Legal_informations_update]):

    def get(self, db: Session, id: Any) -> Optional[Legal_informations]:
        return db.query(self.model).filter(self.model.id == id).first()

    def get_by_profile_id(self,
                          db: Session,
                          profile_id: int,
                          omit_exception: bool = False,
                          as_object: bool = False) -> Optional[List[Legal_informations]]:
        if as_object:
            legal_information = db.query(self.model).filter(
                self.model.profile_id == profile_id).first()
        else:
            legal_information = db.query(
                self.model.id).filter(
                self.model.profile_id == profile_id).first()
        if not legal_information and not omit_exception:
            model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
            raise Exception(
                f"Записи в '{model_name}' по profile_id={profile_id} не найдено.")
        return legal_information


legal_informations = CRUDLegal_informations(Legal_informations)
