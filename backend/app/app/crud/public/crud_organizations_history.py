from app.models.public import Organizations_history
from app.schemas.public import Organization_history_item
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types, ModelType
from typing import Any, List, Optional, Dict, Union, Type


class CRUDOrganizations_history(CRUDBase[Organizations_history, None, None]):
    def __init__(self, model: Type[ModelType] = None):
        super(CRUDOrganizations_history, self).__init__(model)
        # Комментирую старые имена на будущее, т.к. результат этого запроса
        # должен попадать в схему, составленную фронт-эндом. Генерация имён полей
        # привычным способом не сработает.
        # self.id = f"{self.organizations_history}.id"
        # self.profile_id = f"{self.organizations_history}.profile_id"
        # self.first_exhibition_date = f"{self.organizations_history}.first_exhibition_date"
        # self.last_exhibition_date = f"{self.organizations_history}.last_exhibition_date"
        # self.tests_over_past_year = f"{self.organizations_history}.tests_over_past_year"
        # self.availability_premises = f"{self.organizations_history}.availability_premises"
        # self.presentation = f"{self.organizations_history}.presentation"
        # self.tribal_reviews_declared = f"{self.organizations_history}.tribal_reviews_declared"
        # self.tribal_reviews_completed = f"{self.organizations_history}.tribal_reviews_completed"
        # self.succession_of_exhibition_activities = f"{self.organizations_history}.succession_of_exhibition_activities"
        # self.exhibition_activities_past_periods = f"{self.organizations_history}.exhibition_activities_past_periods"
        # self.exceptions_in_reports = f"{self.organizations_history}.exceptions_in_reports"
        # self.sanctions = f"{self.organizations_history}.sanctions"
        self.id = f"id"
        self.profile_id = f"profile_id"
        self.first_exhibition_date = f"first_exhibition_date"
        self.last_exhibition_date = f"last_exhibition_date"
        self.tests_over_past_year = f"tests_over_past_year"
        self.availability_premises = f"availability_premises"
        self.presentation = f"presentation"
        self.tribal_reviews_declared = f"tribal_reviews_declared"
        self.tribal_reviews_completed = f"tribal_reviews_completed"
        self.succession_of_exhibition_activities = f"succession_of_exhibition_activities"
        self.exhibition_activities_past_periods = f"exhibition_activities_past_periods"
        self.exceptions_in_reports = f"exceptions_in_reports"
        self.sanctions = f"sanctions"

    def get(self,
            db: Session,
            profile_id: int,
            omit_exception: bool = False,
            as_object: bool = False) -> Optional[List[Organization_history_item]]:
        if as_object:
            # Смысл as_object:
            # Для редактирования данных нам нужен ORM-объект, а не Row-объект.
            # ORM-объект можно получить, если указать в цели запроса только модель:
            #
            # db.query(self.model)
            #
            # Это имеет смысл, т.к. в этом случае мы сможем изменить данные лишь в таблице
            # к модели которой привязан CRUD.
            organizations_history = db.query(self.model)\
                .filter(self.model.profile_id == profile_id,
                        self.model.is_deleted == False)
        else:
            organizations_history = db.query(
                self.model.id.label(self.l_id),
                self.model.profile_id.label(self.l_profile_id),
                self.model.first_exhibition_date.label(self.l_first_exhibition_date),
                self.model.last_exhibition_date.label(self.l_last_exhibition_date),
                self.model.availability_premises.label(self.l_availability_premises),
                self.model.tests_over_past_year.label(self.l_tests_over_past_year),
                self.model.presentation.label(self.l_presentation),
                self.model.tribal_reviews_declared.label(
                    self.l_tribal_reviews_declared),
                self.model.tribal_reviews_completed.label(
                    self.l_tribal_reviews_completed),
                self.model.succession_of_exhibition_activities.label(
                    self.l_succession_of_exhibition_activities),
                self.model.exhibition_activities_past_periods.label(
                    self.l_exhibition_activities_past_periods),
                self.model.exceptions_in_reports.label(self.l_exceptions_in_reports),
                self.model.sanctions.label(self.l_sanctions)
            ).filter(self.model.profile_id == profile_id,
                     self.model.is_deleted == False)
        organizations_history = organizations_history.first()
        if not organizations_history and not omit_exception:
            model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
            raise Exception(
                f"Записи в '{model_name}' по profile_id={profile_id} не найдено.")
        return organizations_history


organizations_history = CRUDOrganizations_history(Organizations_history)
