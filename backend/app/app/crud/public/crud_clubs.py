from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

# Models
from app.models.public.profile_types import Profile_types
from app.models.public.legal_informations import Legal_informations
from app.models.public.users import Users
from app.models.public.profiles import Profiles
from app.models.public.bank_data import Bank_data
from app.models.public.club_address import Club_addresses
from app.models.public.contacts import Contacts
from app.models.relations.rel_users_profiles import Rel_users_profiles
from app.models.relations.rel_profiles_federations import Rel_profiles_federations
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.models.catalogs.organization_statuses import Organization_statuses
from app.models.catalogs.federations import Federations
from app.models.catalogs.address_types import Address_types
from app.models.catalogs.street_types import Street_types
from app.models.catalogs.house_types import House_types
from app.models.catalogs.flat_types import Flat_types
from app.models.catalogs.cities import Cities
from app.models.catalogs.city_types import City_types
from app.models.catalogs.contact_types import Contact_types as c_t_model

# CRUDs
from app.crud.base import ModelType, CRUDBase, Contact_types as cont_t_enum
from app.crud.relations.crud_profiles_contacts import rel_profiles_contacts as rpc
from app.crud.public.crud_club_address import club_address
from app.crud.public.crud_organizations_history import organizations_history as org_h
from app.crud.public.crud_organizations_membership_dues import organizations_membership_dues as org_m_d
from app.crud.public.crud_legal_informations import legal_informations as c_legal_info
from app.crud.public.crud_contacts import contacts as crud_contacts
from app.crud.catalogs.crud_federations import federations as c_fed
from app.crud.catalogs.crud_contact_types import contact_types as c_ctypes
from app.crud.catalogs.crud_address_types import address_types as c_add_types
from app.crud.catalogs.crud_organization_statuses import organization_statuses as c_org_stat
from app.crud.accesses.crud_user_accesses import user_accesses
from app.crud.accesses.crud_profile_type_requests import profile_type_requests
from app.crud.relations.crud_profiles_contacts import rel_profiles_contacts
from app.crud.public.crud_bank_data import crud_bank_data
from app.crud.relations.crud_rel_profiles_federations import rel_profiles_federations as rpf

# Schemas
import schemas
from app.schemas import Clubs_db, \
    Clubs_by_id, \
    Clubs_create, \
    Clubs_update, \
    Main_block_view, \
    Main_block_edit, \
    Contacts_blocks_view, \
    Contacts_blocks_edit, \
    Payment_blocks, \
    History_blocks, \
    Access_blocks_view, \
    Access_blocks_edit, \
    Access_block, \
    Contact_info_block_view, \
    Contact_info_block_edit, \
    Address_blocks_view, \
    Address_blocks_edit, \
    Address_info_block_view, \
    Address_info_block_edit, \
    Organization_history_item, \
    Organizations_membership_dues_item, \
    Clubs_by_id_catalogs, \
    Contact_types_block, \
    Organization_statuses_block, \
    Bank_data_update, \
    Bank_data_create, \
    Rel_profiles_federations_update, \
    Legal_informations_update

# HTTPException
from fastapi import HTTPException

# sqlalchemy
from sqlalchemy import func
from sqlalchemy.orm import Session


# FastAPI
from fastapi.encoders import jsonable_encoder


class CRUDClubs(CRUDBase[Profiles, Clubs_create, Clubs_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDClubs, self).__init__(model)
        self.title = "Клубы"
        self.id = "id"
        self.profiles_id = f"{self.profiles}.id"
        self.users_id = f"{self.users}.id"
        self.users_type = f"{self.users}.users_type"
        self.legal_informations_id = f"{self.legal_informations}.id"
        self.legal_informations_name = f"club_full_name"
        self.legal_informations_short_name = f"club_short_name"
        self.legal_informations_owner_position = f"owner_position"
        self.legal_informations_owner_name = f"owner_name"
        self.legal_informations_registration_date = f"legal_registration_date"
        self.legal_informations_liquidate_date = f"legal_liquidation_date"
        self.legal_informations_organization_status_id = f"legal_status_id"
        self.legal_informations_ogrn = f"ogrn"
        self.legal_informations_inn = f"inn"
        self.legal_informations_kpp = f"kpp"
        self.legal_informations_okpo = f"okpo"
        self.legal_informations_okved = f"okved"
        self.legal_informations_active_member = f"is_public"
        self.legal_informations_is_enable_web = f"is_active_user"
        self.legal_informations_folder_number = f"folder_number"
        self.federation_id = f"federation_id"
        self.organization_statuses_id = f"{self.organization_statuses}.id"
        self.organization_statuses_name = f"legal_status_name"
        self.federations_short_name = f"federation_name"
        self.bank_data_bank_name = f"bank_name"
        self.bank_data_bic_number = f"bic_number"
        self.bank_data_rs_number = f"bank_rs"
        self.address_types_name = f"{self.address_types}.name"
        self.club_address_id = f"{self.club_address}.id"
        self.club_address_address_type_id = f"{self.club_address}.address_type_id"
        self.club_address_postcode = f"{self.club_address}.postcode"
        self.club_address_city_id = f"{self.club_address}.city_id"
        self.club_address_street_type_id = f"{self.club_address}.street_type_id"
        self.club_address_street_name = f"{self.club_address}.street_name"
        self.club_address_house_type_id = f"{self.club_address}.house_type_id"
        self.club_address_house_name = f"{self.club_address}.house_name"
        self.club_address_flat_type_id = f"{self.club_address}.flat_type_id"
        self.club_address_flat_name = f"{self.club_address}.flat_name"
        self.club_address_geo_lat = f"{self.club_address}.geo_lat"
        self.club_address_geo_lon = f"{self.club_address}.geo_lon"
        self.club_address_is_deleted = f"{self.club_address}.is_deleted"
        self.club_address_type_id = f"type_id"
        self.club_address_type_name = f"type_name"
        self.city_types_name = f"{self.city_types}.name"
        self.cities_name = f"{self.cities}.name"
        self.contacts_id = f"id"
        self.contacts_type_id = f"type_id"
        self.contacts_postcode = f"postcode"
        self.contacts_value = f"value"
        self.contacts_is_main = f"is_main"
        self.contacts_description = f"description"
        self.contacts_is_deleted = f"is_deleted"
        self.regions_name = f"{self.regions}.name"
        self.street_types_name = f"{self.street_types}.name"
        self.house_types_name = f"{self.house_types}.name"
        self.flat_types_name = f"{self.flat_types}.name"
        self.contact_type_name = f"type_name"

    def get_all(self,
                db: Session,
                *,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id',
                filters: schemas.Filters_model = None,
                is_deleted: bool = False
                ) -> List[Dict[str, Any]]:
        sub_phones = rpc.make_subquery_contacts(db=db,
                                                type=cont_t_enum.phones,
                                                main=True)
        sub_email = rpc.make_subquery_contacts(db=db,
                                               type=cont_t_enum.emails,
                                               main=True)
        sub_site = rpc.make_subquery_contacts(db=db,
                                              type=cont_t_enum.sites,
                                              main=True)
        ca1 = club_address._subquery(db=db,
                                     address_type_id=2,
                                     name="ca1")
        ca2 = club_address._subquery(db=db,
                                     address_type_id=1,
                                     name="ca2")
        entry = db.query(self.model.id.label(self.id),
                         Legal_informations.name.label(self.l_legal_informations_name),
                         Legal_informations.short_name.label(
                             self.l_legal_informations_short_name),
                         Federations.short_name.label(self.l_federations_short_name),
                         Legal_informations.folder_number.label(
                             self.l_legal_informations_folder_number),
                         Legal_informations.owner_position.label(
                             self.l_legal_informations_owner_position),
                         Legal_informations.owner_name.label(
                             self.l_legal_informations_owner_name),
                         Organization_statuses.name.label(
                             self.l_organization_statuses_name),
                         Legal_informations.ogrn.label(self.l_legal_informations_ogrn),
                         Legal_informations.kpp.label(self.l_legal_informations_kpp),
                         Legal_informations.inn.label(self.l_legal_informations_inn),
                         Legal_informations.okpo.label(self.l_legal_informations_okpo),
                         Legal_informations.okved.label(
                             self.l_legal_informations_okved),
                         Bank_data.bank_name.label(self.l_bank_data_bank_name),
                         Bank_data.bic_number.label(self.l_bank_data_bic_number),
                         Bank_data.rs_number.label(self.l_bank_data_rs_number),
                         func.concat(ca1.c.postcode, " ", ca1.c.catalogs_city_types_name, " ",
                                     ca1.c.catalogs_cities_name, " ", ca1.c.catalogs_street_types_name, " ",
                                     ca1.c.street_name, " ", ca1.c.catalogs_house_types_name, " ",
                                     ca1.c.house_name, " ", ca1.c.catalogs_flat_types_name, " ",
                                     ca1.c.flat_name).label("legal_address"),
                         ca1.c.geo_lat.concat(", ").concat(
            ca1.c.geo_lon).label("legal_address_coordinates"),

            func.concat(ca2.c.postcode, " ", ca2.c.catalogs_city_types_name, " ",
                        ca2.c.catalogs_cities_name, " ", ca2.c.catalogs_street_types_name, " ",
                        ca2.c.street_name, " ", ca2.c.catalogs_house_types_name, " ",
                        ca2.c.house_name, " ", ca2.c.catalogs_flat_types_name, " ",
                        ca2.c.flat_name).label("fact_address"),
            ca2.c.geo_lat.concat(", ").concat(
            ca2.c.geo_lon).label("fact_address_coordinates"),
            sub_phones.c.phones.label(cont_t_enum.phones.name),
            sub_email.c.emails.label(cont_t_enum.emails.name),
            sub_site.c.sites.label(cont_t_enum.sites.name),
            Legal_informations.active_member.label(
                             self.l_legal_informations_active_member),
            Legal_informations.is_enable_web.label(
                             self.l_legal_informations_is_enable_web)
        )\
            .select_from(self.model) \
            .join(Profile_types) \
            .join(Rel_users_profiles) \
            .join(Legal_informations) \
            .join(Users) \
            .join(Organization_statuses, isouter=True)\
            .join(Rel_profiles_federations, isouter=True)\
            .join(Federations, isouter=True)\
            .join(Bank_data, isouter=True)\
            .join(sub_phones, isouter=True)\
            .join(sub_email, isouter=True)\
            .join(sub_site, isouter=True)\
            .join(ca1, ca1.c.profile_id == self.model.id, isouter=True)\
            .join(ca2, ca2.c.profile_id == self.model.id, isouter=True)

        entry = entry.filter(
            Legal_informations.is_deleted == False,
            Profile_types.name == "Club")

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        # Пользовательские фильтры
        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)
        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        entry = entry.offset(skip).limit(limit)

        result = entry.all()

        return [dict(row) for row in result]

    def get(self,
            db: Session,
            id: int,
            is_deleted: bool = False
            ) -> Dict[str, Any]:
        main = db.query(self.model.id.label(self.l_profiles_id),
                        Legal_informations.registration_date.label(
                            self.l_legal_informations_registration_date),
                        Legal_informations.liquidate_date.label(
                            self.l_legal_informations_liquidate_date),
                        Legal_informations.organization_status_id.label(
                            self.l_legal_informations_organization_status_id),
                        Organization_statuses.name.label(
                            self.l_organization_statuses_name),
                        Legal_informations.name.label(self.l_legal_informations_name),
                        Legal_informations.short_name.label(
                            self.l_legal_informations_short_name),
                        Legal_informations.owner_position.label(
                            self.l_legal_informations_owner_position),
                        Legal_informations.owner_name.label(
                            self.l_legal_informations_owner_name),
                        Rel_profiles_federations.federation_id.label(
                            self.l_federation_id),
                        Federations.short_name.label(self.l_federations_short_name),
                        Legal_informations.folder_number.label(
                            self.l_legal_informations_folder_number),
                        Legal_informations.is_enable_web.label(
                            self.l_legal_informations_is_enable_web),
                        Legal_informations.ogrn.label(self.l_legal_informations_ogrn),
                        Legal_informations.inn.label(self.l_legal_informations_inn),
                        Legal_informations.kpp.label(self.l_legal_informations_kpp),
                        Legal_informations.okpo.label(self.l_legal_informations_okpo),
                        Legal_informations.okved.label(self.l_legal_informations_okved),
                        Bank_data.bank_name.label(self.l_bank_data_bank_name),
                        Bank_data.bic_number.label(self.l_bank_data_bic_number),
                        Bank_data.rs_number.label(self.l_bank_data_rs_number),
                        Legal_informations.active_member.label(
                            self.l_legal_informations_active_member)
                        )\
            .join(Profile_types)\
            .join(Rel_users_profiles)\
            .join(Legal_informations)\
            .join(Users) \
            .join(Organization_statuses, isouter=True) \
            .join(Rel_profiles_federations, isouter=True) \
            .join(Federations, isouter=True) \
            .join(Bank_data, isouter=True)\
            .filter(self.model.id == id)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            main = main.filter(self.model.is_deleted == False)

        main = main.first()

        if not main:
            raise HTTPException(status_code=404,
                                detail=f"Клуба с profile_id = {id} не найдено.")

        contacts_clubs = db.query(Contacts.id.label(self.l_contacts_id),
                                  c_t_model.name.label(self.contact_type_name),
                                  Contacts.type_id.label(self.contacts_type_id),
                                  Contacts.value.label(self.l_contacts_value),
                                  Contacts.is_main.label(self.l_contacts_is_main),
                                  Contacts.description.label(self.l_contacts_description)) \
            .join(Rel_profiles_contacts)\
            .join(c_t_model, isouter=True)\
            .filter(Contacts.is_deleted == False,
                    Rel_profiles_contacts.profile_id == id)
        contacts_clubs = contacts_clubs.all()
        addresses = db.query(Club_addresses.id,
                             Address_types.name.label(self.club_address_type_name),
                             Club_addresses.type.label(self.club_address_type_id),
                             Club_addresses.postcode.label(
                                 self.l_club_address_postcode),
                             City_types.name.label(self.l_city_types_name),
                             Club_addresses.city_id.label(self.l_club_address_city_id),
                             Cities.name.label(self.l_cities_name),
                             Club_addresses.street_type_id.label(
                                 self.l_club_address_street_type_id),
                             Street_types.name.label(self.l_street_types_name),
                             Club_addresses.street_name.label(
                                 self.l_club_address_street_name),
                             Club_addresses.house_type_id.label(
                                 self.l_club_address_house_type_id),
                             House_types.name.label(self.l_house_types_name),
                             Club_addresses.house_name.label(
                                 self.l_club_address_house_name),
                             Club_addresses.flat_type_id.label(
                                 self.l_club_address_flat_type_id),
                             Flat_types.name.label(self.l_flat_types_name),
                             Club_addresses.flat_name.label(
                                 self.l_club_address_flat_name),
                             Club_addresses.geo_lat.label(self.l_club_address_geo_lat),
                             Club_addresses.geo_lon.label(self.l_club_address_geo_lon),
                             func.concat(Club_addresses.postcode, ' ',
                                         City_types.name, ' ',
                                         Cities.name, ' ',
                                         Street_types.name, ' ',
                                         Club_addresses.street_name, ' ',
                                         House_types.name, ' ',
                                         Club_addresses.house_name, ' ',
                                         Flat_types.name, ' ',
                                         Club_addresses.flat_name, ' ',
                                         Club_addresses.geo_lat, ' ',
                                         Club_addresses.geo_lon
                                         ).label('address')
                             )\
            .join(Address_types)\
            .join(Cities, isouter=True)\
            .join(City_types, isouter=True)\
            .join(Street_types, isouter=True)\
            .join(House_types, isouter=True)\
            .join(Flat_types, isouter=True)\
            .filter(Club_addresses.profile_id == id)\
            .order_by(Club_addresses.address_type_id)
        addresses = addresses.all()
        pay_list = org_m_d.get(db=db, profile_id=id, omit_exception=True)

        history_info = org_h.get(db=db, profile_id=id, omit_exception=True)

        catalogs_dict = dict(
            organization_statuses=c_org_stat.get_all(db=db),
            federations=c_fed.get_federation_by_profile_id(db=db, profile_id=id),
            contact_types=c_ctypes.get_all(db=db),
            address_types=c_add_types.get_all(db=db)
        )

        # Здесь начинается работа со списками заявок
        u_access = user_accesses.get_list_accesses_profile_id(db=db, profile_id=id)
        prof_t_r = profile_type_requests.get_order_types_list(db=db)
        u_access_ids = {u[1]: {u} for u in u_access}
        prof_t_r_ids = {p[0]: {p} for p in prof_t_r}
        active_ids = set(u_access_ids.keys()) & set(prof_t_r_ids.keys())

        return dict(main=dict(main),
                    contacts=[dict(contact) for contact in contacts_clubs],
                    addresses=[dict(address) for address in addresses],
                    payments=[dict(pay) for pay in pay_list],
                    history=dict(history_info) if history_info else None,
                    catalogs=catalogs_dict,
                    access=dict(active_ids=active_ids, prof_t_r=prof_t_r)
                    )

    def update(
            self,
            db: Session,
            id: int,
            update_data: Dict[str, Any]
    ) -> bool:
        legal_info_dict = update_data.get('legal_informations')
        bank_dict = update_data.get('bank_data')
        profs_feds_dict = update_data.get('profiles_federations')
        history_schema_dict = update_data.get('history')
        contact_data = update_data.get('contacts')
        address = update_data.get('addresses')
        payments = update_data.get('payments')

        ##########################################################################
        # Беру код из backend/app/app/api/api_v1/endpoints/catalogs/federations.py
        # для обновления контактов.
        contacts = crud_contacts.get_by_profile_id(db, profile_id=id)
        contact_data = contact_data
        delete_contact_ids, update_contacts, create_contacts = crud_contacts.split_objs_type_id_in_crud(
            db_objects=contacts,
            objs_in=contact_data
        )
        if delete_contact_ids:
            rel_profiles_contacts.delete_by_contact_ids(
                db=db, contact_ids=delete_contact_ids, commit=False)
            crud_contacts.delete_multi(db=db, ids=delete_contact_ids, commit=False)
        create_objs = crud_contacts.create_multi(
            db=db, create_contacts=create_contacts, commit=True)
        if create_objs:
            rel_profiles_contacts.create_multi_by_contact_ids(
                db=db,
                contact_ids=[contact.id for contact in create_objs],
                profile_id=id,
                commit=False
            )
        if update_contacts:
            crud_contacts.update_multi(db=db, objs_in=update_contacts, commit=False)
        ##########################################################################
        ##########################################################################
        # Здесь работаю с адресными данными. Т.к. запись в club_address одна на profile_id, работают только с
        # одним элементом.
        all_addresses = club_address.get_all_objects(
            db=db, condition=[club_address.model.profile_id == id])
        all_addresses_data = address
        delete_address_ids, update_address, create_address = club_address.split_objs_type_id_in_crud(
            db_objects=all_addresses,
            objs_in=all_addresses_data
        )

        if delete_address_ids:
            club_address.delete_multi(db=db, ids=delete_address_ids, commit=False)
        if create_address:
            for address in create_address:
                if 'profile_id' not in address:
                    address['profile_id'] = id
            club_address.create_multi(
                db=db, create_addresss=create_address, commit=False)

        for addr_data in update_address:
            if "id" in addr_data:
                address_obj = club_address.get_object(db=db, id=addr_data['id'])
                del addr_data['id']
                club_address.update(
                    db=db,
                    db_obj=address_obj,
                    obj_in=jsonable_encoder(addr_data),
                    commit=False)
        ##########################################################################
        ##########################################################################
        # Беру код из backend/app/app/api/api_v1/endpoints/catalogs/federations.py
        # для обновления платежей.
        org_mem_dues = org_m_d.get(db, profile_id=id, omit_exception=True)
        org_mem_dues_data = payments
        delete_org_mem_due_ids, update_org_mem_dues, create_org_mem_dues = org_m_d.split_objs_type_id_in_crud(
            db_objects=org_mem_dues,
            objs_in=org_mem_dues_data,
            profile_id=id
        )
        if delete_org_mem_due_ids:
            org_m_d.delete_multi(db=db, ids=delete_org_mem_due_ids, commit=False)
        if create_org_mem_dues:
            org_m_d.create_multi(
                db=db,
                create_org_mem_dues=create_org_mem_dues,
                commit=False)
        if update_org_mem_dues:
            org_m_d.update_multi(db=db, objs_in=update_org_mem_dues, commit=False)
        ##########################################################################
        # ############################### Работаю с Organization_history #########
        if 'profile_id' not in history_schema_dict:
            history_schema_dict['profile_id'] = id

        history_obj = org_h.get(
            db=db,
            profile_id=id,
            omit_exception=True,
            as_object=True)

        # history_schema_dict = {k: v for k, v in history_schema_dict.items() if v is not None}
        if history_obj:
            org_h.update(db=db, db_obj=history_obj, obj_in=history_schema_dict)
        else:
            org_h.create(db=db, obj_in_dict=history_schema_dict)
        ##########################################################################
        ##########################################################################
        # ############################### Main_schema ############################
        # ############################### Работаю с Legal_informations ###########
        legal_informations_obj = c_legal_info.get_by_profile_id(db=db,
                                                                profile_id=id,
                                                                omit_exception=True,
                                                                as_object=True)
        legal_informations_update_obj_in = Legal_informations_update(**legal_info_dict)
        c_legal_info.update(db=db,
                            db_obj=legal_informations_obj,
                            obj_in=legal_informations_update_obj_in,
                            commit=False)

        # Rel_profiles_federations
        rel_prof_fed = rpf.get_legal_info(db=db, profile_id=id)
        profs_feds_dict['profile_id'] = id
        if rel_prof_fed is not None:
            rpf.update(db=db, db_obj=rel_prof_fed, obj_in=profs_feds_dict, commit=False)
        else:
            rpf.create(db, obj_in_dict=profs_feds_dict, commit=False)

        # /Rel_profiles_federations
        # ############################### Работаю с Bank_data ####################
        bank_orm_obj = crud_bank_data.get_by_profile_id(db=db,
                                                        profile_id=id,
                                                        omit_exception=True,
                                                        as_object=True)
        if bank_orm_obj:
            bank_data_update_obj_in = Bank_data_update(
                **bank_dict, profile_id=id, id=bank_orm_obj.id)
            crud_bank_data.update(db=db,
                                  db_obj=bank_orm_obj,
                                  obj_in=bank_data_update_obj_in,
                                  commit=False)
        else:
            bank_data_create = Bank_data_create(**bank_dict, profile_id=id)
            crud_bank_data.create(db=db,
                                  obj_in=bank_data_create,
                                  commit=False)
        ##########################################################################

        # Вот тут всё и обновиться.
        try:
            db.commit()
        except Exception:
            raise HTTPException(status_code=400,
                                detail=f"Редактирование питомника по profile_id={id} не удалось.")

        return True


clubs_crud_explicit = CRUDClubs(Profiles)
