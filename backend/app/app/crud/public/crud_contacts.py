from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from fastapi.encoders import jsonable_encoder
from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from sqlalchemy import case, insert, delete
from app.crud.base import ModelType, CRUDBase, Contact_types
from app.crud.relations.crud_profiles_contacts import CRUDRel_profiles_contacts
from app.models.public.contacts import Contacts
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.models.relations.rel_national_breed_clubs_contacts import Rel_national_breed_clubs_contacts
from app.schemas.public import Contacts_create, \
    Contacts_update
from pydantic import BaseModel
from app import schemas
from app import crud
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts


class CRUDContacts(CRUDBase[Contacts, Contacts_create, Contacts_update]):
    def __init__(self, model: Type[ModelType] = None):
        super(CRUDContacts, self).__init__(model)
        self.rel_profiles_contacts_profile_id = f"{self.rel_profiles_contacts}.profile_id"
        self.value = 'value'
        self.description = 'description'
        self.is_main = 'is_main'

    def get(self,
            db: Session,
            id: int
            ) -> Dict[str, Any]:
        contact = db.query(self.model.id,
                           self.model.value,
                           self.model.description,
                           self.model.is_main,
                           Rel_profiles_contacts.profile_id)\
            .join(Rel_profiles_contacts)\
            .filter(self.model.id == id, self.model.is_deleted == False)
        contact = contact.first()
        if not contact:
            exp = {
                "title": self.title,
                "item_title": f'Нет записи по id {id}',
                "block_icon": 'info_outline'
            }
            raise Exception(exp)

        return dict(contact)

    def get_all(self,
                db: Session,
                *,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id',
                filters: schemas.Filters_model = None,
                is_deleted: bool = False,
                profile_id: int = None
                ) -> List[dict]:
        contacts = db.query(self.model.id,
                            self.model.value,
                            self.model.description,
                            self.model.is_main,
                            self.model.type_id,
                            Rel_profiles_contacts.profile_id) \
            .join(Rel_profiles_contacts)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            contacts = contacts.filter(self.model.is_deleted == False)
        if profile_id is not None:
            contacts = contacts.filter(Rel_profiles_contacts.profile_id == profile_id)

        # Пользовательские фильтры
        if filters:
            contacts = self._make_filter_model_without_strings(
                filters_model=filters, query=contacts)
        contacts = self._sort_params_without_strings(query=contacts,
                                                     sort_direction=sort_direction,
                                                     sort_field=sort_field,
                                                     filters=filters)
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        contacts = contacts.offset(skip).limit(limit)
        result = contacts.all()
        return [dict(contact) for contact in result]

    def get_all_objects(self,
                        db: Session,
                        is_deleted: bool = False,
                        condition: list = None,
                        omit_exception: bool = False) -> Optional[List[ModelType]]:
        m_name = self.model.__name__
        query = db.query(self.model).join(Rel_profiles_contacts)
        if condition is not None:
            query = query.filter(*condition)
        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            query = query.filter(self.model.is_deleted == False)
        objs = query.all()
        if not objs and not omit_exception:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Не найдено записи по id = {id}"
                                )
        return objs

    def update(self,
               db: Session,
               id: int,
               update_data: dict = None) -> bool:
        contact_data = update_data.get('contact')
        profile_data = update_data.get('rel_profiles_contacts')

        contact = db.query(self.model).filter(self.model.id == id).first()
        contact = super().update(db, db_obj=contact, obj_in=contact_data)
        profile_data['contact_id'] = contact.id
        relation = db.query(Rel_profiles_contacts).filter(
            Rel_profiles_contacts.contact_id == contact.id).first()
        relation = crud.rel_profiles_contacts.update(
            db=db, db_obj=relation, obj_in=profile_data)
        return True

    def create(self,
               db: Session,
               create_data: Dict[str, Any]) -> bool:
        contact_data = create_data.get('contact')
        profile_data = create_data.get('rel_profiles_contacts')

        if not profile_data:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail=f"Параметра 'profile_id' не хватает")
        try:
            contact = super().create(db, obj_in_dict=contact_data)
            profile_data['contact_id'] = contact.id
            rel = crud.rel_profiles_contacts.create(db, obj_in_dict=profile_data)
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=str(e))
        return True

    def create_not_deleted(
            self,
            db: Session,
            *,
            obj_in: Contacts_create = None,
            obj_in_encoded: dict = None
    ) -> Contacts:
        if obj_in:
            new_contact_data, profile_data = self.split_obj_in(
                obj_in, exclude_unset=True)
        elif obj_in_encoded:
            new_contact_data, profile_data = obj_in_encoded, None
        else:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Don't have any data in crud.base.create."
            )
        new_contact = self.model(**new_contact_data)
        db.add(new_contact)
        db.commit()
        db.refresh(new_contact)
        return new_contact

    def split_obj_in(self, obj_in=None, exclude_unset=False):
        """
        Функция разделяющая obj_in на
        словари breed и additional
        """
        if not obj_in:
            return None, None
        obj_in_data = jsonable_encoder(obj_in)
        additional = {}
        item = {}
        for k, v in obj_in_data.items():
            if k in ['profile_id']:
                if exclude_unset:
                    if v:
                        additional.update({k: v})
                else:
                    additional.update({k: v})
            else:
                if exclude_unset:
                    if v:
                        item.update({k: v})
                else:
                    item.update({k: v})
        return item, additional

    def drop_main_flag(self, db: Session, id: Any):
        contact = self.get(db=db, id=id)
        relation = profile_id = None
        if isinstance(contact, dict) and 'blocks' in contact:
            profile_id, type_id = contact['blocks'][0]['profile_id'], contact['blocks'][0]['type_id']
        elif isinstance(contact, BaseModel):
            contact = contact.dict()
            profile_id, type_id = contact['blocks'][0]['profile_id'], contact['blocks'][0]['type_id']
        else:
            relation, type_id = contact.rel[0], contact.type_id
        if not type_id:
            raise Exception("У контакта отсутствует 'type_id'")
        contacts = db.query(Rel_profiles_contacts)\
            .join(self.model)\
            .filter(Rel_profiles_contacts.profile_id == relation.profile_id if relation else profile_id,
                    self.model.type_id == type_id,
                    self.model.id != id)
        contacts = contacts.all()
        for contact in contacts:
            setattr(contact, 'is_main', False)
            db.add(contact)
        db.commit()
        return contacts

    def get_by_profile_id(self, db: Session, profile_id: int) -> List[dict]:
        contacts = db.query(self.model.id,
                            self.model.value,
                            self.model.description,
                            self.model.is_main,
                            self.model.type_id,
                            Rel_profiles_contacts.profile_id)\
            .select_from(Rel_profiles_contacts)\
            .join(self.model)\
            .filter(Rel_profiles_contacts.profile_id == profile_id,
                    self.model.is_deleted == False)
        contacts = contacts.all()
        return contacts

    def get_by_nbc_id(self, db: Session, nbc_id: int):
        contacts = db.query(self.model) \
            .join(Rel_national_breed_clubs_contacts, Rel_national_breed_clubs_contacts.contact_id == self.model.id) \
            .filter(self.model.is_deleted == False, Rel_national_breed_clubs_contacts.national_breed_club_id == nbc_id)

        return contacts.all()

    def split_objs_type_id_in_crud(self,
                                   db_objects: List[ModelType],
                                   objs_in: List[dict],
                                   ):
        """
        Функция разделяющая obj_in на
        списки delete_contact_ids, update_contacts, create_contacts
        """
        # Объекты существующие в базе данных
        db_objects = {obj.id: {obj} for obj in db_objects}

        existing_contacts_in = {}
        create_contacts = []

        for contact in objs_in:
            if ('id' in contact) and (contact['id']):
                contact = {key: value for key,
                           value in contact.items() if value is not None}
                existing_contacts_in.update({contact['id']: contact})
            else:
                if 'id' in contact:
                    del contact['id']
                if not contact['value']:
                    raise Exception(
                        'При создании нового контакта поле value является обязательным')
                # Добавляем обязательный параметр создаваемому контакту
                # type = getattr(Contact_types, contact_type)
                # type_id = type.value
                # contact.update({'type_id': type_id})
                create_contacts.append(contact)

            # id обновляемых контактов = пересечение входных id и существующих в бд
        update_ids = set(db_objects.keys()) & set(existing_contacts_in.keys())
        update_contacts = [
            contact for id,
            contact in existing_contacts_in.items() if id in update_ids]

        # id удаляемых = разность существующих в бд и входных id
        delete_contact_ids = list(set(db_objects.keys()) -
                                  set(existing_contacts_in.keys()))

        return delete_contact_ids, update_contacts, create_contacts

    def delete_multi(self, db: Session, ids: List[int], commit=True):
        if ids:
            removed = db.query(self.model).\
                filter(self.model.id.in_(ids)). \
                delete(synchronize_session=False)
            if commit:
                db.commit()
            return removed

    def create_multi(self, db: Session, create_contacts: List[dict], commit=True):
        if create_contacts:
            create_objs = []
            for contact in create_contacts:
                obj = self.model(**contact)
                create_objs.append(obj)

            db.add_all(create_objs)
            if commit:
                db.commit()

            return create_objs

    def update_multi(self,
                     db: Session, *,
                     objs_in: dict,
                     commit=True):

        if objs_in:

            values, descriptions, is_main = {}, {}, {}
            for contact_data in objs_in:
                id = contact_data['id']
                for k, v in contact_data.items():
                    if k == self.value:
                        values[id] = v
                    elif k == self.description:
                        descriptions[id] = v
                    elif k == self.is_main:
                        is_main[id] = v

            # Если у всех обновляемых контактов количество полей одинаково -> update
            # одним запросом
            if len(values) == len(descriptions) == len(is_main):

                db.query(self.model).filter(
                    self.model.id.in_(values)
                ).update({self.model.value: case(
                    values,
                    value=self.model.id
                ),
                    self.model.description: case(
                        descriptions,
                        value=self.model.id
                ),
                    self.model.is_main: case(
                        is_main,
                        value=self.model.id
                )
                }, synchronize_session=False)

            # Если у обновляемых контактов количество полей разное -> update
            # несколькими запросами
            else:

                ids = list(
                    (set(
                        values.keys()) | set(
                        descriptions.keys()) | set(
                        is_main.keys())))

                db_objs = db.query(self.model).filter(self.model.id.in_(ids)).all()

                for obj in db_objs:
                    if obj.id in values:
                        obj.value = values[obj.id]
                    if obj.id in descriptions:
                        obj.description = descriptions[obj.id]
                    if obj.id in is_main:
                        obj.is_main = is_main[obj.id]

                db.add_all(db_objs)

            if commit:
                db.commit()

            return True

    def remove_by_names(self,
                        db: Session,
                        rel_crud: CRUDRel_profiles_contacts,
                        generated_names: List[str] = None) -> Contacts:
        query_to_remove = db.query(self.model).filter(self.model.description.in_(generated_names),
                                                      self.model.value.in_(generated_names))
        selected_contacts = query_to_remove.all()
        contacts_ids = [c.id for c in selected_contacts]
        removed = rel_crud.delete_by_contact_ids(db=db,
                                                 contact_ids=contacts_ids)
        query_to_remove.delete()
        db.commit()
        return True


contacts = CRUDContacts(Contacts)
