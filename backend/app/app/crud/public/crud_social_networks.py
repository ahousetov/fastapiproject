from typing import Any, Optional

from fastapi import HTTPException
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.public.social_networks import Social_networks
from app.schemas.public.social_networks import Social_networks_create
from app.schemas.public.social_networks import Social_networks_update


class CRUDSocial_networks(
    CRUDBase[Social_networks,
             Social_networks_create,
             Social_networks_update]):
    def get(self, db: Session, id: Any, **kwargs) -> Optional[Social_networks]:
        return db.query(self.model).filter(self.model.id == id).first()


social_networks = CRUDSocial_networks(
    Social_networks)
