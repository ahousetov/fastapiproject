from app.models.public import Organizations_membership_dues
from app.schemas.public import Organizations_membership_dues_item
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types, ModelType
from typing import Any, List, Optional, Dict, Union, Type


class CRUDOrganizations_membership_dues(
        CRUDBase[Organizations_membership_dues, None, None]):
    def __init__(self, model: Type[ModelType] = None):
        super(CRUDOrganizations_membership_dues, self).__init__(model)
        # Комментирую старые имена на будущее, т.к. результат этого запроса
        # должен попадать в схему, составленную фронт-эндом. Генерация имён полей
        # привычным способом не сработает.
        self.id = f"id"
        self.is_deleted = f"is_deleted"
        self.profile_id = f"profile_id"
        self.year_payment = f"year_payment"
        self.date_payment = f"date_payment"
        self.is_paid = f"is_paid"
        self.comments = f"comments"

    def get(self,
            db: Session,
            profile_id: int,
            omit_exception: bool = False) -> Optional[List[Organizations_membership_dues_item]]:
        org_mem_dues = db.query(self.model.id.label(self.l_id),
                                self.model.year_payment.label(self.l_year_payment),
                                self.model.date_payment.label(self.l_date_payment),
                                self.model.is_paid.label(self.l_is_paid),
                                self.model.comments.label(self.l_comments))\
            .filter(self.model.profile_id == profile_id, self.model.is_deleted == False)
        org_mem_dues = org_mem_dues.all()
        if not org_mem_dues and not omit_exception:
            model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
            raise Exception(
                f"Записи в '{model_name}' по profile_id={profile_id} не найдено.")
        return org_mem_dues

    def create_multi(self, db: Session, create_org_mem_dues: List[dict], commit=True):
        if create_org_mem_dues:
            create_objs = [self.model(**org_mem_due)
                           for org_mem_due in create_org_mem_dues]
            db.add_all(create_objs)
            if commit:
                db.commit()
            return create_objs

    def split_objs_type_id_in_crud(self,
                                   db_objects: List[ModelType],
                                   objs_in: List[dict],
                                   profile_id: int = None
                                   ):
        """
        Функция разделяющая obj_in на
        списки delete_org_mem_dues_ids, update_org_mem_dues, create_org_mem_dues
        """
        # Объекты существующие в базе данных
        # в данном случае здесь будут обрабатываться данные о платежах.
        # Их может не быть, поэтому составление списка {id: (payments obj)} не
        # имеет смысла.
        db_objects = {obj.id: {obj} for obj in db_objects}
        existing_org_mem_dues_in = {}
        create_org_mem_dues = []
        for org_mem_dues in objs_in:
            if ('id' in org_mem_dues) and (org_mem_dues['id']):
                org_mem_dues = {key: value for key,
                                value in org_mem_dues.items() if value is not None}
                existing_org_mem_dues_in.update({org_mem_dues['id']: org_mem_dues})
            else:
                if 'id' in org_mem_dues:
                    del org_mem_dues['id']
                if profile_id:
                    org_mem_dues['profile_id'] = profile_id
                create_org_mem_dues.append(org_mem_dues)

        # id обновляемых контактов = пересечение входных id и существующих в бд
        update_ids = set()
        if db_objects:
            update_ids = set(db_objects.keys()) & set(existing_org_mem_dues_in.keys())
        update_org_mem_dues = [
            org_mem_dues for id,
            org_mem_dues in existing_org_mem_dues_in.items() if id in update_ids]

        # id удаляемых = разность существующих в бд и входных id

        delete_org_mem_dues_ids = list(
            set(db_objects.keys()) - set(existing_org_mem_dues_in.keys()))
        return delete_org_mem_dues_ids, update_org_mem_dues, create_org_mem_dues

    def delete_multi(self, db: Session, ids: List[int], commit=True):
        if ids:
            removed = db.query(self.model).\
                filter(self.model.id.in_(ids)). \
                delete(synchronize_session=False)
            if commit:
                db.commit()
            return removed

    def update_multi(self, db: Session, *, objs_in: List[dict], commit=True):
        if objs_in:
            # values, descriptions, is_main = {}, {}, {}
            is_deleted, profile_id, year_payment, date_payment, is_paid, comments = {}, {}, {}, {}, {}, {}
            for org_mem_dues_data in objs_in:
                id = org_mem_dues_data['id']
                for k, v in org_mem_dues_data.items():
                    if k == self.is_deleted:
                        is_deleted[id] = v
                    elif k == self.profile_id:
                        profile_id[id] = v
                    elif k == self.year_payment:
                        year_payment[id] = v
                    elif k == self.date_payment:
                        date_payment[id] = v
                    elif k == self.is_paid:
                        is_paid[id] = v
                    elif k == self.comments:
                        comments[id] = v

            # Если у всех обновляемых платежей количество полей одинаково -> update одним запросом
            # if len(values) == len(descriptions) == len(is_main):
            if len(is_deleted) == len(profile_id) == len(year_payment) == len(
                    date_payment) == len(is_paid) == len(comments):
                db.query(self.model).filter(
                    self.model.id.in_(is_deleted)
                ).update({self.model.value: case(
                    is_deleted,
                    value=self.model.id
                ),
                    self.model.profile_id: case(
                    profile_id,
                    value=self.model.id
                ),
                    self.model.year_payment: case(
                    year_payment,
                    value=self.model.id
                ),
                    self.model.date_payment: case(
                    date_payment,
                    value=self.model.id
                ),
                    self.model.is_paid: case(
                    is_paid,
                    value=self.model.id
                ),
                    self.model.comments: case(
                    comments,
                    value=self.model.id
                ),
                }, synchronize_session=False)

            # Если у обновляемых контактов количество полей разное -> update
            # несколькими запросами
            else:
                ids = list(set(is_deleted.keys()) |
                           set(profile_id.keys()) |
                           set(year_payment.keys()) |
                           set(date_payment.keys()) |
                           set(is_paid.keys()) |
                           set(comments.keys()))

                db_objs = db.query(self.model).filter(self.model.id.in_(ids)).all()
                for obj in db_objs:
                    if obj.id in is_deleted:
                        obj.is_deleted = is_deleted[obj.id]
                    if obj.id in profile_id:
                        obj.profile_id = profile_id[obj.id]
                    if obj.id in year_payment:
                        obj.year_payment = year_payment[obj.id]
                    if obj.id in date_payment:
                        obj.date_payment = date_payment[obj.id]
                    if obj.id in is_paid:
                        obj.is_paid = is_paid[obj.id]
                    if obj.id in comments:
                        obj.comments = comments[obj.id]
                db.add_all(db_objs)

            if commit:
                db.commit()

            return True


organizations_membership_dues = CRUDOrganizations_membership_dues(
    Organizations_membership_dues)
