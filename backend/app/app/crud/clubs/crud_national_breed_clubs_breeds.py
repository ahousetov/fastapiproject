from typing import Any, Optional, List

from fastapi import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import func, select
from sqlalchemy import and_

from app.crud.base import CRUDBase
from app.models.clubs.national_breed_clubs import National_breed_clubs
from app.models.clubs.national_breed_clubs_breeds import National_breed_clubs_breeds
from app.models.dogs.breeds import Breeds
from app.models.dogs.fci_groups import FCI_groups
from app.models.relations import Rel_breeds_sections_groups
from app.schemas.clubs.national_breed_clubs_breeds import National_breed_clubs_breeds_create
from app.schemas.clubs.national_breed_clubs_breeds import National_breed_clubs_breeds_update


class CRUDNational_breed_clubs_breeds(CRUDBase[National_breed_clubs_breeds,
                                               National_breed_clubs_breeds_create,
                                               National_breed_clubs_breeds_update]):
    def get(self, db: Session, id: Any, **
            kwargs) -> Optional[National_breed_clubs_breeds]:
        return db.query(self.model).filter(self.model.id == id).first()

    def make_subquery_breeds_agg(self, db: Session) -> Any:

        subquery = db.query(self.model.national_breed_club_id.label('id'),
                            func.string_agg(Breeds.name_rus, ', ').label('value')) \
            .join(Breeds, Breeds.id == self.model.breed_id) \
            .filter(self.model.is_deleted == False) \
            .group_by(self.model.national_breed_club_id) \
            .subquery('breeds')

        return subquery

    def make_subquery_breeds_json(self, db: Session) -> Any:

        tab = db.query(self.model.national_breed_club_id.label('nbc_id'),
                       Breeds.id,
                       func.concat(
            Breeds.name_rus, '/', Breeds.name_en).label('breed_name'),
            Breeds.fci_number,
            FCI_groups.number
        ) \
            .join(Breeds, Breeds.id == self.model.breed_id) \
            .outerjoin(Rel_breeds_sections_groups, Rel_breeds_sections_groups.breed_id == Breeds.id) \
            .outerjoin(FCI_groups, FCI_groups.id == Rel_breeds_sections_groups.group_id) \
            .filter(self.model.is_deleted == False) \
            .subquery('tab')

        subquery = db.query(tab.c.nbc_id,
                            func.jsonb_agg(
                                func.json_build_object(
                                    'id', tab.c.id,
                                    'breed_name', tab.c.breed_name,
                                    'fci_number', tab.c.fci_number,
                                    'group_number', tab.c.number
                                )).label('value')) \
            .group_by(tab.c.nbc_id) \
            .subquery('breeds')

        # subquery = db.query(Breeds.id,
        #                     func.jsonb_agg(
        #                         func.json_build_object(
        #                             'id', Breeds.id,
        #                             'breed_name', func.concat(Breeds.name_rus, '/', Breeds.name_rus).label('breed_name'),
        #                             'fci_number', Breeds.fci_number,
        #                             'group_number', FCI_groups.number
        #                         )).label('value')
        #                     ) \
        #     .select_from(self.model) \
        #     .join(Breeds, Breeds.id == self.model.breed_id) \
        #     .join(Rel_breeds_sections_groups, Rel_breeds_sections_groups.breed_id == Breeds.id) \
        #     .join(FCI_groups, FCI_groups.id == Rel_breeds_sections_groups.group_id) \
        #     .filter(self.model.is_deleted == False) \
        #     .group_by(Breeds.id)  \
        #     .subquery('breeds')
        return subquery

    def __make_subquery_sub_breeds(self, db: Session):
        """
        Делает подзапрос, в котором соединяются НКП и первые три породы, которые к ним относятся.
        Если к НКП не привязано ни одной породы, то его в результате запроса не будет,
        поэтому присоединять через LEFT JOIN.
        """

    def get_by_nbc_id(self, db: Session, nbc_id: int):
        rel_breeds = db.query(self.model) \
            .filter(self.model.is_deleted == False, National_breed_clubs_breeds.national_breed_club_id == nbc_id)

        return rel_breeds.all()

    def delete_by_breed_ids(
            self, db: Session, breed_ids: List[int], commit=True) -> Any:
        if breed_ids:
            query_for_remove_rel = db.query(
                self.model).filter(
                self.model.breed_id.in_(breed_ids))
            removed = query_for_remove_rel.delete()
            if commit:
                db.commit()
            return removed

    def create_multi_by_breed_ids(
            self, db: Session, breed_ids: List[int], nbc_id: int, commit=True):
        if breed_ids and nbc_id:
            create_objs = []
            for breed_id in breed_ids:
                obj = self.model(breed_id=breed_id, national_breed_club_id=nbc_id)
                create_objs.append(obj)

            db.add_all(create_objs)
            if commit:
                db.commit()

            return create_objs

        # """
        # Делает подзапрос, в котором соединяются НКП и первые три породы, которые к ним относятся через запятую.
        # Если к НКП не привязано ни одной породы, то его в результате запроса не будет,
        # поэтому присоединять через LEFT JOIN.
        # """

        # sub_breeds = self.__make_subquery_sub_breeds(db=db)
        #
        # query = db.query(
        #     National_breed_clubs.id.label("national_breed_club_id"),
        #     func.string_agg(sub_breeds.c.breed_name, ', ').label("breeds")
        # ).select_from(
        #     National_breed_clubs
        # ).join(
        #     sub_breeds, and_(sub_breeds.c.national_breed_club_id == National_breed_clubs.id)
        # ).group_by(
        #     National_breed_clubs.id,
        # ).subquery("breeds")

    # def __make_subquery_sub_breeds(self, db: Session):
    #     """
    #     Делает подзапрос, в котором соединяются НКП и первые три породы, которые к ним относятся.
    #     Если к НКП не привязано ни одной породы, то его в результате запроса не будет,
    #     поэтому присоединять через LEFT JOIN.
    #     """
    #
    #     # Выполнение оконной функции для простановки порядковых номеров породам, относящимся к НКП
    #     sub_breeds_window_func = db.query(
    #         self.model.national_breed_club_id,
    #         Breeds.name_rus.label("breed_name"),
    #         func.rank().over(
    #             partition_by=self.model.national_breed_club_id,
    #             order_by=Breeds.name_rus,
    #         ).label('sn')
    #     ).select_from(
    #         self.model
    #     ).join(
    #         Breeds, and_(Breeds.id == self.model.breed_id),
    #     ).where(
    #         self.model.national_breed_club_id == National_breed_clubs.id,
    #         self.model.is_deleted == False,  # noqa
    #         Breeds.is_deleted == False,  # noqa
    #     ).subquery("sub_breeds_window_func")
    #
    #     # Создание подзапроса на получение НКП и первых трех пород, которые к ним относятся
    #     query = db.query(
    #         sub_breeds_window_func.c.national_breed_club_id,
    #         sub_breeds_window_func.c.breed_name.label("breed_name"),
    #         sub_breeds_window_func.c.sn
    #     ).select_from(
    #         sub_breeds_window_func
    #     ).where(
    #         sub_breeds_window_func.c.sn <= 3
    #     ).subquery("sub_breeds")
    #     return query


national_breed_clubs_breads = CRUDNational_breed_clubs_breeds(
    National_breed_clubs_breeds)
