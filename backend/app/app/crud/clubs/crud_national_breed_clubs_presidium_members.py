from typing import Any, Optional, List, Type

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import func, select, case

from app.crud.base import CRUDBase
from app.models.clubs.national_breed_clubs_presidium_members import National_breed_clubs_presidium_members
from app.schemas.clubs.national_breed_clubs_presidium_members import National_breed_clubs_presidium_members_create
from app.schemas.clubs.national_breed_clubs_presidium_members import National_breed_clubs_presidium_members_update
from app.crud.base import ModelType, CRUDBase

from app.models.clubs.national_breed_clubs import National_breed_clubs


class CRUDNational_breed_clubs_presidium_members(CRUDBase[National_breed_clubs_presidium_members,
                                                          National_breed_clubs_presidium_members_create,
                                                          National_breed_clubs_presidium_members_update]):
    def __init__(self, model: Type[ModelType] = None):
        super(CRUDNational_breed_clubs_presidium_members, self).__init__(model)
        self.name = 'name'
        self.position = 'position'

    def get(self, db: Session, id: Any, **
            kwargs) -> Optional[National_breed_clubs_presidium_members]:
        return db.query(self.model).filter(self.model.id == id).first()

    def make_subquery_agg(self,
                          db: Session) -> Any:

        subquery = db.query(self.model.national_breed_club_id.label('id'),
                            func.string_agg(self.model.name, ', ').label('value')) \
            .filter(self.model.is_deleted == False) \
            .group_by(self.model.national_breed_club_id) \
            .subquery("presidium_members")

        return subquery

    def make_subquery_json(self,
                           db: Session) -> Any:

        subquery = db.query(self.model.national_breed_club_id.label('id'),
                            func.jsonb_agg(
                                func.json_build_object(
                                    'id', self.model.id,
                                    'position', self.model.position,
                                    'name', self.model.name,
                                    'is_deleted', self.model.is_deleted)
        ).label('value')) \
            .filter(self.model.is_deleted == False) \
            .group_by(self.model.national_breed_club_id) \
            .subquery("members")

        return subquery

    def get_by_nbc_id(self, db: Session, nbc_id: int):
        contacts = db.query(self.model) \
            .filter(self.model.is_deleted == False, self.model.national_breed_club_id == nbc_id)

        return contacts.all()

    def delete_multi(self, db: Session, ids: List[int], commit=True):
        if ids:
            removed = db.query(self.model).\
                filter(self.model.id.in_(ids)). \
                delete(synchronize_session=False)
            if commit:
                db.commit()
            return removed

    def create_multi(self, db: Session,
                     create_members: List[dict], nbc_id: int, commit=True):
        if create_members and nbc_id:
            create_objs = []
            for member in create_members:
                obj = self.model(**member, national_breed_club_id=nbc_id)
                create_objs.append(obj)

            db.add_all(create_objs)
            if commit:
                db.commit()

            return create_objs

    def update_multi(self,
                     db: Session, *,
                     objs_in: dict,
                     commit=True):

        if objs_in:

            name, position = {}, {}
            for contact_data in objs_in:
                id = contact_data['id']
                for k, v in contact_data.items():
                    if k == self.name:
                        name[id] = v
                    elif k == self.position:
                        position[id] = v

            # Если у всех обновляемых контактов количество полей одинаково -> update
            # одним запросом
            if len(name) == len(position):

                db.query(self.model).filter(
                    self.model.id.in_(name)
                ).update({self.model.name: case(
                    name,
                    value=self.model.id
                ),
                    self.model.position: case(
                        position,
                        value=self.model.id
                )
                }, synchronize_session=False)

            # Если у обновляемых контактов количество полей разное -> update
            # несколькими запросами
            else:

                ids = list((set(name.keys()) | set(position.keys())))

                db_objs = db.query(self.model).filter(self.model.id.in_(ids)).all()

                for obj in db_objs:
                    if obj.id in name:
                        obj.value = name[obj.id]
                    if obj.id in position:
                        obj.description = position[obj.id]

                db.add_all(db_objs)

            if commit:
                db.commit()

            return True


national_breed_clubs_presidium_members = CRUDNational_breed_clubs_presidium_members(
    National_breed_clubs_presidium_members)
