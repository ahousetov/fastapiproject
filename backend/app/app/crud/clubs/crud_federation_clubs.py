from typing import Any, List, Optional

from fastapi import HTTPException
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.clubs.federation_clubs import Federation_clubs
from app.schemas.clubs.federation_clubs import Federation_clubs_create, \
    Federation_clubs_update


class CRUDFederation_clubs(CRUDBase[Federation_clubs,
                                    Federation_clubs_create,
                                    Federation_clubs_update]):

    def get(self, db: Session, id: Any) -> Optional[Federation_clubs]:
        return db.query(self.model).filter(self.model.id == id).first()

    def get_by_federation_id(self, db: Session,
                             federation_id: int) -> Optional[Federation_clubs]:
        federation_club = db.query(self.model).filter(
            self.model.federation_id == federation_id).first()
        if not federation_club:
            model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
            raise Exception(
                f"Записи в '{model_name}' по federation_id={federation_id} не найдено.")

        return federation_club

    def create(
        self,
        db: Session,
        obj_in: Federation_clubs_create = None,
        obj_in_encoded: dict = None
    ) -> Federation_clubs:
        if (not obj_in) and (not obj_in_encoded):
            raise HTTPException(
                status_code=404,
                detail="Данных для создания новых пород не передано в 'create_not_deleted'",
                headers={"X-Error": "There goes my error"}
            )
        obj_in_data = obj_in if obj_in else obj_in_encoded
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete_by_profile_ids(self, db: Session, profile_ids: List[int]) -> Any:
        query_for_remove_rel = db.query(
            self.model).filter(
            self.model.profile_id.in_(profile_ids))
        removed = query_for_remove_rel.delete()
        db.commit()
        return removed

    def delete_by_federation_ids(self, db: Session, federation_ids: List[int]) -> Any:
        query_for_remove_rel = db.query(self.model).filter(
            self.model.federation_id.in_(federation_ids))
        removed = query_for_remove_rel.delete()
        db.commit()
        return removed


federation_clubs = CRUDFederation_clubs(Federation_clubs)
