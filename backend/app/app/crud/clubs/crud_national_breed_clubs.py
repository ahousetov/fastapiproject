from typing import Any, Optional, List, Dict
from pydantic import BaseModel

from fastapi import HTTPException
from sqlalchemy.engine import Row
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, Query
from sqlalchemy import and_, text, asc, desc, case

from app import crud
from app.schemas import Filters_model
from app.crud.base import CRUDBase, CreateSchemaType, ModelType
from app.crud.base import Contact_types
from app.crud.clubs.crud_national_breed_clubs_breeds import national_breed_clubs_breads
from app.crud.clubs.crud_national_breed_clubs_presidium_members import national_breed_clubs_presidium_members
from app.crud.relations.crud_rel_national_breed_clubs_contacts import rel_national_breed_clubs_contacts
from app.models.clubs.national_breed_clubs import National_breed_clubs
from app.models.public.contacts import Contacts
from app.models.catalogs import contact_types
from app.models.relations.rel_national_breed_clubs_contacts import Rel_national_breed_clubs_contacts
from app.models.clubs.national_breed_clubs_presidium_members import National_breed_clubs_presidium_members
from app.models.clubs.national_breed_clubs_breeds import National_breed_clubs_breeds
from app.models.dogs.breeds import Breeds
from app.models.dogs.fci_groups import FCI_groups
from app.models.relations import Rel_breeds_sections_groups
from sqlalchemy import func

from app.schemas.clubs.national_breed_clubs import National_breed_clubs_create, \
    National_breed_clubs_update, \
    OutputEditedNBC, \
    ContactsUpdate, ContactsCreate, MembersUpdate, MembersCreate


class CRUDNational_breed_clubs(CRUDBase[National_breed_clubs,
                                        National_breed_clubs_create,
                                        National_breed_clubs_update]):

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str,
            sort_field: str,
            filters: Filters_model = None,
            is_deleted: bool = False
    ) -> List[Dict[str, Any]]:
        """
        Получить список НКП с информацией о НКП, членах президиума,
        породах, телефонах, электронных почтах и сайтах.
        """
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)

        query = self.__get_query(db=db)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            query = query.filter(self.model.is_deleted == False)

        # Пользовательские фильтры
        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        query = query.offset(skip).limit(limit)

        rows = query.all()

        return [dict(row) for row in rows]

    # def get(
    #         self,
    #         db: Session,
    #         id: int,
    #         is_deleted: bool = False
    # ) -> OutputDetailNBC:
    #     """
    #     Получить НКП по id с подробной информацией о НКП, членах президиума,
    #     породах и контактах.
    #     """
    #     query = self.__get_query_json(db=db)
    #
    #     if not is_deleted and hasattr(self.model, 'is_deleted'):
    #         query = query.filter(self.model.is_deleted == False)
    #     query = query.filter(self.model.id == id)
    #     row = query.first()
    #
    #     main = MainBlockView(**row)
    #     contacts = ContactsBlocksView(
    #         contacts=[ContactInfoBlockView(**contact) for contact in row.contacts] if row.contacts else None)
    #     members = MembersBlocksView(
    #         presidium_members=[MemberInfoBlockView(**member)
    #                            for member in row.presidium_members] if row.presidium_members else None)
    #     breeds = BreedsBlocksView(
    #         breeds=[BreedsInfoBlockView(**breed) for breed in row.breeds] if row.breeds else None)
    #
    #     dict()
    #
    #     return OutputDetailNBC(blocks=[main, members, breeds, contacts])

    # def get_extra_by_id(
    #         self,
    #         db: Session,
    #         id: int,
    #         not_deleted: bool = False
    # ) -> OutputDetailNBC:
    #     """
    #     Получить НКП по id с подробной информацией о НКП, членах президиума,
    #     породах и контактах.
    #     """
    #     is_deleted = [self.model.is_deleted == False] if not_deleted else []
    #
    #     query = self.__get_query_json(db=db)
    #     query = query.filter(self.model.id == id, *is_deleted)
    #     row = query.first()
    #
    #     main = MainBlockView(**row)
    #     contacts = ContactsBlocksView(
    #         contacts=[ContactInfoBlockView(**contact) for contact in row.contacts] if row.contacts else None)
    #     members = MembersBlocksView(
    #         presidium_members=[MemberInfoBlockView(**member)
    #                            for member in row.presidium_members] if row.presidium_members else None)
    #     breeds = BreedsBlocksView(
    #         breeds=[BreedsInfoBlockView(**breed) for breed in row.breeds] if row.breeds else None)
    #
    #     return OutputDetailNBC(blocks=[main, members, breeds, contacts])

    # def get_extra_by_id_for_put(
    #         self,
    #         db: Session,
    #         id: int,
    #         not_deleted: bool = False
    # ) -> OutputEditedNBC:
    #     """
    #     Получить НКП по id с подробной информацией о НКП, членах президиума,
    #     породах и контактах после его изменения/создания.
    #     """
    #     is_deleted = [self.model.is_deleted == False] if not_deleted else []
    #
    #     query = self.__get_query_json(db=db)
    #     query = query.filter(self.model.id == id, *is_deleted)
    #     row = query.first()
    #
    #     return OutputEditedNBC(**row)

    def __get_query(self, db: Session) -> Query:
        """
        Запрос на получение не удаленных НКП с информацией о НКП, членах президиума,
        породах, телефонах, электронных почтах и сайтах. Каждое поле доп. таблиц саггрегировано в строку через запятую.
        """

        sub_phones = rel_national_breed_clubs_contacts.make_subquery_contacts_agg(
            db=db, type=Contact_types.phones)
        sub_emails = rel_national_breed_clubs_contacts.make_subquery_contacts_agg(
            db=db, type=Contact_types.emails)
        sub_sites = rel_national_breed_clubs_contacts.make_subquery_contacts_agg(
            db=db, type=Contact_types.sites)
        sub_members = national_breed_clubs_presidium_members.make_subquery_agg(db=db)
        sub_breeds = national_breed_clubs_breads.make_subquery_breeds_agg(db=db)

        query = db.query(self.model.id,
                         self.model.name.label('nbc_name'),
                         self.model.owner_position,
                         self.model.owner_name,
                         self.model.comment.label('comments'),
                         sub_members.c.value.label('presidium_members'),
                         sub_breeds.c.value.label('breeds'),
                         sub_phones.c.value.label('phones'),
                         sub_emails.c.value.label('emails'),
                         sub_sites.c.value.label('sites')
                         ) \
            .outerjoin(sub_members, sub_members.c.id == self.model.id) \
            .outerjoin(sub_breeds, sub_breeds.c.id == self.model.id) \
            .outerjoin(sub_phones, sub_phones.c.id == self.model.id) \
            .outerjoin(sub_emails, sub_emails.c.id == self.model.id) \
            .outerjoin(sub_sites, sub_sites.c.id == self.model.id) \

        return query

    # def __get_query_json(self, db: Session) -> Query:
    #     """
    #     Запрос на получение не удаленных НКП с информацией о НКП, членах президиума,
    #     породах, телефонах, электронных почтах и сайтах. Каждое поле доп. таблиц саггрегировано в json.
    #     """
    #     sub_contacts = rel_national_breed_clubs_contacts.make_subquery_contacts_json(db=db)
    #     sub_members = national_breed_clubs_presidium_members.make_subquery_json(db=db)
    #     sub_breeds = national_breed_clubs_breads.make_subquery_breeds_json(db=db)
    #
    #     query = db.query(self.model.id,
    #                      self.model.name.label('nbc_name'),
    #                      self.model.owner_position,
    #                      self.model.owner_name,
    #                      self.model.comment.label('comments'),
    #                      sub_members.c.value.label('presidium_members'),
    #                      sub_breeds.c.value.label('breeds'),
    #                      sub_contacts.c.value.label('contacts'),
    #                      ) \
    #         .outerjoin(sub_members, sub_members.c.id == self.model.id) \
    #         .outerjoin(sub_breeds, sub_breeds.c.nbc_id == self.model.id) \
    #         .outerjoin(sub_contacts, sub_contacts.c.id == self.model.id) \
    #
    #     return query

    def get(
        self,
        db: Session,
        id: int,
        is_deleted: bool = False
    ) -> Dict[str, Any]:
        """
        Запрос на получение не удаленных НКП с информацией о НКП, членах президиума,
        породах, телефонах, электронных почтах и сайтах. Каждое поле доп. таблиц саггрегировано в json.
        """

        nbc = db.query(self.model.id,
                       self.model.name.label('nbc_name'),
                       self.model.owner_position,
                       self.model.owner_name,
                       self.model.comment.label('comments'),
                       ) \
            .filter(self.model.id == id)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            nbc = nbc.filter(self.model.is_deleted == False)

        nbc = nbc.first()
        if not nbc:
            raise HTTPException(
                status_code=404,
                detail=f"Национального клуба пород по id = {id} не найдено.")

        nbc = dict(nbc)

        contacts = db.query(Contacts.id,
                            Contacts.type_id,
                            contact_types.Contact_types.name.label('type_name'),
                            Contacts.value,
                            Contacts.description,
                            Contacts.is_main) \
            .select_from(Rel_national_breed_clubs_contacts) \
            .join(Contacts, Contacts.id == Rel_national_breed_clubs_contacts.contact_id) \
            .outerjoin(contact_types.Contact_types, contact_types.Contact_types.id == Contacts.type_id) \
            .filter(Contacts.is_deleted == False,
                    Rel_national_breed_clubs_contacts.national_breed_club_id == nbc['id']) \
            .order_by(Contacts.type_id, Contacts.id)
        contacts = contacts.all()
        contacts = [dict(contact) for contact in contacts]

        presidium_members = db.query(National_breed_clubs_presidium_members.id,
                                     National_breed_clubs_presidium_members.position,
                                     National_breed_clubs_presidium_members.name,
                                     National_breed_clubs_presidium_members.is_deleted) \
            .filter(National_breed_clubs_presidium_members.is_deleted == False,
                    National_breed_clubs_presidium_members.national_breed_club_id == nbc['id'])
        presidium_members = presidium_members.all()
        presidium_members = [dict(member) for member in presidium_members]

        breeds = db.query(Breeds.id,
                          func.concat(
                              Breeds.name_rus, '/', Breeds.name_en).label('breed_name'),
                          Breeds.fci_number,
                          FCI_groups.number
                          ) \
            .select_from(National_breed_clubs_breeds) \
            .join(Breeds, Breeds.id == National_breed_clubs_breeds.breed_id) \
            .outerjoin(Rel_breeds_sections_groups, Rel_breeds_sections_groups.breed_id == Breeds.id) \
            .outerjoin(FCI_groups, FCI_groups.id == Rel_breeds_sections_groups.group_id) \
            .filter(National_breed_clubs_breeds.is_deleted == False,
                    National_breed_clubs_breeds.national_breed_club_id == nbc['id'])

        breeds = breeds.all()
        breeds = [dict(breed) for breed in breeds]

        return dict(nbc=nbc,
                    contacts=contacts,
                    presidium_members=presidium_members,
                    breeds=breeds)

    # def create(
    #         self,
    #         db: Session,
    #         *,
    #         obj_in: CreateSchemaType = None,
    #         obj_in_dict: dict = None,
    #         commit=True
    # ) -> ModelType:
    #     """
    #     Создание НКП.
    #     """
    #     if not obj_in_dict:
    #         raise ValueError
    #     new_nbc = self.model(**obj_in_dict)
    #     db.add(new_nbc)
    #     print(db.new)
    #     return db.new

    # def update(
    #     self,
    #     db: Session,
    #     *,
    #     db_obj: ModelType,
    #     obj_in: Any,
    #     commit: bool = True
    # ) -> ModelType:
    #     obj_data = jsonable_encoder(db_obj)
    #     upd_data = obj_in if isinstance(obj_in, dict) else obj_in.dict(exclude_unset=True)
    #
    #     for field in obj_data:
    #         if field in upd_data and upd_data[field]:
    #             setattr(db_obj, field, upd_data[field])
    #     db.add(db_obj)
    #     if commit:
    #         db.commit()
    #         db.refresh(db_obj)
    #     return db_obj

    # def update(
    #     self,
    #     db: Session,
    #     *,
    #     db_obj: ModelType,
    #     obj_in: Any,
    #     commit: bool = True
    # ) -> ModelType:
    #     obj_data = jsonable_encoder(db_obj)
    #     upd_data = obj_in if isinstance(obj_in, dict) else obj_in.dict(exclude_unset=True)
    #
    #     for field in obj_data:
    #         if field in upd_data and upd_data[field]:
    #             setattr(db_obj, field, upd_data[field])
    #     db.add(db_obj)
    #     if commit:
    #         db.commit()
    #         db.refresh(db_obj)
    #     return db_obj

    def update(
        self,
        db: Session,
        id: int,
        update_data: dict = None
    ) -> bool:

        nbc_info = update_data.get('nbc')
        member_data = update_data.get('members')
        contact_data = update_data.get('contacts')
        breed_data = update_data.get('breeds')

        # Получение и изменение основной таблицы
        db_nbc = db.query(self.model).filter(self.model.id == id).first()
        if db_nbc is None:
            raise HTTPException(status_code=404,
                                detail=str("НКП с указанным id не существует"))
        super().update(db, db_obj=db_nbc, obj_in=nbc_info)
        #

        # Таблица Presidium members
        db_members = crud.national_breed_clubs_presidium_members.get_by_nbc_id(
            db=db, nbc_id=db_nbc.id)

        delete_member_ids, update_members, create_members = self.split_objs_in_crud(
            db_objs=db_members,
            objs_in=member_data,
            schema_update=MembersUpdate,
            schema_create=MembersCreate)

        crud.national_breed_clubs_presidium_members.delete_multi(
            db=db, ids=delete_member_ids)
        create_members = crud.national_breed_clubs_presidium_members.create_multi(db=db,
                                                                                  create_members=create_members,
                                                                                  nbc_id=db_nbc.id)
        self.multi_update(db=db, objs_in=update_members,
                          model=National_breed_clubs_presidium_members)
        # Rel Breeds
        db_rel_breeds = crud.national_breed_clubs_breads.get_by_nbc_id(
            db=db, nbc_id=db_nbc.id)

        db_rel_breeds = {obj.breed_id: {obj} for obj in db_rel_breeds}
        existing_rel_breeds_in = {}
        # Разделение входных данных на создаваемые id=None и существющие id=int
        for obj in breed_data:
            if 'id' in obj and obj['id']:
                obj = {key: value for key, value in obj.items() if value is not None}
                existing_rel_breeds_in.update({obj['id']: obj})
        # id создаваемых связей с таблицей breeds
        create_rel_breed_ids = list(
            set(existing_rel_breeds_in.keys() - set(db_rel_breeds.keys())))
        # id удаляемых = разность существующих в бд и входных id
        delete_rel_breed_ids = list(
            set(db_rel_breeds.keys()) - set(existing_rel_breeds_in.keys()))

        crud.national_breed_clubs_breads.delete_by_breed_ids(
            db=db, breed_ids=delete_rel_breed_ids)
        crud.national_breed_clubs_breads.create_multi_by_breed_ids(db=db,
                                                                   breed_ids=create_rel_breed_ids,
                                                                   nbc_id=db_nbc.id)
        # Contacts
        db_contacts = crud.contacts.get_by_nbc_id(db=db, nbc_id=db_nbc.id)

        delete_contact_ids, update_contacts, create_contacts = self.split_objs_in_crud(
            db_objs=db_contacts,
            objs_in=contact_data,
            schema_update=ContactsUpdate,
            schema_create=ContactsCreate)

        crud.rel_national_breed_clubs_contacts.delete_by_contact_ids(
            db=db, contact_ids=delete_contact_ids)
        crud.contacts.delete_multi(db=db, ids=delete_contact_ids)
        create_objs = crud.contacts.create_multi(db=db, create_contacts=create_contacts)
        crud.rel_national_breed_clubs_contacts.create_multi_by_contact_ids(
            db=db,
            contact_ids=[
                contact.id for contact in create_objs] if create_objs else None,
            nbc_id=db_nbc.id)
        self.multi_update(db=db, objs_in=update_contacts, model=Contacts)
        # a = crud.national_breed_clubs.multi_update(db=db, objs_in=update_contacts, model=Contacts, commit=True)
        #
        # # Получаем набор данных для ответа
        # national_breed_club = self.get_extra_by_id_for_put(
        #     db=db,
        #     id=id,
        #     not_deleted=True
        # )

        return True

        # # Структура получится в таком роде:
        # # {values: {23: '+79999999999', 45: 'test'}, is_main: {25: True, 45: False}}
        # ids = []
        # object_fields = {}
        #
        # fields_number = len(objs_in[0])
        # same_length = False
        # for row in objs_in:
        #     id = row.pop('id')
        #     ids.append(id)
        #
        #     # Важно знать совпадает ли кол-во полей у всех входящ объектов
        #     if len(row) != fields_number:
        #         same_length = True
        #     for field_name, field_value in row.items():
        #         if field_name not in object_fields:
        #             object_fields[field_name] = {}
        #
        #         object_fields[field_name][id] = field_value
        #
        # if same_length:
        #     query = db.query(model).filter(model.id.in_(ids))
        #
        #     field_cases = {}
        #     for k, v in object_fields.items():
        #         field_cases[getattr(model, k)] = case(v, value=model.id)
        #
        #     query.update(field_cases, synchronize_session=False)
        #
        # else:
        #     db

    def create(
        self,
        db: Session,
        create_data: Dict[str, Any] = None
    ) -> bool:

        nbc_info = create_data.get('nbc')
        member_data = create_data.get('members')
        contact_data = create_data.get('contacts')
        breed_data = create_data.get('breeds')

        # Создание записи в основной таблице
        db_nbc = super().create(db=db, obj_in=nbc_info, commit=True)

        # Таблица Presidium members

        db_members = crud.national_breed_clubs_presidium_members.get_by_nbc_id(
            db=db, nbc_id=db_nbc.id)

        delete_member_ids, update_members, create_members = crud.national_breed_clubs.split_objs_in_crud(
            db_objs=db_members,
            objs_in=member_data,
            schema_update=MembersUpdate,
            schema_create=MembersCreate)

        crud.national_breed_clubs_presidium_members.delete_multi(
            db=db, ids=delete_member_ids)
        create_members = crud.national_breed_clubs_presidium_members.create_multi(db=db,
                                                                                  create_members=create_members,
                                                                                  nbc_id=db_nbc.id)
        # crud.national_breed_clubs_presidium_members.update_multi(db=db, objs_in=update_members)
        crud.national_breed_clubs.multi_update(db=db, objs_in=update_members,
                                               model=National_breed_clubs_presidium_members)
        #

        # Rel Breeds
        db_rel_breeds = crud.national_breed_clubs_breads.get_by_nbc_id(
            db=db, nbc_id=db_nbc.id)

        db_rel_breeds = {obj.breed_id: {obj} for obj in db_rel_breeds}
        existing_rel_breeds_in = {}
        # Разделение входных данных на создаваемые id=None и существющие id=int
        for obj in breed_data:
            if 'id' in obj and obj['id']:
                obj = {key: value for key, value in obj.items() if value is not None}
                existing_rel_breeds_in.update({obj['id']: obj})
        # id создаваемых связей с таблицей breeds
        create_rel_breed_ids = list(
            set(existing_rel_breeds_in.keys() - set(db_rel_breeds.keys())))
        # id удаляемых = разность существующих в бд и входных id
        delete_rel_breed_ids = list(
            set(db_rel_breeds.keys()) - set(existing_rel_breeds_in.keys()))

        crud.national_breed_clubs_breads.delete_by_breed_ids(
            db=db, breed_ids=delete_rel_breed_ids)
        crud.national_breed_clubs_breads.create_multi_by_breed_ids(db=db,
                                                                   breed_ids=create_rel_breed_ids,
                                                                   nbc_id=db_nbc.id)
        #

        # Contacts
        db_contacts = crud.contacts.get_by_nbc_id(db=db, nbc_id=db_nbc.id)

        delete_contact_ids, update_contacts, create_contacts = crud.national_breed_clubs.split_objs_in_crud(
            db_objs=db_contacts,
            objs_in=contact_data,
            schema_update=ContactsUpdate,
            schema_create=ContactsCreate)

        crud.rel_national_breed_clubs_contacts.delete_by_contact_ids(
            db=db, contact_ids=delete_contact_ids)
        crud.contacts.delete_multi(db=db, ids=delete_contact_ids)
        create_objs = crud.contacts.create_multi(db=db, create_contacts=create_contacts)
        crud.rel_national_breed_clubs_contacts.create_multi_by_contact_ids(
            db=db,
            contact_ids=[
                contact.id for contact in create_objs] if create_objs else None,
            nbc_id=db_nbc.id)
        # crud.contacts.update_multi(db=db, objs_in=update_contacts)
        crud.national_breed_clubs.multi_update(
            db=db, objs_in=update_contacts, model=Contacts)
        #

        return True


national_breed_clubs = CRUDNational_breed_clubs(National_breed_clubs)
