from app.schemas.common import *
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

# Models
from app.db.base_class import Base
from app.models.public.profile_types import Profile_types
from app.models.public.legal_informations import Legal_informations
from app.models.public.users import Users
from app.models.public.profiles import Profiles
from app.models.nurseries.nurseries import Nurseries
from app.models.relations.rel_users_profiles import Rel_users_profiles
from app.models.relations.rel_profiles_federations import Rel_profiles_federations
from app.models.relations.rel_nurseries_breeding_breeds import Rel_nurseries_breeding_breeds
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups
from app.models.catalogs.organization_statuses import Organization_statuses
from app.models.catalogs.federations import Federations
from app.models.public.nursery_stamp_codes import Nursery_stamp_codes
from app.models.public.stamp_codes import Stamp_codes
from app.models.requests.nursery_registration_requests import Nursery_registration_requests
from app.models.dogs.fci_groups import FCI_groups
from app.models.dogs.fci_breed_statuses import FCI_breed_statuses
from app.models.dogs.breeds import Breeds
from app.models.public.club_address import Club_addresses
from app.models.public.contacts import Contacts
from app.models.relations.rel_users_profiles import Rel_users_profiles
from app.models.relations.rel_profiles_federations import Rel_profiles_federations
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.models.catalogs.organization_statuses import Organization_statuses
from app.models.catalogs.federations import Federations
from app.models.catalogs.address_types import Address_types
from app.models.catalogs.street_types import Street_types
from app.models.catalogs.house_types import House_types
from app.models.catalogs.flat_types import Flat_types
from app.models.catalogs.cities import Cities
from app.models.catalogs.city_types import City_types
from app.models.catalogs.contact_types import Contact_types as c_t_model

# CRUDs
from app.crud.base import ModelType, CRUDBase, Contact_types as cont_t_enum
from app.crud.relations.crud_profiles_contacts import rel_profiles_contacts as rpc
from app.crud.relations.crud_rel_profiles_federations import rel_profiles_federations as rpf
from app.crud.public.crud_club_address import club_address
from app.crud.public.crud_organizations_membership_dues import organizations_membership_dues as org_m_d
from app.crud.public.crud_organizations_history import organizations_history as org_h
from app.crud.accesses.crud_user_accesses import user_accesses
from app.crud.accesses.crud_profile_type_requests import profile_type_requests
from app.crud.public.crud_contacts import contacts as crud_contacts
from app.crud.public.crud_legal_informations import legal_informations as c_legal_info
from app.crud.relations.crud_rel_nurseries_breeding_breeds import rel_nurseries_breeding_breeds as rnbb
from app.crud.public.crud_stamp_codes import stamp_codes as c_stamp_code


# Schemas
import schemas
from app.schemas.public.organizations_membership_dues import Organizations_membership_dues_item
from app.schemas.public.legal_informations import Legal_informations_update
from app.schemas.relations.rel_nurseries_breeding_breeds import \
    Rel_nurseries_breeding_breed_create, \
    Rel_nurseries_breeding_breed_update, \
    Rel_nurseries_breeding_breed
from app.schemas.public.club_address import Club_address_update_nurseries


# sqlalchemy
from sqlalchemy import and_, exc, text, sql, func
from sqlalchemy.orm import Session
from fastapi import APIRouter, Body, Depends, HTTPException

# FastAPI
from fastapi import status
from fastapi.encoders import jsonable_encoder


class CRUDNurseries(CRUDBase[Profiles, None, None]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDNurseries, self).__init__(model)
        self.title = "Питомники"
        self.id = "id"
        self.legal_informations_id = f"legal_informations_id"
        self.legal_informations_name = f"kennel_full_name"
        self.legal_informations_short_name = f"kennel_short_name"
        self.legal_informations_owner_position = f"owner_position"
        self.legal_informations_owner_name = f"owner_name"
        self.legal_informations_registration_date = f"legal_registration_date"
        self.legal_informations_liquidate_date = f"legal_liquidation_date"
        self.legal_informations_organization_status_id = f"legal_status_id"
        self.legal_informations_ogrn = f"ogrn"
        self.legal_informations_inn = f"inn"
        self.legal_informations_kpp = f"kpp"
        self.legal_informations_okpo = f"okpo"
        self.legal_informations_okved = f"okved"
        self.legal_informations_is_public = f"is_public"
        self.legal_informations_is_enable_web = f"is_active_user"
        self.legal_informations_folder_number = f"folder_number"
        self.legal_informations_city_id = f"{self.legal_informations}.city_id"
        self.federations_short_name = f"federation_name"
        self.organization_statuses_name = f"legal_status_name"
        self.rel_user_profiles_id = f"rel_users_profiles_user_id"
        self.nurseries_id = f"kennel_id"
        self.ca1_c_postcode = f"legal_index"
        self.ca1_c_catalogs_city_types_name = f"legal_city_type_name"
        self.ca1_c_catalogs_cities_name = f"legal_city_name"
        self.ca1_c_catalogs_street_types_name_and_street_name = f"legal_street_full"
        self.ca1_c_catalogs_house_types_name_and_house_name = f"legal_house_full"
        self.ca1_c_catalogs_flat_types_name = f"legal_flat_type_name"
        self.ca1_c_flat_name = f"legal_flat"
        self.ca1_c_geo_lat = f"legal_geo_lat"
        self.ca1_c_geo_lon = f"legal_geo_lon"
        self.ca2_c_postcode = f"local_index"
        self.ca2_c_catalogs_city_types_name = f"local_city_type_name"
        self.ca2_c_catalogs_cities_name = f"local_city_name"
        self.ca2_c_catalogs_street_types_name_and__street_name = f"local_street_full"
        self.ca2_c_catalogs_house_types_name_and_house_name = f"local_house_full"
        self.ca2_c_catalogs_flat_types_name = f"local_flat_type"
        self.ca2_c_flat_name = f"local_flat"
        self.ca2_c_geo_lat = f"local_geo_lat"
        self.ca2_c_geo_lon = f"local_geo_lon"
        self.address_types_name = f"address_type_name"
        self.club_address_id = f"id"
        self.club_address_address_type_id = f"address_type_id"
        self.club_address_postcode = f"postcode"
        self.club_address_city_id = f"city_id"
        self.club_address_street_type_id = f"street_type_id"
        self.club_address_street_name = f"street_name"
        self.club_address_house_type_id = f"house_type_id"
        self.club_address_house_name = f"house_name"
        self.club_address_flat_type_id = f"flat_type_id"
        self.club_address_flat_name = f"flat_name"
        self.club_address_geo_lat = f"geo_lat"
        self.club_address_geo_lon = f"geo_lon"
        self.club_address_is_deleted = f"is_deleted"
        self.club_address_type_id = f"type_id"
        self.club_address_type_name = f"type_name"
        self.city_types_name = f"city_types_name"
        self.cities_name = f"{self.cities}.name"
        self.contacts_id = f"id"
        self.contacts_type_id = f"type_id"
        self.contacts_postcode = f"postcode"
        self.contacts_value = f"value"
        self.contacts_is_main = f"is_main"
        self.contacts_description = f"description"
        self.contacts_is_deleted = f"is_deleted"
        self.regions_name = f"{self.regions}.name"
        self.street_types_name = f"{self.street_types}.name"
        self.house_types_name = f"{self.house_types}.name"
        self.flat_types_name = f"{self.flat_types}.name"
        self.contact_type_name = f"type_name"
        self.address = f"address"
        self.address_coordinates = f"address_coordinates"
        self.stamp_code = f"brand_code"
        self.active_member = f"is_active_user"

    def _make_get_query_(self, db: Session):
        ##########################################################################
        # Составляем подзапрос для поиска контактов. Далее, результат присоединяется
        # по profile_id
        #
        # Parameters:
        #     db(Session)- объект сессии базы данных
        #     type(Contact_types(Enum))- перечисление по типу контактов:
        #                                     1 - телефон
        #                                     2 - email
        #                                     3 - сайт
        #     main(Bool) - "основной" или очередной контакт. В таблице указан список
        #                     контактов, но "основным" может быть только один из них.
        #
        # По умолчанию:
        # make_subquery_contacts(self,
        # db: Session,
        ##                          type: Contact_types = None,
        # main: bool = False) -> Any:
        sub_phones = rpc.make_subquery_contacts(db=db, type=cont_t_enum.phones)
        sub_email = rpc.make_subquery_contacts(db=db, type=cont_t_enum.emails)
        sub_site = rpc.make_subquery_contacts(db=db, type=cont_t_enum.sites)
        ca1 = club_address._subquery(db=db,
                                     address_type_id=2,
                                     name="ca1")
        ca2 = club_address._subquery(db=db,
                                     address_type_id=1,
                                     name="ca2")
        entry = db.query(
            self.model.id.label(self.id),
            Rel_users_profiles.user_id.label(self.l_rel_user_profiles_id),
            Legal_informations.id.label(self.l_legal_informations_id),
            Legal_informations.organization_status_id.label(
                self.l_legal_informations_organization_status_id),
            Legal_informations.name.label(self.l_legal_informations_name),
            Legal_informations.short_name.label(self.l_legal_informations_short_name),
            Legal_informations.owner_position.label(
                self.l_legal_informations_owner_position),
            Legal_informations.owner_name.label(self.l_legal_informations_owner_name),
            Legal_informations.folder_number.label(
                self.l_legal_informations_folder_number),
            Legal_informations.is_enable_web.label(
                self.l_legal_informations_is_enable_web),
            Federations.short_name.label(self.l_federations_short_name),
            Organization_statuses.name.label(self.l_organization_statuses_name),
            func.concat(ca1.c.postcode, " ",
                        ca1.c.catalogs_city_types_name, " ",
                        ca1.c.catalogs_cities_name, " ",
                        ca1.c.catalogs_street_types_name, " ",
                        ca1.c.street_name, " ",
                        ca1.c.catalogs_house_types_name, " ",
                        ca1.c.house_name, " ",
                        ca1.c.catalogs_flat_types_name, " ",
                        ca1.c.flat_name).label(self.address),
            func.concat(ca1.c.geo_lat, " ", ca1.c.geo_lon).label(
                self.address_coordinates),
            sub_phones.c.phones.label(cont_t_enum.phones.name),
            sub_email.c.emails.label(cont_t_enum.emails.name),
            sub_site.c.sites.label(cont_t_enum.sites.name),
            Legal_informations.is_public.label(self.legal_informations_is_public),
            Stamp_codes.stamp_code.label(self.stamp_code)
        )\
            .select_from(self.model)\
            .join(Profile_types) \
            .join(Rel_users_profiles) \
            .join(Legal_informations)\
            .join(Nurseries)\
            .join(Users)\
            .join(Organization_statuses, isouter=True)\
            .join(Rel_profiles_federations, isouter=True)\
            .join(Federations, isouter=True)\
            .join(Nursery_stamp_codes, isouter=True)\
            .join(Stamp_codes, isouter=True)\
            .join(sub_phones, isouter=True)\
            .join(sub_email, isouter=True)\
            .join(sub_site, isouter=True)\
            .join(ca1, ca1.c.profile_id == self.model.id, isouter=True)\
            .join(ca2, ca2.c.profile_id == self.model.id, isouter=True)
        return entry

    def update_multi(self, db: Session, *, objs_in: dict, commit=True):
        if objs_in:
            breed_id, nursery_id = {}, {}
            for org_mem_dues_data in objs_in:
                id = org_mem_dues_data['id']
                for k, v in org_mem_dues_data.items():
                    if k == self.breed_id:
                        breed_id[id] = v
                    elif k == self.profile_id:
                        nursery_id[id] = v

            # Если у всех обновляемых платежей количество полей одинаково -> update
            # одним запросом
            if len(breed_id) == len(nursery_id):
                db.query(self.model).filter(
                    self.model.id.in_(breed_id)
                ).update(
                    {
                        self.model.value: case(
                            breed_id,
                            value=self.model.id
                        ),
                        self.model.profile_id: case(
                            nursery_id,
                            value=self.model.id
                        )
                    },
                    synchronize_session=False)

            # Если у обновляемых контактов количество полей разное -> update
            # несколькими запросами
            else:
                ids = list(set(breed_id.keys()) | set(nursery_id.keys()))
                db_objs = db.query(self.model).filter(self.model.id.in_(ids)).all()
                for obj in db_objs:
                    if obj.id in breed_id:
                        obj.is_deleted = breed_id[obj.id]
                    if obj.id in nursery_id:
                        obj.profile_id = nursery_id[obj.id]
                db.add_all(db_objs)

            if commit:
                db.commit()

            return True

    def split_objs_breeds_in_crud(self,
                                  db_objects: List[Rel_nurseries_breeding_breed],
                                  objs_in: List[dict],
                                  nursery_id: int = None):
        """
        Функция разделяющая obj_in на
        списки delete_org_mem_dues_ids, update_org_mem_dues, create_org_mem_dues

        Логика разделения пород, для обновления их списка в питомниках, несколько отличается от
        прочих разделения.

        Мы НЕ СОЗДАЁМ НОВЫХ ПОРОД в справочнике пород. Мы ли выбираем существующие и обновляем
        таблицу Rel_nurseries_breeding_breeds
        """
        db_objects = {obj.breed_id: obj for obj in db_objects}
        existing_b_breeds_in = {}
        create_b_breeds = []
        update_b_breeds = []
        for b_breeds in objs_in:
            if b_breeds.get('id', None) is not None:
                if not (b_breeds['id'] in db_objects):
                    new_rel_b_breeeds_entry = {'breed_id': b_breeds['id'],
                                               'nursery_id': nursery_id}
                    create_b_breeds.append(new_rel_b_breeeds_entry)
                else:
                    b_breeds = {key: value for key,
                                value in b_breeds.items() if value is not None}
                    existing_b_breeds_in.update({b_breeds['id']: b_breeds})

        # Мы НЕ ДОЛЖНЫ ОБНОВЛЯТЬ ТАБЛИЦУ Nurseries. Мы обновляем только Rel_nurseries_breeding_breeds.
        # Обновление Rel_nurseries_breeding_breeds происходит за счёт create_b_breeds и delete_b_breeds_ids
        # Дальнейший блок не нужен.
        # upd_id = set()
        # if db_objects:
        #     upd_id = set(db_objects.keys()) & set(existing_b_breeds_in.keys())
        #     update_b_breeds = [self._orm_obj_dict(rel_obj) for key, rel_obj in db_objects.items() if key in upd_id] if upd_id else []
        #     # update_b_breeds = [self.row2dict(rel_obj) for key, rel_obj in db_objects.items() if key in upd_id] if upd_id else []

        # id удаляемых = разность существующих в бд и входных id
        delete_b_breeds_ids = list(set(db_objects.keys()) -
                                   set(existing_b_breeds_in.keys()))
        return delete_b_breeds_ids, [], create_b_breeds

    def get_all(
            self,
            db: Session,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: schemas.Filters_model = None
    ) -> List[Any]:
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        sort_direction, sort_field = self._sort_params(sort_direction=sort_direction,
                                                       sort_field=sort_field,
                                                       filters=filters)
        model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
        nurseries_entry = self._make_get_query_(db=db)
        # Пользовательские фильтры
        nurseries_entry = nurseries_entry.filter(
            self.model.is_deleted == False,
            Legal_informations.is_deleted == False,
            Profile_types.name == "Nursery",
            Nursery_stamp_codes.is_deleted == False
        )
        if filters:
            nurseries_entry = self._make_filter_model_without_strings(
                filters_model=filters, query=nurseries_entry)
        nurseries_entry = nurseries_entry.order_by(
            text(f"{sort_field} {sort_direction}"))
        nurseries_entry = nurseries_entry.offset(skip).limit(limit)
        nurseries_entry = nurseries_entry.all()
        return nurseries_entry

    def get(self, db: Session, id: int) -> Dict[str, Any]:
        kennel_id = None  # Здесь будет id найденного питомника
        result = {}
        # Основная информация
        main = db.query(
            self.model.id.label(self.id),
            Rel_users_profiles.user_id.label(self.l_rel_user_profiles_id),
            Legal_informations.id.label(self.l_legal_informations_id),
            Legal_informations.organization_status_id,
            Legal_informations.city_id.label(self.l_legal_informations_city_id),
            Legal_informations.name.label(self.l_legal_informations_name),
            Legal_informations.short_name.label(self.l_legal_informations_short_name),
            Legal_informations.owner_name,
            Legal_informations.owner_position,
            Legal_informations.folder_number,
            Legal_informations.active_member.label(self.active_member),
            Legal_informations.is_enable_web,
            Nurseries.id.label(self.l_nurseries_id),
            Legal_informations.is_public.label(self.legal_informations_is_public),
            Rel_profiles_federations.federation_id,
            Federations.name.label(self.federations_short_name),
            Stamp_codes.stamp_code.label(self.stamp_code),
            Nursery_registration_requests.open_access_to_all_requests
        ).join(Rel_users_profiles)\
         .join(Legal_informations)\
         .join(Nurseries, isouter=True)\
         .join(Rel_profiles_federations, isouter=True) \
         .join(Federations, isouter=True)\
         .join(Nursery_stamp_codes)\
         .join(Stamp_codes)\
         .join(Nursery_registration_requests)\
         .where(self.model.id == id)\
         .filter(Nursery_stamp_codes.is_deleted == False,
                 Stamp_codes.is_deleted == False,
                 Nursery_registration_requests.is_deleted == False,
                 Nursery_registration_requests.status_id == 3)
        main = main.first()
        if not main:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Не найдено питомников с profile_id = {id}"
            )
        else:
            result['main'] = {**main}
            kennel_id = getattr(main, "kennel_id", None)

        # Контактные данные
        contact_nurseries = db.query(
            Contacts.id.label(self.l_contacts_id),
            Contacts.type_id.label(self.l_contacts_type_id),
            c_t_model.name.label(self.l_contact_type_name),
            Contacts.value.label(self.l_contacts_value),
            Contacts.is_main.label(self.l_contacts_is_main),
            Contacts.description.label(self.l_contacts_description)) \
            .join(Rel_profiles_contacts)\
            .join(c_t_model, isouter=True)\
            .filter(Contacts.is_deleted == False,
                    Rel_profiles_contacts.profile_id == id)
        contact_nurseries = contact_nurseries.all()
        result['contacts'] = [{**c}
                              for c in contact_nurseries] if contact_nurseries else []

        # Адресные данные
        addresses = db.query(
            Club_addresses.id.label(self.l_club_address_id),
            Address_types.name.label(self.club_address_type_name),
            Club_addresses.type.label(self.club_address_type_id),
            Club_addresses.postcode.label(self.l_club_address_postcode),
            City_types.name.label(self.l_city_types_name),
            Club_addresses.city_id.label(self.l_club_address_city_id),
            Cities.name.label(self.l_cities_name),
            Club_addresses.street_type_id.label(self.l_club_address_street_type_id),
            Street_types.name.label(self.l_street_types_name),
            Club_addresses.street_name.label(self.l_club_address_street_name),
            Club_addresses.house_type_id.label(self.l_club_address_house_type_id),
            House_types.name.label(self.l_house_types_name),
            Club_addresses.house_name.label(self.l_club_address_house_name),
            Club_addresses.flat_type_id.label(self.l_club_address_flat_type_id),
            Flat_types.name.label(self.l_flat_types_name),
            Club_addresses.flat_name.label(self.l_club_address_flat_name),
            Club_addresses.geo_lat.label(self.l_club_address_geo_lat),
            Club_addresses.geo_lon.label(self.l_club_address_geo_lon),
            func.concat(Club_addresses.postcode, " ",
                        City_types.name, " ",
                        Cities.name, " ",
                        Street_types.name, " ",
                        Club_addresses.street_name, " ",
                        House_types.name, " ",
                        Club_addresses.house_name, " ",
                        Flat_types.name, " ",
                        Club_addresses.flat_name
                        ).label("address")
        ).join(Address_types)\
            .join(Cities, isouter=True)\
            .join(City_types, isouter=True)\
            .join(Street_types, isouter=True)\
            .join(House_types, isouter=True)\
            .join(Flat_types, isouter=True)\
            .filter(Club_addresses.profile_id == id)\
            .order_by(Club_addresses.address_type_id)
        addresses = addresses.all()
        result['addresses'] = [{**addr} for addr in addresses] if addresses else []

        # Разведение пород
        breeding_breeds = db.query(
            Breeds.id.label("id"),
            Breeds.fci_number,
            FCI_groups.number.label("group_number"),
            func.concat(Breeds.name_rus, ' / ', Breeds.name_en).label('breed_name')
        ).select_from(Rel_nurseries_breeding_breeds) \
            .join(Breeds)\
            .join(FCI_breed_statuses)\
            .join(Rel_breeds_sections_groups)\
            .join(FCI_groups)\
            .where(Rel_nurseries_breeding_breeds.nursery_id == kennel_id)\
            .order_by(Breeds.name_rus)
        breeding_breeds = breeding_breeds.all()
        result['breeds'] = [{**bb} for bb in breeding_breeds] if breeding_breeds else []

        # Дополнительная информация
        additional_info = db.query(
            Nurseries.certificate_registration_nursery_id.label(
                "certificate_registration_nursery_id"),
            Nurseries.registration_date.label("registration_date"),
            Nurseries.prefix.label("prefix"),
            Nurseries.suffix.label("suffix"),
            Nurseries.name_lat.label("name_lat"),
            Nurseries.owner_specialist_rkf.label("owner_specialist_rkf"),
            Nurseries.owner_date_speciality.label("owner_date_speciality"),
            Nurseries.owner_special_education.label("owner_special_education"),
            Nurseries.owner_place_speciality.label("owner_place_speciality"),
            Nurseries.owner_speciality.label("owner_speciality"),
            Nurseries.experience_dog_breeding.label("experience_dog_breeding"),
            Nurseries.puppies_total_count.label("puppies_total_count"),
            Nurseries.owner_ranks.label("owner_ranks"),
            Nurseries.dogs_ranks.label("dogs_ranks")
        ).where(Nurseries.id == kennel_id).first()
        result['additional'] = {**additional_info} if additional_info else {}

        history_info = org_h.get(db=db, profile_id=id, omit_exception=True)
        result['history'] = {**history_info} if history_info else {}

        # платежи
        pay_list = org_m_d.get(db=db, profile_id=id, omit_exception=True)
        result['payments'] = [{**p} for p in pay_list] if pay_list else []
        ##########################################################################
        # Здесь начинается работа со списками заявок
        u_access = user_accesses.get_list_accesses_profile_id(db=db, profile_id=id)
        prof_t_r = profile_type_requests.get_order_types_list(db=db)
        u_access_ids = {u[1]: {u} for u in u_access}
        prof_t_r_ids = {p[0]: {p} for p in prof_t_r}
        active_ids = set(u_access_ids.keys()) & set(prof_t_r_ids.keys())
        result['access'] = {'view': [{'access_name': p[3], 'value': (p[0] in active_ids)} for p in prof_t_r],
                            'edit': [i for i in active_ids]}
        ##########################################################################
        return result

    def update(self,
               db: Session,
               profile_id: int,
               update_data: dict = None):

        # Получаем kennel_id по profile_id и далее используем для обнолвения данных.
        kennel_id = db.query(Nurseries.id).where(
            Nurseries.profile_id == profile_id).first()
        if kennel_id:
            kennel_id = kennel_id[0]

        ##########################################################################
        ############################################ Работаю с Contacts ##########
        # Беру код из backend/app/app/api/api_v1/endpoints/catalogs/federations.py
        # для обновления контактов.
        contacts = crud_contacts.get_all_objects(
            db=db,
            condition=[Rel_profiles_contacts.profile_id == profile_id],
            omit_exception=True
        )
        contact_data = [c.dict(exclude_unset=True)
                        for c in update_data['contacts']] if update_data['contacts'] else []
        del_cnt_ids, upd_cnt, crt_cnt = crud_contacts.split_objs_type_id_in_crud(db_objects=contacts,
                                                                                 objs_in=contact_data)
        # Методы delete_by_contact_ids и delete_multi ничего не делают, если передать им пустые списки
        # поэтому их работа не ограничивается никакими условиями.
        rpc.delete_by_contact_ids(db=db, contact_ids=del_cnt_ids, commit=False)
        crud_contacts.delete_multi(db=db, ids=del_cnt_ids, commit=False)

        # create_multi_by_contact_ids имеет сомнительные операции под добавлению чего-то в в БД.
        # поскольку
        if crt_cnt:
            create_objs = crud_contacts.create_multi(
                db=db, create_contacts=crt_cnt, commit=True)
            rpc.create_multi_by_contact_ids(db=db, contact_ids=[contact.id for contact in create_objs],
                                            profile_id=profile_id, commit=False)

        # Метод update_multi также не выполняет никаких действий в случае пустого списка
        # поэтому спокойно вызываем
        crud_contacts.update_multi(db=db, objs_in=upd_cnt, commit=False)

        ##########################################################################
        ############################################ Работаю с Address ###########
        # Здесь работаю с адресными данными. Т.к. запись в club_address одна на profile_id, работают только с
        # одним элементом.
        all_addrs = club_address.get_all_objects(
            db=db,
            condition=[club_address.model.profile_id == profile_id],
            omit_exception=True
        )
        all_addrs_data = [a.dict() for a in update_data['addresses']
                          ] if update_data['addresses'] else []
        del_addr_ids, upd_addr, crt_addr = club_address.split_objs_type_id_in_crud(
            db_objects=all_addrs,
            objs_in=all_addrs_data,
            profile_id=profile_id
        )
        if del_addr_ids:
            club_address.delete_multi(db=db, ids=del_addr_ids, commit=False)
        if crt_addr:
            try:
                crt_addr = [Club_address_update_nurseries(**ca).dict(exclude_unset=True)
                            for ca in crt_addr]
                addresses_created = club_address.create_multi(
                    db=db, create_addresss=crt_addr, commit=False)
            except Exception as e:
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=str(e))

        for addr_data in upd_addr:
            if "id" in addr_data:
                address_obj = club_address.get_object(
                    db=db, id=profile_id, id_name='profile_id')
                del addr_data['id']
                club_address.update(db=db, db_obj=address_obj,
                                    obj_in=jsonable_encoder(addr_data), commit=False)
        ##########################################################################
        ########################################### Работаю с Payments ###########
        # Беру код из backend/app/app/api/api_v1/endpoints/catalogs/federations.py для обновления платежей.
        # org_mem_dues = org_m_d.get_by_profile_id(db, profile_id=profile_id, omit_exception=True)
        org_mem_dues = org_m_d.get_all_objects(
            db,
            condition=[org_m_d.model.profile_id == profile_id],
            omit_exception=True
        )
        org_mem_dues_data = [p.dict() for p in update_data['payments']
                             ] if update_data['payments'] else []
        delete_org_mem_due_ids, update_org_mem_dues, create_org_mem_dues = org_m_d.split_objs_type_id_in_crud(
            db_objects=org_mem_dues,
            objs_in=org_mem_dues_data,
            profile_id=profile_id)
        org_m_d.delete_multi(db=db, ids=delete_org_mem_due_ids, commit=False)
        if create_org_mem_dues:
            create_objs = org_m_d.create_multi(db=db,
                                               create_org_mem_dues=create_org_mem_dues,
                                               commit=False)
        if update_org_mem_dues:
            org_m_d.update_multi(db=db,
                                 objs_in=[Organizations_membership_dues_item(**uomd).dict(exclude_unset=True)
                                          for uomd in update_org_mem_dues],
                                 commit=False)
        ############################################ Работаю с Payments ##########
        ##################################### Работаю с Organization_history #####
        history_obj = org_h.get(db=db, profile_id=profile_id,
                                omit_exception=True, as_object=True)
        if history_obj:
            history_in = schemas.public.Organization_history_item(
                **update_data['history'])
            updated_history = org_h.update(db=db, db_obj=history_obj, obj_in=history_in)
        else:
            history_in = schemas.public.Organization_history_item(
                **update_data['history'])
            updated_history = org_h.create(db=db, obj_in=history_in)
        ##########################################################################
        ############################################ Работаю с Breeds ############
        if kennel_id:
            # Разведение пород
            # Ставим условие: Rel_nurseries_breeding_breeds.nursery_id == profile_id
            # т.к. в Rel_nurseries_breeding_breeds, поле nursery_id является ForeignKey
            # к Profiles.
            breeding_breeds = db.query(Rel_nurseries_breeding_breeds)\
                .where(Rel_nurseries_breeding_breeds.nursery_id == kennel_id)
            breeding_breeds = breeding_breeds.all()
            breeds = [c.dict(exclude_unset=True)
                      for c in update_data['breeds']] if update_data['breeds'] else []
            del_b_breeds, _, crt_b_breeds = self.split_objs_breeds_in_crud(db_objects=breeding_breeds,
                                                                           objs_in=breeds,
                                                                           nursery_id=kennel_id)
            rnbb.delete_by_breed_ids(db=db, breed_ids=del_b_breeds,
                                     nursery_id=kennel_id, commit=True)
            created = rnbb.create_multi_by_breeds_ids(
                db=db, breed_ids=crt_b_breeds, nursery_id=kennel_id, commit=True)
        ##########################################################################
        ################################################## Main_schema ###########
        ######################################### Работаю с Legal_informations ###
        legal_informations_obj = c_legal_info.get_by_profile_id(db=db,
                                                                profile_id=profile_id,
                                                                omit_exception=True,
                                                                as_object=True)
        legal_informations_update_obj_in = Legal_informations_update(
            **update_data['main'])
        update_legal_info = c_legal_info.update(db=db,
                                                db_obj=legal_informations_obj,
                                                obj_in=legal_informations_update_obj_in,
                                                commit=False)
        ######################################### Работаю с Legal_informations ###
        ############################################ Работаю с Federations #######
        rel_prof_fed = rpf.get_legal_info(db=db, profile_id=profile_id)
        rel_prof_fed_obj_in = schemas.Rel_profiles_federations_update(
            profile_id=profile_id,
            federations_id=update_data['main'].get('federation_id', None)
        )
        updated_rel_profiles = rpf.update(db=db,
                                          db_obj=rel_prof_fed,
                                          obj_in=rel_prof_fed_obj_in,
                                          commit=False)
        ######################################### Работаю с Federations ##########
        ########################################## Работаю с Additional ##########
        if kennel_id:
            additional_info_obj = db.query(Nurseries).where(
                Nurseries.id == kennel_id).first()
            add_obj_in = schemas.nurseries.Nurseries_additional(
                **update_data['additional'])
            if additional_info_obj:
                additional_info_map = self._orm_obj_dict(
                    super(CRUDNurseries, self).update(db=db,
                                                      db_obj=additional_info_obj,
                                                      obj_in=add_obj_in,
                                                      commit=False)
                )
            else:
                additional_info_map = self._orm_obj_dict(
                    super(CRUDNurseries, self).create(db=db,
                                                      obj_in=add_obj_in,
                                                      commit=False)
                )
        ########################################## Работаю с kennel_id ###########
        ########################################## Работаю с stamp_code ##########
        stamp_code_obj = c_stamp_code.get_object(
            db=db,
            id=profile_id,
            omit_exception=True
        )
        try:
            if stamp_code_obj:
                upd_obj_in = schemas.Stamp_codes_update(**update_data['main'])
                super(CRUDNurseries, self).update(
                    db=db,
                    db_obj=stamp_code_obj,
                    obj_in=upd_obj_in,
                    commit=False
                )
            else:
                crt_obj_in = schemas.Stamp_codes_create(**update_data['main'])
                super(CRUDNurseries, self).create(
                    db=db,
                    obj_in=crt_obj_in,
                    commit=False
                )
        except AttributeError as ae:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail=str(ae))

        ########################################## Работаю с stamp_code ##########
        ################################################## Main_schema ###########

        # Вот тут всё и обновиться.
        try:
            db.commit()
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=f"Редактирование питомника по profile_id={profile_id} не удалось.")


nurseries_crud_explicit = CRUDNurseries(model=Profiles)

# def get(self, db: Session, id: int) -> schemas.Nurseries_by_id:
#     kennel_id = None # Здесь будет id найденного питомника
#     main = None
#     breeding_breeds = None
#     additional_info = None
#     history = None
#     payments = None
#     main_block = None
#     breeding_breeds_block = None
#     additional_info_block = None
#     history_block = None
#     payments_block = None
#     blocks_list = []
#    #Основная информация
#     main = db.query(
#         self.model.id.label(self.id),
#         Rel_users_profiles.user_id.label(self.l_rel_user_profiles_id),
#         Legal_informations.id.label(self.l_legal_informations_id),
#         Legal_informations.organization_status_id.label(self.l_legal_informations_organization_status_id),
#         Legal_informations.city_id.label(self.l_legal_informations_city_id),
#         Legal_informations.name.label(self.l_legal_informations_name),
#         Legal_informations.short_name.label(self.l_legal_informations_short_name),
#         Legal_informations.owner_name,
#         Legal_informations.owner_position,
#         Legal_informations.folder_number,
#         Legal_informations.active_member,
#         Legal_informations.is_enable_web,
#         Nurseries.id.label(self.l_nurseries_id),
#         Legal_informations.is_public.label(self.legal_informations_is_public),
#         Rel_profiles_federations.federation_id,
#         Federations.short_name.label(self.federations_short_name),
#         Stamp_codes.stamp_code,
#         Nursery_registration_requests.open_access_to_all_requests
#     ).join(Rel_users_profiles)\
#         .join(Legal_informations)\
#         .join(Nurseries, isouter=True)\
#         .join(Rel_profiles_federations, isouter=True) \
#         .join(Federations, isouter=True) \
#         .join(Nursery_stamp_codes)\
#         .join(Stamp_codes)\
#         .join(Nursery_registration_requests)\
#         .where(self.model.id == id)\
#         .filter(Nursery_stamp_codes.is_deleted == False,
#                 Stamp_codes.is_deleted == False,
#                 Nursery_registration_requests.is_deleted == False,
#                 Nursery_registration_requests.status_id == 3)
#     main = main.first()
#     main_dict = {}
#     if not main:
#         raise HTTPException(
#             status_code=404,
#             detail=f"Не найдено питомников с profile_id = {id}"
#         )
#     else:
#         main_dict = {**main}
#         kennel_id = getattr(main, "kennel_id", None)
#     # main_block = schemas.Nurseries_main_block_view(**main)
#     # blocks_list.append(main_block)
#
#    #Контактные данные
#     contact_nurseries = db.query(
#         Contacts.id.label(self.l_contacts_id),
#         Contacts.type_id.label(self.contacts_type_id), #c_t_model.name.label(self.contact_type_name),
#         Contacts.value.label(self.l_contacts_value),
#         Contacts.is_main.label(self.l_contacts_is_main),
#         Contacts.description.label(self.l_contacts_description)) \
#         .join(Rel_profiles_contacts)\
#         .join(c_t_model, isouter=True)\
#         .filter(Contacts.is_deleted == False,
#                 Rel_profiles_contacts.profile_id == id)
#     contact_nurseries = contact_nurseries.all()
#     contacts = [{**c} for c in contact_nurseries] if contact_nurseries else []
#
#    #Адресные данные (на обзор)
#     addresses = db.query(
#         Club_addresses.id,
#         Address_types.name.label("type_name"),
#         Address_types.name.label(self.club_address_type_name),
#         Club_addresses.type.label(self.club_address_type_id),
#         Club_addresses.postcode.label(self.l_club_address_postcode),
#         City_types.name.label(self.l_city_types_name),
#         Club_addresses.city_id.label(self.l_club_address_city_id),
#         Cities.name.label(self.l_cities_name),
#         Club_addresses.street_type_id.label(self.l_club_address_street_type_id),
#         Street_types.name.label(self.l_street_types_name),
#         Club_addresses.street_name.label(self.l_club_address_street_name),
#         Club_addresses.house_type_id.label(self.l_club_address_house_type_id),
#         House_types.name.label(self.l_house_types_name),
#         Club_addresses.house_name.label(self.l_club_address_house_name),
#         Club_addresses.flat_type_id.label(self.l_club_address_flat_type_id),
#         Flat_types.name.label(self.l_flat_types_name),
#         Club_addresses.flat_name.label(self.l_club_address_flat_name),
#         Club_addresses.geo_lat.label(self.l_club_address_geo_lat),
#         Club_addresses.geo_lon.label(self.l_club_address_geo_lon),
#         func.concat(Club_addresses.postcode, " ",
#                     City_types.name, " ",
#                     Cities.name, " ",
#                     Street_types.name, " ",
#                     Club_addresses.street_name, " ",
#                     House_types.name, " ",
#                     Club_addresses.house_name, " ",
#                     Flat_types.name, " ",
#                     Club_addresses.flat_name
#                     ).label("address"))\
#         .select_from(Club_addresses)\
#         .join(Address_types)\
#         .join(Cities, isouter=True)\
#         .join(City_types, isouter=True)\
#         .join(Street_types, isouter=True)\
#         .join(House_types, isouter=True)\
#         .join(Flat_types, isouter=True)\
#         .filter(Club_addresses.profile_id == id)\
#         .order_by(Club_addresses.address_type_id)
#     addresses = addresses.all()
#     addresses_block = [{**addr} for addr in addresses] if addresses else []
#
#    #Разведение пород
#     breeding_breeds = db.query(
#         Breeds.id.label("id"),
#         Breeds.fci_number,
#         FCI_groups.number.label("group_number"),
#         func.concat(Breeds.name_rus, ' / ',Breeds.name_en).label('breed_name')
#         ).select_from(Rel_nurseries_breeding_breeds) \
#         .join(Breeds)\
#         .join(Rel_breeds_sections_groups)\
#         .join(FCI_groups)\
#         .where(Rel_nurseries_breeding_breeds.nursery_id == kennel_id)\
#         .order_by(Breeds.name_rus)
#     breeding_breeds = breeding_breeds.all()
#     breeds = [{**bb} for bb in breeding_breeds] if breeding_breeds else []
#
#    #Дополнительная информация
#     additional_info = db.query(
#         Nurseries.certificate_registration_nursery_id.label("certificate_registration_nursery_id"),
#         Nurseries.registration_date.label("registration_date"),
#         Nurseries.prefix.label("prefix"),
#         Nurseries.suffix.label("suffix"),
#         Nurseries.name_lat.label("name_lat"),
#         Nurseries.owner_specialist_rkf.label("owner_specialist_rkf"),
#         Nurseries.owner_date_speciality.label("owner_date_speciality"),
#         Nurseries.owner_special_education.label("owner_special_education"),
#         Nurseries.owner_place_speciality.label("owner_place_speciality"),
#         Nurseries.owner_speciality.label("owner_speciality"),
#         Nurseries.experience_dog_breeding.label("experience_dog_breeding"),
#         Nurseries.puppies_total_count.label("puppies_total_count"),
#         Nurseries.owner_ranks.label("owner_ranks"),
#         Nurseries.dogs_ranks.label("dogs_ranks")
#     ).where(Nurseries.id == kennel_id)
#     additional_info = additional_info.first()
#     add_info_dict = {**additional_info} if additional_info else {}
#
#     history_info = org_h.get(db=db, profile_id=id, omit_exception=True)
#     history_info_dict = {**history_info} if history_info else {}
#
#    #платежи
#     pay_list = org_m_d.get_by_profile_id(db=db, profile_id=id, omit_exception=True)
#     pay_list_dict = [{**p} for p in pay_list] if pay_list else []
#    #pay_list = [Organizations_membership_dues_item(**p) for p in pay_list] if pay_list else []
#    #payments_block = schemas.Nurseries_payments_block(payments=pay_list)
#    #blocks_list.append(payments_block)
# ###################################################################################
#    #Здесь начинается работа со списками заявок
#     u_access = user_accesses.get_list_accesses_profile_id(db=db, profile_id=id)
#     prof_t_r = profile_type_requests.get_order_types_list(db=db)
#     u_access_ids = {u[1]:{u} for u in u_access}
#     prof_t_r_ids = {p[0]:{p} for p in prof_t_r}
#     active_ids = set(u_access_ids.keys()) & set(prof_t_r_ids.keys())
#     access = [{'access_name': p[3], 'value': (p[0] in active_ids)} for p in prof_t_r]
# ###################################################################################
#     return (main_dict, contacts, addresses_block, breeds,
#             add_info_dict, history_info_dict, pay_list_dict, access)
