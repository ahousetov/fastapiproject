from sqlalchemy import text
from sqlalchemy.orm import Session, Query
from sqlalchemy import func
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

from app.schemas import Filters_model
from app.crud.base import ModelType, CRUDBase
from app.models.catalogs.cities import Cities
from app.models.catalogs.city_types import City_types
from app.models.catalogs.federal_district import Federal_Districts
from app.models.catalogs.regions import Regions
from app.schemas.catalogs.cities import Cities_create, \
    Cities_update, \
    Cities_blocks, \
    Cities_block
from fastapi import APIRouter, Body, Depends, HTTPException


class CRUDCities(CRUDBase[Cities, Cities_create, Cities_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDCities, self).__init__(model)
        self.fci_breed_statuses_id = f"{self.fci_breed_statuses}.id"
        self.fci_breed_statuses_name = f"{self.fci_breed_statuses}.name"
        self.fci_groups_number = f"{self.fci_groups}.number"
        self.fci_groups_name = f"{self.fci_groups}.name"
        self.fci_sections_number = f"{self.fci_groups}.number"
        self.fci_sections_name = f"{self.fci_groups}.name"
        self.rel_breeds_sections_groups_group_id = f"{self.rel_breeds_sections_groups}.group_id"
        self.rel_breeds_sections_groups_section_id = f"{self.rel_breeds_sections_groups}.section_id"
        self.federal_districts_name = f"{self.federal_districts}.name"
        self.regions_name = f"{self.regions}.name"
        self.city_types_name = f"{self.city_types}.name"
        self.city_geo = "geo"

    def get(self, db: Session, id: Any) -> Optional[Any]:
        city = db.query(
            self.model.id,
            self.model.name,
            self.model.name_eng,
            self.model.federal_district_id,
            self.model.region_id,
            self.model.type_id,
            self.model.geo_lat,
            self.model.geo_lon,
            Federal_Districts.name.label(self.l_federal_districts_name),
            Regions.name.label(self.l_regions_name),
            City_types.name.label(self.l_city_types_name)
        )\
            .join(Federal_Districts, isouter=True)\
            .join(Regions, isouter=True)\
            .join(City_types, isouter=True)
        city = city.filter(self.model.is_deleted == False,
                           self.model.id == id)
        city = city.first()
        if not city:
            raise HTTPException(
                status_code=404,
                detail=f"Не найдено города по id = {id}"
            )
        return city

    def get_all(
            self,
            db: Session, *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: Filters_model = None,
            is_deleted: bool = False
    ) -> List[Dict[str, Any]]:
        query = db.query(
            self.model.id,
            self.model.name,
            self.model.name_eng,
            self.model.federal_district_id,
            self.model.region_id,
            self.model.type_id,
            self.model.geo_lat,
            self.model.geo_lon,
            func.concat(self.model.geo_lat, ', ', self.model.geo_lon).label('geo'),
            Federal_Districts.name.label(self.l_federal_districts_name),
            Regions.name.label(self.l_regions_name),
            City_types.name.label(self.l_city_types_name)
        ) \
            .join(Federal_Districts, isouter=True) \
            .join(Regions, isouter=True) \
            .join(City_types, isouter=True)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            query = query.filter(self.model.is_deleted == False)

        # Пользовательские фильтры
        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        query = query.offset(skip).limit(limit)

        rows = query.all()

        return [dict(row) for row in rows]


cities = CRUDCities(Cities)
