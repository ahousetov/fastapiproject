from sqlalchemy.orm import Session, Query
from fastapi import HTTPException, status
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

import schemas
from app.crud.base import CRUDBase
from app.models.catalogs.federal_district import Federal_Districts
from app.schemas import Filters_model
from app.schemas.catalogs.federal_district import Federal_district_create, \
    Federal_district_update, \
    Federal_district_blocks, \
    Federal_district_block


class CRUDFederalDistrict(
        CRUDBase[Federal_Districts, Federal_district_create, Federal_district_update]):

    def get(self,
            db: Session,
            id: int
            ) -> Dict[str, Any]:
        entry = db.query(self.model.id, self.model.name) \
            .filter(self.model.id == id,
                    self.model.is_deleted == False)
        entry = entry.first()
        if not entry:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Не найдено федерального округа по id = {id}"
            )
        return dict(entry)

    def get_all(self,
                db: Session, *,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id',
                filters: Filters_model = None,
                is_deleted: bool = False
                ) -> List[dict]:
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        sort_direction, sort_field = self._sort_params(sort_direction=sort_direction,
                                                       sort_field=sort_field,
                                                       filters=filters)
        entry = db.query(self.model.id, self.model.name)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        # Пользовательские фильтры
        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)

        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        entry = entry.offset(skip).limit(limit)

        rows = entry.all()

        return [dict(row) for row in rows]

    def update(
        self,
        db: Session,
        id: int,
        update_data: Dict[str, Any] = None
    ) -> bool:

        district = db.query(self.model).filter(self.model.id == id).first()
        if district is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Нет {self.title} по id = {id}"
            )

        district = super().update(db,
                                  db_obj=district,
                                  obj_in=update_data)

        return True

    def create(
        self,
        db: Session,
        create_data: dict = None
    ) -> bool:
        super().create(db, obj_in_dict=create_data)
        return True


federal_district = CRUDFederalDistrict(Federal_Districts)
