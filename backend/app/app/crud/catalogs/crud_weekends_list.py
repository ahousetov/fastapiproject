from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.catalogs.weekends_list import Weekends_list
from app.schemas.catalogs import Weekends_list_base, Weekends_list_create, Weekends_list_update, Weekends_list_delete


class CRUDWeekends_list(
        CRUDBase[Weekends_list, Weekends_list_create, Weekends_list_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDWeekends_list, self).__init__(model)


weekends_list = CRUDWeekends_list(Weekends_list)
