from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from fastapi import HTTPException, status

from app.crud.base import CRUDBase
from app.models.catalogs.street_types import Street_types
from app.schemas.catalogs.street_types import Street_type_create, \
    Street_type_update, \
    Street_type_blocks, \
    Street_type_block


class CRUDStreet_types(CRUDBase[Street_types, Street_type_create, Street_type_update]):
    def get(self,
            db: Session,
            id: int,
            is_deleted: bool = False
            ) -> Any:
        entry = db.query(self.model.id, self.model.name)
        if not is_deleted and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)
        entry = entry.filter(self.model.id == id)
        result = entry.first()
        if not result:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Не найдено элемента по id = {id} в таблице {self.title}"
            )
        return result

    def get_all(self,
                db: Session,
                *,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id',
                filters: dict = None,
                is_deleted: bool = False) -> List[dict]:

        entry = db.query(self.model.id, self.model.name)
        if not is_deleted and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)
        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)
        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        entry = entry.offset(skip).limit(limit)
        result = entry.all()
        return result


street_types = CRUDStreet_types(Street_types)
