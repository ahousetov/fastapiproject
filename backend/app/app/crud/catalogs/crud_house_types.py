from fastapi import HTTPException, status
from sqlalchemy.orm import Session, Query
from sqlalchemy import literal, exc, text, and_, or_, Integer, asc, desc, case
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

import schemas
from app.crud.base import CRUDBase
from app.models.catalogs.house_types import House_types
from app.schemas.catalogs.house_types import House_type_create, \
    House_type_update, \
    House_type_blocks, \
    House_type_block


class CRUDHouse_types(CRUDBase[House_types, House_type_create, House_type_update]):
    def get(self, db: Session, id: Any) -> Optional[House_type_blocks]:
        try:
            house_type = db.query(self.model.id, self.model.name)\
                .filter(self.model.id == id,
                        self.model.is_deleted == False).first()
            if not house_type:
                raise Exception
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=str(e))
        return house_type

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: dict = None
    ) -> List[schemas.House_type]:
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        sort_direction, sort_field = self._sort_params(sort_direction=sort_direction,
                                                       sort_field=sort_field,
                                                       filters=filters)
        model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
        entry = db.query(self.model.id, self.model.name)
        fil_query = self._make_filter_model(filters_model=filters,
                                            column_descriptors=entry.column_descriptions,
                                            not_deleted=True)
        entry = entry.filter(text(fil_query))\
            .order_by(text(f"{sort_field} {sort_direction}"))\
            .offset(skip).limit(limit)
        entry = entry.all()
        return entry


house_types = CRUDHouse_types(House_types)
