from sqlalchemy.orm import Session, Query
# fastapi
from fastapi import HTTPException, status
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from app.crud.base import CRUDBase
from app.models.catalogs.flat_types import Flat_types
from app.schemas.catalogs.flat_types import Flat_type_create, \
    Flat_type_update, \
    Flat_type_blocks, \
    Flat_type_block, \
    Flat_type
from sqlalchemy import literal, exc, text, and_, or_, Integer, asc, desc, case


class CRUDFlat_types(CRUDBase[Flat_types, Flat_type_create, Flat_type_update]):
    def get(self, db: Session, id: int) -> Optional[Flat_type_blocks]:
        try:
            flat_type = db.query(self.model.id,
                                 self.model.name)\
                .filter(self.model.id == id,
                        self.model.is_deleted == False)\
                .first()
            if not flat_type:
                raise Exception
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=str(e))
        return flat_type

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: dict = None
    ) -> List[Flat_type]:
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        sort_direction, sort_field = self._sort_params(sort_direction=sort_direction,
                                                       sort_field=sort_field,
                                                       filters=filters)
        model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
        entry = db.query(self.model.id, self.model.name)
        fil_query = self._make_filter_model(filters_model=filters,
                                            column_descriptors=entry.column_descriptions,
                                            not_deleted=True)
        entry = entry.filter(text(fil_query))\
            .order_by(text(f"{sort_field} {sort_direction}"))\
            .offset(skip).limit(limit)
        entry = entry.all()
        return entry


flat_types = CRUDFlat_types(Flat_types)
