from fastapi import HTTPException, status
from sqlalchemy import literal, exc, text
from app.crud.base import CRUDBase
from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional
from app.models.catalogs.organization_statuses import Organization_statuses
from app.schemas.catalogs.organization_statuses import Organization_statuses_create, \
    Organization_statuses_update, \
    Organization_statuses_blocks, \
    Organization_statuses_block, \
    Organization_statuses_value_list


class CRUDOrganization_statuses(CRUDBase[Organization_statuses,
                                         Organization_statuses_create,
                                         Organization_statuses_update]):
    def get(self, db: Session, id: Any) -> Optional[Organization_statuses_blocks]:
        try:
            value = self.get_object(db=db, id=id)
        except HTTPException as httpe:
            raise httpe
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                detail="Непредвиденная ошибка при получении статуса организации.")
        return self._orm_obj_dict(value)

    def get_all(self, db: Session,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id',
                ) -> Optional[List[Organization_statuses_block]]:
        try:
            value_list = db.query(self.model.id, self.model.name) \
                .order_by(text(f"{sort_field} {sort_direction}")) \
                .offset(skip).limit(limit).all()
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)

        return [Organization_statuses_block(**value)
                for value in value_list] if value_list else []


organization_statuses = CRUDOrganization_statuses(Organization_statuses)
