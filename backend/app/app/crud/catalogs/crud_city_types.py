from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from app.crud.base import CRUDBase
from app.models.catalogs.city_types import City_types
from app.schemas import Filters_model
from app.schemas.catalogs.city_types import City_types_create, \
    City_types_update, \
    City_types_blocks


class CRUDCity_types(CRUDBase[City_types, City_types_create, City_types_update]):

    def get(self, db: Session, id: Any) -> Optional[City_types_blocks]:
        city_type = db.query(self.model.id, self.model.name) \
            .filter(self.model.id == id, self.model.is_deleted == False)
        city_type = city_type.first()
        if not city_type:
            raise HTTPException(
                status_code=404,
                detail=f"Не найдено пород по id = {id}"
            )
        return city_type

    def get_all(
            self,
            db: Session, *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: Filters_model = None,
            is_deleted: bool = False
    ) -> List[dict]:
        query = db.query(self.model.id, self.model.name)
        if not is_deleted and hasattr(self.model, 'is_deleted'):
            query = query.filter(self.model.is_deleted == False)

        # Пользовательские фильтры
        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        query = query.offset(skip).limit(limit)

        rows = query.all()

        return [dict(row) for row in rows]


city_types = CRUDCity_types(City_types)
