from sqlalchemy.orm import Session, Query
from sqlalchemy import literal, exc, text
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from app.crud.base import CRUDBase
from app.models.catalogs.contact_types import Contact_types
from app.schemas.catalogs.contact_types import Contact_types_create, \
    Contact_types_update, \
    Contact_types_blocks, \
    Contact_types_block, \
    Contact_types_in_DBBase, \
    Contact_types_value_list


class CRUDContact_types(
        CRUDBase[Contact_types, Contact_types_create, Contact_types_update]):

    def get(
        self,
        db: Session,
        id: Any
    ) -> Optional[Contact_types_blocks]:
        try:
            value = super(type(self), self).get(db=db, id=id)
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)
        block = Contact_types_block(id=value.id, name=value.name)
        contact_types_blocks = Contact_types_blocks(title=self.title,
                                                    item_title=value.name,
                                                    blocks=[block]
                                                    )
        return contact_types_blocks

    def get_all(
        self,
        db: Session,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
    ) -> Optional[List[Contact_types_in_DBBase]]:
        try:
            value_list = db.query(self.model.id, self.model.name) \
                .order_by(text(f"{sort_field} {sort_direction}")) \
                .offset(skip).limit(limit).all()
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)
        return [Contact_types_in_DBBase(**value)
                for value in value_list] if value_list else []

    def get_valuelist(
        self,
        db: Session,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
    ) -> Optional[List[Contact_types_value_list]]:
        try:
            value_list = db.query(self.model.id.label("value"),
                                  self.model.name.label("name")) \
                .order_by(text(f"{sort_field} {sort_direction}")) \
                .offset(skip).limit(limit).all()
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)
        return value_list


contact_types = CRUDContact_types(Contact_types)
