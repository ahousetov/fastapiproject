from sqlalchemy.orm import Session, Query
from sqlalchemy import literal, exc, text
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from app.crud.base import CRUDBase
from app.models.catalogs.address_types import Address_types
from app.schemas.catalogs.address_types import Address_types_create, \
    Address_types_update, \
    Address_types_blocks, \
    Address_types_block, \
    Address_types_in_DBBase, \
    Address_types_value_list


class CRUDAddress_types(
        CRUDBase[Address_types, Address_types_create, Address_types_update]):

    def get(self,
            db: Session,
            id: Any
            ) -> Optional[Address_types_blocks]:
        try:
            value = super(type(self), self).get(db=db, id=id)
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)
        block = Address_types_block(id=value.id, name=value.name)
        address_types_blocks = Address_types_blocks(title=self.title,
                                                    item_title=value.name,
                                                    blocks=[block]
                                                    )
        return address_types_blocks

    def get_all(self,
                db: Session,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id') -> Optional[List[Address_types_in_DBBase]]:
        try:
            value_list = db.query(self.model.id, self.model.name) \
                .order_by(text(f"{sort_field} {sort_direction}")) \
                .offset(skip).limit(limit).all()
        except Exception as e:
            exp = {
                "title": self.title,
                "item_title": str(e),
                "block_icon": 'info_outline'
            }
            raise Exception(exp)
        return [Address_types_in_DBBase(**value)
                for value in value_list] if value_list else []


address_types = CRUDAddress_types(Address_types)
