from typing import Any, Dict, List, Optional, Type, Union
from sqlalchemy import text
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

from app import crud
from app.crud.base import CRUDBase, Contact_types, ModelType
from app.crud.relations.crud_profiles_contacts import rel_profiles_contacts as rpc
from app import schemas
from app.models.catalogs.federations import Federations
from app.models.public.profiles import Profiles
from app.models.public.legal_informations import Legal_informations
from app.models.clubs.federation_clubs import Federation_clubs
from app.models.relations.rel_profiles_federations import Rel_profiles_federations
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.models.public.contacts import Contacts
from app.models.catalogs import contact_types
from app.schemas.catalogs.federations import Federations_create, \
    Federations_update, \
    Federations_blocks, \
    Federations_block, \
    Federation_info_block, \
    Federation_info_value_list
from app.schemas.public.contacts import Contact_block, Contacts_extra_data
from app.schemas.catalogs.federations import ContactsUpdate, ContactsCreate


class CRUDFederations(CRUDBase[Federations, Federations_create, Federations_update]):
    def __init__(self, model: Type[ModelType] = None):
        super(CRUDFederations, self).__init__(model)
        self.legal_informations_owner_name = f"{self.legal_informations}.owner_name"
        self.legal_informations_owner_position = f"{self.legal_informations}.owner_position"
        self.legal_informations_ogrn = f"{self.legal_informations}.ogrn"
        self.legal_informations_inn = f"{self.legal_informations}.inn"
        self.legal_informations_kpp = f"{self.legal_informations}.kpp"
        self.block_title_basic = "Основная информация"
        self.block_title_contact = "Контактные данные"
        self.block_icon_basic = "info_outline"
        self.block_icon_contact = "call"

    def get(self,
            db: Session,
            id: int,
            is_deleted: bool = False
            ) -> dict:
        federation = db.query(self.model.id,
                              self.model.name,
                              self.model.short_name,
                              Federation_clubs.profile_id,
                              Legal_informations.owner_name.label(
                                  self.l_legal_informations_owner_name),
                              Legal_informations.owner_position.label(
                                  self.l_legal_informations_owner_position),
                              Legal_informations.ogrn.label(
                                  self.l_legal_informations_ogrn),
                              Legal_informations.inn.label(
                                  self.l_legal_informations_inn),
                              Legal_informations.kpp.label(self.l_legal_informations_kpp)) \
            .select_from(Profiles) \
            .join(Federation_clubs, Federation_clubs.profile_id == Profiles.id) \
            .join(self.model, self.model.id == Federation_clubs.federation_id) \
            .outerjoin(Legal_informations, Legal_informations.profile_id == Profiles.id) \
            .filter(self.model.id == id)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            federation = federation.filter(self.model.is_deleted == False)

        federation = federation.first()
        if not federation:
            exp = {
                "title": self.title,
                "item_title": f'Нет записи по id {id}',
                "block_icon": 'info_outline'
            }
            raise Exception(exp)

        federation = dict(federation)

        contacts = db.query(Contacts.id,
                            contact_types.Contact_types.name.label('type_name'),
                            Contacts.type_id,
                            Contacts.value,
                            Contacts.is_main,
                            Contacts.description) \
            .join(Rel_profiles_contacts, Rel_profiles_contacts.contact_id == Contacts.id) \
            .outerjoin(contact_types.Contact_types, contact_types.Contact_types.id == Contacts.type_id) \
            .filter(Contacts.is_deleted == False,
                    Rel_profiles_contacts.profile_id == federation['profile_id']) \
            .order_by(Contacts.type_id, Contacts.id)
        contacts = contacts.all()
        contacts = [dict(contact) for contact in contacts]

        return dict(federation=federation, contacts=contacts)

    def get_federation_by_profile_id(self, db: Session, profile_id: int):
        federation = db.query(self.model.id, self.model.short_name)\
            .join(Rel_profiles_federations)\
            .filter(Rel_profiles_federations.profile_id == profile_id)
        federation = federation.first()
        return Federation_info_block(**federation) if federation else None

    def get_all(self,
                db: Session,
                *,
                skip: int = 0,
                limit: int = 100,
                sort_direction: str = 'asc',
                sort_field: str = 'id',
                filters: schemas.Filters_model = None,
                is_deleted: bool = False
                ) -> List[dict]:
        sub_phones = rpc.make_subquery_contacts(db=db, type=Contact_types.phones)
        sub_email = rpc.make_subquery_contacts(db=db, type=Contact_types.emails)
        sub_site = rpc.make_subquery_contacts(db, type=Contact_types.sites)

        entry = db.query(self.model.id,
                         self.model.name,
                         self.model.short_name,
                         Legal_informations.owner_name.label(
                             self.l_legal_informations_owner_name),
                         Legal_informations.owner_position.label(
                             self.l_legal_informations_owner_position),
                         Legal_informations.ogrn.label(self.l_legal_informations_ogrn),
                         Legal_informations.inn.label(self.l_legal_informations_inn),
                         Legal_informations.kpp.label(self.l_legal_informations_kpp),
                         sub_phones.c.phones,
                         sub_email.c.emails,
                         sub_site.c.sites) \
            .select_from(Profiles) \
            .join(Federation_clubs, Federation_clubs.profile_id == Profiles.id) \
            .join(self.model) \
            .outerjoin(Legal_informations, Legal_informations.profile_id == Profiles.id) \
            .outerjoin(sub_phones) \
            .outerjoin(sub_email) \
            .outerjoin(sub_site)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)

        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        entry = entry.offset(skip).limit(limit)

        result = entry.all()

        return [dict(row) for row in result]

    def update(
        self,
        db: Session,
        id: int,
        update_data: Dict[str, Any]
    ) -> bool:
        legal_information_data = update_data.get('legal_informations')
        contact_data = update_data.get('contacts')

        federation_club = crud.federation_clubs.get_by_federation_id(
            db, federation_id=id)
        legal_information = crud.legal_informations.get_by_profile_id(db,
                                                                      profile_id=federation_club.profile_id,
                                                                      as_object=True)
        crud.legal_informations.update(
            db, db_obj=legal_information, obj_in=legal_information_data)
        contacts = crud.contacts.get_by_profile_id(
            db, profile_id=federation_club.profile_id)

        delete_contact_ids, update_contacts, create_contacts = crud.national_breed_clubs.split_objs_in_crud(
            db_objs=contacts,
            objs_in=contact_data,
            schema_update=ContactsUpdate,
            schema_create=ContactsCreate)

        crud.rel_profiles_contacts.delete_by_contact_ids(
            db=db, contact_ids=delete_contact_ids)
        crud.contacts.delete_multi(db=db, ids=delete_contact_ids)
        create_objs = crud.contacts.create_multi(db=db, create_contacts=create_contacts)
        crud.rel_profiles_contacts.create_multi_by_contact_ids(
            db=db,
            contact_ids=[contact.id for contact in create_objs] if create_objs else [],
            profile_id=federation_club.profile_id)
        crud.national_breed_clubs.multi_update(
            db=db, objs_in=update_contacts, model=Contacts)

        return True

    def remove_by_names(self, db: Session, *,
                        generated_names: List[str] = None) -> Federations:
        query_to_remove = db.query(
            self.model).filter(
            self.model.name.in_(generated_names))
        query_to_remove.delete()
        db.commit()
        return True

    def get_federations_value_list(self,
                                   db: Session,
                                   sort_field: str = 'id',
                                   sort_direction: str = 'asc') -> List[Federation_info_value_list]:
        federations = db.query(self.model.id, self.model.short_name)\
            .filter(self.model.is_deleted == False)\
            .order_by(text(f"{sort_field} {sort_direction}"))
        federations = federations.all()
        return [Federation_info_value_list(
            value=f.id, name=f.short_name) for f in federations] if federations else None


federations = CRUDFederations(Federations)
