from typing import Any, Optional

from fastapi import HTTPException
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.catalogs.social_networks_types import Social_networks_types
from app.schemas.catalogs.social_networks_types import \
    Social_networks_types_create
from app.schemas.catalogs.social_networks_types import \
    Social_networks_types_update


class CRUDSocial_networks_types(
    CRUDBase[Social_networks_types,
             Social_networks_types_create,
             Social_networks_types_update]):
    def get(self, db: Session, id: Any, **kwargs) -> Optional[Social_networks_types]:
        return db.query(self.model).filter(self.model.id == id).first()


social_networks_types = CRUDSocial_networks_types(
    Social_networks_types)
