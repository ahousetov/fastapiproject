from typing import Any, List, Optional
from sqlalchemy.sql import expression
from fastapi import HTTPException, status
from sqlalchemy import func, select
from sqlalchemy.dialects.postgresql import aggregate_order_by
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types
from app.db.base_class import Base
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.models.public.contacts import Contacts
from app.models.public.profiles import Profiles
from app.schemas.relations.rel_profiles_contacts import \
    Rel_profiles_contacts_create, \
    Rel_profiles_contacts_update


class CRUDRel_profiles_contacts(CRUDBase[Rel_profiles_contacts,
                                         Rel_profiles_contacts_create,
                                         Rel_profiles_contacts_update]):

    def get(self, db: Session, id: Any) -> Optional[Rel_profiles_contacts]:
        rel = db.query(self.model).filter(self.model.id == id).first()
        if not rel:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Не получается найти запись в {self.title}")
        return rel

    def make_subquery_contacts(self,
                               db: Session,
                               type: Contact_types = None,
                               main: bool = False) -> Any:
        """
        Составляем подзапрос для поиска контактов. Далее, результат присоединяется
        по profile_id

        Parameters:
            db(Session)- объект сессии базы данных
            type(Contact_types(Enum))- перечисление по типу контактов:
                                            1 - телефон
                                            2 - email
                                            3 - сайт
            main(Bool) - "основной" или очередной контакт. В таблице указан список
                            контактов, но "основным" может быть только один из них.

            По умолчанию:
                make_subquery_contacts(self,
                                       db: Session,
                                       type: Contact_types = None,
                                       main: bool = False) -> Any:
        """
        if type:
            if main:
                subquery = select(self.model.profile_id,
                                  func.string_agg(Contacts.value, ', ').label(type.name))\
                    .join(Contacts)\
                    .group_by(self.model.profile_id, Contacts.type_id)\
                    .where(self.model.profile_id == Profiles.id,
                           Contacts.type_id == type.value,
                           Contacts.is_deleted == False,
                           Contacts.is_main == True).subquery(str(type.name))
            else:
                subquery = select(self.model.profile_id,
                                  func.string_agg(Contacts.value, ', ').label(type.name))\
                    .join(Contacts)\
                    .group_by(self.model.profile_id, Contacts.type_id)\
                    .where(self.model.profile_id == Profiles.id,
                           Contacts.type_id == type.value,
                           Contacts.is_deleted == False).subquery(str(type.name))
        else:
            subquery = None
        return subquery

    def make_subquery_contacts_json(self,
                                    db: Session,
                                    type: Contact_types = None,
                                    main: bool = False) -> Any:
        if type:
            is_main = [Contacts.is_main == True] if main else []
            subquery = db.query(self.model.profile_id,
                                func.jsonb_agg(
                                    func.json_build_object(
                                        'id', Contacts.id,
                                        'type_id', Contacts.type_id,
                                        'value', Contacts.value,
                                        'description', Contacts.description,
                                        'is_main', Contacts.is_main)
                                ).label(type.name)) \
                .join(Contacts) \
                .group_by(self.model.profile_id, Contacts.type_id) \
                .where(self.model.profile_id == Profiles.id,
                       Contacts.type_id == type.value,
                       Contacts.is_deleted == False,
                       *is_main)\
                .subquery(type.name)
        else:
            subquery = None
        return subquery

    def sub_phones(self, db: Session, profile_id: int = 0, main: bool = False) -> Any:
        if main:
            sub_result = db.query(self.model.profile_id,
                                  Contacts.type_id,
                                  func.string_agg(Contacts.value, ', ')) \
                .join(Contacts) \
                .group_by(self.model.profile_id, Contacts.type_id) \
                .where(self.model.profile_id == profile_id,
                       Contacts.type_id == Contact_types.phones,
                       Contacts.is_deleted == False,
                       Contacts.is_main == True)
        else:
            sub_result = db.query(self.model.profile_id,
                                  Contacts.type_id,
                                  func.string_agg(Contacts.value, ', '))\
                .join(Contacts)\
                .group_by(self.model.profile_id, Contacts.type_id)\
                .where(self.model.profile_id == profile_id,
                       Contacts.type_id == Contact_types.phones,
                       Contacts.is_deleted == False)
        return sub_result

    def sub_emails(self, db: Session, profile_id: int = 0, main: bool = False) -> Any:
        if main:
            sub_result = db.query(self.model.profile_id,
                                  Contacts.type_id,
                                  func.string_agg(Contacts.value, ', '))\
                .join(Contacts)\
                .group_by(self.model.profile_id, Contacts.type_id)\
                .where(self.model.profile_id == profile_id,
                       Contacts.type_id == Contact_types.emails,
                       Contacts.is_deleted == False,
                       Contacts.is_main == True)
        else:
            sub_result = db.query(self.model.profile_id,
                                  Contacts.type_id,
                                  func.string_agg(Contacts.value, ', '))\
                .join(Contacts)\
                .group_by(self.model.profile_id, Contacts.type_id)\
                .where(self.model.profile_id == profile_id,
                       Contacts.type_id == Contact_types.emails,
                       Contacts.is_deleted == False)
        return sub_result

    def sub_sites(self, db: Session, profile_id: int = 0, main: bool = False) -> Any:
        if main:
            sub_result = db.query(self.model.profile_id,
                                  Contacts.type_id,
                                  func.string_agg(Contacts.value, ', ')) \
                .join(Contacts) \
                .group_by(self.model.profile_id, Contacts.type_id) \
                .where(self.model.profile_id == profile_id,
                       Contacts.type_id == Contact_types.sites,
                       Contacts.is_deleted == False,
                       Contacts.is_main == True)
        else:
            sub_result = db.query(self.model.profile_id,
                                  Contacts.type_id,
                                  func.string_agg(Contacts.value, ', ')) \
                .join(Contacts) \
                .group_by(self.model.profile_id, Contacts.type_id) \
                .where(self.model.profile_id == profile_id,
                       Contacts.type_id == Contact_types.sites,
                       Contacts.is_deleted == False)
        return sub_result

    def create_not_deleted(self,
                           db: Session,
                           obj_in: Rel_profiles_contacts_create = None,
                           obj_in_encoded: dict = None) -> Rel_profiles_contacts:
        if (not obj_in) and (not obj_in_encoded):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Данных для создания новых пород не передано в 'create_not_deleted'",
                headers={"X-Error": "There goes my error"}
            )
        obj_in_data = obj_in if obj_in else obj_in_encoded
        try:
            db_obj = self.model(**obj_in_data)
            db.add(db_obj)
            db.commit()
            db.refresh(db_obj)
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=str(e))
        return db_obj

    def create_multi_by_contact_ids(
            self, db: Session, contact_ids: List[int], profile_id: int, commit=True):
        if contact_ids:
            create_objs = []
            for contact_id in contact_ids:
                obj = self.model(contact_id=contact_id, profile_id=profile_id)
                create_objs.append(obj)

            db.add_all(create_objs)
            if commit:
                db.commit()

            return create_objs

    def get_all_contacts(self,
                         db: Session,
                         profile_id: int = None,
                         except_contact_id: int = -1):
        if not profile_id:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail=f"Profile_id не указан.")
        sub = db.query(self.model.contact_id)\
            .join(Contacts)\
            .where(self.model.profile_id == profile_id,
                   self.model.contact_id != except_contact_id,
                   Contacts.is_main == True)
        sub = sub.first()[0]
        if not sub:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=f"Не получается найти контакты по profile_id == {profile_id}")
        return sub

    def delete_by_contact_ids(
            self, db: Session, contact_ids: List[int], commit=True) -> Any:
        if contact_ids:
            query_for_remove_rel = db.query(
                self.model).filter(
                self.model.contact_id.in_(contact_ids))
            try:
                removed = query_for_remove_rel.delete()
                if commit:
                    db.commit()
            except Exception as e:
                raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                    detail=f"Не получается удалить контакты с id = ({contact_ids})")
            return removed


rel_profiles_contacts = CRUDRel_profiles_contacts(Rel_profiles_contacts)
