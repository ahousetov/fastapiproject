from app.crud.base import CRUDBase
from sqlalchemy.orm import Session
from fastapi import HTTPException
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups
from app.schemas.relations.rel_breeds_sections_groups import \
    Rel_breeds_sections_groups_create, \
    Rel_breeds_sections_groups_update
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from app.schemas.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups_aliased


class CRUDRel_breeds_sections_groups(CRUDBase[Rel_breeds_sections_groups,
                                              Rel_breeds_sections_groups_create,
                                              Rel_breeds_sections_groups_update]):

    def get(self, db: Session, id: Any) -> Optional[Rel_breeds_sections_groups]:
        return db.query(self.model).filter(self.model.id == id).first()

    def get_by_breed_id(self, db: Session,
                        breed_id: int) -> Optional[Rel_breeds_sections_groups]:
        rel = db.query(self.model).filter(self.model.breed_id == breed_id).first()
        return rel

    def create_not_deleted(self,
                           db: Session,
                           obj_in: Rel_breeds_sections_groups_create = None,
                           obj_in_encoded: dict = None) -> Rel_breeds_sections_groups:
        if (not obj_in) and (not obj_in_encoded):
            raise HTTPException(
                status_code=404,
                detail="Данных для создания новых пород не передано в 'create_not_deleted'",
                headers={"X-Error": "There goes my error"}
            )
        obj_in_data = obj_in if obj_in else obj_in_encoded
        obj_in_data = Rel_breeds_sections_groups_aliased(**obj_in_data).dict()
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete_by_breed_ids(self, db: Session, breed_ids: List[int]) -> Any:
        query_for_remove_rel = db.query(
            self.model).filter(
            self.model.breed_id.in_(breed_ids))
        removed = query_for_remove_rel.delete()
        db.commit()
        return removed


rel_breeds_sections_groups = CRUDRel_breeds_sections_groups(Rel_breeds_sections_groups)
