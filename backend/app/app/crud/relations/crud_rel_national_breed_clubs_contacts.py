from typing import Any, Optional, List
from sqlalchemy import func
from sqlalchemy import and_, or_
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.crud.base import Contact_types
from app.models import Contact_types as Contact_types_model
from app.models.public.contacts import Contacts
from app.models.relations.rel_national_breed_clubs_contacts import Rel_national_breed_clubs_contacts
from app.schemas.relations.rel_national_breed_clubs_contacts import Rel_national_breed_clubs_contacts_create
from app.schemas.relations.rel_national_breed_clubs_contacts import Rel_national_breed_clubs_contacts_update


class CRUDRel_national_breed_clubs_contacts(CRUDBase[Rel_national_breed_clubs_contacts,
                                                     Rel_national_breed_clubs_contacts_create,
                                                     Rel_national_breed_clubs_contacts_update]):
    def get(self, db: Session, id: Any, **
            kwargs) -> Optional[Rel_national_breed_clubs_contacts]:
        return db.query(self.model).filter(self.model.id == id).first()

    def make_subquery_contacts_agg(
            self,
            db: Session,
            type: Contact_types = None,
            main: bool = False
    ) -> Any:
        if not type:
            return

        is_main = [Contacts.is_main == True] if main else []

        tab = db.query(self.model.national_breed_club_id.label('id'),
                       Contacts.value,
                       Contacts.is_main) \
            .join(Contacts, Contacts.id == self.model.contact_id) \
            .filter(and_(Contacts.is_deleted == False, Contacts.type_id == type.value, *is_main)) \
            .order_by(Contacts.is_main.desc()) \
            .subquery('tab')

        subquery = db.query(tab.c.id,
                            func.string_agg(tab.c.value, ', ').label('value')) \
            .group_by(tab.c.id) \
            .subquery(type.name)

        return subquery

    def make_subquery_contacts_json(
            self,
            db: Session,
            main: bool = False
    ) -> Any:

        is_main = [Contacts.is_main == True] if main else []

        tab = db.query(self.model.national_breed_club_id.label('nbc_id'),
                       Contacts.id,
                       Contacts.type_id,
                       Contact_types_model.name.label('type_name'),
                       Contacts.value,
                       Contacts.description,
                       Contacts.is_main) \
            .join(Contacts, Contacts.id == self.model.contact_id) \
            .outerjoin(Contact_types_model, Contacts.type_id == Contact_types_model.id) \
            .filter(and_(Contacts.is_deleted == False,
                         # Contacts.type_id == type.value,
                         *is_main)) \
            .order_by(Contacts.is_main.desc()) \
            .subquery('tab')

        subquery = db.query(tab.c.nbc_id.label('id'),
                            func.jsonb_agg(
                                func.json_build_object(
                                    'id', tab.c.id,
                                    'type_id', tab.c.type_id,
                                    'type_name', tab.c.type_name,
                                    'value', tab.c.value,
                                    'description', tab.c.description,
                                    'is_main', tab.c.is_main)).label('value')) \
            .group_by(tab.c.nbc_id) \
            .subquery('contacts')

        return subquery

    def delete_by_contact_ids(
            self, db: Session, contact_ids: List[int], commit=True) -> Any:
        if contact_ids:
            query_for_remove_rel = db.query(
                self.model).filter(
                self.model.contact_id.in_(contact_ids))
            removed = query_for_remove_rel.delete()
            if commit:
                db.commit()
            return removed

    def create_multi_by_contact_ids(
            self, db: Session, contact_ids: List[int], nbc_id: int, commit=True):
        if contact_ids and nbc_id:
            create_objs = []
            for contact_id in contact_ids:
                obj = self.model(contact_id=contact_id, national_breed_club_id=nbc_id)
                create_objs.append(obj)

            db.add_all(create_objs)
            if commit:
                db.commit()

            return create_objs


rel_national_breed_clubs_contacts = CRUDRel_national_breed_clubs_contacts(
    Rel_national_breed_clubs_contacts)
