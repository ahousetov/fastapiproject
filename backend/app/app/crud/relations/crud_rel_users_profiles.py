from typing import Any, List, Optional
from fastapi import HTTPException
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types
from app.models.relations.rel_users_profiles import Rel_users_profiles
from app.schemas.relations.rel_users_profiles import \
    Rel_users_profiles_create, \
    Rel_users_profiles_update


class CRUDRel_users_profiles(CRUDBase[Rel_users_profiles,
                                      Rel_users_profiles_create,
                                      Rel_users_profiles_update]):

    def get(self, db: Session, id: Any) -> Optional[Rel_users_profiles]:
        return db.query(self.model).filter(self.model.id == id).first()

    def create_not_deleted(self,
                           db: Session,
                           obj_in: Rel_users_profiles_create = None,
                           obj_in_encoded: dict = None) -> Rel_users_profiles:
        if (not obj_in) and (not obj_in_encoded):
            raise HTTPException(
                status_code=404,
                detail="Данных для создания новых пород не передано в 'create_not_deleted'",
                headers={"X-Error": "There goes my error"}
            )
        obj_in_data = obj_in if obj_in else obj_in_encoded
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete_by_user_ids(self, db: Session, user_ids: List[int]) -> Any:
        query_for_remove_rel = db.query(
            self.model).filter(
            self.model.user_id.in_(user_ids))
        removed = query_for_remove_rel.delete()
        db.commit()
        return removed


rel_users_profiles = CRUDRel_users_profiles(Rel_users_profiles)
