from typing import Any, List, Optional
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase
from app.models.relations.rel_nurseries_breeding_breeds import Rel_nurseries_breeding_breeds
from app.schemas.relations.rel_nurseries_breeding_breeds import Rel_nurseries_breeding_breed_create, \
    Rel_nurseries_breeding_breed_update


class CRUDRel_nurseries_breeding_breeds(CRUDBase[Rel_nurseries_breeding_breeds,
                                                 Rel_nurseries_breeding_breed_create,
                                                 Rel_nurseries_breeding_breed_update]):

    def create_multi_by_breeds_ids(
            self, db: Session, breed_ids: List[dict], nursery_id: int, commit=True) -> Any:
        if breed_ids:
            create_objs = []
            for new_rel_entry in breed_ids:
                rnbb_create = Rel_nurseries_breeding_breed_create(breed_id=new_rel_entry['breed_id'],
                                                                  nursery_id=new_rel_entry['nursery_id'])
                obj = self.model(**rnbb_create.dict())
                create_objs.append(obj)

            db.add_all(create_objs)
            if commit:
                db.commit()
            return create_objs

    def delete_by_breed_ids(
            self, db: Session, breed_ids: List[int], nursery_id: int, commit=True) -> Any:
        if breed_ids:
            query_for_remove_rel = db.query(self.model)\
                .filter(self.model.breed_id.in_(breed_ids),
                        self.model.nursery_id == nursery_id)
            removed = query_for_remove_rel.delete()
            if commit:
                db.commit()
            return removed


rel_nurseries_breeding_breeds = CRUDRel_nurseries_breeding_breeds(
    Rel_nurseries_breeding_breeds)
