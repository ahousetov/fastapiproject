from typing import Any, Optional
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase
from app.models.relations.rel_national_breed_clubs_social_networks import Rel_national_breed_clubs_social_networks
from app.schemas.relations.rel_national_breed_clubs_social_networks import \
    Rel_national_breed_clubs_social_networks_create
from app.schemas.relations.rel_national_breed_clubs_social_networks import \
    Rel_national_breed_clubs_social_networks_update


class CRUDRel_national_breed_clubs_social_networks(
    CRUDBase[Rel_national_breed_clubs_social_networks,
             Rel_national_breed_clubs_social_networks_create,
             Rel_national_breed_clubs_social_networks_update]):
    def get(self, db: Session, id: Any, **
            kwargs) -> Optional[Rel_national_breed_clubs_social_networks]:
        return db.query(self.model).filter(self.model.id == id).first()


rel_national_breed_clubs_social_networks = CRUDRel_national_breed_clubs_social_networks(
    Rel_national_breed_clubs_social_networks)
