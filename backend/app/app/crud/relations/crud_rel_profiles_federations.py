from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

# FastAPI
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder

# SQLAlchemy
from sqlalchemy import func, select
from sqlalchemy.dialects.postgresql import aggregate_order_by
from sqlalchemy.orm import Session
from sqlalchemy.sql import expression

# CRUDs
from app.crud.base import CRUDBase, Contact_types, ModelType, UpdateSchemaType

# Models
from app.db.base_class import Base
from app.models.relations.rel_profiles_federations import Rel_profiles_federations
from app.models.catalogs.federations import Federations
from app.models.public.profiles import Profiles
from app.models.public.legal_informations import Legal_informations

# Schemas
from app.schemas.relations.rel_profiles_federations import \
    Rel_profiles_federations_create, \
    Rel_profiles_federations_update


class CRUDRel_profiles_federations(CRUDBase[Rel_profiles_federations,
                                            Rel_profiles_federations_create,
                                            Rel_profiles_federations_update]):

    def get(self, db: Session, id: Any) -> Optional[Rel_profiles_federations]:
        return db.query(self.model).filter(self.model.id == id).first()

    def update(
        self,
        db: Session,
        db_obj: Rel_profiles_federations,
        obj_in: Union[Rel_profiles_federations_update, Dict[str, Any]],
        commit: bool = True
    ) -> Rel_profiles_federations:
        if db_obj is not None:
            db_obj_dict = self._orm_obj_dict(db_obj)
            upd_data = obj_in if isinstance(
                obj_in, dict) else obj_in.dict(
                exclude_unset=True)

            for field in db_obj_dict:
                if field in upd_data:
                    if upd_data[field]:
                        setattr(db_obj, field, upd_data[field])
            db.add(db_obj)
            if commit:
                db.commit()
                db.refresh(db_obj)
            return db_obj

    def sub_phones(self, db: Session, profile_id: int = 0) -> Any:
        return db.query(self.model.profile_id,
                        Federations.type_id,
                        func.string_agg(Federations.value, ', '))\
            .join(Federations)\
            .group_by(self.model.profile_id, Federations.type_id)\
            .where(self.model.profile_id == profile_id,
                   Federations.type_id == Contact_types.Phone)

    def sub_emails(self, db: Session, profile_id: int = 0) -> Any:
        return db.query(self.model.profile_id,
                        Federations.type_id,
                        func.string_agg(Federations.value, ', '))\
            .join(Federations)\
            .group_by(self.model.profile_id, Federations.type_id)\
            .where(self.model.profile_id == profile_id,
                   Federations.type_id == Contact_types.EMail)

    def create_not_deleted(self,
                           db: Session,
                           obj_in: Rel_profiles_federations_create = None,
                           obj_in_encoded: dict = None) -> Rel_profiles_federations:
        if (not obj_in) and (not obj_in_encoded):
            raise HTTPException(
                status_code=404,
                detail="Данных для создания новых пород не передано в 'create_not_deleted'",
                headers={"X-Error": "There goes my error"}
            )
        obj_in_data = obj_in if obj_in else obj_in_encoded
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_all_federations(self,
                            db: Session,
                            profile_id: int = None,
                            except_contact_id: int = -1):
        if not profile_id:
            return None
        sub = db.query(self.model.contact_id)\
            .join(Federations)\
            .where(self.model.profile_id == profile_id,
                   self.model.contact_id != except_contact_id,
                   Federations.is_main == True)
        sub = sub.first()[0]
        return sub

    def get_legal_info(self,
                       db: Session,
                       profile_id: int = None,
                       as_object: bool = True) -> Optional[Rel_profiles_federations]:
        if not profile_id:
            return None
        if not as_object:
            return None
        rel_legal_info = db.query(self.model)\
            .select_from(Profiles)\
            .join(self.model)\
            .join(Legal_informations)\
            .where(Profiles.id == profile_id)
        rel_legal_info = rel_legal_info.first()
        return rel_legal_info

    def delete_by_federation_ids(self, db: Session, federation_ids: List[int]) -> Any:
        query_for_remove_rel = db.query(self.model).filter(
            self.model.federation_id.in_(federation_ids))
        removed = query_for_remove_rel.delete()
        db.commit()
        return removed


rel_profiles_federations = CRUDRel_profiles_federations(Rel_profiles_federations)
