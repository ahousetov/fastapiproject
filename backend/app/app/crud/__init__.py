# For a new basic set of CRUD operations you could just do

# from .base import CRUDBase
# from app.models.item import Item
# from app.schemas.item import ItemCreate, ItemUpdate
# item = CRUDBase[Item, ItemCreate, ItemUpdate](Item)
from enum import Enum
from .accesses import *
from .dogs import *
from .catalogs import *
from .public import legal_informations, contacts, profiles, club_address, users, clubs_crud_explicit
from .relations import *
from .clubs import *
from .requests import *
from .declarants import *
from .nurseries import *
from . import documents


from .crud_user import User, UserCreate, UserUpdate
from .crud_item import Item, ItemCreate, ItemUpdate
