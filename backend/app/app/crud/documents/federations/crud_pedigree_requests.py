from sqlalchemy import exc, text, and_, or_, func, alias, case, extract, asc, desc
from sqlalchemy.orm import Session, aliased
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app import schemas
from app.crud.base import ModelType, CRUDBase
from app.models.requests.pedigree_requests import Pedigree_requests
from app.schemas.requests import Pedigree_requests_create, Pedigree_requests_update
from app.models.requests.pedigree_request_statuses import Pedigree_request_statuses
from app.models.requests.pedigree_header_declarant_requests import Pedigree_header_declarant_requests
from app.models.requests.pedigree_declarant_requests_histories import Pedigree_declarant_requests_histories
from app.models.requests.pedigree_declarant_requests import Pedigree_declarant_requests
from app.models.public.profiles import Profiles
from app.models.public.legal_informations import Legal_informations
from app.models.catalogs.federations import Federations
from app.models.declarants.declarants import Declarants


class CRUDPedigree_requests(
        CRUDBase[Pedigree_requests, Pedigree_requests_create, Pedigree_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_requests, self).__init__(model)

        # short table and subquery names
        self.hdr = 'hdr'
        self.req = 'req'
        self.hreq = 'hreq'
        self.hist = 'hist'
        self.rstat = 'rstat'
        self.dcl = 'dcl'
        self.fd = 'fd'
        self.pf = 'pf'
        self.li = 'li'
        self.dreq = 'dreq'
        self.inwork = 'inwork'
        self.isexp = 'isexp'
        self.rejected = 'rejected'
        self.finished = 'finished'

        # aliased models
        self.a_hdr = aliased(Pedigree_header_declarant_requests, name=self.hdr)
        self.a_req = aliased(Pedigree_declarant_requests, name=self.req)
        self.a_hreq = aliased(self.model, name=self.hreq)
        self.a_hist = aliased(Pedigree_declarant_requests_histories, name=self.hist)
        self.a_rstat = aliased(Pedigree_request_statuses, name=self.rstat)
        self.a_dcl = aliased(Declarants, name=self.dcl)
        self.a_fd = aliased(Federations, name=self.fd)
        self.a_pf = aliased(Profiles, name=self.pf)
        self.a_li = aliased(Legal_informations, name=self.li)

        # short label names
        self.value = 'value'    # AGG
        # pedigree_declarant_requests_histories.status_id from first subquery
        self.rej_count = 'rej_count'
        self.profile_id = 'profile_id'  # declarants.club_id from rejected subquery
        self.status_name = 'status_name'    # AGG
        self.author_name = 'author_name'    # AGG
        self.federation_name = 'federation_name'    # federation.short_name
        self.club_name = 'club_name'    # AGG
        self.declarants_count = 'declarants_count'  # AGG
        self.inwork_count = 'inwork_count'  # AGG
        self.express_count = 'express_count'    # AGG
        self.rejected_count = 'rejected_count'  # AGG
        self.finished_count = 'finished_count'  # AGG
        self.date_change = 'date_change'    # AGG
        self.sender_type = 'sender_type'    # AGG
        self.sort_order = 'sort_order'  # AGG

    def get_all(
        self,
        db: Session,
        *,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None,
        is_deleted: bool = False
    ) -> List[dict]:

        # dreq, inwork, isexp subqueries
        subq = db.query(self.a_hreq.id, func.count(self.a_hdr.id).label(self.value)) \
            .join(self.a_req, self.a_req.pedigree_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_hreq, self.a_hreq.id == self.a_hdr.pedigree_request_id)

        dreq = subq.filter(and_(self.a_hdr.is_deleted == False, self.a_hdr.status_id.notin_([7, 8]))) \
            .group_by(self.a_hreq.id)
        inwork = subq.filter(and_(self.a_hdr.is_deleted == False, self.a_hdr.status_id.in_([1]))) \
            .group_by(self.a_hreq.id)
        isexp = subq.filter(and_(self.a_hdr.is_deleted == False, self.a_req.express == True))\
            .group_by(self.a_hreq.id)

        dreq = aliased(dreq.subquery(), name=self.dreq)
        inwork = aliased(inwork.subquery(), name=self.inwork)
        isexp = aliased(isexp.subquery(), name=self.isexp)
        #

        # rejected subquery
        subq = db.query(self.a_hreq.id, self.a_hdr.status_id, self.a_hist.status_id.label(self.rej_count)) \
            .join(self.a_req, self.a_req.pedigree_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_hreq, self.a_hreq.id == self.a_hdr.pedigree_request_id) \
            .join(self.a_hist, self.a_hist.pedigree_header_declarant_request_id == self.a_hdr.id) \
            .filter(and_(self.a_hdr.is_deleted == False,
                         self.a_hist.status_id.in_([2, 4, 11]),
                         self.a_hist.is_deleted == False)) \
            .subquery()

        rejected = db.query(subq.c.id, func.count(subq.c.rej_count).label(self.value)) \
            .filter(subq.c.status_id.in_([1])) \
            .group_by(subq.c.id) \
            .subquery()

        rejected = aliased(rejected, name=self.rejected)
        #

        # finished subquery
        finished = db.query(self.a_hreq.id, func.count(self.a_hdr.id).label(self.value)) \
            .join(self.a_req, self.a_req.pedigree_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_hreq, and_(self.a_hreq.id == self.a_hdr.pedigree_request_id,
                                    self.a_hreq.is_deleted == False)) \
            .filter(and_(self.a_hdr.is_deleted == False, self.a_hdr.status_id.in_([4, 6, 10]))) \
            .group_by(self.a_hreq.id) \
            .subquery()
        finished = aliased(finished, name=self.finished)
        #

        # First subquery
        # SELECT
        first_subquery = db.query(
            self.a_hreq.id,
            self.a_hreq.date_archive,
            self.a_hreq.date_change,
            self.a_dcl.club_id.label(self.profile_id),
            self.a_pf.profile_type_id,
            self.a_hreq.date_create,
            self.a_hreq.status_id,
            self.a_rstat.name.label(self.status_name),
            func.concat(
                self.a_dcl.last_name,
                ' ',
                self.a_dcl.first_name,
                ' ',
                self.a_dcl.second_name)
            .label(self.author_name),
            self.a_fd.short_name.label(self.federation_name),
            self.a_li.folder_number,
            func.coalesce(self.a_li.short_name, self.a_li.name).label(self.club_name),
            self.a_hreq.rkf_payment_date,
            self.a_hreq.rkf_payment_number,
            func.coalesce(dreq.c.value, 0).label(self.declarants_count),
            func.coalesce(inwork.c.value, 0).label(self.inwork_count),
            func.coalesce(isexp.c.value, 0).label(self.express_count),
            func.coalesce(rejected.c.value, 0).label(self.rejected_count),
            func.coalesce(finished.c.value, 0).label(self.finished_count)
        )

        # JOIN
        first_subquery = first_subquery \
            .join(self.a_rstat, self.a_rstat.id == self.a_hreq.status_id) \
            .join(self.a_dcl, self.a_dcl.id == self.a_hreq.declarant_id) \
            .join(self.a_fd, self.a_fd.id == self.a_hreq.federation_id) \
            .join(self.a_pf, self.a_pf.id == self.a_dcl.club_id) \
            .join(self.a_li, self.a_li.profile_id == self.a_pf.id) \
            .outerjoin(dreq, dreq.c.id == self.a_hreq.id) \
            .outerjoin(inwork, inwork.c.id == self.a_hreq.id) \
            .outerjoin(isexp, isexp.c.id == self.a_hreq.id) \
            .outerjoin(rejected, rejected.c.id == self.a_hreq.id) \
            .outerjoin(finished, finished.c.id == self.a_hreq.id)

        # WHERE
        first_subquery = first_subquery \
            .filter(and_(self.a_hreq.is_deleted == False, self.a_hreq.status_id.notin_([7, 8]))) \
            .subquery()
        # /First subquery

        # Second subquery
        # SELECT
        second_subquery = db.query(
            first_subquery.c.id,
            first_subquery.c.profile_id,
            first_subquery.c.profile_type_id,
            first_subquery.c.date_create,
            func.coalesce(first_subquery.c.date_archive, first_subquery.c.date_change)
                .label(self.date_change),
            first_subquery.c.status_id,
            case(
                [
                    ((and_(first_subquery.c.status_id.in_(
                        [3]), first_subquery.c.inwork_count > 0)), 'В работе'),
                    ((first_subquery.c.date_archive is not None), func.concat(
                        'Архив(', first_subquery.c.status_name, ')')),
                ],
                else_=first_subquery.c.status_name
            ).label(self.status_name),
            first_subquery.c.author_name,
            first_subquery.c.federation_name,
            first_subquery.c.folder_number,
            case(
                [
                    (first_subquery.c.profile_type_id == 3, 'Клуб'),
                    (first_subquery.c.profile_type_id == 4, 'Питомник'),
                ]
            ).label(self.sender_type),
            first_subquery.c.club_name,
            first_subquery.c.rkf_payment_date,
            first_subquery.c.rkf_payment_number,
            first_subquery.c.declarants_count,
            first_subquery.c.inwork_count,
            first_subquery.c.express_count,
            first_subquery.c.rejected_count,
            case(
                [
                    (first_subquery.c.date_archive is not None,
                     99),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.express_count > 0,
                          first_subquery.c.inwork_count > 0,
                          first_subquery.c.rejected_count > 0),
                     0),
                    (and_(first_subquery.c.status_id.in_([1]),
                          first_subquery.c.express_count > 0),
                     1),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.inwork_count > 0,
                          first_subquery.c.rejected_count > 0),
                     2),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.inwork_count > 0,
                          first_subquery.c.rejected_count == 0),
                     3),
                    (and_(first_subquery.c.status_id.in_([1]),
                          first_subquery.c.express_count == 0),
                     4),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.finished_count == first_subquery.c.declarants_count),
                     6),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.inwork_count == 0),
                     5),
                    (first_subquery.c.status_id.in_([2]),
                     7)
                ],
                else_=8
            ).label(self.sort_order)
        ).subquery()
        # /Second subquery

        # Query
        query = db.query(second_subquery) \
            .order_by(
                second_subquery.c.sort_order.asc(),
                case([(second_subquery.c.sort_order.in_([0, 1, 2, 3, 4]),
                     second_subquery.c.date_create)]).asc(),
                case([(second_subquery.c.sort_order.in_([5, 6, 7, 8]),
                     second_subquery.c.date_create)]).desc(),
                case([(second_subquery.c.sort_order.in_([99]),
                     second_subquery.c.status_id)]).asc()
        )
        #

        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        query = query.offset(skip).limit(limit)

        # # Пользовательская сортировка по полю
        # # Получаем поле (объект) из подзапроса по имени поля которое ввел пользователь
        # sort_field = getattr(second_subquery.c, sort_field, None)
        # if sort_field is not None:
        #     user_sort = asc(sort_field) if sort_direction == 'asc' else desc(sort_field)
        #     query = query.order_by(user_sort)
        #
        # query = query.offset(skip).limit(limit)
        # /Query

        result = query.all()

        return [dict(each) for each in result]


pedigree_requests = CRUDPedigree_requests(Pedigree_requests)
