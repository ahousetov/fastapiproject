from sqlalchemy import exc, text, and_, or_, func, alias, case, extract, asc, desc
from sqlalchemy.orm import Session, aliased
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app import schemas
from app.crud.base import ModelType, CRUDBase
from app.models.requests.litter_requests import Litter_requests
from app.schemas.requests import Litter_requests_create, Litter_requests_update
from app.models.requests.litter_declarant_requests import Litter_declarant_requests
from app.models.requests.litter_declarant_requests_histories import Litter_declarant_requests_histories
from app.models.requests.litter_header_declarant_requests import Litter_header_declarant_requests
from app.models.requests.litter_requests import Litter_requests
from app.models.requests.request_statuses import Request_statuses
from app.models.declarants.declarants import Declarants
from app.models.catalogs.federations import Federations
from app.models.public.profiles import Profiles
from app.models.public.legal_informations import Legal_informations


class CRUDLitter_requests(CRUDBase[Litter_requests,
                                   Litter_requests_create,
                                   Litter_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDLitter_requests, self).__init__(model)

        # short table and subquery names
        self.req = 'req'
        self.rstat = 'rstat'
        self.dcl = 'dcl'
        self.fd = 'fd'
        self.pf = 'pf'
        self.li = 'li'
        self.hdr = 'hdr'
        self.dreq = 'dreq'
        self.hist = 'hist'
        self.requests = 'requests'
        self.inwork = 'inwork'
        self.rejected = 'rejected'
        self.after_violated = 'after_violated'

        # aliased models
        self.a_req = aliased(Litter_requests, name=self.req)
        self.a_rstat = aliased(Request_statuses, name=self.rstat)
        self.a_dcl = aliased(Declarants, name=self.dcl)
        self.a_fd = aliased(Federations, name=self.fd)
        self.a_pf = aliased(Profiles, name=self.pf)
        self.a_li = aliased(Legal_informations, name=self.li)
        self.a_hdr = aliased(Litter_header_declarant_requests, name=self.hdr)
        self.a_dreq = aliased(Litter_declarant_requests, name=self.dreq)
        self.a_hist = aliased(Litter_declarant_requests_histories, name=self.hist)

        # short label names
        self.id = 'id'
        self.request_count = 'request_count'
        self.express_count = 'express_count'
        self.value = 'value'
        self.rej_count = 'rej_count'
        self.lvl = 'lvl'
        self.prev_status_id = 'prev_status_id'
        self.profile_id = 'profile_id'
        self.status_name = 'status_name'
        self.author_name = 'author_name'
        self.federation_id = 'federation_id'
        self.federation_name = 'federation_name'
        self.club_name = 'club_name'
        self.requests_count = 'requests_count'
        self.inwork_count = 'inwork_count'
        self.rejected_count = 'rejected_count'
        self.is_express_contain = 'is_express_contain'
        self.is_after_violated = 'is_after_violated'
        self.date_change = 'date_change'
        self.sender_type = 'sender_type'
        self.sort_order = 'sort_order'

    def get_all(
        self,
        db: Session,
        *,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None,
        is_deleted: bool = False
    ) -> List[dict]:

        # requests subquery
        requests = db.query(self.a_req.id.label(self.id),
                            func.count(self.a_hdr.id).label(self.request_count),
                            func.sum(
                                case([(self.a_dreq.express == True, 1)], else_=0)
        ).label(self.express_count)) \
            .join(self.a_dreq, self.a_dreq.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_req, self.a_req.id == self.a_hdr.litter_request_id) \
            .filter(and_(self.a_hdr.is_deleted == False, self.a_hdr.status_id.notin_([6, 7]))) \
            .group_by(self.a_req.id) \
            .subquery()
        requests = aliased(requests, name=self.requests)
        #

        # inwork subquery
        inwork = db.query(self.a_req.id, func.count(self.a_hdr.id).label(self.value)) \
            .join(self.a_dreq, self.a_dreq.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_req, self.a_req.id == self.a_hdr.litter_request_id) \
            .filter(and_(self.a_hdr.is_deleted == False, self.a_hdr.status_id.in_([1]))) \
            .group_by(self.a_req.id) \
            .subquery()
        inwork = aliased(inwork, name=self.inwork)
        #

        # rejected subquery
        subq = db.query(self.a_req.id, self.a_hdr.status_id, self.a_hist.status_id.label(self.rej_count)) \
            .join(self.a_dreq, self.a_dreq.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_req, self.a_req.id == self.a_hdr.litter_request_id) \
            .join(self.a_hist, self.a_hist.litter_header_declarant_request_id == self.a_hdr.id) \
            .filter(and_(self.a_hdr.is_deleted == False,
                         self.a_hist.status_id.in_([2, 4, 8]),
                         self.a_hist.is_deleted == False)) \
            .subquery()

        rejected = db.query(subq.c.id, func.count(subq.c.rej_count).label(self.value)) \
            .filter(subq.c.status_id.in_([1])) \
            .group_by(subq.c.id) \
            .subquery()
        rejected = aliased(rejected, name=self.rejected)
        #

        # after_violated subquery
        subq = db.query(self.a_req.id,
                        self.a_hist.status_id,
                        func.rank().over(
                            partition_by=self.a_hdr.id,
                            order_by=self.a_hist.date_create.desc()
                        ).label(self.lvl)) \
            .join(self.a_dreq, self.a_dreq.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_hist, self.a_hist.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_req, self.a_req.id == self.a_hdr.litter_request_id) \
            .filter(and_(self.a_hdr.is_deleted == False, self.a_hdr.status_id == 1)) \
            .order_by(self.a_req.id, self.a_hdr.id, self.a_hist.date_create.desc()) \
            .subquery()

        after_violated = db.query(subq.c.id, subq.c.status_id.label(self.prev_status_id)) \
            .filter(and_(subq.c.lvl == 2, subq.c.status_id == 9)) \
            .group_by(subq.c.id, subq.c.status_id) \
            .subquery()
        after_violated = aliased(after_violated, name=self.after_violated)
        #

        # First subquery
        # SELECT
        first_subquery = db.query(
            self.a_req.id,
            self.a_req.date_archive,
            self.a_dcl.club_id.label(self.profile_id),
            self.a_pf.profile_type_id,
            self.a_req.date_create,
            self.a_req.date_change,
            self.a_req.status_id,
            self.a_rstat.name.label(self.status_name),
            func.concat(
                self.a_dcl.last_name, ' ', self.a_dcl.first_name, ' ', self.a_dcl.second_name
            ).label(self.author_name),
            self.a_fd.id.label(self.federation_id),
            self.a_fd.short_name.label(self.federation_name),
            self.a_li.folder_number,
            func.coalesce(self.a_li.short_name, self.a_li.name).label(self.club_name),
            self.a_req.rkf_payment_date,
            self.a_req.rkf_payment_number,
            func.coalesce(requests.c.request_count, 0).label(self.requests_count),
            func.coalesce(inwork.c.value, 0).label(self.inwork_count),
            func.coalesce(rejected.c.value, 0).label(self.rejected_count),
            case([(requests.c.express_count > 0, True)],
                 else_=False).label(self.is_express_contain),
            case([(after_violated.c.id is None, False)],
                 else_=True).label(self.is_after_violated)
        )

        # JOIN
        first_subquery = first_subquery \
            .join(self.a_rstat, self.a_rstat.id == self.a_req.status_id) \
            .join(self.a_dcl, self.a_dcl.id == self.a_req.declarant_id) \
            .join(self.a_fd, self.a_fd.id == self.a_req.federation_id) \
            .join(self.a_pf, self.a_pf.id == self.a_dcl.club_id) \
            .join(self.a_li, self.a_li.profile_id == self.a_pf.id) \
            .outerjoin(requests, requests.c.id == self.a_req.id) \
            .outerjoin(inwork, inwork.c.id == self.a_req.id) \
            .outerjoin(rejected, rejected.c.id == self.a_req.id) \
            .outerjoin(after_violated, after_violated.c.id == self.a_req.id)

        # WHERE
        first_subquery = first_subquery \
            .filter(and_(self.a_req.is_deleted == False, self.a_req.status_id.notin_([6, 7]))) \
            .subquery()

        # /First subquery

        # Second subquery
        # SELECT
        second_subquery = db.query(
            first_subquery.c.date_archive,
            first_subquery.c.id,
            first_subquery.c.profile_id,
            first_subquery.c.profile_type_id,
            first_subquery.c.date_create,
            func.coalesce(first_subquery.c.date_archive, first_subquery.c.date_change)
                .label(self.date_change),
            first_subquery.c.status_id,
            case(
                [
                    (and_(first_subquery.c.status_id == 3,
                     first_subquery.c.inwork_count > 0), 'В работе'),
                    (first_subquery.c.date_archive is not None, func.concat(
                        'Архив(', first_subquery.c.status_name, ')'))
                ],
                else_=first_subquery.c.status_name
            ).label(self.status_name),
            first_subquery.c.author_name,
            first_subquery.c.federation_id,
            first_subquery.c.federation_name,
            first_subquery.c.folder_number,
            case(
                [
                    (first_subquery.c.profile_type_id == 3, 'Клуб'),
                    (first_subquery.c.profile_type_id == 4, 'Питомник')
                ]
            ).label(self.sender_type),
            first_subquery.c.club_name,
            first_subquery.c.rkf_payment_date,
            first_subquery.c.rkf_payment_number,
            first_subquery.c.requests_count,
            first_subquery.c.inwork_count,
            first_subquery.c.rejected_count,
            first_subquery.c.is_express_contain,
            first_subquery.c.is_after_violated,
            case(
                [
                    (first_subquery.c.date_archive is not None,
                     99),
                    (and_(first_subquery.c.status_id == 3,
                          first_subquery.c.is_express_contain == True,
                          first_subquery.c.inwork_count > 0,
                          first_subquery.c.rejected_count > 0),
                     1),
                    (and_(first_subquery.c.status_id == 3,
                          first_subquery.c.is_express_contain == True,
                          first_subquery.c.inwork_count > 0),
                     2),
                    (and_(first_subquery.c.status_id == 1,
                          first_subquery.c.is_express_contain == True),
                     3),
                    (and_(first_subquery.c.status_id == 3,
                          first_subquery.c.is_after_violated == True),
                     4),
                    (and_(first_subquery.c.status_id == 3,
                          first_subquery.c.inwork_count > 0,
                          first_subquery.c.rejected_count > 0),
                     5),
                    (and_(first_subquery.c.status_id == 3,
                          first_subquery.c.inwork_count > 0),
                     6),
                    (first_subquery.c.status_id == 1,
                     7),
                    (first_subquery.c.status_id == 3,
                     8),
                    (first_subquery.c.status_id == 2,
                     9)
                ],
                else_=10
            ).label(self.sort_order)
        ).subquery()
        # /Second subquery

        # Query
        query = db.query(second_subquery).order_by(
            second_subquery.c.sort_order,
            case([(second_subquery.c.date_archive is not None, second_subquery.c.status_id)]),
            second_subquery.c.date_create
        )

        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        query = query.offset(skip).limit(limit)

        # # Пользовательская сортировка по полю
        # # Получаем поле (объект) из подзапроса по имени поля которое ввел пользователь
        # sort_field = getattr(second_subquery.c, sort_field, None)
        # if sort_field is not None:
        #     user_sort = asc(sort_field) if sort_direction == 'asc' else desc(sort_field)
        #     query = query.order_by(user_sort)
        #
        # query = query.offset(skip).limit(limit)
        # /Query

        result = query.all()

        return [dict(each) for each in result]


litter_requests = CRUDLitter_requests(Litter_requests)
