import datetime
from typing import Any, List, Optional, Dict, Type, Union
from sqlalchemy import exc, text, and_, or_, func, alias, case, extract, asc, desc
from sqlalchemy.orm import Session, aliased
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app import schemas
from app.crud.base import ModelType, CRUDBase
from app.models.requests.pedigree_header_declarant_requests import Pedigree_header_declarant_requests
from app.schemas.requests import Pedigree_header_declarant_requests_base,\
    Pedigree_header_declarant_requests_create, \
    Pedigree_header_declarant_requests_update, \
    Pedigree_header_declarant_requests_delete
from app.models.requests.pedigree_request_statuses import Pedigree_request_statuses
from app.models.requests.pedigree_requests import Pedigree_requests
from app.models.requests.pedigree_declarant_documents import Pedigree_declarant_documents
from app.models.requests.pedigree_declarant_requests_histories import Pedigree_declarant_requests_histories
from app.models.requests.pedigree_declarant_requests import Pedigree_declarant_requests
from app.models.public.profiles import Profiles
from app.models.public.legal_informations import Legal_informations
from app.models.public.stamp_codes import Stamp_codes
from app.models.dogs.breeds import Breeds
from app.models.catalogs.federations import Federations
from app.models.catalogs.weekends_list import Weekends_list
from app.models.declarants.declarants import Declarants


class CRUDPedigree_header_declarant_requests(CRUDBase[Pedigree_header_declarant_requests,
                                                      Pedigree_header_declarant_requests_create,
                                                      Pedigree_header_declarant_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_header_declarant_requests, self).__init__(model)

        # short table names
        self.hdr = 'hdr'
        self.req = 'req'
        self.rstat = 'rstat'
        self.prev_stat = 'prev_stat'
        self.docs = 'docs'
        self.hreq = 'hreq'
        self.dcl = 'dcl'
        self.br = 'br'
        self.fd = 'fd'
        self.pf = 'pf'
        self.li = 'li'
        self.scode = 'scode'
        self.date_first = 'date_first'
        self.hist = 'hist'
        self.returned = 'returned'
        self.wl = 'wl'

        # aliased models
        self.a_hdr = aliased(self.model, name=self.hdr)
        self.a_req = aliased(Pedigree_declarant_requests, name=self.req)
        self.a_rstat = aliased(Pedigree_request_statuses, name=self.rstat)
        self.a_prev_stat = aliased(Pedigree_request_statuses, name=self.prev_stat)
        self.a_docs = aliased(Pedigree_declarant_documents, name=self.docs)
        self.a_hreq = aliased(Pedigree_requests, name=self.hreq)
        self.a_dcl = aliased(Declarants, name=self.dcl)
        self.a_br = aliased(Breeds, name=self.br)
        self.a_fd = aliased(Federations, name=self.fd)
        self.a_pf = aliased(Profiles, name=self.pf)
        self.a_li = aliased(Legal_informations, name=self.li)
        self.a_scode = aliased(Stamp_codes, name=self.scode)
        self.a_hist = aliased(Pedigree_declarant_requests_histories, name=self.hist)
        self.a_wl = aliased(Weekends_list, name=self.wl)

        # short label names
        self.value = 'value'
        self.id = 'id'
        self.date_first_field = 'date_first'
        self.weekend_days_count = 'weekend_days_count'
        self.is_weekend_count = 'is_weekend_count'
        self.date_last = 'date_last'
        self.req_id = 'req_id'
        self.status_name = 'status_name'
        self.federation_name = 'federation_name'
        self.breed_name = 'breed_name'
        self.days_passed = 'days_passed'
        self.club_name = 'club_name'
        self.name = 'name'
        self.stamp_code = 'stamp_code'
        self.documents_count = 'documents_count'
        self.return_count = 'return_count'
        self.sender_type = 'sender_type'
        self.is_weekend = 'is_weekend'
        self.sort_order = 'sort_order'

    def get_all(
        self,
        db: Session,
        *,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None,
        is_deleted: bool = False
    ) -> List[dict]:
        # date first subquery
        # Создаем поля с кастомными именами (..._id с as id)
        id = Pedigree_declarant_requests_histories.pedigree_header_declarant_request_id.label(
            self.id)
        date_first = Pedigree_declarant_requests_histories.date_create.label(
            self.date_first_field)
        # Подзапрос c новыми полями
        date_first = db.query(id, date_first) \
            .filter(and_(Pedigree_declarant_requests_histories.is_deleted == False),
                    Pedigree_declarant_requests_histories.status_id.in_([3])) \
            .subquery()
        # подзапрос будет с кастомным именем
        date_first = aliased(date_first, name=self.date_first)
        #

        # returned subquery
        value = func.count(self.a_hist.id).label(self.value)
        returned = db.query(self.a_hdr.id, value)
        returned = returned\
            .join(self.a_hist, self.a_hist.pedigree_header_declarant_request_id == self.a_hdr.id) \
            .filter(and_(self.a_hdr.is_deleted == False,
                         self.a_hist.is_deleted == False,
                         self.a_hist.status_id.in_([4, 11]))) \
            .group_by(self.a_hdr.id) \
            .subquery()
        returned = aliased(returned, name=self.returned)
        #

        # weekend_days_count label
        weekend_days_count = db.query(func.count(self.a_wl.weekend_date)).filter(
            and_(self.a_wl.is_deleted == False,
                 self.a_wl.weekend_date.between(func.max(date_first.c.date_first),
                                                datetime.datetime.now())
                 )
        ).label(self.weekend_days_count)
        #

        # is_weekend_count label
        is_weekend_count = db.query(func.count(self.a_wl.id)).filter(
            and_(self.a_wl.is_deleted == False,
                 func.date(func.max(date_first.c.date_first)) == self.a_wl.weekend_date
                 )
        ).label(self.is_weekend_count)
        #

        # first subquery (first всмысле главный самый внутренний)
        # SELECT
        first_subquery = db.query(
            self.a_hdr.id,
            self.a_hdr.date_create,
            self.a_hdr.status_id,
            self.a_hdr.barcode,
            self.a_hdr.date_change.label(self.date_last),
            self.a_req.id.label(self.req_id),
            self.a_req.email,
            self.a_req.express,
            self.a_pf.profile_type_id,
            case(
                (self.a_hdr.status_id == 12, func.concat(
                    'Архив (', self.a_prev_stat.name, ')')),
                else_=self.a_rstat.name
            ).label(self.status_name),
            self.a_fd.short_name.label(self.federation_name),
            self.a_li.folder_number,
            self.a_br.name_rus.label(self.breed_name),
            func.min(date_first.c.date_first).label(self.date_first_field),
            extract('day',
                    datetime.datetime.now() - func.max(date_first.c.date_first)
                    ).label(self.days_passed),
            weekend_days_count,
            is_weekend_count,
            func.coalesce(self.a_li.short_name, self.a_li.name).label(self.club_name),
            func.concat(
                self.a_req.owner_last_name, ' ', self.a_req.owner_first_name, ' ', self.a_req.owner_second_name
            ).label(self.name),
            func.concat(
                self.a_scode.stamp_code,
                ' ',
                self.a_req.stamp_number).label(
                self.stamp_code),
            (func.count(self.a_docs.id) + 2).label(self.documents_count),
            func.coalesce(returned.c.value, 0).label(self.return_count)
        )

        # JOIN
        first_subquery = first_subquery \
            .join(self.a_req, self.a_req.pedigree_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_rstat, self.a_rstat.id == self.a_hdr.status_id) \
            .join(self.a_prev_stat, self.a_prev_stat.id == self.a_hdr.prev_status_id, isouter=True) \
            .join(self.a_docs,
                  and_(self.a_docs.pedigree_header_declarant_request_id == self.a_hdr.id,
                       self.a_docs.is_deleted == False),
                  isouter=True) \
            .join(self.a_hreq, self.a_hreq.id == self.a_hdr.pedigree_request_id) \
            .join(self.a_dcl, self.a_dcl.id == self.a_hreq.declarant_id) \
            .join(self.a_br, self.a_br.id == self.a_req.breed_id, isouter=True) \
            .join(self.a_fd, self.a_fd.id == self.a_hreq.federation_id) \
            .join(self.a_pf, self.a_pf.id == self.a_dcl.club_id) \
            .join(self.a_li, self.a_li.profile_id == self.a_pf.id) \
            .join(self.a_scode, self.a_scode.id == self.a_req.stamp_code_id, isouter=True) \
            .join(date_first, date_first.c.id == self.a_hdr.id, isouter=True) \
            .join(returned, returned.c.id == self.a_hdr.id, isouter=True)

        # WHERE
        first_subquery = first_subquery.filter(
            and_(
                self.a_hdr.is_deleted == False,
                self.a_hdr.status_id.in_([3, 4, 5, 6, 9, 10, 11]),
                or_(
                    self.a_hdr.prev_status_id is None,
                    self.a_hdr.prev_status_id.in_([3, 4, 5, 6, 9, 10, 11])
                )
            )
        )
        #

        # GROUP_BY
        first_subquery = first_subquery.group_by(
            self.a_hdr.id,
            self.a_req.id,
            self.a_hdr.date_create,
            self.a_hdr.status_id,
            self.a_hdr.date_change,
            self.a_rstat.name,
            self.a_req.email,
            self.a_li.short_name,
            self.a_li.name,
            self.a_li.folder_number,
            self.a_fd.short_name,
            self.a_br.name_rus,
            self.a_req.owner_last_name,
            self.a_req.owner_first_name,
            self.a_req.owner_second_name,
            self.a_req.express,
            self.a_pf.profile_type_id,
            self.a_scode.stamp_code,
            returned.c.value,
            self.a_prev_stat.name)
        #

        # LIMIT
        first_subquery = first_subquery.limit(100)
        first_subquery = first_subquery.subquery()
        #
        # /first subquery

        # second subquery
        second_subquery = db.query(
            first_subquery,
            case(
                [
                    (first_subquery.c.profile_type_id == 3, 'Клуб'),
                    (first_subquery.c.profile_type_id == 4, 'Питомник')
                ]
            ).label(self.sender_type),
            case(
                [
                    (first_subquery.c.is_weekend_count == 0, 'Нет'),
                ],
                else_='Да'
            ).label(self.is_weekend),
            case(
                [
                    (first_subquery.c.status_id.in_([12]), 99),
                    (and_(first_subquery.c.status_id.in_(
                        [9]), first_subquery.c.express == True), 0),
                    (and_(first_subquery.c.status_id.in_(
                        [5]), first_subquery.c.express == True), 1),
                    (and_(first_subquery.c.status_id.in_(
                        [3]), first_subquery.c.express == True), 2),
                    (and_(
                        first_subquery.c.status_id.in_(
                            [3, 5, 9]),                          # When
                        first_subquery.c.express == False,
                        (first_subquery.c.days_passed -
                         first_subquery.c.weekend_days_count) >= 18,
                        first_subquery.c.return_count > 0),
                     3                                                                      # Then
                     ),
                    (and_(
                        first_subquery.c.status_id.in_([3, 5, 9]),
                        first_subquery.c.express == False,
                        (first_subquery.c.days_passed -
                         first_subquery.c.weekend_days_count) > 21,
                        first_subquery.c.return_count == 0),
                     4
                     ),
                    (and_(
                        first_subquery.c.status_id.in_([3, 5, 9]),
                        first_subquery.c.express == False,
                        (first_subquery.c.days_passed -
                         first_subquery.c.weekend_days_count).between(18, 21),
                        first_subquery.c.return_count == 0),
                     5
                     ),
                    (and_(
                        first_subquery.c.status_id.in_([3, 5, 9]),
                        first_subquery.c.express == False,
                        (first_subquery.c.days_passed -
                         first_subquery.c.weekend_days_count) < 18,
                        first_subquery.c.return_count > 0),
                     6
                     ),
                    (and_(first_subquery.c.status_id.in_(
                        [9]), first_subquery.c.express == False), 7),
                    (and_(first_subquery.c.status_id.in_(
                        [5]), first_subquery.c.express == False), 8),
                    (and_(first_subquery.c.status_id.in_(
                        [3]), first_subquery.c.express == False), 9),
                    (first_subquery.c.status_id.in_([11]), 10),
                    (first_subquery.c.status_id.in_([4]), 11),
                    (first_subquery.c.status_id.in_([6, 10]), 12),
                ],
                else_=13
            ).label(self.sort_order),
        ).subquery()
        #
        # /second subquery

        # Query
        query = db.query(second_subquery).order_by(
            second_subquery.c.sort_order.asc(),
            case(
                [
                    (second_subquery.c.sort_order.in_(
                        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]), second_subquery.c.date_first)
                ]
            ).asc(),
            case(
                [
                    (second_subquery.c.sort_order.in_(
                        [10, 11, 12]), second_subquery.c.date_first)
                ]
            ).desc()
        )

        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        query = query.offset(skip).limit(limit)

        # # Пользовательская сортировка по полю
        # # Получаем поле (объект) из подзапроса по имени поля которое ввел пользователь
        # sort_field = getattr(second_subquery.c, sort_field, None)
        # if sort_field is not None:
        #     user_sort = asc(sort_field) if sort_direction == 'asc' else desc(sort_field)
        #     query = query.order_by(user_sort)
        #
        # query = query.offset(skip).limit(limit)
        # /Query

        result = query.all()

        return [dict(each) for each in result]


pedigree_header_declarant_requests = CRUDPedigree_header_declarant_requests(
    Pedigree_header_declarant_requests)
