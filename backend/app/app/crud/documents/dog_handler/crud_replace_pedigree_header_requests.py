import copy

from sqlalchemy import exc, text, and_, or_, func, alias, case, extract, asc, desc
from sqlalchemy.orm import Session, aliased
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app import schemas
from app.schemas import Filters_model
from app.crud.base import ModelType, CRUDBase
from app.models.requests.replace_pedigree_header_requests import Replace_pedigree_header_requests
from app.schemas.requests import Replace_pedigree_header_requests_create, Replace_pedigree_header_requests_update
from app.models.requests.replace_pedigree_request_types import Replace_pedigree_request_types
from app.models.requests.replace_pedigree_export_old_requests import Replace_pedigree_export_old_requests
from app.models.requests.replace_pedigree_old_requests import Replace_pedigree_old_requests
from app.models.requests.replace_pedigree_duplicate_requests import Replace_pedigree_duplicate_requests
from app.models.requests.replace_pedigree_owner_requests import Replace_pedigree_owner_requests
from app.models.requests.replace_pedigree_out_rkf_fci_requests import Replace_pedigree_out_rkf_fci_requests
from app.models.requests.replace_pedigree_foreign_requests import Replace_pedigree_foreign_requests
from app.models.requests.replace_pedigree_declarant_error_requests import Replace_pedigree_declarant_error_requests
from app.models.requests.replace_pedigree_request_statuses import Replace_pedigree_request_statuses
from app.models.requests.replace_pedigree_requests_histories import Replace_pedigree_requests_histories
from app.models.declarants.declarants import Declarants
from app.models.public.legal_informations import Legal_informations
from app.models.public.profiles import Profiles
from app.models.public.stamp_codes import Stamp_codes
from app.models.catalogs.federations import Federations
from app.models.dogs.breeds import Breeds


class CRUDReplace_pedigree_header_requests(CRUDBase[Replace_pedigree_header_requests,
                                                    Replace_pedigree_header_requests_create,
                                                    Replace_pedigree_header_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDReplace_pedigree_header_requests, self).__init__(model)

        # short table and subquery names
        self.hdr = 'hdr'
        self.rtype = 'rtype'
        self.dcl = 'dcl'
        self.li = 'li'
        self.pf = 'pf'
        self.fd = 'fd'
        self.req_1 = 'req_1'
        self.req_2 = 'req_2'
        self.req_3 = 'req_3'
        self.req_4 = 'req_4'
        self.req_5 = 'req_5'
        self.req_6 = 'req_6'
        self.req_7 = 'req_7'
        self.hist = 'hist'
        self.rstat = 'rstat'
        self.prev_stat = 'prev_stat'
        self.brd = 'brd'
        self.scode = 'scode'

        # aliased models
        self.a_hdr = aliased(Replace_pedigree_header_requests, name=self.hdr)
        self.a_rtype = aliased(Replace_pedigree_request_types, name=self.rtype)
        self.a_dcl = aliased(Declarants, name=self.dcl)
        self.a_li = aliased(Legal_informations, name=self.li)
        self.a_pf = aliased(Profiles, name=self.pf)
        self.a_fd = aliased(Federations, name=self.fd)
        self.a_req_1 = aliased(Replace_pedigree_export_old_requests, name=self.req_1)
        self.a_req_2 = aliased(Replace_pedigree_old_requests, name=self.req_2)
        self.a_req_3 = aliased(Replace_pedigree_duplicate_requests, name=self.req_3)
        self.a_req_4 = aliased(Replace_pedigree_owner_requests, name=self.req_4)
        self.a_req_5 = aliased(Replace_pedigree_out_rkf_fci_requests, name=self.req_5)
        self.a_req_6 = aliased(Replace_pedigree_foreign_requests, name=self.req_6)
        self.a_req_7 = aliased(
            Replace_pedigree_declarant_error_requests,
            name=self.req_7)
        self.a_hist = aliased(Replace_pedigree_requests_histories, name=self.hist)
        self.a_rstat = aliased(Replace_pedigree_request_statuses, name=self.rstat)
        self.a_prev_stat = aliased(
            Replace_pedigree_request_statuses,
            name=self.prev_stat)
        self.a_brd = aliased(Breeds, name=self.brd)
        self.a_scode = aliased(Stamp_codes, name=self.scode)

        # short label names
        self.type_name = 'type_name'
        self.profile_id = 'profile_id'
        self.profile_type_id = 'profile_type_id'
        self.federation_id = 'federation_id'
        self.federation_name = 'federation_name'
        self.first_rkf_date = 'first_rkf_date'
        self.club_name = 'club_name'
        self.author_name = 'author_name'
        self.stamp_code_id = 'stamp_code_id'
        self.stamp_number = 'stamp_number'
        self.breed_id = 'breed_id'
        self.dog_name = 'dog_name'
        self.stamp_code_not_rkf = 'stamp_code_not_rkf'
        self.breed_name = 'breed_name'
        self.stamp_code = 'stamp_code'
        self.profile_type_name = 'profile_type_name'
        self.status_name = 'status_name'
        self.sort_order = 'sort_order'

    def get_all(
        self,
        db: Session,
        *,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None,
        is_deleted: bool = False
    ) -> List[dict]:
        # Subquery
        # SELECT
        subquery = db.query(
            self.a_hdr.id,
            self.a_hdr.type_id,
            self.a_hdr.status_id,
            self.a_hdr.prev_status_id,
            self.a_hdr.date_create,
            self.a_hdr.date_change,
            self.a_hdr.barcode,
            self.a_hdr.express,
            self.a_rtype.name.label(self.type_name),
            self.a_pf.id.label(self.profile_id),
            self.a_pf.profile_type_id.label(self.profile_type_id),
            self.a_fd.id.label(self.federation_id),
            func.coalesce(
                self.a_fd.short_name,
                self.a_fd.name).label(
                self.federation_name),
            self.a_li.folder_number,
            func.min(self.a_hist.date_create).label(self.first_rkf_date),
            func.concat(
                self.a_dcl.last_name,
                ' ',
                self.a_dcl.first_name).label(
                self.author_name),
            func.coalesce(self.a_li.short_name, self.a_li.name).label(self.club_name),
            func.coalesce(self.a_req_1.stamp_code_id, self.a_req_2.stamp_code_id,
                          self.a_req_3.stamp_code_id, self.a_req_4.stamp_code_id,
                          self.a_req_7.stamp_code_id
                          ).label(self.stamp_code_id),
            func.coalesce(self.a_req_1.stamp_number, self.a_req_2.stamp_number,
                          self.a_req_3.stamp_number, self.a_req_4.stamp_number,
                          self.a_req_5.stamp_number, self.a_req_6.stamp_number,
                          self.a_req_7.stamp_number
                          ).label(self.stamp_number),
            func.coalesce(self.a_req_1.breed_id, self.a_req_2.breed_id, self.a_req_3.breed_id,
                          self.a_req_4.breed_id, self.a_req_5.breed_id, self.a_req_6.breed_id,
                          self.a_req_7.breed_id
                          ).label(self.breed_id),
            func.coalesce(self.a_req_1.dog_name, self.a_req_2.dog_name, self.a_req_3.dog_name,
                          self.a_req_4.dog_name, self.a_req_5.dog_name, self.a_req_6.dog_name,
                          self.a_req_7.dog_name
                          ).label(self.dog_name),
            func.coalesce(
                self.a_req_5.stamp_code,
                self.a_req_6.stamp_code).label(
                self.stamp_code_not_rkf)
        )

        # JOIN
        subquery = subquery \
            .join(self.a_rtype, self.a_rtype.id == self.a_hdr.type_id) \
            .join(self.a_dcl, self.a_dcl.id == self.a_hdr.declarant_id) \
            .join(self.a_li, self.a_li.profile_id == self.a_dcl.club_id) \
            .join(self.a_pf, self.a_pf.id == self.a_dcl.club_id) \
            .outerjoin(self.a_fd, self.a_fd.id == self.a_hdr.federation_id) \
            .outerjoin(self.a_req_1, self.a_req_1.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_req_2, self.a_req_2.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_req_3, self.a_req_3.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_req_4, self.a_req_4.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_req_5, self.a_req_5.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_req_6, self.a_req_6.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_req_7, self.a_req_7.replace_pedigree_header_requests_id == self.a_hdr.id) \
            .outerjoin(self.a_hist, and_(self.a_hist.replace_pedigree_header_requests_id == self.a_hdr.id,
                                         self.a_hist.status_id.in_([3])))     # Отправлена в РКФ

        # WHERE
        # Custom filter in subquery
        # Нужно определить какие поля относятся к подзапросу а какие к запросу
        subquery_filters, query_filters = copy.deepcopy(filters), copy.deepcopy(filters)
        if filters:
            # subquery_filters = list(
            # filter(lambda field: field in subquery.statement.selected_columns, filters.filterModel)
            # )
            for field in filters.filterModel:
                # Интересно получилось в одну строку написать
                # del (subquery_filters.filterModel if field_key in subquery.statement.selected_columns
                # else query_filters.filterModel)[field_key]

                if field not in subquery.statement.selected_columns:
                    del subquery_filters.filterModel[field]
                else:
                    del query_filters.filterModel[field]

            # Добавляем фильтры в подзапрос только тех полей которые там есть
            subquery = self._make_filter_model_without_strings(query=subquery,
                                                               filters_model=filters)

        # GROUP_BY
        subquery = subquery.group_by(
            self.a_hdr.id, self.a_hdr.type_id, self.a_hdr.status_id, self.a_hdr.date_create, self.a_hdr.date_change,
            self.a_hdr.barcode, self.a_hdr.express, self.a_rtype.name, self.a_pf.id, self.a_pf.profile_type_id,
            self.a_fd.id, self.a_fd.short_name, self.a_fd.name, self.a_li.folder_number, self.a_dcl.last_name,
            self.a_dcl.first_name, self.a_li.short_name, self.a_li.name, self.a_req_1.stamp_code_id,
            self.a_req_2.stamp_code_id, self.a_req_3.stamp_code_id, self.a_req_4.stamp_code_id,
            self.a_req_7.stamp_code_id, self.a_req_1.stamp_number, self.a_req_2.stamp_number, self.a_req_3.stamp_number,
            self.a_req_4.stamp_number, self.a_req_5.stamp_number, self.a_req_6.stamp_number, self.a_req_7.stamp_number,
            self.a_req_1.breed_id, self.a_req_2.breed_id, self.a_req_3.breed_id, self.a_req_4.breed_id,
            self.a_req_5.breed_id, self.a_req_6.breed_id, self.a_req_7.breed_id, self.a_req_1.dog_name,
            self.a_req_2.dog_name, self.a_req_3.dog_name, self.a_req_4.dog_name, self.a_req_5.dog_name,
            self.a_req_6.dog_name, self.a_req_7.dog_name, self.a_req_5.stamp_code, self.a_req_6.stamp_code
        ) \
            .subquery()
        # /Subquery

        # Query
        # SELECT
        query = db.query(
            subquery,
            func.concat(self.a_brd.name_rus).label(self.breed_name),
            func.concat(func.coalesce(self.a_scode.stamp_code, subquery.c.stamp_code_not_rkf),
                        ' ',
                        subquery.c.stamp_number
                        ).label(self.stamp_code),
            case([
                (subquery.c.profile_type_id == 3, 'Клуб'),
                (subquery.c.profile_type_id == 4, 'Питомник')
            ]
            ).label(self.profile_type_name),
            case(
                [
                    (subquery.c.status_id.in_([3]), 'Не обработана'),
                    (subquery.c.status_id == 11, func.concat(
                        'Архив (', self.a_prev_stat.name, ')'))
                ],
                else_=self.a_rstat.name
            ).label(self.status_name),
            case(
                [
                    (subquery.c.status_id.in_([11]), 99),
                    (and_(subquery.c.status_id.in_([9]),
                     subquery.c.express == True), 0),
                    (and_(subquery.c.status_id.in_([5]),
                     subquery.c.express == True), 1),
                    (and_(subquery.c.status_id.in_([3]),
                     subquery.c.express == True), 2),
                    (subquery.c.status_id.in_([9]), 3),
                    (subquery.c.status_id.in_([5]), 4),
                    (subquery.c.status_id.in_([3]), 5),
                    (subquery.c.status_id.in_([8]), 6),
                    (subquery.c.status_id.in_([4]), 7),
                    (subquery.c.status_id.in_([7, 10]), 8)
                ],
                else_=9
            ).label(self.sort_order)
        )

        # JOIN
        query = query \
            .join(self.a_rstat, self.a_rstat.id == subquery.c.status_id) \
            .outerjoin(self.a_prev_stat, self.a_prev_stat.id == subquery.c.prev_status_id) \
            .outerjoin(self.a_brd, self.a_brd.id == subquery.c.breed_id) \
            .outerjoin(self.a_scode, self.a_scode.id == subquery.c.stamp_code_id)

        # ORDER BY
        query = query.order_by(
            query.statement.selected_columns.sort_order.asc(),
            subquery.c.date_create.asc())

        # Фильтры для основного запроса
        if query_filters:
            # Добавляем фильтры в запрос только тех полей которые к нему относятся
            query = self._make_filter_model_without_strings(query=query,
                                                            filters_model=query_filters)

        # Пользовательская сортировка по полю
        # Получаем поле (объект) из подзапроса по имени поля которое ввел пользователь
        # sort_field = getattr(query.statement.selected_columns, sort_field, None)
        # if sort_field is not None:
        #     user_sort = asc(sort_field) if sort_direction == 'asc' else desc(sort_field)
        #     query = query.order_by(user_sort)
        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        query = query.offset(skip).limit(limit)
        # /Query

        result = query.all()

        return result


replace_pedigree_header_requests = CRUDReplace_pedigree_header_requests(
    Replace_pedigree_header_requests)
