from datetime import datetime
from typing import Any, List, Optional, Dict, Type, Union
from sqlalchemy import exc, text, and_, or_, func, alias, case, extract, asc, desc
from sqlalchemy.orm import Session, aliased
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app import schemas
from app.crud.base import ModelType, CRUDBase
from app.schemas import Filters_model
from app.schemas.requests import \
    Litter_header_declarant_requests_create, \
    Litter_header_declarant_requests_update
from app.models.requests.litter_header_declarant_requests import Litter_header_declarant_requests
from app.models import Litter_declarant_requests, \
    Litter_requests, \
    Litter_declarant_requests_histories, \
    Litter_declarant_documents, \
    Request_statuses, \
    Declarants, \
    Breeds, \
    Federations, \
    Profiles, \
    Legal_informations, \
    Stamp_codes, \
    Weekends_list


class CRUDLitter_header_declarant_requests(CRUDBase[Litter_header_declarant_requests,
                                                    Litter_header_declarant_requests_create,
                                                    Litter_header_declarant_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDLitter_header_declarant_requests, self).__init__(model)

        # short table names
        self.hdr = 'hdr'
        self.req = 'req'
        self.rstat = 'rstat'
        self.prev_stat = 'prev_stat'
        self.hreq = 'hreq'
        self.dcl = 'dcl'
        self.br = 'br'
        self.fd = 'fd'
        self.pf = 'pf'
        self.li = 'li'
        self.scode = 'scode'
        self.docs = 'docs'
        self.hist = 'hist'
        self.wl = 'wl'
        self.date_first = 'date_first'
        self.docs = 'docs'
        self.returned = 'returned'

        # aliased models
        self.a_hdr = aliased(Litter_header_declarant_requests, name=self.hdr)
        self.a_req = aliased(Litter_declarant_requests, name=self.req)
        self.a_rstat = aliased(Request_statuses, name=self.rstat)
        self.a_prev_stat = aliased(Request_statuses, name=self.prev_stat)
        self.a_hreq = aliased(Litter_requests, name=self.hreq)
        self.a_dcl = aliased(Declarants, name=self.dcl)
        self.a_br = aliased(Breeds, name=self.br)
        self.a_fd = aliased(Federations, name=self.fd)
        self.a_pf = aliased(Profiles, name=self.pf)
        self.a_li = aliased(Legal_informations, name=self.li)
        self.a_scode = aliased(Stamp_codes, name=self.scode)
        self.a_docs = aliased(Litter_declarant_documents, name=self.docs)
        self.a_hist = aliased(Litter_declarant_requests_histories, name=self.hist)
        self.a_wl = aliased(Weekends_list, name=self.wl)

        # short label names
        self.id = 'id'
        self.value = 'value'
        self.weekend_days_count = 'weekend_days_count'
        self.is_weekend_count = 'is_weekend_count'
        self.req_id = 'req_id'
        self.profile_id = 'profile_id'
        self.status_name = 'status_name'
        self.federation_name = 'federation_name'
        self.breed_name = 'breed_name'
        self.days_passed = 'days_passed'
        self.f_date_first = 'date_first'
        self.club_name = 'club_name'
        self.author_name = 'author_name'
        self.documents_count = 'documents_count'
        self.return_count = 'return_count'
        self.sender_type = 'sender_type'
        self.is_weekend = 'is_weekend'
        self.sort_order = 'sort_order'
        self.date_last = 'date_last'

    def get_all(
        self,
        db: Session,
        *,
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None,
        is_deleted: bool = False
    ) -> List[dict]:

        # date_first subquery
        date_first = db.query(Litter_declarant_requests_histories.litter_header_declarant_request_id.label(self.id),
                              Litter_declarant_requests_histories.date_create.label(self.value)) \
            .filter(and_(Litter_declarant_requests_histories.is_deleted == False,
                         Litter_declarant_requests_histories.status_id.in_([3]))) \
            .subquery()
        date_first = aliased(date_first, name=self.date_first)

        # docs subquery
        docs = db.query(self.a_hdr.id, func.count(self.a_docs.id).label(self.value)) \
            .join(self.a_req, self.a_req.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_docs, self.a_docs.litter_header_declarant_request_id == self.a_hdr.id) \
            .filter(and_(self.a_hdr.is_deleted == False, self.a_docs.is_deleted == False)) \
            .group_by(self.a_hdr.id) \
            .subquery()
        docs = aliased(docs, name=self.docs)

        # returned subquery
        returned = db.query(self.a_hdr.id, func.count(self.a_hist.id).label(self.value)) \
            .join(self.a_req, self.a_req.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_hist, self.a_hist.litter_header_declarant_request_id == self.a_hdr.id) \
            .filter(and_(self.a_hdr.is_deleted == False,
                         self.a_hist.is_deleted == False,
                         self.a_hist.status_id.in_([4, 8]))) \
            .group_by(self.a_hdr.id) \
            .subquery()
        returned = aliased(returned, name=self.returned)

        # weekend_days_count label
        weekend_days_count = db.query(func.count(self.a_wl.weekend_date)) \
            .filter(and_(self.a_wl.is_deleted == False,
                         self.a_wl.weekend_date.between(func.max(date_first.c.value),
                                                        datetime.now()))) \
            .label(self.weekend_days_count)

        # is_weekend_count label
        is_weekend_count = db.query(func.count(self.a_wl.id)) \
            .filter(and_(self.a_wl.is_deleted == False,
                         func.date(func.max(date_first.c.value)) == self.a_wl.weekend_date)) \
            .label(self.is_weekend_count)

        # First subquery
        # SELECT
        first_subquery = db.query(
            self.a_hdr.id,
            self.a_hdr.date_create,
            self.a_hdr.date_change.label(self.date_last),
            self.a_hdr.status_id,
            self.a_hdr.barcode,
            self.a_req.id.label(self.req_id),
            self.a_req.express,
            self.a_req.email,
            self.a_pf.id.label(self.profile_id),
            self.a_pf.profile_type_id,
            case([(self.a_hdr.status_id == 10, func.concat('Архив (', self.a_prev_stat.name, ')'))],
                 else_=self.a_rstat.name
                 ).label(self.status_name),
            self.a_fd.short_name.label(self.federation_name),
            self.a_li.folder_number,
            self.a_scode.stamp_code,
            self.a_br.name_rus.label(self.breed_name),
            func.min(date_first.c.value).label(self.f_date_first),
            func.date_part(
                'day',
                datetime.now() -
                func.max(
                    date_first.c.value)).label(
                self.days_passed),
            weekend_days_count,
            is_weekend_count,
            func.coalesce(self.a_li.short_name, self.a_li.name).label(self.club_name),
            func.concat(
                self.a_req.last_name,
                ' ',
                self.a_req.first_name,
                ' ',
                self.a_req.second_name)
            .label(self.author_name),
            (func.coalesce(docs.c.value, 0) + 4).label(self.documents_count),
            func.coalesce(returned.c.value, 0).label(self.return_count)
        )

        # JOIN
        first_subquery = first_subquery \
            .join(self.a_req, self.a_req.litter_header_declarant_request_id == self.a_hdr.id) \
            .join(self.a_rstat, self.a_rstat.id == self.a_hdr.status_id) \
            .outerjoin(self.a_prev_stat, self.a_prev_stat.id == self.a_hdr.prev_status_id) \
            .join(self.a_hreq, self.a_hreq.id == self.a_hdr.litter_request_id) \
            .join(self.a_dcl, self.a_dcl.id == self.a_hreq.declarant_id) \
            .outerjoin(self.a_br, self.a_br.id == self.a_req.breed_id) \
            .join(self.a_fd, self.a_fd.id == self.a_hreq.federation_id) \
            .join(self.a_pf, self.a_pf.id == self.a_dcl.club_id) \
            .join(self.a_li, self.a_li.profile_id == self.a_pf.id) \
            .outerjoin(self.a_scode, self.a_scode.id == self.a_req.stamp_code_id) \
            .outerjoin(date_first, date_first.c.id == self.a_hdr.id) \
            .outerjoin(docs, docs.c.id == self.a_hdr.id) \
            .outerjoin(returned, returned.c.id == self.a_hdr.id)

        # WHERE
        first_subquery = first_subquery.filter(
            and_(
                self.a_hdr.is_deleted == False,
                or_(
                    self.a_hdr.prev_status_id is None,
                    self.a_hdr.prev_status_id.in_([3, 4, 5, 8, 9])
                )
            ))

        # GROUP BY
        first_subquery = first_subquery.group_by(
            self.a_hdr.id,
            self.a_req.id,
            self.a_hdr.date_create,
            self.a_hdr.date_change,
            self.a_hdr.status_id,
            self.a_rstat.name,
            self.a_req.email,
            self.a_li.short_name,
            self.a_li.name,
            self.a_li.folder_number,
            self.a_fd.short_name,
            self.a_br.name_rus,
            self.a_req.last_name,
            self.a_req.first_name,
            self.a_req.second_name,
            self.a_pf.id,
            self.a_pf.profile_type_id,
            self.a_scode.stamp_code,
            docs.c.value,
            returned.c.value,
            self.a_prev_stat.name
        ).subquery()

        # /First subquery

        # Second subquery
        second_subquery = db.query(
            first_subquery,
            case(
                [
                    (first_subquery.c.profile_type_id == 3, 'Клуб'),
                    (first_subquery.c.profile_type_id == 3, 'Питомник')
                ]
            ).label(self.sender_type),
            case([(first_subquery.c.is_weekend_count == 0, 'Нет')],
                 else_='Да'
                 ).label(self.is_weekend),
            case(
                [
                    (first_subquery.c.status_id.in_([10]),
                     99),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.express == True,
                          first_subquery.c.return_count > 0),
                     1),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.express == True,
                          first_subquery.c.return_count == 0),
                     2),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.return_count > 0,
                          (first_subquery.c.days_passed - first_subquery.c.weekend_days_count) > 21),
                     3),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.return_count > 0,
                          (first_subquery.c.days_passed - first_subquery.c.weekend_days_count).between(18, 21)),
                     4),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.return_count == 0,
                          (first_subquery.c.days_passed - first_subquery.c.weekend_days_count) > 21),
                     5),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.return_count == 0,
                          (first_subquery.c.days_passed - first_subquery.c.weekend_days_count).between(18, 21)),
                     6),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.return_count > 0),
                     7),
                    (and_(first_subquery.c.status_id.in_([3]),
                          first_subquery.c.return_count == 0),
                     8),
                    (first_subquery.c.status_id.in_([8]),
                     9),
                    (first_subquery.c.status_id.in_([9]),
                     10),
                    (first_subquery.c.status_id.in_([4]),
                     11),
                    (first_subquery.c.status_id.in_([5]),
                     12)
                ],
                else_=13
            ).label(self.sort_order)
        ).subquery()

        # /Second subquery

        # Query
        query = db.query(second_subquery).order_by(
            second_subquery.c.sort_order.asc(),
            case([(second_subquery.c.sort_order.in_([1, 2]),                                    # when
                   second_subquery.c.days_passed - second_subquery.c.weekend_days_count)]       # then
                 ).desc(),
            case([(second_subquery.c.sort_order.in_([3, 4, 5, 6, 7, 8]),
                 second_subquery.c.date_first)]).asc(),
            case([(second_subquery.c.sort_order.in_([9, 10, 11, 12, 13]),
                 second_subquery.c.date_first)]).desc()
        )

        if filters:
            query = self._make_filter_model_without_strings(
                filters_model=filters, query=query)

        query = self._sort_params_without_strings(query=query,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        query = query.offset(skip).limit(limit)

        # # Фильтры
        # if filters:
        #     query = self._make_filter_model_without_strings(subquery=second_subquery,
        #                                                     filters_model=filters,
        #                                                     query=query)
        #
        # # Пользовательская сортировка по полю
        # # Получаем поле (объект) из подзапроса по имени поля которое ввел пользователь
        # sort_field = getattr(second_subquery.c, sort_field, None)
        # if sort_field is not None:
        #     user_sort = asc(sort_field) if sort_direction == 'asc' else desc(sort_field)
        #     query = query.order_by(user_sort)
        #
        # query = query.offset(skip).limit(limit)
        # /Query

        result = query.all()

        return [dict(each) for each in result]


litter_header_declarant_requests = CRUDLitter_header_declarant_requests(
    Litter_header_declarant_requests)
