from typing import Type
from app.crud.base import ModelType, CRUDBase
from app.models.requests.pedigree_header_declarant_requests import Pedigree_header_declarant_requests
from app.schemas.requests import Pedigree_header_declarant_requests_base,\
    Pedigree_header_declarant_requests_create, \
    Pedigree_header_declarant_requests_update


class CRUDPedigree_header_declarant_requests(CRUDBase[Pedigree_header_declarant_requests,
                                                      Pedigree_header_declarant_requests_create,
                                                      Pedigree_header_declarant_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_header_declarant_requests, self).__init__(model)


pedigree_header_declarant_requests = CRUDPedigree_header_declarant_requests(
    Pedigree_header_declarant_requests)
