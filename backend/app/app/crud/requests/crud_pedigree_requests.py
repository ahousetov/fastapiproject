from typing import Type
from app.crud.base import ModelType, CRUDBase
from app.models.requests.pedigree_requests import Pedigree_requests
from app.schemas.requests import Pedigree_requests_create, Pedigree_requests_update


class CRUDPedigree_requests(
        CRUDBase[Pedigree_requests, Pedigree_requests_create, Pedigree_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_requests, self).__init__(model)


pedigree_requests = CRUDPedigree_requests(Pedigree_requests)
