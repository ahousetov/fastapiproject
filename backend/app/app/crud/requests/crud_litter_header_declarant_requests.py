from typing import Type

from app.crud.base import ModelType, CRUDBase
from app.models.requests.litter_header_declarant_requests import Litter_header_declarant_requests
from app.schemas.requests import \
    Litter_header_declarant_requests_create, \
    Litter_header_declarant_requests_update


class CRUDLitter_header_declarant_requests(CRUDBase[Litter_header_declarant_requests,
                                                    Litter_header_declarant_requests_create,
                                                    Litter_header_declarant_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDLitter_header_declarant_requests, self).__init__(model)


litter_header_declarant_requests = CRUDLitter_header_declarant_requests(
    Litter_header_declarant_requests)
