from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.requests.litter_declarant_requests import Litter_declarant_requests
from app.schemas.requests import \
    Litter_declarant_requests_create, \
    Litter_declarant_requests_update


class CRUDLitter_declarant_requests(CRUDBase[Litter_declarant_requests,
                                             Litter_declarant_requests_create,
                                             Litter_declarant_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDLitter_declarant_requests, self).__init__(model)


litter_declarant_requests = CRUDLitter_declarant_requests(Litter_declarant_requests)
