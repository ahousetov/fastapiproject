from sqlalchemy import exc, text, and_, or_, func, alias, case, extract, asc, desc
from sqlalchemy.orm import Session, aliased
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.requests.litter_requests import Litter_requests
from app.schemas.requests import Litter_requests_create, Litter_requests_update
from app.models.requests.litter_declarant_requests import Litter_declarant_requests
from app.models.requests.litter_declarant_requests_histories import Litter_declarant_requests_histories
from app.models.requests.litter_header_declarant_requests import Litter_header_declarant_requests
from app.models.requests.litter_requests import Litter_requests
from app.models.requests.request_statuses import Request_statuses
from app.models.declarants.declarants import Declarants
from app.models.catalogs.federations import Federations
from app.models.public.profiles import Profiles
from app.models.public.legal_informations import Legal_informations


class CRUDLitter_requests(CRUDBase[Litter_requests,
                                   Litter_requests_create,
                                   Litter_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDLitter_requests, self).__init__(model)


litter_requests = CRUDLitter_requests(Litter_requests)
