from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.requests.pedigree_request_statuses import Pedigree_request_statuses
from app.schemas.requests import Pedigree_request_statuses_base, \
    Pedigree_request_statuses_create, \
    Pedigree_request_statuses_update, \
    Pedigree_request_statuses_delete


class CRUDPedigree_request_statuses(CRUDBase[Pedigree_request_statuses,
                                             Pedigree_request_statuses_create,
                                             Pedigree_request_statuses_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_request_statuses, self).__init__(model)
        # self.fci_breed_statuses_id = f"{self.l_fci_breed_statuses}.id"
        # self.fci_breed_statuses_name = f"{self.l_fci_breed_statuses}.name"
        # self.fci_groups_number = f"{self.l_fci_groups}.number"
        # self.fci_groups_name = f"{self.l_fci_groups}.name"
        # self.fci_groups_id = f"{self.l_fci_groups}.id"
        # self.fci_sections_number = f"{self.l_fci_sections}.number"
        # self.fci_sections_name = f"{self.l_fci_sections}.name"
        # self.fci_sections_id = f"{self.l_fci_sections}.id"
        # self.rel_breeds_sections_groups_group_id = f"{self.l_rel_breeds_sections_groups}.group_id"
        # self.rel_breeds_sections_groups_section_id = f"{self.l_rel_breeds_sections_groups}.section_id"


pedigree_request_statuses = CRUDPedigree_request_statuses(Pedigree_request_statuses)
