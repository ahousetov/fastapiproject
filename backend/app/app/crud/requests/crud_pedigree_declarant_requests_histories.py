from typing import Type
from app.crud.base import ModelType, CRUDBase
from app.models.requests.pedigree_declarant_requests_histories import Pedigree_declarant_requests_histories
from app.schemas.requests.pedigree_declarant_requests_histories import Pedigree_declarant_requests_histories_base, \
    Pedigree_declarant_requests_histories_create, \
    Pedigree_declarant_requests_histories_update


class CRUDPedigree_declarant_requests_histories(CRUDBase[Pedigree_declarant_requests_histories,
                                                Pedigree_declarant_requests_histories_create,
                                                Pedigree_declarant_requests_histories_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_declarant_requests_histories, self).__init__(model)
        # self.fci_breed_statuses_id = f"{self.l_fci_breed_statuses}.id"
        # self.fci_breed_statuses_name = f"{self.l_fci_breed_statuses}.name"
        # self.fci_groups_number = f"{self.l_fci_groups}.number"
        # self.fci_groups_name = f"{self.l_fci_groups}.name"
        # self.fci_groups_id = f"{self.l_fci_groups}.id"
        # self.fci_sections_number = f"{self.l_fci_sections}.number"
        # self.fci_sections_name = f"{self.l_fci_sections}.name"
        # self.fci_sections_id = f"{self.l_fci_sections}.id"
        # self.rel_breeds_sections_groups_group_id = f"{self.l_rel_breeds_sections_groups}.group_id"
        # self.rel_breeds_sections_groups_section_id = f"{self.l_rel_breeds_sections_groups}.section_id"


pedigree_declarant_requests_histories = CRUDPedigree_declarant_requests_histories(
    Pedigree_declarant_requests_histories)
