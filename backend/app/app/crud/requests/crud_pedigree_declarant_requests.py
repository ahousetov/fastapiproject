from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder

from app.crud.base import ModelType, CRUDBase
from app.models.requests import Pedigree_declarant_requests
from app.schemas.requests import Pedigree_declarant_requests_base, \
    Pedigree_declarant_requests_create, \
    Pedigree_declarant_requests_update, \
    Pedigree_declarant_requests_delete


class CRUDPedigree_declarant_requests(CRUDBase[Pedigree_declarant_requests,
                                               Pedigree_declarant_requests_create,
                                               Pedigree_declarant_requests_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDPedigree_declarant_requests, self).__init__(model)


pedigree_declarant_requests = CRUDPedigree_declarant_requests(
    Pedigree_declarant_requests)
