from typing import Type
from app.crud.base import ModelType, CRUDBase
from app.models.requests.request_statuses import Request_statuses
from app.schemas.requests import Request_statuses_base, \
    Request_statuses_create, \
    Request_statuses_update, \
    Request_statuses_delete


class CRUDRequest_statuses(CRUDBase[Request_statuses,
                                    Request_statuses_create,
                                    Request_statuses_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDRequest_statuses, self).__init__(model)


request_statuses = CRUDRequest_statuses(Request_statuses)
