from typing import Any, List, Optional, Dict, Type, Union

from app.crud.base import ModelType, CRUDBase
from app.models.requests.litter_declarant_requests_histories import Litter_declarant_requests_histories
from app.schemas.requests import \
    Litter_declarant_requests_histories_create, \
    Litter_declarant_requests_histories_update


class CRUDLitter_declarant_requests_histories(CRUDBase[Litter_declarant_requests_histories,
                                                       Litter_declarant_requests_histories_create,
                                                       Litter_declarant_requests_histories_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDLitter_declarant_requests_histories, self).__init__(model)


litter_declarant_requests_histories = CRUDLitter_declarant_requests_histories(
    Litter_declarant_requests_histories)
