from typing import Any, List, Optional
from sqlalchemy import func, select
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types
from app.models.accesses.user_accesses import User_accesses
from app.models.public.users import Users
from app.models.relations.rel_users_profiles import Rel_users_profiles
from app.models.public.profiles import Profiles
from app.models.accesses.request_types import Request_types
from app.schemas.accesess.accesses import User_accesses_value_list
from fastapi import HTTPException


class CRUDUser_accesses(CRUDBase[User_accesses, None, None]):

    def get_list_accesses_profile_id(self,
                                     db: Session,
                                     profile_id: int) -> Optional[User_accesses]:
        accesses = db.query(
            self.model.user_id,
            self.model.request_type_id
        ).join(Users)\
            .join(Rel_users_profiles)\
            .join(Profiles)\
            .filter(Profiles.id == profile_id)
        accesses = accesses.all()
        return accesses

    def get_valuelist(self, db: Session) -> Optional[User_accesses_value_list]:
        value_list = db.query(Request_types.id.label("value"),
                              Request_types.description.label("name"))\
            .where(Request_types.is_deleted == False)\
            .order_by(Request_types.id)
        value_list = value_list.all()
        if not value_list:
            raise HTTPException(status_code=404, detail="Типы запросов не найдены")
        return value_list


user_accesses = CRUDUser_accesses(User_accesses)
