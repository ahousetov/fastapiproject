from typing import Any, List, Optional
from sqlalchemy import func, select, text
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase, Contact_types
from app.models.accesses.profile_type_requests import Profile_type_requests
from app.models.accesses.request_types import Request_types


class CRUDProfile_type_requests(CRUDBase[Profile_type_requests, None, None]):

    def get_order_types_list(self, db: Session) -> Optional[Profile_type_requests]:
        types_of_orders_list = db.query(
            self.model.request_type_id,
            self.model.is_default,
            Request_types.name,
            Request_types.description
        ).join(Request_types)\
            .filter(self.model.profile_type_id == 3,
                    Request_types.is_deleted == False)\
            .order_by(text(f"request_type_id asc"))
        types_of_orders_list = types_of_orders_list.all()
        return types_of_orders_list


profile_type_requests = CRUDProfile_type_requests(Profile_type_requests)
