from sqlalchemy import exc, text
from sqlalchemy.orm import Session
from typing import Any, List, Optional, Dict, Type, Union
from fastapi import APIRouter, Body, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder

import schemas
from app import crud
from app.schemas import Filters_model
from app.crud.base import ModelType, CRUDBase
from app.crud.relations.crud_rel_breeds_sections_groups import CRUDRel_breeds_sections_groups
from app.models.dogs.breeds import Breeds
from app.models.dogs.fci_breed_statuses import FCI_breed_statuses
from app.models.dogs.fci_groups import FCI_groups
from app.models.dogs.fci_sections import FCI_sections
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups
from app.models.clubs.national_breed_clubs_breeds import National_breed_clubs_breeds
from app.schemas.dogs import Breeds_create, Breeds_update, Breeds_blocks, Breeds_block
from app.schemas.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups_aliased


class CRUDBreeds(CRUDBase[Breeds, Breeds_create, Breeds_update]):

    def __init__(self, model: Type[ModelType] = None):
        super(CRUDBreeds, self).__init__(model)
        self.fci_breed_statuses_id = f"{self.fci_breed_statuses}.id"
        self.fci_breed_statuses_name = f"{self.fci_breed_statuses}.name"
        self.fci_groups_number = f"{self.fci_groups}.number"
        self.fci_groups_name = f"{self.fci_groups}.name"
        self.fci_groups_id = f"{self.fci_groups}.id"
        self.fci_sections_number = f"{self.fci_sections}.number"
        self.fci_sections_name = f"{self.fci_sections}.name"
        self.fci_sections_id = f"{self.fci_sections}.id"
        self.rel_breeds_sections_groups_group_id = f"{self.rel_breeds_sections_groups}.group_id"
        self.rel_breeds_sections_groups_section_id = f"{self.rel_breeds_sections_groups}.section_id"

    def get(self, db: Session, id: int = None, as_object: bool = False):
        if as_object:
            breed = db.query(self.model).where(self.model.id == id,
                                               self.model.is_deleted == False)
        else:
            breed = db.query(
                self.model.id,
                self.model.name_rus,
                self.model.name_original,
                self.model.name_en,
                self.model.fci_number,
                self.model.fci_status_id,
                self.model.is_cacib,
                self.model.is_fci,
                self.model.is_specific,
                self.model.for_dog_handler,
                FCI_breed_statuses.id.label(self.l_fci_breed_statuses_id),
                FCI_breed_statuses.name.label(self.l_fci_breed_statuses_name),
                FCI_groups.number.label(self.l_fci_groups_number),
                FCI_groups.name.label(self.l_fci_groups_name),
                Rel_breeds_sections_groups.group_id.label(
                    self.l_rel_breeds_sections_groups_group_id),
                FCI_sections.number.label(self.l_fci_sections_number),
                FCI_sections.name.label(self.l_fci_sections_name),
                Rel_breeds_sections_groups.section_id.label(
                    self.l_rel_breeds_sections_groups_section_id)
            ) \
                .join(FCI_breed_statuses, isouter=True) \
                .join(Rel_breeds_sections_groups, isouter=True) \
                .join(FCI_groups, isouter=True) \
                .join(FCI_sections, isouter=True) \
                .filter(self.model.id == id, self.model.is_deleted == False)
        breed = breed.first()
        if not breed:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Не найдено пород по id = {id}"
            )
        return breed

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: Filters_model = None,
            is_deleted: bool = False
    ) -> List[dict]:
        entry = db.query(
            self.model.id,
            self.model.name_rus,
            self.model.name_original,
            self.model.name_en,
            self.model.fci_number,
            self.model.fci_status_id,
            self.model.is_cacib,
            self.model.is_fci,
            self.model.is_specific,
            self.model.for_dog_handler,
            FCI_breed_statuses.id.label(self.l_fci_breed_statuses_id),
            FCI_breed_statuses.name.label(self.l_fci_breed_statuses_name),
            FCI_groups.number.label(self.l_fci_groups_number),
            FCI_groups.name.label(self.l_fci_groups_name),
            Rel_breeds_sections_groups.group_id.label(
                self.l_rel_breeds_sections_groups_group_id),
            FCI_sections.number.label(self.l_fci_sections_number),
            FCI_sections.name.label(self.l_fci_sections_name),
            Rel_breeds_sections_groups.section_id.label(
                self.l_rel_breeds_sections_groups_section_id)
        ) \
            .join(FCI_breed_statuses, isouter=True) \
            .join(Rel_breeds_sections_groups, isouter=True) \
            .join(FCI_groups, isouter=True) \
            .join(FCI_sections, isouter=True)

        if not is_deleted and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        # Пользовательские фильтры
        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)

        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        entry = entry.offset(skip).limit(limit)

        rows = entry.all()

        return [dict(row) for row in rows]

    def get_by_nbc_id(self, db: Session, nbc_id: int):
        contacts = db.query(self.model) \
            .join(National_breed_clubs_breeds, National_breed_clubs_breeds.breed_id == self.model.id) \
            .filter(self.model.is_deleted == False, National_breed_clubs_breeds.national_breed_club_id == nbc_id)

        return contacts.all()

    def split_obj_in(self, obj_in=None, exclude_unset=False):
        """
        Функция разделяющая obj_in на
        словари breed и additional
        """
        if not obj_in:
            return None, None
        obj_in_data = jsonable_encoder(obj_in)
        additional, breed = {}, {}
        for k, v in obj_in_data.items():
            if k in [self.l_rel_breeds_sections_groups_group_id,
                     self.l_rel_breeds_sections_groups_section_id]:
                pre = self.rel_breeds_sections_groups + "_"
                key = k.replace(pre, '') if pre in k else k
                if exclude_unset:
                    if v:
                        additional.update({key: v})
                else:
                    additional.update({key: v})
            else:
                if exclude_unset:
                    if v:
                        breed.update({k: v})
                else:
                    breed.update({k: v})
        return breed, additional

    def create(
        self,
        db: Session,
        create_data: dict = None
    ) -> bool:
        """
        Переопределяем метод из base.py.
        Это неободимо для того, чтобы использовать метод split_obj_in,
        созданный специально для для разделения входных данных на то, что
        пойдёт в таблицу и на то, что должно, по идее, пойти в таблицу relations.

        Использовать это в base - проблемотично, т.к. не получится отделить по смысл те
        методы, где разделение данных необходимо, от тех методов, где необходимо.

        Переопределив метод, мы инкапсулируем логику внутри crud.breeds. Логика base не тронута и
        расширена, за счёт новых входных параметров:

        ...
        obj_in: CreateSchemaType = None,
        obj_in_dict: dict = None
        ...
        """
        try:
            breed = create_data.get('breed')
            additional = create_data.get('rel_breeds_sections_groups')
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Данных для создания новых пород не передано в 'create'",
                headers={"X-Error": "There goes my error"}
            )

        try:
            new_breed, new_relation = None, None
            new_breed = self.model(**breed, is_deleted=False)
            db.add(new_breed)
            db.commit()
            if additional:
                additional['breed_id'] = new_breed.id
                new_relation = Rel_breeds_sections_groups(**additional)
                db.add(new_relation)
                db.commit()
            db.refresh(new_breed)
            if new_relation:
                db.refresh(new_relation)
        except exc.IntegrityError as ex:
            headers_dict = {}
            detail_str = ''
            for s in ex.orig.pgerror.split("\n"):
                if 'ERROR' in s:
                    headers_dict.update({"X-Error": s.replace('ERROR:  ', '')})
                if 'DETAIL' in s:
                    detail_str = s.replace('DETAIL:  ', '')
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=detail_str,
                                headers=headers_dict)
        return True

    def update(
            self,
            db: Session,
            id: int,
            update_data: Dict[str, Any]
    ) -> bool:
        breed_data = update_data.get('breed')
        rel_breeds_sections_groups_data = update_data.get('rel_breeds_sections_groups')

        breed = crud.breeds.get_object(db=db, id=id)
        super().update(db, db_obj=breed, obj_in=breed_data)
        rel = crud.rel_breeds_sections_groups.get_by_breed_id(db, breed_id=id)
        rel = crud.rel_breeds_sections_groups.update(
            db, db_obj=rel, obj_in=rel_breeds_sections_groups_data)

        return True

    def remove_by_names(
            self,
            db: Session,
            rel_crud: CRUDRel_breeds_sections_groups,
            generated_names: List[str] = None
    ) -> ModelType:
        query_to_remove_rus = db.query(self.model).filter(self.model.name_rus.in_(generated_names),
                                                          self.model.name_en.in_(
                                                              generated_names),
                                                          self.model.name_original.in_(generated_names))
        selected_breeds = query_to_remove_rus.all()
        breeds_ids = [b.id for b in selected_breeds]
        removed = rel_crud.delete_by_breed_ids(db=db, breed_ids=breeds_ids)
        query_to_remove_rus.delete()
        db.commit()
        return True


breeds = CRUDBreeds(Breeds)
