from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from fastapi import APIRouter, Depends, HTTPException, status

from app import schemas
from app.crud.base import CRUDBase
from app.models.dogs.fci_groups import FCI_groups
from app.schemas.dogs.fci_groups import FCI_group_create, \
    FCI_group_update, \
    FCI_groups_blocks, \
    FCI_groups_block


class CRUDFCI_group(CRUDBase[FCI_groups, FCI_group_create, FCI_group_update]):

    def get(self,
            db: Session,
            id: Any,
            is_deleted: bool = False
            ) -> Dict[str, Any]:
        fci_group = db.query(self.model.id,
                             self.model.name,
                             self.model.name_en,
                             self.model.number)\
            .filter(self.model.id == id)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            fci_group = fci_group.filter(self.model.is_deleted == False)

        fci_group = fci_group.first()
        if fci_group is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=str(f"Нет {self.title} по id = {id}"))
        return {
            'title': self.title,
            'item_title': fci_group.name,
            'blocks': [fci_group]
        }

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'number',
            filters: schemas.Filters_model = None,
            is_deleted: bool = False
    ) -> List[Dict[str, Any]]:
        entry = db.query(self.model.id,
                         self.model.name,
                         self.model.name_en,
                         self.model.number)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)

        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        entry = entry.offset(skip).limit(limit)

        result = entry.all()

        return [dict(row) for row in result]


fci_groups = CRUDFCI_group(FCI_groups)
