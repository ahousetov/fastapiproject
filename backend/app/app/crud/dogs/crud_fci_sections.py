from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

from app import schemas
from app.crud.base import CRUDBase
from app.models.dogs.fci_sections import FCI_sections
from app.schemas.dogs.fci_sections import FCI_section_create, \
    FCI_section_update,  \
    FCI_sections_blocks,  \
    FCI_sections_block, \
    FCI_section


class CRUDFCI_section(CRUDBase[FCI_sections, FCI_section_create, FCI_section_update]):

    def get(self,
            db: Session,
            id: Any,
            is_deleted: bool = False) -> Dict[str, Any]:
        fci_section = db.query(self.model.id,
                               self.model.name,
                               self.model.name_en,
                               self.model.number)\
            .filter(self.model.id == id)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            fci_section = fci_section.filter(self.model.is_deleted == False)

        fci_section = fci_section.first()
        if fci_section is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=str(f"Нет {self.title} по id = {id}"))
        return {
            'title': self.title,
            'item_title': fci_section.name,
            'blocks': [fci_section]
        }

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'number',
            filters: schemas.Filters_model = None,
            is_deleted: bool = False
    ) -> List[Dict[str, Any]]:
        entry = db.query(self.model.id,
                         self.model.name,
                         self.model.name_en,
                         self.model.number)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)

        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        entry = entry.offset(skip).limit(limit)

        result = entry.all()

        return [dict(row) for row in result]


fci_sections = CRUDFCI_section(FCI_sections)
