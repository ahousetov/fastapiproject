from fastapi import HTTPException, status
from sqlalchemy import exc, text
from sqlalchemy.orm import Session, Query
from typing import Any, Dict, Generic, List, Optional

from app import schemas
from app.crud.base import CRUDBase
from app.models.dogs.fci_breed_statuses import FCI_breed_statuses
from app.schemas.dogs.fci_breed_statuses import FCI_breed_statuses_create, \
    FCI_breed_statuses_update, \
    FCI_breed_statuses_blocks, \
    FCI_breed_statuses_block


class CRUDFCI_breed_statuses(CRUDBase[FCI_breed_statuses,
                                      FCI_breed_statuses_create,
                                      FCI_breed_statuses_update]):
    def get(
            self,
            db: Session,
            id: int,
            is_deleted: bool = False
    ) -> Dict[str, Any]:
        try:
            fci_breed_status = db.query(self.model.id, self.model.name)\
                .filter(self.model.id == id)

            if is_deleted is not None and hasattr(self.model, 'is_deleted'):
                fci_breed_status = fci_breed_status.filter(
                    self.model.is_deleted == False)

            fci_breed_status = fci_breed_status.first()
            if not fci_breed_status:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"Не найдено пород по id = {id}"
                )
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=str(e))
        return {'title': self.title,
                'item_title': fci_breed_status.name,
                'blocks': [fci_breed_status]
                }

    def get_all(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: schemas.Filters_model = None,
            is_deleted: bool = False
    ) -> List[Dict[str, Any]]:
        entry = db.query(self.model.id, self.model.name)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            entry = entry.filter(self.model.is_deleted == False)

        if filters:
            entry = self._make_filter_model_without_strings(
                filters_model=filters, query=entry)

        entry = self._sort_params_without_strings(query=entry,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=filters)

        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        entry = entry.offset(skip).limit(limit)

        result = entry.all()

        return [dict(row) for row in result]


fci_breed_statuses = CRUDFCI_breed_statuses(FCI_breed_statuses)
