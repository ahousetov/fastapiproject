# python
import random
from enum import Enum

# fastapi
from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder


from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from app.db.base_class import Base


# schemas
from app.schemas.common import RKF_office_base_model
from app.schemas import Filters_model, Operators, Filter_types, \
    Text_filter_type, \
    Number_filter_type, \
    Filter_type_base
from app.schemas.filters import Condition

# catalogs
from app.models.catalogs.address_types import Address_types
from app.models.catalogs.street_types import Street_types
from app.models.catalogs.house_types import House_types
from app.models.catalogs.flat_types import Flat_types
from app.models.catalogs.cities import Cities
from app.models.catalogs.regions import Regions
from app.models.catalogs.city_types import City_types
from app.models.catalogs.federal_district import Federal_Districts
from app.models.catalogs.federations import Federations
from app.models.catalogs.organization_statuses import Organization_statuses

# relations
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.models.relations.rel_profiles_federations import Rel_profiles_federations

# public
from app.models.public.club_address import Club_addresses
from app.models.public.profiles import Profiles
from app.models.public.contacts import Contacts
from app.models.public.legal_informations import Legal_informations
from app.models.public.profiles import Profiles
from app.models.public.users import Users
from app.models.public.organizations_history import Organizations_history
from app.models.public.organizations_membership_dues import Organizations_membership_dues
from app.models.public.bank_data import Bank_data


# dogs
from app.models.dogs.breeds import Breeds
from app.models.dogs.fci_breed_statuses import FCI_breed_statuses
from app.models.dogs.fci_groups import FCI_groups
from app.models.dogs.fci_sections import FCI_sections


# sqlalchemy
from sqlalchemy import inspect
from sqlalchemy import literal, exc, text, and_, or_, Integer, asc, desc, case
from sqlalchemy.orm import Session, Query
from sqlalchemy.sql import column, expression
from sqlalchemy.ext import compiler
from sqlalchemy.engine.row import Row as Row_type


ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=RKF_office_base_model)
UpdateSchemaType = TypeVar("Update_schema_type", bound=RKF_office_base_model)


class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):

    def __init__(self, model: Type[ModelType] = None):
        """
        CRUD object with default methods to Create, Read, Update, Delete (CRUD).

        **Parameters**

        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
        """
        self.model = model
        self.title = self.model.get_title(model)
        self.club_address = f"{Club_addresses.__table__.schema}.{Club_addresses.__name__.lower()}"
        self.address_types = f"{Address_types.__table__.schema}.{Address_types.__name__.lower()}"
        self.street_types = f"{Street_types.__table__.schema}.{Street_types.__name__.lower()}"
        self.house_types = f"{House_types.__table__.schema}.{House_types.__name__.lower()}"
        self.flat_types = f"{Flat_types.__table__.schema}.{Flat_types.__name__.lower()}"
        self.profiles = f"{Profiles.__table__.schema}.{Profiles.__name__.lower()}"
        self.cities = f"{Cities.__table__.schema}.{Cities.__name__.lower()}"
        self.contacts = f"{Contacts.__table__.schema}.{Contacts.__name__.lower()}"
        self.regions = f"{Regions.__table__.schema}.{Regions.__name__.lower()}"
        self.city_types = f"{City_types.__table__.schema}.{City_types.__name__.lower()}"
        self.breeds = f"{Breeds.__table__.schema}.{Breeds.__name__.lower()}"
        self.federations = f"{Federations.__table__.schema}.{Federations.__name__.lower()}"
        self.federal_districts = f"{Federal_Districts.__table__.schema}.{Federal_Districts.__name__.lower()}"
        self.fci_breed_statuses = f"{FCI_breed_statuses.__table__.schema}.{FCI_breed_statuses.__name__.lower()}"
        self.fci_groups = f"{FCI_groups.__table__.schema}.{FCI_groups.__name__.lower()}"
        self.fci_sections = f"{FCI_sections.__table__.schema}.{FCI_sections.__name__.lower()}"
        self.rel_breeds_sections_groups = f"{Rel_breeds_sections_groups.__table__.schema}." \
                                            f"{Rel_breeds_sections_groups.__name__.lower()}"
        self.rel_profiles_contacts = f"{Rel_profiles_contacts.__table__.schema}." \
                                       f"{Rel_profiles_contacts.__name__.lower()}"
        self.rel_profiles_federations = f"{Rel_profiles_federations.__table__.schema}.{Rel_profiles_federations.__name__.lower()}"
        self.legal_informations = f"{Legal_informations.__table__.schema}.{Legal_informations.__name__.lower()}"
        self.users = f"{Users.__table__.schema}.{Users.__name__.lower()}"
        self.organization_statuses = f"{Organization_statuses.__table__.schema}.{Organization_statuses.__name__.lower()}"
        self.organizations_history = f"{Organizations_history.__table__.schema}.{Organizations_history.__name__.lower()}"
        self.organizations_membership_dues = f"{Organizations_membership_dues.__table__.schema}." \
                                             f"{Organizations_membership_dues.__name__.lower()}"
        self.bank_data = f"{Bank_data.__table__.schema}.{Bank_data.__name__.lower()}"

    def __getattribute__(self, item):
        """
        Особая pythonic-фича. Позволяет перевести имя поля модели из sql-формата
        в формат схемы для FastAPI.

        Пример:

        def __init__(self, ...):
            self.some_field = "schema.model.model_field"

        print(self.some_field)
        "schema.model.model_field"

        print(self.some_field)
        "schema_model_model_field"

        Важно понимать, что если поле УЖЕ задано с префиксом 'l_' то ничего не будет:

         def __init__(self, ...):
            self.some_field = "schema.model.model_field"

        print(self.some_field)
        "schema.model.model_field"

        В тоже время, если такого аргумента попросту нет в объекте, то обращение к нему
        вернёт AttributeError.

        Фича была введена по замечанию начальства о том, что ".replace('.', '_')", используемый для
        приведения имени поля к подходящему виду для схемы FastAPI, используется слишком часто.
        """
        if item.startswith('l_'):
            with_l, without_l = item, item[2:]
            if with_l in self.__dict__:
                return super(CRUDBase, self).__getattribute__(item)
            if without_l in self.__dict__:
                return super(CRUDBase, self).__getattribute__(without_l).replace(".", "_")
        return super(CRUDBase, self).__getattribute__(item)

    # TODO Есть предложение сделать этот метод действительно статическим и сделать его доступным вне объекта
    def _orm_obj_dict(self, obj):
        return {col.key: getattr(obj, col.key)
                for col in inspect(obj).mapper.column_attrs}

    def row2dict(self, row):
        d = {[column.name]: str(getattr(row, column.name))
             for column in row.__table__.columns}
        return d

    def __get_column_names_list_(self, column_descriptors:list = None, model_name: str = None):
        column_names = {}
        entity = None
        for c in column_descriptors:
            if c['name'] == self.model.__name__:
                if model_name:
                    column_names[model_name] = [str(col.key) for col in c['entity'].__table__.c._all_columns]
                else:
                    for col in c['entity'].__table__.c._all_columns:
                        column_names[str(col.key)] = None
            else:
                expr = c['expr'].expression.key
                column_names[expr.replace('.', '_')] = expr
        return column_names

    def _skip_limit_params(self, skip=None, limit=None, filters=None):
        filter_skip = getattr(filters, 'skip', None)
        filter_limit = getattr(filters, 'limit', None)
        skip = filter_skip if filter_skip else skip
        limit = filter_limit if filter_limit else limit
        return skip, limit

    def _sort_field_from_label_or_id(self, sort_field_label):
        if sort_field_label in self.model.__table__.columns.keys():
            sort_field_label = f"{self.model.__table__.schema}.{self.model.__name__.lower()}.{sort_field_label}"
        else:
            field_in_crud = sort_field_label
            for k, v in self.__dict__.items():
                if k in sort_field_label:
                    attr = getattr(self, f"l_{k}", "id")
                    if attr == sort_field_label:
                        field_in_crud = getattr(self, k, "id")
            sort_field_label = field_in_crud if field_in_crud else "id"
        return sort_field_label

    def _sort_params(self, sort_direction=None, sort_field=None, filters=None):
        if sort_direction:
            sort_direction = 'desc' if sort_direction.lower() == 'desc' else 'asc'
        if filters:
            sortModel = getattr(filters, 'sortModel', None)
            if sortModel:
                s_d = sortModel.get('sort_direction', None)
                s_f = sortModel.get('sort_field', None)
                sort_direction = s_d if s_d else sort_direction
                sort_field = s_f if s_f else sort_field
        sort_field = self._sort_field_from_label_or_id(sort_field_label=sort_field)
        return sort_direction, sort_field

    def _sort_params_without_strings(self,
                                     query: Query,
                                     sort_direction: str = None,
                                     sort_field: str = None,
                                     filters=None):
        if filters:
            sortModel = getattr(filters, 'sortModel', None)
            if sortModel:
                s_d = sortModel.get('sort_direction', None)
                s_f = sortModel.get('sort_field', None)
                sort_direction = s_d if s_d else sort_direction
                sort_field = s_f if s_f else sort_field
        if sort_field and sort_direction:
            sort_field = getattr(query.statement.selected_columns, sort_field, None)
            if sort_field is not None:
                user_sort = asc(sort_field) if sort_direction == 'asc' else desc(sort_field)
                query = query.order_by(user_sort)

        return query

    def __condition_to_sql__(self,
                             sql_string: str = '',
                             field_name: str = '',
                             model_name: str = '',
                             cond: Filters_model = None,
                             operator: str = ''):
        type = getattr(cond, Filter_type_base.name.value, None)
        f_n = f"{model_name}.{field_name}" \
            if model_name \
            else self._sort_field_from_label_or_id(sort_field_label=field_name)
        if getattr(cond, Filter_types.name.value, None) == Filter_types.text.value and type:
            # Здесь добавляем в field_cond условие для текстового фильтра.
            if len(cond.filter) >= 3:
                if type == Text_filter_type.not_contains.value:
                    sql_string += f"{f_n} NOT LIKE '%{cond.filter}%'"
                if type == Text_filter_type.contains.value:
                    sql_string += f"{f_n} LIKE '%{cond.filter}%'"
                if type == Text_filter_type.equals.value:
                    sql_string += f"{f_n} = '{cond.filter}'"
                if type == Text_filter_type.not_equals.value:
                    sql_string += f"{f_n} != '{cond.filter}'"
                if type == Text_filter_type.starts_with.value:
                    sql_string += f"{f_n} LIKE '{cond.filter}%'"
                if type == Text_filter_type.ends_with.value:
                    sql_string += f"{f_n} LIKE '%{cond.filter}'"
        elif getattr(cond, Filter_types.name.value, None) == Filter_types.number.value and type:
            # Здесь добавляем в sql_string условие для числового фильтра.
            if type == Number_filter_type.equals.value:
                sql_string += f"{f_n} = {cond.filter}"
            if type == Number_filter_type.less_than.value:
                sql_string += f"{f_n} <= {cond.filter}"
            if type == Number_filter_type.greater_than.value:
                sql_string += f"{f_n} >= {cond.filter}"
        if operator and sql_string:
            sql_string += f' {operator} '
        return sql_string

    def __condition_to_sql_without_strings__(self,
                                             field: Any,
                                             cond: Any = None
                                             ):
        type = getattr(cond, Filter_type_base.name.value, None)
        result = None
        if getattr(cond, Filter_types.name.value, None) == Filter_types.text.value and type:
            # if len(cond.filter) >= 3:
            if type == Text_filter_type.not_contains.value:
                result = (field.notlike(f'%{cond.filter}%'))
            elif type == Text_filter_type.contains.value:
                result = (field.like(f'%{cond.filter}%'))
            elif type == Text_filter_type.equals.value:
                result = (field == cond.filter)
            elif type == Text_filter_type.not_equals.value:
                result = (field != cond.filter)
            elif type == Text_filter_type.starts_with.value:
                result = (field.startswith(cond.filter))
            elif type == Text_filter_type.ends_with.value:
                result = (field.endswith(cond.filter))
        elif getattr(cond, Filter_types.name.value, None) == Filter_types.number.value and type:
            if type == Number_filter_type.equals.value:
                result = (field == cond.filter)
            if type == Number_filter_type.less_than.value:
                result = (field <= cond.filter)
            if type == Number_filter_type.greater_than.value:
                result = (field >= cond.filter)

        return result


    def _make_filter_model(self,
                           filters_model: Filters_model,
                           column_descriptors: list,
                           not_deleted: bool = True):
        result = ''
        model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
        column_names = self.__get_column_names_list_(column_descriptors=column_descriptors,
                                                     model_name=model_name)
        if_delete_filter = f"{model_name}.is_deleted = False"
        if not filters_model:
            if not_deleted:
                result = if_delete_filter
            return result
        filters = filters_model.filterModel
        if filters:
            for field_name, conditions in filters.items():
                if ((not field_name in column_names) and (not field_name in column_names[model_name])) \
                        or conditions.is_empty():
                    continue
                type, field_cond = None, ''
                if not result and not_deleted:
                    result += if_delete_filter
                # Основные параметры (не использ)
                # main_conditions = conditions.dict(include=Condition().dict().keys())
                # Не входящ в обяз параметры
                multiple_conditions = conditions.dict(exclude=Condition().dict().keys())

                has_operator = getattr(conditions, Operators.name.value, None)
                if multiple_conditions and has_operator:
                    operator, filter_type = has_operator, getattr(conditions, Filter_types.name.value, None)
                    m_name = model_name if field_name in column_names[model_name] else ''
                    field_cond = ''.join(
                        [self.__condition_to_sql__(sql_string='',
                                                   field_name=field_name if m_name else column_names[field_name],
                                                   model_name=m_name,
                                                   cond=Condition(**c),
                                                   operator=operator)
                         for _, c in multiple_conditions.items()]
                         )
                    if field_cond.endswith(f' {operator} '):
                        field_cond = field_cond[: -len(f' {operator} ')]
                    if field_cond:
                        result += f' AND ( {field_cond} )'
                else:
                    # Логика следующего  need_model_name следующая:
                    # если field_name найден в column_names[model_name], то
                    # нужно вернуть True, так как префикс модели будет необходим
                    #
                    # Если field_name найдено просто в column_names, то
                    # префикс модели НЕ НУЖЕН и нам НУЖНО ВЕРНУТЬ False. В этом
                    # случае m_name = '' и __condition_to_sql__ отработает
                    # используя только полное имя поля, без префикс модели.
                    need_model_name = field_name in column_names[model_name] \
                        if model_name in column_names \
                        else \
                        not (field_name in column_names)
                    m_name = model_name if need_model_name else ''
                    field_cond = self.__condition_to_sql__(sql_string='',
                                                           field_name=field_name if m_name else column_names[field_name],
                                                           model_name=m_name,
                                                           cond=conditions)
                    if field_cond:
                        result+=f' AND {field_cond}'

        if not result and not_deleted:
            result += if_delete_filter
        elif result.find(if_delete_filter) > 0 and not_deleted:
            result = ''.join(if_delete_filter, ' AND ', result)
        return result

    def _make_filter_query(self,
                           filters_model: Filters_model,
                           query: Query,
                           not_deleted: bool = True):
        text_filters = self._make_filter_model(filters_model=filters_model,
                                               not_deleted=not_deleted)

    def _make_filter_model_without_strings(self,
                                           filters_model: Filters_model,
                                           query: Query,
                                           not_deleted: bool = False,
                                           subquery: Optional[Any] = None):
        if not_deleted is True:
            is_deleted = getattr(query.statement.selected_columns, 'is_deleted', None)
            if is_deleted is not None:
                query = query.filter(is_deleted == False)
        filters = filters_model.filterModel
        for field_name, conditions in filters.items():
            if (getattr(query.statement.selected_columns, field_name, None) is None) or conditions.is_empty():
                continue

            # Получаем объект поля из query
            # Строка: clubs_name -> Column object: public.clubs.name
            # Не волнует имя поля подзапроса или таблицы, агрегатное поле или нет. Должно работать в любом случае
            field = getattr(query.statement.selected_columns, field_name)

            # Устарело
            # Если есть подзапрос нам можно в фильтрах использовать имена полей подзапроса даже агрегатные
            # if subquery is not None:
            #     field = getattr(subquery.c, field_name)
            # Если у нас только запрос то получаем column obj из query по имени поля
            # Строка: clubs_name -> Column object: public.clubs.name
            # else:
            #     field = getattr(query.subquery().c, field_name)
            #     field = field.base_columns.pop()

            # Основные параметры (не использ)
            # main_conditions = conditions.dict(include=Condition().dict().keys())
            # Не входящ в обяз параметры
            multiple_conditions = conditions.dict(exclude=Condition().dict().keys())

            has_operator = getattr(conditions, Operators.name.value, None)


            if multiple_conditions and has_operator:
                # cond = has_cond
                operator, filter_type = has_operator, getattr(conditions, Filter_types.name.value, None)

                correct_filters = []
                for _, c in multiple_conditions.items():

                    filter = self.__condition_to_sql_without_strings__(field, Condition(**c))
                    if filter is not None:
                        correct_filters.append(filter)

                if operator == 'AND':
                    query = query.filter(and_(*correct_filters))
                elif operator == 'OR':
                    query = query.filter(or_(*correct_filters))

            else:
                filter = self.__condition_to_sql_without_strings__(field, conditions)
                query = query.filter(filter)
        return query

    @staticmethod
    def split_objs_in_crud(db_objs: List[ModelType],
                           objs_in: dict,
                           schema_update: Any,
                           schema_create: Any
                           ):
        """
        Функция разделяющая obj_in на
        списки delete_obj_ids, update_objs, create_objs
        """

        # Объекты существующие в базе данных
        db_objs = {obj.id: {obj} for obj in db_objs}

        existing_obj_in = {}
        create_objs = []

        # Разделение входных данных на создаваемые контакты id=None и существющие id=int
        for obj in objs_in:
            if 'id' in obj and obj['id']:

                obj = {key: value for key, value in obj.items() if value is not None}

                existing_obj_in.update({obj['id']: obj})

            else:
                if 'id' in obj:
                    del obj['id']

                # Добавляем обязательный параметр создаваемому контакту
                create_objs.append(schema_create(**obj).dict(exclude_unset=True))

        # id обновляемых контактов = пересечение входных id и существующих в бд
        update_ids = set(db_objs.keys()) & set(existing_obj_in.keys())
        update_objs = [schema_update(**obj).dict(exclude_unset=True)
                       for id, obj in existing_obj_in.items() if id in update_ids]

        # id удаляемых = разность существующих в бд и входных id
        delete_obj_ids = list(set(db_objs.keys()) - set(existing_obj_in.keys()))

        return delete_obj_ids, update_objs, create_objs

    def get_object(self,
                   db: Session,
                   id: int = None,
                   id_name: str = 'id',
                   is_deleted: bool = False,
                   omit_exception: bool = False)-> Optional[ModelType]:
        """
        get_object - универсальный метод для получения ORM-объекта модели по id.
        Т.к. имя поле id может быть разным (да, почти всегда поле называется 'id',
        но если имя поля другое, что что, новый метод делать?) указывается дополнительно
        имя поля 'id_name'.

        Возвращаем ORM-объект по id или выбрасываем исключение.
        """
        try:
            id_field = getattr(self.model, id_name, self.model.id) if id_name else self.model.id
        except Exception as e:
            if omit_exception:
                return None
            else:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail=f"id_name не указан в base.get, как и поля id"
                )
        query = db.query(self.model).filter(id_field == id)
        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            query = query.filter(self.model.is_deleted == is_deleted)

        first_obj = query.first()
        if not first_obj:
            if omit_exception:
                return None
            else:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"Не найдено записи по id = {id}"
                )
        return first_obj

    def get_all_objects(self,
                        db: Session,
                        is_deleted: bool = False,
                        condition: list = None,
                        omit_exception: bool = False) -> Optional[List[ModelType]]:
        m_name = self.model.__name__
        query = db.query(self.model)
        if condition is not None:
            query = query.filter(*condition)
        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            query = query.filter(self.model.is_deleted == False)
        objs = query.all()
        if not objs and not omit_exception:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Не найдено записи по id = {id}"
                )
        return objs

    def get_valuelist(self,
                      db: Session,
                      name_field: str = 'name',
                      skip: int = 0,
                      limit: int = 100,
                      is_deleted: bool = False) -> Optional[List[dict]]:
        try:
            n_field = getattr(self.model, name_field, self.model.id) if name_field else self.model.id
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"name_field не указан в base.get, как и поля id"
            )
        items = db.query(self.model.id, n_field)

        if is_deleted is not None and hasattr(self.model, 'is_deleted'):
            items = items.filter(self.model.is_deleted == is_deleted)

        items = items.order_by(self.model.id)
        items = items.offset(skip).limit(limit)
        items = items.all()
        value_list = [{'value': i.id, 'name': getattr(i, name_field, 'empty')} for i in items]
        return value_list

    def get_multi_not_deleted(
            self,
            db: Session,
            *,
            skip: int = 0,
            limit: int = 100,
            sort_direction: str = 'asc',
            sort_field: str = 'id',
            filters: dict = None
    ) -> List[ModelType]:
        skip, limit = self._skip_limit_params(skip=skip, limit=limit, filters=filters)
        sort_direction, sort_field = self._sort_params(sort_direction=sort_direction,
                                                       sort_field=sort_field,
                                                       filters=filters)
        model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
        entry = db.query(self.model)
        fil_query = self._make_filter_model(filters_model=filters,
                                            column_descriptors=entry.column_descriptions,
                                            not_deleted=True)
        entry = entry.filter(text(fil_query))\
            .order_by(text(f"{sort_field} {sort_direction}"))\
            .offset(skip).limit(limit)
        entry = entry.all()
        return entry

    def create(
            self,
            db: Session,
            *,
            obj_in: CreateSchemaType = None,
            obj_in_dict: dict = None,
            commit=True,
            is_deleted: bool = None
    ) -> ModelType:
        if obj_in:
            obj_in_data = jsonable_encoder(obj_in, exclude_unset=True)
        elif obj_in_dict:
            obj_in_data = obj_in_dict
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Данные для метода base.create не переданы"
            )
        try:
            if is_deleted:
                db_obj = self.model(**obj_in_data, is_deleted=is_deleted)
            else:
                db_obj = self.model(**obj_in_data)
            db.add(db_obj)
            if commit:
                db.commit()
                db.refresh(db_obj)
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=str(e))
        return db_obj


    def update(
        self,
        db: Session,
        *,
        db_obj: ModelType,
        obj_in: Union[UpdateSchemaType, Dict[str, Any]],
        commit: bool = True
    ) -> ModelType:
        if db_obj:
            obj_data = jsonable_encoder(db_obj)
            upd_data = obj_in if isinstance(obj_in, dict) else obj_in.dict(exclude_unset=True)
            for field in obj_data:
                if field in upd_data:
                    setattr(db_obj, field, upd_data[field])
            try:
                db.add(db_obj)
            except Exception as e:
                raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                    detail=str(e))
            if commit:
                try:
                    db.commit()
                    db.refresh(db_obj)
                except Exception as e:
                    raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                        detail=str(e))
            return db_obj

    @staticmethod
    def multi_update(db: Session, *,
                     objs_in: dict,
                     model: Type[ModelType],
                     commit: bool = True,
                     ):
        """Метод для множественного обновления записей в требуемой таблице"""

        # Перед получением атрибута из модели или получением значения по ключу 'id' не делается проверок, тк
        # ожидается что данные загружаемые в этот метод уже прошли валидацию
        if objs_in:
            # Узнаем совпадает ли кол-во полей у всех входящ объектов
            fields_number = len(objs_in[0])
            for row in objs_in[1:]:
                if len(row) != fields_number:
                    same_length = False
                    break
            else:
                same_length = True
            # Если у всех обновляемых контактов количество полей одинаково -> update одним запросом
            if same_length:
                # Структура object_fields получится в таком роде:
                # {'values': {23: '+79999999999', 45: 'test'}, 'is_main': {25: True, 45: False}}
                ids = []
                object_fields = {}
                for row in objs_in:
                    id = row.pop('id')
                    ids.append(id)
                    for field_name, field_value in row.items():
                        if field_name not in object_fields:
                            object_fields[field_name] = {}
                        object_fields[field_name][id] = field_value
                query = db.query(model).filter(model.id.in_(ids))
                field_cases = {}
                for field_name, values_by_id in object_fields.items():
                    field_cases[getattr(model, field_name)] = case(values_by_id, value=model.id)
                try:
                    query.update(field_cases, synchronize_session=False)
                except Exception as e:
                    raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                        detail=str(e))
            # Если у обновляемых контактов количество полей разное -> update несколькими запросами
            else:
                rows_by_id = {row['id']: row for row in objs_in}
                db_objs = db.query(model).filter(model.id.in_(rows_by_id.keys())).all()
                for obj in db_objs:
                    if obj.id in rows_by_id:
                        for field_name, field_value in rows_by_id[obj.id].items():
                            setattr(obj, field_name, field_value)
                try:
                    db.add_all(db_objs)
                except Exception as e:
                    raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                        detail=str(e))
            if commit:
                try:
                    db.commit()
                except Exception as e:
                    raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                        detail=str(e))
            return True

    def remove(self, db: Session, *, id: int) -> ModelType:
        obj = db.query(self.model).filter(self.model.id == id).first()
        if not obj:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=str(e))
        try:
            db.delete(obj)
            db.commit()
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=str(e))
        return obj

    def remove_by_names(self, db: Session, *, generated_names: List[str] = None) -> ModelType:
        try:
            query_to_remove = db.query(self.model).filter(self.model.name.in_(generated_names))
            query_to_remove.delete()
            db.commit()
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=str(e))
        return True

    def is_deleted(self,
                   db: Session,
                   db_obj: ModelType,
                   field_name: str = 'is_deleted') -> Optional[ModelType]:
        obj_data = jsonable_encoder(db_obj)
        try:
            for field in obj_data:
                if field_name in field:
                    setattr(db_obj, field, True)
            db.add(db_obj)
            db.commit()
            db.refresh(db_obj)
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=str(e))
        return db_obj

    def random_choose(self, db: Session):
        rand = random.randrange(0, db.query(self.model).count())
        rand_row = db.query(self.model)[rand]
        return rand_row

    def random_choose_id(self, db: Session):
        rand = random.randrange(0, db.query(self.model).count())
        rand_row = db.query(self.model)[rand]
        return rand_row.id


class Contact_types(Enum):
        phones = 1
        emails = 2
        sites = 3




    # Старая структура фильтров
    # def _make_filter_model(self,
    #                        filters_model: Filters_model,
    #                        column_descriptors: list,
    #                        not_deleted: bool = True):
    #     result = ''
    #     model_name = f"{self.model.__table__.schema}.{self.model.__name__.lower()}"
    #     column_names = self.__get_column_names_list_(column_descriptors=column_descriptors,
    #                                                  model_name=model_name)
    #     if_delete_filter = f"{model_name}.is_deleted = False"
    #     if not filters_model:
    #         if not_deleted:
    #             result = if_delete_filter
    #         return result
    #     filters = filters_model.filterModel
    #     if filters:
    #         for field_name, conditions in filters.items():
    #             if ((not field_name in column_names) and (not field_name in column_names[model_name])) \
    #                     or conditions.is_empty():
    #                 continue
    #             type, field_cond = None, ''
    #             if not result and not_deleted:
    #                 result += if_delete_filter
    #             has_cond = getattr(conditions, 'conditions', None)
    #             has_operator = getattr(conditions, Operators.name.value, None)
    #             if has_cond and has_operator:
    #                 cond = has_cond
    #                 operator, filter_type = has_operator, getattr(conditions, Filter_types.name.value, None)
    #                 m_name = model_name if field_name in column_names[model_name] else ''
    #                 field_cond = ''.join(
    #                     [self.__condition_to_sql__(sql_string='',
    #                                                field_name=field_name if m_name else column_names[field_name],
    #                                                model_name=m_name,
    #                                                cond=c,
    #                                                operator=operator)
    #                      for c in cond]
    #                      )
    #                 if field_cond.endswith(f' {operator} '):
    #                     field_cond = field_cond[: -len(f' {operator} ')]
    #                 if field_cond:
    #                     result += f' AND ( {field_cond} )'
    #             else:
    #                 # Логика следующего  need_model_name следующая:
    #                 # если field_name найден в column_names[model_name], то
    #                 # нужно вернуть True, так как префикс модели будет необходим
    #                 #
    #                 # Если field_name найдено просто в column_names, то
    #                 # префикс модели НЕ НУЖЕН и нам НУЖНО ВЕРНУТЬ False. В этом
    #                 # случае m_name = '' и __condition_to_sql__ отработает
    #                 # используя только полное имя поля, без префикс модели.
    #                 need_model_name = field_name in column_names[model_name] \
    #                     if model_name in column_names \
    #                     else \
    #                     not (field_name in column_names)
    #                 m_name = model_name if need_model_name else ''
    #                 field_cond = self.__condition_to_sql__(sql_string='',
    #                                                        field_name=field_name if m_name else column_names[field_name],
    #                                                        model_name=m_name,
    #                                                        cond=conditions)
    #                 if field_cond:
    #                     result+=f' AND {field_cond}'
    #
    #     if not result and not_deleted:
    #         result += if_delete_filter
    #     elif result.find(if_delete_filter) > 0 and not_deleted:
    #         result = ''.join(if_delete_filter, ' AND ', result)
    #     return result
    #
    # def _make_filter_query(self,
    #                        filters_model: Filters_model,
    #                        query: Query,
    #                        not_deleted: bool = True):
    #     text_filters = self._make_filter_model(filters_model=filters_model,
    #                                            not_deleted=not_deleted)
    #
    # def _make_filter_model_without_strings(self,
    #                                        filters_model: Filters_model,
    #                                        query: Any,
    #                                        not_deleted: bool = False,
    #                                        subquery: Optional[Any] = None):
    #
    #     if not_deleted is True:
    #         is_deleted = getattr(query.statement.selected_columns, 'is_deleted', None)
    #         if is_deleted is not None:
    #             query = query.filter(is_deleted == False)
    #     filters = filters_model.filterModel
    #     for field_name, conditions in filters.items():
    #         if (getattr(query.statement.selected_columns, field_name, None) is None) or conditions.is_empty():
    #             continue
    #
    #         # Получаем объект поля из query
    #         # Строка: clubs_name -> Column object: public.clubs.name
    #         # Не волнует имя поля подзапроса или таблицы, агрегатное поле или нет. Должно работать в любом случае
    #         field = getattr(query.statement.selected_columns, field_name)
    #
    #         # Устарело
    #         # Если есть подзапрос нам можно в фильтрах использовать имена полей подзапроса даже агрегатные
    #         # if subquery is not None:
    #         #     field = getattr(subquery.c, field_name)
    #         # Если у нас только запрос то получаем column obj из query по имени поля
    #         # Строка: clubs_name -> Column object: public.clubs.name
    #         # else:
    #         #     field = getattr(query.subquery().c, field_name)
    #         #     field = field.base_columns.pop()
    #
    #         has_cond = getattr(conditions, 'conditions', None)
    #         has_operator = getattr(conditions, Operators.name.value, None)
    #
    #         if has_cond and has_operator:
    #             cond = has_cond
    #             operator, filter_type = has_operator, getattr(conditions, Filter_types.name.value, None)
    #
    #             correct_filters = []
    #             for c in cond:
    #                 filter = self.__condition_to_sql_without_strings__(field, c)
    #                 if filter is not None:
    #                     correct_filters.append(filter)
    #
    #             if operator == 'AND':
    #                 query = query.filter(and_(*correct_filters))
    #             elif operator == 'OR':
    #                 query = query.filter(or_(*correct_filters))
    #
    #         else:
    #             filter = self.__condition_to_sql_without_strings__(field, conditions)
    #             query = query.filter(filter)
    #     return query