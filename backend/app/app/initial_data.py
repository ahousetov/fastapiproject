import logging
import pprint

#from app.db.init_db import init_db
from db.init_db import init_db
#from app.db.session import SessionLocal

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from app.core.config import settings

logger.info(pprint.pformat(settings, indent=4))

from db.session import SessionLocal

def init() -> None:
    db = SessionLocal()
    init_db(db)


def main() -> None:
    logger.info("Creating initial data")
    init()
    logger.info("Initial data created")


if __name__ == "__main__":
    main()
