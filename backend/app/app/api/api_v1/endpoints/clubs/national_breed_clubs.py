from typing import List, Optional, Dict, Any
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps
from app.schemas.clubs.national_breed_clubs import OutputNBC
from app.schemas.clubs.national_breed_clubs import InputCreateNBC, OutputDetailNBC, InputUpdateNBC, \
    InputNBCMainInfo, ContactsUpdate, ContactsCreate, \
    MembersUpdate, MembersCreate, OutputEditedNBC, National_breed_clubs_delete, \
    ContactsBlocksView, ContactInfoBlockView, MembersBlocksView, MemberInfoBlockView, \
    BreedsBlocksView, BreedsInfoBlockView, MainBlockView

router = APIRouter()


@router.get("/", response_model=List[OutputNBC])
def read_national_breed_clubs(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'nbc_name'
) -> List[OutputNBC]:
    """
    Получить список НКП. Помеченные удалёнными не отображаются.
    """

    try:
        national_breed_clubs = crud.national_breed_clubs.get_all(
            db,
            skip=skip,
            limit=limit,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return national_breed_clubs


@router.get("/{national_breed_club_id}", response_model=OutputDetailNBC)
def get_national_breed_club_by_id(
        db: Session = Depends(deps.get_db),
        *,
        national_breed_club_id: int
) -> Dict[str, Any]:
    """
    Получить НКП по id. Помеченные удалёнными не отображаются.
    """
    try:
        national_breed_club = crud.national_breed_clubs.get(
            db=db,
            id=national_breed_club_id
        )
        nbc = national_breed_club.get('nbc', {})
        main = MainBlockView(**nbc)
        contacts = ContactsBlocksView(**national_breed_club)
        members = MembersBlocksView(**national_breed_club)
        breeds = BreedsBlocksView(**national_breed_club)
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    if not national_breed_club:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=str("НКП с указанным id не существует"))

    return dict(blocks=[main, members, breeds, contacts])


@router.patch("/", response_model=List[OutputNBC])
def filter_national_breed_clubs(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: Optional[str] = None,
        sort_field: Optional[str] = None,
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    """
    Получить список НКП с фильтрами. Помеченные удалёнными не отображаются.
    """

    try:
        national_breed_clubs = crud.national_breed_clubs.get_all(
            db,
            skip=skip,
            limit=limit,
            sort_direction=sort_direction,
            sort_field=sort_field,
            filters=filters,
        )
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return national_breed_clubs


@router.post("/", status_code=status.HTTP_201_CREATED)
def create_national_breed_club(
        *,
        db: Session = Depends(deps.get_db),
        nbc_in: InputCreateNBC
) -> None:
    """
    Создание нового Национального Клуба Пород.
    """
    try:
        contacts = [dict(contact)
                    for contact in nbc_in.contacts] if nbc_in.contacts else []
        breeds = [dict(breed) for breed in nbc_in.breeds] if nbc_in.breeds else []
        members = [
            dict(member) for member in nbc_in.presidium_members] if nbc_in.presidium_members else []

        nbc = nbc_in.dict(include=InputNBCMainInfo().dict().keys(), exclude_unset=True)
        create_data = dict(
            nbc=nbc,
            members=members,
            breeds=breeds,
            contacts=contacts
        )
        crud.national_breed_clubs.create(db, create_data=create_data)
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )


@router.put("/{nbc_id}", status_code=status.HTTP_200_OK)
def create_national_breed_club(
        *,
        db: Session = Depends(deps.get_db),
        nbc_id: int,
        nbc_in: InputUpdateNBC,
) -> None:
    """
    Редактирование Национального Клуба Пород.
    """
    try:
        contacts = [dict(contact)
                    for contact in nbc_in.contacts] if nbc_in.contacts else []
        breeds = [dict(breed) for breed in nbc_in.breeds] if nbc_in.breeds else []
        members = [
            dict(member) for member in nbc_in.presidium_members] if nbc_in.presidium_members else []

        nbc = nbc_in.dict(include=InputNBCMainInfo().dict().keys(), exclude_unset=True)
        update_data = dict(
            nbc=nbc,
            members=members,
            breeds=breeds,
            contacts=contacts
        )
        crud.national_breed_clubs.update(db, id=nbc_id, update_data=update_data)
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )


@router.delete("/{nbc_id}", response_model=National_breed_clubs_delete)
def make_deleted_club_address(
        nbc_id: int,
        db: Session = Depends(deps.get_db),
) -> National_breed_clubs_delete:
    """
    Помечаем контакт, как удалённый.
    """
    try:
        nbc = crud.national_breed_clubs.get_object(db, id=nbc_id)
        crud.national_breed_clubs.is_deleted(db, db_obj=nbc)
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return crud.national_breed_clubs._orm_obj_dict(nbc)
