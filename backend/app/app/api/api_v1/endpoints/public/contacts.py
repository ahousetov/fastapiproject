from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps
from app.models.public.contacts import Contacts
from app.schemas.public.contacts import ContactsInput, RelProfilesContactsInput

router = APIRouter()


@router.get("/", response_model=List[schemas.Contacts])
def read_contacts(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Получить общий список типов населённых пунтков. Помеченные удалёнными
    не отображаются.
    """
    contacts = crud.contacts.get_all(db, skip=skip, limit=limit)
    return contacts


@router.get("/{contact_id}", response_model=schemas.Contact_blocks)
def read_contact_by_id(
        contact_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить конкретный контакт по id
    """
    try:
        contact = crud.contacts.get(db, id=contact_id)

        block = dict(
            contacts=[contact]
        )
        # contact_blocks = Contact_blocks(
        contact_blocks = dict(
            item_title=contact['value'],
            blocks=[block]
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return contact_blocks


@router.get("/profile/{profile_id}", response_model=List[schemas.Contacts])
def read_contacts_by_id(
        profile_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить список контактов по id профиля.
    """
    try:
        contacts = crud.contacts.get_by_profile_id(
            db=db,
            profile_id=profile_id
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return contacts


@router.put("/{contact_id}", status_code=status.HTTP_200_OK)
def update_contact(
        contact_id: int,
        contact_in: schemas.Contacts_update,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Обновить данные по контакту.
    """
    try:
        contact_data = ContactsInput(
            **contact_in.dict(exclude_unset=True)).dict(exclude_unset=True)
        profile_data = RelProfilesContactsInput(
            **contact_in.dict(exclude_unset=True)).dict(exclude_unset=True)

        update_data = dict(contact=contact_data,
                           rel_profiles_contacts=profile_data)

        crud.contacts.update(db, id=contact_id, update_data=update_data)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )


@router.post("/", status_code=status.HTTP_201_CREATED)
def create_contact(
        *,
        db: Session = Depends(deps.get_db),
        distr_in: schemas.Contacts_create
) -> Any:
    """
    Создаём новый контакт.

    id: int;
    value: str;
    description: Optional[str];
    is_main: bool;
    type_id: Optional[int];
    profile_id: Optional[int];
    """
    try:
        contact_data = ContactsInput(
            **distr_in.dict(exclude_unset=True)).dict(exclude_unset=True)
        profile_data = RelProfilesContactsInput(
            **distr_in.dict(exclude_unset=True)).dict(exclude_unset=True)

        create_data = dict(contact=contact_data,
                           rel_profiles_contacts=profile_data)

        crud.contacts.create(db, create_data=create_data)

    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )


@router.delete("/{contact_id}", response_model=schemas.Contacts_delete)
def make_deleted_contact(
        contact_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Помечаем контакт, как удалённый.
    """
    try:
        contact = db.query(Contacts).filter(Contacts.id == contact_id).first()
        contact = crud.contacts.is_deleted(db, db_obj=contact)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return schemas.Contacts_delete(**crud.contacts._orm_obj_dict(contact))
