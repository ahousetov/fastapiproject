from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps
from app.models.public.club_address import Club_addresses

router = APIRouter()


@router.get("/", response_model=List[schemas.Club_address])
def read_club_address(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
) -> Any:
    """
    Получить общий список адресных данных. Помеченные удалёнными
    не отображаются.
    """
    result_list = crud.club_address.get_all(
        db,
                                            skip=skip,
                                            limit=limit,
                                            sort_direction=sort_direction,
        sort_field=sort_field
    )
    return result_list


@router.post("/", response_model=schemas.Club_address)
def create_club_address(
        *,
        db: Session = Depends(deps.get_db),
        club_address_in: schemas.Club_address_create
) -> Any:
    """
    Создаём новый тип населённого пункта. Параметр - имя.
    """
    try:
        club_address = crud.club_address.create(
            db=db,
            obj_in=club_address_in
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_406_NOT_ACCEPTABLE,
            detail="Невозможно создать запись адресной информации"
        )
    return club_address


@router.get("/{id}", response_model=schemas.Club_addresses_blocks)
def read_club_address_by_id(
        id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить адресные данные по id
    """
    try:
        club_address = crud.club_address.get(db=db, id=id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Внутренняя ошибка при получении адресов клубов"
        )
    return {'blocks': [club_address]}


@router.patch("/", response_model=List[schemas.Club_address])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> Any:
    result_list = crud.club_address.get_all(
        db,
        skip=skip,
                                            limit=limit,
                                            sort_direction=sort_direction,
                                            sort_field=sort_field,
                                            filters=filters)
    return result_list


@router.get("/profile/{profile_id}", response_model=schemas.Club_address)
def read_club_address_by_profile_id(
        profile_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить список адресных данных по id профиля.
    """
    try:
        club_address = crud.club_address.get(db=db, profile_id=profile_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Внутренняя ошибка при получении адресов клубов"
        )
    return club_address


@router.put("/{club_address_id}", response_model=schemas.Club_address)
def update_club_address(
        club_address_id: int,
        club_address_in: schemas.Club_address_update,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Обновить адресные данные.
    """
    try:
        club_address = crud.club_address.get(db, id=club_address_id)
        club_address = crud.club_address.update(db,
                                                db_obj=club_address,
                                                obj_in=club_address_in)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail=str(e))
    return club_address


@router.delete("/{club_address_id}", response_model=schemas.Club_address_delete)
def make_deleted_club_address(
        club_address_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Помечаем контакт, как удалённый.
    """
    try:
        club_address = db.query(Club_addresses).filter(
            Club_addresses.id == club_address_id).first()
        club_address = crud.club_address.is_deleted(db, db_obj=club_address)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail=str(e))
    return club_address
