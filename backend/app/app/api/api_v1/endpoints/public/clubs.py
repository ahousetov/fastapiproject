from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=List[schemas.Clubs_db])
def read_clubs(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
) -> Any:
    """
    Получить список клубов.
    """
    clubs_list = crud.clubs_crud_explicit.get_all(db=db,
                                                  skip=skip,
                                                  limit=limit,
                                                  sort_direction=sort_direction,
                                                  sort_field=sort_field,
                                                  filters=None)
    return clubs_list


@router.patch("/", response_model=List[schemas.Clubs_db])
def filtered_clubs(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> Any:
    """
    Получить фильтрованный список клубов.
    """
    filtered_clubs = crud.clubs_crud_explicit.get_all(db=db,
                                                      skip=skip,
                                                      limit=limit,
                                                      sort_direction=sort_direction,
                                                      sort_field=sort_field,
                                                      filters=filters)
    return filtered_clubs


@router.get("/{profile_id}", response_model=schemas.Clubs_by_id)
def get_club_by_id(
        db: Session = Depends(deps.get_db),
        profile_id: int = None
) -> Any:
    """
    Получить список клубов.
    """
    try:
        data = crud.clubs_crud_explicit.get(db=db, id=profile_id)
        main = data.get('main')
        main_schema = schemas.public.clubs.Main_block_view(**main)
        contacts_block = schemas.public.clubs.Contacts_blocks_view(**data)
        address_block = schemas.public.clubs.Address_blocks_view(**data)
        payment_block = schemas.public.clubs.Payment_blocks(**data)
        history = data.get('history')
        history_block = schemas.public.clubs.History_blocks(**history) if history \
            else schemas.public.clubs.History_blocks()
        access = data.get('access')
        prof_t_r = access.get('prof_t_r')
        active_ids = access.get('active_ids')
        access = schemas.public.clubs.Access_blocks_view(
            access=[
                schemas.public.clubs.Access_block(
                    access_name=p[3], value=(
                        p[0] in active_ids)) for p in prof_t_r]
        )
        clubs_by_id = schemas.Clubs_by_id(title_item=main_schema.club_full_name,
                                      blocks=[main_schema, contacts_block, address_block, payment_block, history_block,
                                              access])
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e)
        )
    return clubs_by_id


@router.get("/editinfo/{profile_id}", response_model=schemas.Clubs_by_id)
def get_edit_info_club_by_id(
        db: Session = Depends(deps.get_db),
        profile_id: int = None
) -> schemas.Clubs_by_id:
    """
    Получить информацию для редактирования.
    """
    try:
        data = crud.clubs_crud_explicit.get(db=db, id=profile_id)

        main = data.get('main')
        main_schema = schemas.public.clubs.Main_block_edit(**main)

        contacts_block = schemas.public.clubs.Contacts_blocks_edit(**data)

        address_block = schemas.public.clubs.Address_blocks_edit(**data)

        payment_block = schemas.public.clubs.Payment_blocks(**data)

        history = data.get('history')
        history_block = schemas.public.clubs.History_blocks(**history) if history \
            else schemas.public.clubs.History_blocks()

        access = data.get('access')
        active_ids = access.get('active_ids')
        access = schemas.public.clubs.Access_blocks_edit(access=[i for i in active_ids])

        club_edit_info = schemas.Clubs_by_id(title_item=main_schema.club_full_name,
                                             blocks=[main_schema, contacts_block, address_block,
                                                     payment_block, history_block, access])

    except HTTPException as he:
         raise he
    except Exception as e:
         raise HTTPException(
             status_code=status.HTTP_501_NOT_IMPLEMENTED,
             detail=str(e)
         )
    return club_edit_info


@router.put("/{profile_id}", status_code=status.HTTP_200_OK)
def update_clubs(db: Session = Depends(deps.get_db),
                 profile_id: int = None,
                 clubs_in: schemas.Clubs_update = None) -> None:
    try:
        # update_data = dict(
        #     legal_informations=schemas.public.clubs.LegalInfoEdit(**clubs_in.dict()).dict(exclude_unset=True),
        #     main=clubs_in.dict(include=schemas.public.clubs.Main_block_edit().dict().keys(), exclude_unset=True),
        #     contacts=[dict(contact) for contact in clubs_in.contacts] if clubs_in.contacts else [],
        #     addresses=[dict(address) for address in clubs_in.addresses] if clubs_in.addresses else [],
        #     payments=[dict(pay) for pay in clubs_in.payments] if clubs_in.payments else [],
        #     history=clubs_in.dict(include=schemas.public.clubs.History_blocks().dict().keys()),
        #     catalogs=dict(),   # catalogs_dict,
        #     access=dict()      # active_ids=active_ids, prof_t_r=prof_t_r)
        # )
        contacts = [dict(contact)
                    for contact in clubs_in.contacts] if clubs_in.contacts else []
        addresses = [address.dict(by_alias=True)
                     for address in clubs_in.addresses] if clubs_in.addresses else []
        payments = [dict(pay) for pay in clubs_in.payments] if clubs_in.payments else []
        clubs_in = clubs_in.dict(exclude_unset=True)
        update_data = dict(
            legal_informations=schemas.public.clubs.LegalInfoEdit(
                **clubs_in).dict(exclude_unset=True),
            bank_data=schemas.public.clubs.BankDataEdit(
                **clubs_in).dict(exclude_unset=True),
            profiles_federations=schemas.public.clubs.ProfilesFederationsEdit(
                **clubs_in).dict(exclude_unset=True),
            history=schemas.public.clubs.History_blocks(
                **clubs_in).dict(exclude_unset=True),
            contacts=contacts,
            addresses=addresses,
            payments=payments,
            catalogs=dict(),   # catalogs_dict,
            access=dict()      # active_ids=active_ids, prof_t_r=prof_t_r)
        )
        crud.clubs_crud_explicit.update(db=db, id=profile_id, update_data=update_data)
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
