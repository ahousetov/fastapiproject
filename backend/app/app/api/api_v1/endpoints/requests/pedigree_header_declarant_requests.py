from typing import Any, List, Optional
from fastapi import APIRouter, Body, Depends, HTTPException, Query
from fastapi.exceptions import RequestValidationError
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps
from app.core.config import settings


router = APIRouter()


@router.get("/", response_model=Any)
def read_pedigree_header_declarant_requests(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        # filters: dict = None
) -> Any:
    """
    Получить список заявок на изготовление родословной в личном кабинете кинолога.
    """
    try:
        pedigree_header_declarant_requests = crud.pedigree_header_declarant_requests. \
            get_multi_with_extradata(db,
                                     skip=skip,
                                     limit=limit,
                                     sort_direction=sort_direction,
                                     sort_field=sort_field
                                     # filters=filters2
                                     )
    except Exception as e:
        raise HTTPException(status_code=404, detail=str(e))
    return pedigree_header_declarant_requests
