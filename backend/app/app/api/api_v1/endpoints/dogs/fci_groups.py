from typing import Any, List, Optional, Dict
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
# from app.exceptions import API_JSON_Exception
from app import crud, models, schemas
from app.api import deps
from app.core.config import settings


router = APIRouter()


@router.get("/", response_model=List[schemas.FCI_group])
def read_fci_groups(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'number'
) -> List[Dict[str, Any]]:
    """
    Получить полный список групп пород собак. Помеченные удалёнными не отображаются.
    """
    fci_groups = crud.fci_groups.get_all(db,
                                         skip=skip,
                                         limit=limit,
                                         sort_direction=sort_direction,
                                         sort_field=sort_field)
    return fci_groups


@router.get("/valuelist", response_model=List[schemas.FCI_group_valuelist])
def read_fci_groups_valuelist(
        db: Session = Depends(deps.get_db)
) -> List[schemas.FCI_group_valuelist]:
    """
    Получить полный список секций пород собак. Помеченные удалёнными не отображаются.
    """
    value_list = crud.fci_groups.get_valuelist(db=db)
    return value_list


@router.get("/{fci_group_id}", response_model=schemas.FCI_groups_blocks)
def read_fci_group_by_id(
        fci_group_id: int,
        db: Session = Depends(deps.get_db),
) -> Dict[str, Any]:
    """
    Выбираем группу пород собак по id. Помеченные is_deleted=True не учитываются.
    """
    try:
        fci_group = crud.fci_groups.get(db, id=fci_group_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return fci_group


@router.patch("/", response_model=List[schemas.FCI_group])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'number',
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    fci_groups = crud.fci_groups.get_all(
        db,
        skip=skip,
                                         limit=limit,
                                         sort_direction=sort_direction,
                                         sort_field=sort_field,
                                         filters=filters)
    return fci_groups


@router.post("/",
             status_code=status.HTTP_201_CREATED)
def create_fci_group(
        *,
        db: Session = Depends(deps.get_db),
        fci_group_in: schemas.FCI_group_create
) -> Any:
    """
    Создаём новую группу пород собак. Входной параметр - имя типа.
    """
    fci_group = crud.fci_groups.create(db=db, obj_in=fci_group_in)


@router.put("/{fci_group_id}",
            status_code=status.HTTP_200_OK)
def update_fci_group(
        fci_group_id: int,
        fci_group_in: schemas.FCI_group_update,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Обновляем данные по группе пород собак. Обновляются только названия.
    """
    try:
        # реализация метода get в CRUDFCI_groups возвращает схему FCI_groups_block
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        fci_group = crud.fci_groups.get_object(db=db, id=fci_group_id)
        fci_group = crud.fci_groups.update(db=db, db_obj=fci_group, obj_in=fci_group_in)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )


@router.delete("/{fci_group_id}", response_model=schemas.FCI_group)
def mark_as_deleted_fci_group(
        fci_group_id: int,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Помечаем группу пород собак как удалённую (if_deleted == True) по его id.
    """
    try:
        # реализация метода get в CRUDFCI_groups возвращает схему FCI_groups_block
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        fci_group = crud.fci_groups.get_object(db=db, id=fci_group_id)
        fci_group = crud.fci_groups.is_deleted(db=db, db_obj=fci_group)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return fci_group
