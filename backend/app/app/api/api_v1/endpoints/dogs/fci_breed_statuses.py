from typing import Any, List, Dict

from fastapi import APIRouter, Body, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps
from app.core.config import settings


router = APIRouter()


@router.get("/", response_model=List[schemas.FCI_breed_statuses])
def read_fci_breed_statuses(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
) -> List[Dict[str, Any]]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    fci_breed_statuses = crud.fci_breed_statuses.get_all(db,
                                                         skip=skip,
                                                         limit=limit,
                                                         sort_direction=sort_direction,
                                                         sort_field=sort_field)
    return fci_breed_statuses


@router.get("/valuelist", response_model=List[schemas.FCI_breed_statuses_valuelist])
def read_fci_breed_statuses_valuelist(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
) -> List[schemas.FCI_breed_statuses_valuelist]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    value_list = crud.fci_breed_statuses.get_valuelist(db=db)
    return value_list


@router.get("/{id}", response_model=schemas.FCI_breed_statuses_blocks)
def read_fci_breed_statuses(
        db: Session = Depends(deps.get_db),
        id: int = None
) -> Dict[str, Any]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    fci_breed_statuses = crud.fci_breed_statuses.get(db, id=id)

    return fci_breed_statuses


@router.patch("/", response_model=List[schemas.FCI_breed_statuses])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    """

    """
    fci_breed_statuses = crud.fci_breed_statuses.get_all(
        db,
          skip=skip,
                                                         limit=limit,
                                                         sort_direction=sort_direction,
                                                         sort_field=sort_field,
                                                         filters=filters)
    return fci_breed_statuses
