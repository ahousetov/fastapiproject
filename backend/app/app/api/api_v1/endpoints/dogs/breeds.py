from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud,schemas
from app.api import deps
from app.schemas.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups_aliased
from app.schemas.dogs.breeds import Breeds_base


router = APIRouter()


@router.get("/", response_model=List[schemas.Breeds])
def read_breeds(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'fci_number',
        filters: dict = None
) -> Any:
    """
    Получить общий список пород собак. Помеченные удалёнными не отображаются.
    """
    breeds = crud.breeds.get_all(
        db,
        skip=skip,
        limit=limit,
        sort_direction=sort_direction,
        sort_field=sort_field,
        filters=filters
        )
    return breeds


@router.get("/{breed_id}", response_model=schemas.Breeds_blocks)
def read_breed_by_id(
        breed_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить конкретную породу по id
    """
    try:
        breed = crud.breeds.get(db, id=breed_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return {'item_title': breed.name_rus, 'blocks': [breed]}


@router.put("/{breed_id}",
            status_code=status.HTTP_200_OK)
def update_breed(
        breed_id: int,
        breed_in: schemas.Breeds_update,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Обновить данные по id породы. Обновляется только название.
    """
    try:

        rel_breeds_sections_groups = Rel_breeds_sections_groups_aliased(
            **breed_in.dict(exclude_unset=True)
        ).dict(exclude_unset=True)
        breed = Breeds_base(**breed_in.dict(exclude_unset=True)).dict()

        update_data = dict(breed=breed,
                           rel_breeds_sections_groups=rel_breeds_sections_groups)

        crud.breeds.update(db=db, id=breed_id, update_data=update_data)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))


@router.patch("/", response_model=List[schemas.Breeds])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'fci_number',
        filters: schemas.Filters_model = None
) -> Any:
    breeds = crud.breeds.get_all(
        db,
        skip=skip,
        limit=limit,
        sort_direction=sort_direction,
        sort_field=sort_field,
        filters=filters
    )
    return breeds


@router.post("/", status_code=201)
def create_breed(
        *,
        db: Session = Depends(deps.get_db),
        breed_in: schemas.Breeds_create
) -> Any:
    """
    Создаём новую пород собак. Входной параметр - имя типа.
    """
    rel_breeds_sections_groups = Rel_breeds_sections_groups_aliased(
        **breed_in.dict(exclude_unset=True)
    ).dict(exclude_unset=True)
    breed = Breeds_base(**breed_in.dict(exclude_unset=True)).dict()

    create_data = dict(breed=breed,
                       rel_breeds_sections_groups=rel_breeds_sections_groups)

    try:
        breed = crud.breeds.create(db=db, create_data=create_data)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))


@router.delete("/{breed_id}", response_model=schemas.Breeds_delete)
def make_deleted_breed(
        breed_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Помечаем тип населённого пункта, как удалённый.
    """
    try:
        # реализация метода get в CRUDBreeds возвращает схему Breeds
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        breed = crud.breeds.get_object(db=db, id=breed_id)
        breed = crud.breeds.is_deleted(db, db_obj=breed)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))
    return breed
