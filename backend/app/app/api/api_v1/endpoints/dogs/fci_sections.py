from typing import Any, List, Dict
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps
from app.core.config import settings


router = APIRouter()


@router.get("/", response_model=List[schemas.FCI_section])
def read_fci_sections(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'number'
) -> Any:
    """
    Получить полный список секций пород собак. Помеченные удалёнными не отображаются.
    """
    fci_sections = crud.fci_sections.get_multi_not_deleted(
        db,
                                                           skip=skip,
                                                           limit=limit,
                                                           sort_direction=sort_direction,
                                                           sort_field=sort_field)
    return fci_sections


@router.get("/valuelist", response_model=List[schemas.FCI_section_valuelist])
def read_fci_sections_valuelist(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'number'
) -> List[schemas.FCI_section_valuelist]:
    """
    Получить полный список секций пород собак. Помеченные удалёнными не отображаются.
    """
    fci_sections = crud.fci_sections.get_multi_not_deleted(db,
                                                           skip=skip,
                                                           limit=limit,
                                                           sort_direction=sort_direction,
                                                           sort_field=sort_field)
    value_list = [
        schemas.FCI_section_valuelist(
            value=fs.id,
            name=fs.name) for fs in fci_sections]
    return value_list


@router.get("/{fci_section_id}", response_model=schemas.FCI_sections_blocks)
def read_fci_section_by_id(
        fci_section_id: int,
        db: Session = Depends(deps.get_db),
) -> Dict[str, Any]:
    """
    Выбираем секцию пород собак по id. Помеченные is_deleted=True не учитываются.
    """
    try:
        fci_section = crud.fci_sections.get(db, id=fci_section_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return fci_section


@router.post("/", status_code=201)
def create_fci_section(
        *,
        db: Session = Depends(deps.get_db),
        fci_section_in: schemas.FCI_section_create
) -> Any:
    """
    Создаём новую секцию пород собак. Входной параметр - имя типа.
    """
    crud.fci_sections.create(db=db, obj_in=fci_section_in)


@router.put("/{fci_section_id}", response_model=schemas.FCI_section)
def update_fci_section(
        fci_section_id: int,
        fci_section_in: schemas.FCI_section_update,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Обновляем данные по секции пород собак. Обновляются только названия.
    """
    try:
        # реализация метода get в CRUDFCI_sections возвращает схему FCI_sections
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        fci_section = crud.fci_sections.get_object(db=db, id=fci_section_id)
        fci_section = crud.fci_sections.update(
            db=db, db_obj=fci_section, obj_in=fci_section_in)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    return fci_section


@router.patch("/", response_model=List[schemas.FCI_section])
def read_fci_sections(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'number',
        filters: schemas.Filters_model = None
) -> Any:
    """
    Получить полный список секций пород собак. Помеченные удалёнными не отображаются.
    """
    fci_sections = crud.fci_sections.get_multi_not_deleted(db,
                                                           skip=skip,
                                                           limit=limit,
                                                           sort_direction=sort_direction,
                                                           sort_field=sort_field,
                                                           filters=filters)
    return fci_sections


@router.delete("/{fci_section_id}", response_model=schemas.FCI_section)
def mark_as_deleted_fci_section(
        fci_section_id: int,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Помечаем секцию пород собак как удалённую (if_deleted == True) по его id.
    """
    try:
        # реализация метода get в CRUDFCI_sections возвращает схему FCI_sections
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        fci_section = crud.fci_sections.get_object(db=db, id=fci_section_id)
        fci_section = crud.fci_sections.is_deleted(db=db, db_obj=fci_section)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    return fci_section
