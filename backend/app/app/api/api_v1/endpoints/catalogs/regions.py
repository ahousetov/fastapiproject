from typing import Any, List, Dict
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=List[schemas.Region])
def read_regions(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Получить полный список регионов. Помеченные удалёнными не отображаются.
    """
    regions = crud.regions.get_all(db, skip=skip, limit=limit)
    return regions


@router.get("/valuelist", response_model=List[schemas.Region_valuelist])
def read_regions_valuelist(
        db: Session = Depends(deps.get_db)
) -> List[schemas.Region_valuelist]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    value_list = crud.regions.get_valuelist(db=db)
    return value_list


@router.get("/{region_id}", response_model=schemas.Region_blocks)
def read_region_by_id(
        region_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Выбираем регион по id. Помеченные is_deleted=True не учитываются.
    """
    try:
        region = crud.regions.get(db, id=region_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return {'item_title': region.name, 'blocks': [region]}


@router.post("/", response_model=schemas.Region)
def create_region(
        *,
        db: Session = Depends(deps.get_db),
        region_in: schemas.Region_create
) -> Any:
    """
    Создаём новый регион. Входной параметр - имя региона.
    """
    region = crud.regions.create(db=db, obj_in=region_in)
    return region


@router.patch("/", response_model=List[schemas.Region])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    try:
        regions = crud.regions.get_all(
            db,
            skip=skip,
                                       limit=limit,
                                       sort_direction=sort_direction,
                                       sort_field=sort_field,
                                       filters=filters
        )
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return regions


@router.put("/{region_id}", response_model=schemas.Region)
def update_region(
        region_id: int,
        region_in: schemas.Region_update,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Обновляем данные по региону. Обновляются только названия.
    """
    try:
        region = crud.regions.get_object(db=db, id=region_id, is_deleted=False)
        region = crud.regions.update(db=db, db_obj=region, obj_in=region_in)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    return region


@router.delete("/{region_id}", response_model=schemas.Region)
def mark_as_deleted_region(
        region_id: int,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Помечаем регион как удалённый (if_deleted == True) по его id.
    """
    try:
        region = crud.regions.get_object(db=db, id=region_id, is_deleted=False)
        region = crud.regions.is_deleted(db=db, db_obj=region)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    return region
