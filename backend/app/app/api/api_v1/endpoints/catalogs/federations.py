from typing import Any, List, Dict

from fastapi import APIRouter, Body, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import case
from sqlalchemy.dialects.mysql import insert

from app import crud, models, schemas
from app.api import deps
from app.models import Contacts
from app.core.config import settings
from app.schemas.catalogs.federations import ContactsUpdate, ContactsCreate, Contact_block, \
    Federations_block, LegalInformationsInput

router = APIRouter()


@router.get("/", response_model=List[schemas.Federations])
def read_federations(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> List[Dict[str, Any]]:
    """
    Получить общий список федеральных округов. Помеченные удалёнными не отображаются.
    """
    federations = crud.federations.get_all(db=db, skip=skip, limit=limit)
    return federations


@router.get("/valuelist", response_model=List[schemas.Federation_info_value_list])
def federations_value_list(db: Session = Depends(deps.get_db)
                           ) -> List[schemas.Federation_info_value_list]:
    """
    Получить общий список федеральных округов. Помеченные удалёнными не отображаются.
    """
    value_list = crud.federations.get_valuelist(db=db)
    return value_list


@router.get("/{federation_id}", response_model=schemas.Federations_blocks)
def read_federation_by_id(
        federation_id: int,
        db: Session = Depends(deps.get_db),
) -> dict:
    """
    Получить конкретный округ по id
    """
    try:
        data = crud.federations.get(db=db, id=federation_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    federation = data.get('federation', {})
    federations_block = Federations_block(**federation)
    contacts_block = Contact_block(**data)
    return dict(title=crud.federations.title,
                item_title=federations_block.short_name,
                blocks=[federations_block, contacts_block])


@router.put("/{federation_id}", status_code=status.HTTP_200_OK)
def update_federation(
        federation_id: int,
        federation_in: schemas.Federations_update,
        db: Session = Depends(deps.get_db)
) -> None:
    """
    Обновить данные по Федерации. Обновляются данные только в таблице public.legal_informations и
    public.contacts
    """
    try:
        contacts = [
            dict(contact) for contact in federation_in.contacts] if federation_in.contacts else []
        legal_informations = schemas.catalogs.federations.LegalInformationsInput(
            **(federation_in.dict(exclude_unset=True))).dict()
        update_data = dict(
            contacts=contacts,
            legal_informations=legal_informations
        )
        crud.federations.update(db, id=federation_id, update_data=update_data)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )


@router.patch("/", response_model=List[schemas.Federations])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    federations = crud.federations.get_all(db,
                                           skip=skip,
                                           limit=limit,
                                           sort_direction=sort_direction,
                                           sort_field=sort_field,
                                           filters=filters)
    return federations


# @router.post("/", response_model=schemas.Federations)
# def create_federation(
#         *,
#         db: Session = Depends(deps.get_db),
#         federation_id: schemas.Federations_create
# ) -> Any:
#     """
#     Создаём новый федеральный округ. Параметр - имя.
#     """
#     federation = crud.federations.create_not_deleted(db=db,
#                                                      obj_in=federation_id)
#     return federation
#
#
# @router.delete("/{federation_id}", response_model=schemas.Federations)
# def make_deleted_federation(
#         federation_id: int,
#         db: Session = Depends(deps.get_db),
# ) -> Any:
#     """
#     Помечаем федеральный округ, как удалённый.
#     """
#     try:
#         federation = crud.federations.get(db, id=federation_id)
#         federation = crud.federations.is_deleted(db, db_obj=federation)
#     except Exception as e:
#         raise HTTPException(status_code=400, detail=e)
#     return federation
