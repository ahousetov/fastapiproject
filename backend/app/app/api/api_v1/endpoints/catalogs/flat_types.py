from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps
from app.core.config import settings


router = APIRouter()


@router.get("/", response_model=List[schemas.Flat_type])
def read_flat_types(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Получить полный список типов помещений. Помеченные удалёнными не отображаются.
    """
    flat_types = crud.flat_types.get_all(db, skip=skip, limit=limit)
    return flat_types


@router.get("/valuelist", response_model=List[schemas.Flat_type_valuelist])
def read_flat_types_valuelist(
        db: Session = Depends(deps.get_db)
) -> List[schemas.Flat_type_valuelist]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    value_list = crud.flat_types.get_valuelist(db=db)
    return value_list


@router.get("/{flat_type_id}", response_model=schemas.Flat_type_blocks)
def read_flat_type_by_id(
        flat_type_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Выбираем тип помещений по id. Помеченные is_deleted=True не учитываются.
    """
    try:
        flat_type = crud.flat_types.get(db, id=flat_type_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return {'item_title': flat_type.name, 'blocks': [flat_type]}


@router.post("/", response_model=schemas.Flat_type)
def create_flat_type(
        *,
        db: Session = Depends(deps.get_db),
        flat_type_in: schemas.Flat_type_create
) -> Any:
    """
    Создаём новый тип помещений. Входной параметр - имя типа.
    """
    flat_type = crud.flat_types.create(db=db, obj_in=flat_type_in)
    return flat_type


@router.put("/{flat_type_id}",
            status_code=status.HTTP_200_OK,
            response_model=schemas.Flat_type)
def update_flat_type(
        flat_type_id: int,
        flat_type_in: schemas.Flat_type_update,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Обновляем данные по типу помещений. Обновляются только названия.
    """
    try:
        flat_type = crud.flat_types.get_object(db=db, id=flat_type_id)
        flat_type = crud.flat_types.update(db=db, db_obj=flat_type, obj_in=flat_type_in)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    return flat_type


@router.delete("/{flat_type_id}",
               status_code=status.HTTP_200_OK,
               response_model=schemas.Flat_type)
def mark_as_deleted_flat_type(
        flat_type_id: int,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Помечаем тип помещений как удалённый (if_deleted == True) по его id.
    """
    try:
        flat_type = crud.flat_types.get_object(db=db, id=flat_type_id)
        flat_type = crud.flat_types.is_deleted(db=db, db_obj=flat_type)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    return flat_type
