from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=List[schemas.House_type])
def read_house_types(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Получить полный список типов домов. Помеченные удалёнными не отображаются.
    """
    house_types = crud.house_types.get_all(db, skip=skip, limit=limit)
    return house_types


@router.get("/valuelist", response_model=List[schemas.House_type_valuelist])
def read_house_types_valuelist(
        db: Session = Depends(deps.get_db)
) -> List[schemas.House_type_valuelist]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    value_list = crud.house_types.get_valuelist(db=db)
    return value_list


@router.get("/{house_type_id}", response_model=schemas.House_type_blocks)
def read_house_type_by_id(
        house_type_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Выбираем тип домов по id. Помеченные is_deleted=True не учитываются.
    """
    try:
        house_type = crud.house_types.get(db, id=house_type_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return {'title': 'Тип домов',
            'item_title': house_type.name,
            'blocks': [house_type]}


@router.post("/", response_model=schemas.House_type)
def create_house_type(
        *,
        db: Session = Depends(deps.get_db),
        house_type_in: schemas.House_type_create
) -> Any:
    """
    Создаём новый тип домов. Входной параметр - имя типа.
    """
    house_type = crud.house_types.create(db=db, obj_in=house_type_in)
    return house_type


@router.put("/{house_type_id}", response_model=schemas.House_type)
def update_house_type(
        house_type_id: int,
        house_type_in: schemas.House_type_update,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Обновляем данные по типу домов. Обновляются только названия.
    """
    try:
        house_type = crud.house_types.get_object(
            db=db, id=house_type_id, is_deleted=False)
        house_type = crud.house_types.update(
            db=db, db_obj=house_type, obj_in=house_type_in)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    return house_type


@router.delete("/{house_type_id}", response_model=schemas.House_type)
def mark_as_deleted_house_type(
        house_type_id: int,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Помечаем тип домов как удалённый (if_deleted == True) по его id.
    """
    try:
        house_type = crud.house_types.get_object(
            db=db, id=house_type_id, is_deleted=False)
        house_type = crud.house_types.is_deleted(db=db, db_obj=house_type)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=str(e)
        )
    return house_type
