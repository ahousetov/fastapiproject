from typing import Any, List, Dict
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=List[schemas.Federal_district])
def read_districts(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> List[Dict[str, Any]]:
    """
    Получить общий список федеральных округов. Помеченные удалёнными не отображаются.
    """
    districts = crud.federal_district.get_all(db, skip=skip, limit=limit)
    return districts


@router.get("/valuelist", response_model=List[schemas.Federal_district_valuelist])
def federal_district_valuelist(
        db: Session = Depends(deps.get_db),
) -> List[schemas.Federal_district_valuelist]:
    """
    value-список для справочника 'Федеральные округа'.
    """
    value_list = crud.federal_district.get_valuelist(db=db)
    return value_list


@router.get("/{district_id}", response_model=schemas.Federal_district_blocks)
def read_district_by_id(
        district_id: int,
        db: Session = Depends(deps.get_db),
) -> Dict[str, Any]:
    """
    Получить конкретный округ по id
    """
    try:
        district = crud.federal_district.get(db, id=district_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=str(e))
    result = {
        "title": crud.federal_district.title,
        "item_title": district.get('name'),
        "block_icon": 'info_outline',
        "blocks": [district]
    }
    return result


@router.put("/{district_id}", status_code=status.HTTP_200_OK)
def update_district(
        district_id: int,
        district_in: schemas.Federal_district_update,
        db: Session = Depends(deps.get_db),
) -> None:
    """
    Обновить данные по району. Обновляется только название
    """
    try:
        # реализация метода get в CRUDBreeds возвращает схему Federal_district
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        # district = super(type(crud.federal_district), crud.federal_district)

        update_data = district_in.dict(exclude_unset=True)

        crud.federal_district.update(db, id=district_id, update_data=update_data)

    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))


@router.post("/", status_code=201)
def create_district(
        *,
        db: Session = Depends(deps.get_db),
        distr_in: schemas.Federal_district_create
) -> Any:
    """
    Создаём новый федеральный округ. Параметр - имя.
    """
    try:
        create_data = distr_in.dict(exclude_unset=True)

        crud.federal_district.create(db=db,
                                     create_data=create_data)

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.patch("/", response_model=List[schemas.Federal_district])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    """
    Получаем список федеральных округов, отфильтрованных по условию.
    """
    districts = crud.federal_district.get_all(db,
                                              skip=skip,
                                              limit=limit,
                                              sort_direction=sort_direction,
                                              sort_field=sort_field,
                                              filters=filters)
    return districts


@router.delete("/{district_id}", response_model=schemas.Federal_district)
def make_deleted_district(
        district_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Помечаем федеральный округ, как удалённый.
    """
    try:
        # реализация метода get в CRUDBreeds возвращает схему Breeds
        # а не запись в БД.
        # Для получения объекта, вызывает get родительского объекта.
        district = crud.federal_district.get_object(db=db, id=district_id)
        district = crud.federal_district.is_deleted(db, db_obj=district)
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=str(e))
    return district
