from typing import Any, List, Dict
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=List[schemas.City_type])
def read_city_types(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Получить общий список типов населённых пунтков. Помеченные удалёнными не отображаются.
    """
    city_types = crud.city_types.get_all(db, skip=skip, limit=limit)
    return city_types


@router.get("/valuelist", response_model=List[schemas.City_types_valuelist])
def read_city_types_valuelist(
        db: Session = Depends(deps.get_db),
) -> List[schemas.City_types_valuelist]:
    """
    value-список для справочника 'Типы населённых пунктов'.
    """
    city_types_valuelist = crud.city_types.get_valuelist(db=db, skip=0, limit=100)
    return city_types_valuelist


@router.get("/{city_type_id}", response_model=schemas.City_types_blocks)
def read_city_type_by_id(
        city_type_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить конкретный тип населённого пункта по id
    """
    try:
        city_type = crud.city_types.get(db, id=city_type_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))
    return {'item_title': city_type.name, 'blocks': [city_type]}


@router.put("/{city_type_id}", response_model=schemas.City_type)
def update_city_type(
        city_type_id: int,
        city_type_in: schemas.City_types_update,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Обновить данные по типу населённого пункта. Обновляется только название
    """
    try:
        city_type = crud.city_types.get_object(
            db=db,
            id=city_type_id,
            is_deleted=False
        )
        city_type = crud.city_types.update(
            db,
            db_obj=city_type,
            obj_in=city_type_in
        )
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))
    return city_type


@router.post("/", response_model=schemas.City_type)
def create_city_type(
        *,
        db: Session = Depends(deps.get_db),
        distr_in: schemas.City_types_create
) -> Any:
    """
    Создаём новый тип населённого пункта. Параметр - имя.
    """
    city_type = crud.city_types.create(db=db, obj_in=distr_in)
    return city_type


@router.patch("/", response_model=List[schemas.City_type])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> Any:
    """
    Получаем список типов городов, отфильтрованных по условиям
    """
    city_types = crud.city_types.get_all(
        db,
        skip=skip,
                                         limit=limit,
                                         sort_direction=sort_direction,
                                         sort_field=sort_field,
                                         filters=filters
    )
    return city_types


@router.delete("/{city_type_id}", response_model=schemas.City_type)
def make_deleted_city_type(
        city_type_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Помечаем тип населённого пункта, как удалённый.
    """
    try:
        city_type = crud.city_types.get_object(
            db=db,
            id=city_type_id,
            is_deleted=False
        )
        city_type = crud.city_types.is_deleted(
            db,
            db_obj=city_type
        )
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=str(e))
    return city_type
