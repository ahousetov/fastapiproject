from typing import Any, List, Optional
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app import crud, schemas
from app.api import deps

router = APIRouter()


@router.get("/valuelist",
            response_model=Optional[List[schemas.Organization_statuses_value_list]])
def organization_statuses_value_list(
        db: Session = Depends(deps.get_db),
) -> Optional[List[schemas.Organization_statuses_value_list]]:
    """
    value-список для справочника 'Статусы юридических лиц'.
    """
    org_statuses = crud.organization_statuses.get_valuelist(db=db)
    return org_statuses
