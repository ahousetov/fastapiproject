from typing import Any, List

from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps
from app.core.config import settings


router = APIRouter()


@router.get("/", response_model=List[schemas.Street_type])
def read_street_types(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Получить полный список типов улиц. Помеченные удалёнными не отображаются.
    """
    street_types = crud.street_types.get_all(db, skip=skip, limit=limit)
    return street_types


@router.get("/valuelist", response_model=List[schemas.Street_type_valuelist])
def read_street_types_valuelist(
        db: Session = Depends(deps.get_db)
) -> List[schemas.Street_type_valuelist]:
    """
    Получить полный список статусов признания пород собак. Помеченные удалёнными не отображаются.
    """
    value_list = crud.street_types.get_valuelist(db=db)
    return value_list


@router.get("/{street_type_id}", response_model=schemas.Street_type_blocks)
def read_street_type_by_id(
        street_type_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Выбираем тип улиц по id. Помеченные is_deleted=True не учитываются.
    """
    try:
        street_type = crud.street_types.get(db, id=street_type_id)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return dict(item_title=street_type.name, blocks=[street_type])


@router.post("/", status_code=status.HTTP_201_CREATED)
def create_street_type(
        *,
        db: Session = Depends(deps.get_db),
        street_type_in: schemas.Street_type_create
) -> None:
    """
    Создаём новый тип улиц. Входной параметр - имя региона.
    """
    crud.street_types.create(db=db, obj_in=street_type_in)


@router.put("/{street_type_id}",
            status_code=status.HTTP_200_OK,
            response_model=schemas.Street_type
            )
def update_street_type(
        street_type_id: int,
        street_type_in: schemas.Street_type_update,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Обновляем данные по типу улиц. Обновляются только названия.
    """
    try:
        street_type = crud.street_types.get_object(
            db=db, id=street_type_id, is_deleted=False)
        street_type = crud.street_types.update(
            db=db, db_obj=street_type, obj_in=street_type_in)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return street_type


@router.delete("/{street_type_id}",
               status_code=status.HTTP_200_OK,
               response_model=schemas.Street_type
               )
def mark_as_deleted_street_type(
        street_type_id: int,
        db: Session = Depends(deps.get_db)
) -> Any:
    """
    Помечаем тип улиц как удалённый (if_deleted == True) по его id.
    """
    try:
        street_type = crud.street_types.get_object(
            db=db, id=street_type_id, is_deleted=False)
        street_type = crud.street_types.is_deleted(db=db, db_obj=street_type)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return street_type
