from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app import crud, schemas
from app.api import deps


router = APIRouter()


@router.get("/valuelist", response_model=List[schemas.Address_types_value_list])
def address_types_value_list(
        db: Session = Depends(deps.get_db),
) -> List[schemas.Address_types_value_list]:
    """
    value-список для справочника 'Типов адресов'.
    """
    address_types = crud.address_types.get_valuelist(db=db)
    return address_types
