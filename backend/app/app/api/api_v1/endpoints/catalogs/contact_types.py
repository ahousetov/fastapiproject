from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app import crud, schemas
from app.api import deps


router = APIRouter()


@router.get("/valuelist", response_model=List[schemas.Contact_types_value_list])
def contact_types_value_list(
        db: Session = Depends(deps.get_db),
) -> List[schemas.Contact_types_value_list]:
    """
    value-список для справочника 'Типы контактных данных'.
    """
    value_list = crud.contact_types.get_valuelist(db)
    return value_list
