from typing import Any, List, Dict
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, models, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=List[schemas.Cities_multi])
def read_cities(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100
) -> List[Dict[str, Any]]:
    """
    Получить общий список типов населённых пунтков. Помеченные удалёнными
    не отображаются.
    """
    cities = crud.cities.get_all(db, skip=skip, limit=limit)
    return cities


@router.get("/valuelist", response_model=List[schemas.Cities_valuelist])
def read_cities_valuelist(
        db: Session = Depends(deps.get_db),
) -> List[schemas.Cities_valuelist]:
    """
    value-список для справочника 'Населённые пункты'.
    """
    value_list = crud.cities.get_valuelist(db=db)
    return value_list


@router.get("/{city_id}", response_model=schemas.Cities_blocks)
def read_city_by_id(
        city_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Получить конкретный тип населённого пункта по id
    """
    try:
        city = crud.cities.get(db, id=city_id)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=str(e))
    return {'item_title': city.name, 'blocks': [city]}


@router.put("/{city_id}", response_model=schemas.Cities)
def update_city(
        city_id: int,
        city_in: schemas.Cities_update,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Обновить данные по типу населённого пункта. Обновляется только название.
    """
    try:
        city = crud.cities.get_object(db=db, id=city_id, is_deleted=False)
        city = crud.cities.update(db,
                                  db_obj=city,
                                  obj_in=city_in)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=str(e))
    return city


@router.post("/", response_model=schemas.Cities)
def create_city(
        *,
        db: Session = Depends(deps.get_db),
        distr_in: schemas.Cities_create
) -> Any:
    """
    Создаём новый тип населённого пункта. Параметр - имя.
    """
    city = crud.cities.create(db=db, obj_in=distr_in)
    return city


@router.patch("/", response_model=List[schemas.Cities_multi_patch])
def filtred_list(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'name',
        filters: schemas.Filters_model = None
) -> List[Dict[str, Any]]:
    """
    Получаем список населенных пунктов, отфильтрованных по условию.
    """
    cities = crud.cities.get_all(db,
                                 skip=skip,
                                 limit=limit,
                                 sort_direction=sort_direction,
                                 sort_field=sort_field,
                                 filters=filters)
    return cities


@router.delete("/{city_id}", response_model=schemas.Cities_delete)
def make_deleted_city(
        city_id: int,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    Помечаем тип населённого пункта, как удалённый.
    """
    try:
        city = crud.cities.get_object(db=db, id=city_id)
        city = crud.cities.is_deleted(db, db_obj=city)
    except HTTPException as httpe:
        raise httpe
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=str(e))
    return city
