from fastapi import APIRouter
from . import pedigree_requests, litter_requests, replace_pedigree_header_requests

api_router = APIRouter()

api_router.include_router(pedigree_requests.router, prefix="/pedigree_requests")
api_router.include_router(litter_requests.router, prefix="/litter_requests")
api_router.include_router(replace_pedigree_header_requests.router, prefix="/replace_pedigree_header_requests")

