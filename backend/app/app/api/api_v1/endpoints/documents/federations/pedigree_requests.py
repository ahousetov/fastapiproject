from typing import Any, List, Optional
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import crud, schemas
from app.api import deps


router = APIRouter()


@router.get("/", response_model=Any)
def read_pedigree_requests(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
) -> List[dict]:
    """
    Получить список пакета заявок на изготовление родословной в личном кабинете федерации
    """
    try:

        pedigree_header_declarant_requests = crud.documents.federations.pedigree_requests.get_all(
            db,
            skip=skip,
            limit=limit,
            sort_direction=sort_direction,
            sort_field=sort_field,
            )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return pedigree_header_declarant_requests


@router.patch("/", response_model=Any)
def filter_pedigree_requests(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> List[dict]:
    """
    Получить список пакета заявок на изготовление родословной в личном кабинете федерации
    """
    try:

        pedigree_header_declarant_requests = crud.documents.federations.pedigree_requests.get_all(
            db,
            skip=skip,
            limit=limit,
            sort_direction=sort_direction,
            sort_field=sort_field,
            filters=filters
            )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    return pedigree_header_declarant_requests
