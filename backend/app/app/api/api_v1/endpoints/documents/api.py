from fastapi import APIRouter
from . import federations, dog_handler


api_router = APIRouter()

api_router.include_router(federations.api.api_router, prefix="/federations")
api_router.include_router(dog_handler.api.api_router, prefix="/dog_handler")
