from fastapi import APIRouter
from . import pedigree_header_declarant_requests, litter_header_declarant_requests, replace_pedigree_header_requests

api_router = APIRouter()

api_router.include_router(pedigree_header_declarant_requests.router, prefix="/pedigree_header_declarant_requests")
api_router.include_router(litter_header_declarant_requests.router, prefix="/litter_header_declarant_requests")
api_router.include_router(replace_pedigree_header_requests.router, prefix="/replace_pedigree_header_requests")


