from typing import Any, List
from app import crud, models, schemas
from app.api import deps
from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session


router = APIRouter()


@router.get("/", response_model=List[schemas.Nurseries])
def read_nurseries(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id'
) -> List[schemas.Nurseries]:
    """
    Получить список питомников.
    """
    nurseries_list = crud.nurseries_crud_explicit.get_all(
        db=db, skip=skip, limit=limit,
                                                          sort_direction=sort_direction,
                                                          sort_field=sort_field,
                                                          filters=None
    )
    return nurseries_list


@router.patch("/", response_model=List[schemas.Nurseries])
def filtered_nurseries(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        sort_direction: str = 'asc',
        sort_field: str = 'id',
        filters: schemas.Filters_model = None
) -> List[schemas.Nurseries]:
    """
    Получить фильтрованный список питомников.
    """
    filtered_nurseries = crud.nurseries_crud_explicit.get_all(db=db, skip=skip, limit=limit,
                                                              sort_direction=sort_direction,
                                                              sort_field=sort_field,
                                                              filters=filters)
    return filtered_nurseries


@router.get("/{profile_id}", response_model=schemas.Nurseries_by_id)
def get_nursery_by_id(
        db: Session = Depends(deps.get_db),
        profile_id: int = None
) -> schemas.Nurseries_by_id:
    """
    Получить информацию данные по питомнику.
    """
    try:
        nurseries_info = crud.nurseries_crud_explicit.get(db=db, id=profile_id)
        # nurseries_info - структура имеющая следующую структуру:
        # {
        #   'main': Dict[str: Any],
        #   'contacts': Optional[List[Dict[str: Any]],
        #   'addresses': Optional[List[Dict[str: Any]],
        #   'breeds': Optional[List[Dict[str: Any]],
        #   'additional': Optional[Dict[str: Any]],
        #   'history': Optional[Dict[str: Any]],
        #   'payments': Optional[List[Dict[str: Any]],
        #   'access': Dict[
        #                   'view': List[Dict['access_name': str,
        #                                     'value': bool]
        #                               ],
        #                    'edit': List[bool]
        #                 ]
        # }
        main_block = schemas.Nurseries_main_block_view(**nurseries_info['main'])
        contacts_block = schemas.Contacts_blocks_view(**nurseries_info)
        address_block = schemas.nurseries.Address_blocks_view(**nurseries_info)
        breeding_breeds_block = schemas.Nurseries_breeding_breeds_block(
            **nurseries_info)
        additional_info_block = schemas.Nurseries_additional_block_view(
            **nurseries_info['additional'])
        history_block = schemas.Nurseries_history_block_view(
            **nurseries_info['history'])
        payments_block = schemas.Nurseries_payments_block(**nurseries_info)
        access_block = schemas.nurseries.Access_blocks_view(
            access=nurseries_info['access']['view'])
        block_list = [main_block, contacts_block, address_block, breeding_breeds_block,
                      additional_info_block, history_block, payments_block, access_block]
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e)
        )
    return {'blocks': block_list}


@router.get("/editinfo/{profile_id}", response_model=schemas.Nurseries_by_id)
def get_edit_info_nursery_by_id(
        db: Session = Depends(deps.get_db),
        profile_id: int = None
) -> schemas.Nurseries_by_id:
    """
    Получить информацию для редактирования.
    """
    try:
        # return {
        #     'main': main_dict,
        #     'contacts': contacts,
        #     'addresses': addresses_block,
        #     'breeds': breeds,
        #     'additional': add_info_dict,
        #     'history': history_infO_dict,
        #     'payments': pay_list_dict,
        #     'access': access
        # }

        nurseries_info = crud.nurseries_crud_explicit.get(db=db, id=profile_id)
        main_block = schemas.Nurseries_main_block_editinfo(**nurseries_info['main'])
        contacts_block = schemas.Contacts_blocks_edit(**nurseries_info)
        address_block = schemas.nurseries.Address_blocks_edit(**nurseries_info)
        breeding_breeds_block = schemas.Nurseries_breeding_breeds_block(
            **nurseries_info)
        additional_info_block = schemas.Nurseries_additional_block_view(
            **nurseries_info['additional'])
        history_block = schemas.Nurseries_history_block_edit(
            **nurseries_info['history'])
        payments_block = schemas.Nurseries_payments_block(**nurseries_info)
        access_block = schemas.nurseries.Access_blocks_edit(
            access=nurseries_info['access']['edit'])
        block_list = [main_block, contacts_block, address_block, breeding_breeds_block,
                      additional_info_block, history_block, payments_block, access_block]
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e)
        )
    return {'title': 'Питомники', 'blocks': block_list}


# @router.put("/{profile_id}", response_model=schemas.nurseries.Nurseries_update)
@router.put("/{profile_id}", status_code=status.HTTP_200_OK)
def update_nurseries(db: Session = Depends(deps.get_db),
                     profile_id: int = None,
                     nurseries_in: schemas.nurseries.Nurseries_update = None) -> None:

    try:
        update_data = dict(
            contacts=nurseries_in.contacts,
            addresses=nurseries_in.addresses,
            payments=nurseries_in.payments,
            breeds=nurseries_in.breeds,
            main=nurseries_in.dict(include=schemas.nurseries.Nurseries_main_block_edit().dict().keys(),
                                   exclude_unset=True),
            history=nurseries_in.dict(include=schemas.nurseries.Nurseries_history().dict().keys(),
                                      exclude_unset=True),
            additional=nurseries_in.dict(include=schemas.nurseries.Nurseries_additional_block_view().dict().keys(),
                                         exclude_unset=True)

        )
        update_data['history']['profile_id'] = profile_id
        update_data['main']['profile_id'] = profile_id
        crud.nurseries_crud_explicit.update(
            db=db, profile_id=profile_id, update_data=update_data)
    except HTTPException as he:
        raise he
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_501_NOT_IMPLEMENTED,
            detail=str(e)
        )
