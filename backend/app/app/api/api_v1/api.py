from fastapi import APIRouter
from app.api.api_v1.endpoints import login, users
#from dogs import fci_groups, fci_sections
#from catalogs import house_types, regions, cities, city_types, flat_types, street_types, federal_districts
from app.api.api_v1.endpoints.catalogs import house_types, regions, cities, city_types, flat_types, street_types, \
    federal_districts, federations, organization_statuses, address_types, contact_types
from app.api.api_v1.endpoints.dogs import fci_groups, fci_sections, fci_breed_statuses, breeds
from app.api.api_v1.endpoints.nurseries import nurseries
from app.api.api_v1.endpoints.public import contacts, club_address, clubs
from app.api.api_v1.endpoints.clubs import national_breed_clubs
from app.api.api_v1.endpoints.accesses import accesses
from app.api.api_v1.endpoints import documents


api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
# api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
# api_router.include_router(items.router, prefix="/items", tags=["items"])
api_router.include_router(address_types.router, prefix="/address_types", tags=["address_types"])
api_router.include_router(contact_types.router, prefix="/contact_types", tags=["contact_types"])
api_router.include_router(federal_districts.router, prefix="/federal_districts", tags=["federal_districts"])
api_router.include_router(regions.router, prefix="/regions", tags=["regions"])
api_router.include_router(city_types.router, prefix="/city_types", tags=["city_types"])
api_router.include_router(cities.router, prefix="/cities", tags=["cities"])
api_router.include_router(street_types.router, prefix="/street_types", tags=["street_types"])
api_router.include_router(house_types.router, prefix="/house_types", tags=["house_types"])
api_router.include_router(flat_types.router, prefix="/flat_types", tags=["flat_types"])
api_router.include_router(fci_groups.router, prefix="/fci_groups", tags=["fci_groups"])
api_router.include_router(fci_sections.router, prefix="/fci_sections", tags=["fci_sections"])
api_router.include_router(fci_breed_statuses.router, prefix="/fci_breed_statuses", tags=["fci_breed_statuses"])
api_router.include_router(breeds.router, prefix="/breeds", tags=["breeds"])
api_router.include_router(federations.router, prefix="/federations", tags=["federations"])
api_router.include_router(contacts.router, prefix="/contacts", tags=["contacts"])
api_router.include_router(club_address.router, prefix="/club_address", tags=["club_address"])
api_router.include_router(clubs.router, prefix="/clubs", tags=["clubs"])
api_router.include_router(nurseries.router, prefix="/nurseries", tags=["nurseries"])
api_router.include_router(documents.api_router, prefix="/documents", tags=["documents"])
api_router.include_router(organization_statuses.router,
                          prefix="/organization_statuses",
                          tags=["organization_statuses"])
api_router.include_router(national_breed_clubs.router, prefix="/national_breed_clubs", tags=["national_breed_clubs"])
api_router.include_router(accesses.router, prefix="/accesses", tags=["accesses"])
#api_router.include_router(nurseries.router, prefix="/nurseries", tags=["nurseries"])
