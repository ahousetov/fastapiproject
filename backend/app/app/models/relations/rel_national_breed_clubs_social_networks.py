from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Rel_national_breed_clubs_social_networks(Base):
    """
    Модель для таблицы relations.rel_national_breed_clubs_social_networks.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('clubs')
    - 'extend_existing' - если для таблицы с указанным именем (federation_clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'relations'}

    # region Колонки
    id = Column("id", Integer, primary_key=True, index=True)
    national_breed_club_id = Column("national_breed_club_id", Integer,
                                    ForeignKey('clubs.national_breed_clubs.id'), nullable=False)
    social_networks_id = Column("social_networks_id", Integer,
                                ForeignKey('public.social_networks.id'), nullable=False)
    # endregion Колонки

    # region Отношения
    national_breed_clubs = relationship(
        "National_breed_clubs",
        primaryjoin="and_(National_breed_clubs.id==Rel_national_breed_clubs_social_networks.national_breed_club_id)",
        foreign_keys="Rel_national_breed_clubs_social_networks.national_breed_club_id",
        back_populates="rel_national_breed_clubs_social_networks",
    )
    social_networks = relationship(
        "Social_networks",
        primaryjoin="and_(Social_networks.id==Rel_national_breed_clubs_social_networks.social_networks_id)",
        foreign_keys="Rel_national_breed_clubs_social_networks.social_networks_id",
        back_populates="rel_national_breed_clubs_social_networks",
    )
    # endregion Отношения
