from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Rel_breeds_sections_groups(Base):
    """
    Модель для таблицы relations.rel_breeds_sections_groups.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('relations')
    - 'extend_existing' - если для таблицы с указанным именем (rel_breeds_sections_groups в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Связи классификатора пород - секций пород FCI - групп пород FCI"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'relations'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    breed_id = Column(Integer, ForeignKey('dogs.breeds.id'), nullable=True)
    group_id = Column(Integer, ForeignKey('dogs.fci_groups.id'), nullable=True)
    section_id = Column(Integer, ForeignKey('dogs.fci_sections.id'), nullable=True)

    # Отношения с зависимыми таблицами прописывается как обычный relationsship, НО
    # поскольку таблицы rel_* не вызываются напрямую, делаем трюк с объявлением primaryjoin.
    # Суть в том, что join-логика прописывается именно в кросс-таблице.
    #TODO: почитать о primaryjoin.
    breeds = relationship("Breeds",
                          primaryjoin="and_(Breeds.id==Rel_breeds_sections_groups.breed_id)",
                          foreign_keys="Rel_breeds_sections_groups.breed_id",
                          back_populates="rel")

    groups = relationship("FCI_groups",
                          primaryjoin="and_(FCI_groups.id==Rel_breeds_sections_groups.group_id)",
                          foreign_keys="Rel_breeds_sections_groups.group_id",
                          back_populates="rel_breeds_sections_groups")

    sections = relationship("FCI_sections",
                            primaryjoin="and_(FCI_sections.id==Rel_breeds_sections_groups.section_id)",
                            foreign_keys="Rel_breeds_sections_groups.section_id",
                            back_populates="rel_breeds_sections_groups")


