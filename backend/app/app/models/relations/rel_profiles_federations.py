from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Rel_profiles_federations(Base):
    """
    Модель для таблицы relations.rel_profiles_federations.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('relations')
    - 'extend_existing' - если для таблицы с указанным именем (rel_profiles_federations в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Связь профили - федерации"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'relations'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        nullable=True)
    federation_id = Column(Integer,
                           ForeignKey('catalogs.federations.id'),
                           name="federation_id",
                           nullable=True)

    federations = relationship("Federations",
                               primaryjoin="and_(Federations.id==Rel_profiles_federations.federation_id)",
                               foreign_keys="Rel_profiles_federations.federation_id",
                               back_populates="rel_profiles")

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Rel_profiles_federations.profile_id)",
                            foreign_keys="Rel_profiles_federations.profile_id",
                            back_populates="rel_profiles_federations")

    # legal_informations = relationship("Legal_informations",
    #                                   primaryjoin="and_(Legal_informations.profile_id==Rel_profiles_federations.profile_id)",
    #                                   foreign_keys="Rel_profiles_federations.profile_id",
    #                                   back_populates="legal_informations")

