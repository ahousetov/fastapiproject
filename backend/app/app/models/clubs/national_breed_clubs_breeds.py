from sqlalchemy import Column, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class National_breed_clubs_breeds(Base):
    """
    Модель для таблицы clubs.national_breed_clubs_breeds.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('clubs')
    - 'extend_existing' - если для таблицы с указанным именем (federation_clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'clubs'}

    # region Колонки
    id = Column("id", Integer, primary_key=True, index=True)
    national_breed_club_id = Column("national_breed_club_id", Integer,
                                    ForeignKey('clubs.national_breed_clubs.id'), nullable=False)
    breed_id = Column("breed_id", Integer, ForeignKey('dogs.breeds.id'), nullable=False)
    is_deleted = Column("is_deleted", Boolean, default=False, nullable=False)
    # endregion Колонки

    # region Отношения
    national_breed_clubs = relationship(
        "National_breed_clubs",
        primaryjoin="and_(National_breed_clubs.id==National_breed_clubs_breeds.national_breed_club_id)",
        foreign_keys="National_breed_clubs_breeds.national_breed_club_id",
        back_populates="national_breed_clubs_breeds",
    )
    breeds = relationship(
        "Breeds",
        primaryjoin="and_(Breeds.id==National_breed_clubs_breeds.breed_id)",
        foreign_keys="National_breed_clubs_breeds.breed_id",
        back_populates="national_breed_clubs_breeds",
    )
    # endregion Отношения
