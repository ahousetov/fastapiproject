from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Federation_clubs(Base):
    """
    Модель для таблицы clubs.federation_clubs.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('clubs')
    - 'extend_existing' - если для таблицы с указанным именем (federation_clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Клубы федераций"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'clubs'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        nullable=True)
    federation_id = Column(Integer,
                           ForeignKey('catalogs.federations.id'),
                           name="federation_id",
                           nullable=True)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Federation_clubs.profile_id)",
                            foreign_keys="Federation_clubs.profile_id",
                            back_populates="federation_clubs")

    # profiles = relationship("Profiles",
    #                         primaryjoin="and_(Profiles.id==Federation_clubs.profile_id)",
    #                         foreign_keys="Federation_clubs.profile_id",
    #                         back_populates="rel_federation_clubs")

    federations = relationship("Federations",
                               primaryjoin="and_(Federations.id==Federation_clubs.federation_id)",
                               foreign_keys="Federation_clubs.federation_id",
                               back_populates="rel_federation_clubs")



