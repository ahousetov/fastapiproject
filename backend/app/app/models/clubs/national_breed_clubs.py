from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from app.db.base_class import Base
from .national_breed_clubs_breeds import National_breed_clubs_breeds


class National_breed_clubs(Base):
    """
    Модель для таблицы clubs.national_breed_clubs.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('clubs')
    - 'extend_existing' - если для таблицы с указанным именем (federation_clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Национальные клубы пород"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'clubs'}

    id = Column("id", Integer, primary_key=True, index=True)
    name = Column("name", String, nullable=False)
    owner_name = Column("owner_name", String, nullable=False)
    is_deleted = Column("is_deleted", Boolean, default=False, nullable=False)
    owner_position = Column("owner_position", String)
    comment = Column("comment", String)

    # region Отношения
    national_breed_clubs_breeds = relationship(
        "National_breed_clubs_breeds",
        back_populates="national_breed_clubs",
    )
    national_breed_clubs_presidium_members = relationship(
        "National_breed_clubs_presidium_members",
        back_populates="national_breed_clubs",
    )
    rel_national_breed_clubs_contacts = relationship(
        "Rel_national_breed_clubs_contacts",
        back_populates="national_breed_clubs",
    )
    rel_national_breed_clubs_social_networks = relationship(
        "Rel_national_breed_clubs_social_networks",
        back_populates="national_breed_clubs",
    )
    # endregion Отношения
