from typing import TYPE_CHECKING
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Boolean, Table
from app.db.base_class import Base


class Street_types(Base):
    """
    Модель для таблицы catalogs.street_types.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('catalogs')
    - 'extend_existing' - если для таблицы с указанным именем (Street_types в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Типы улицы"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'catalogs'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    name = Column(String, name="name", index=True)
    is_deleted = Column(Boolean(), name="is_deleted", default=False)

    club_addresses = relationship("Club_addresses", back_populates="street_type")
    nursery_registration_requests = relationship("Nursery_registration_requests",
                                                 back_populates="nursery_registration_requests_street_type")