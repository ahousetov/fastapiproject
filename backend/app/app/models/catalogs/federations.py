from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Federations(Base):
    """
    Модель для таблицы catalogs.federations.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('catalogs')
    - 'extend_existing' - если для таблицы с указанным именем (Federations в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Федерации"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'catalogs'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column("id", Integer, primary_key=True, index=True)
    name = Column("name", String, index=True)
    short_name = Column("short_name", String, index=True)
    is_deleted = Column("is_deleted", Boolean, default=False)

    rel_profiles = relationship("Rel_profiles_federations",
                                cascade="all,delete",
                                back_populates="federations")

    rel_federation_clubs = relationship("Federation_clubs",
                                        cascade="all,delete",
                                        back_populates="federations")

    pedigree_requests = relationship("Pedigree_requests",
                                     cascade="all,delete",
                                     back_populates="federations")

    litter_requests = relationship("Litter_requests",
                                   back_populates="federations")

    replace_pedigree_header_requests = relationship("Replace_pedigree_header_requests",
                                                    back_populates="federations")
