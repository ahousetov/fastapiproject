from sqlalchemy import Column, Integer, Boolean, String
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Social_networks_types(Base):
    """
    Модель для таблицы catalogs.social_networks_types.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('clubs')
    - 'extend_existing' - если для таблицы с указанным именем (federation_clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'catalogs'}

    # region Колонки
    id = Column("id", Integer, primary_key=True, index=True)
    name = Column("name", String, nullable=False)
    is_deleted = Column("is_deleted", Boolean, default=False, nullable=False)
    # endregion Колонки

    # region Отношения
    social_networks = relationship(
        "Social_networks",
        back_populates="social_networks_types",
    )
    # endregion Отношения
