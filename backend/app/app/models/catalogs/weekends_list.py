from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Weekends_list(Base):
    """
    Модель для таблицы catalogs.weekends_list.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('catalogs')
    - 'extend_existing' - если для таблицы с указанным именем (Weekends_list в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Weekends_list"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'catalogs'}

    id = Column("id", Integer, primary_key=True, index=True)
    is_deleted = Column("is_deleted", Boolean, nullable=False)
    weekend_date = Column('weekend_date', TIMESTAMP, nullable=False)
    weekend_type_id = Column('weekend_type_id', Integer, nullable=False)
    week_day_id = Column('week_day_id', Integer, nullable=False)
    comments = Column('comments', VARCHAR, nullable=True)
