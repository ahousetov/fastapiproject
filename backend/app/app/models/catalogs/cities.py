from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Cities(Base):
    """
    Модель для таблицы catalogs.city_types.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('catalogs')
    - 'extend_existing' - если для таблицы с указанным именем (city_typess в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Населённые пункты"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'catalogs'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}

    id = Column(Integer, name="id", primary_key=True, index=True)
    name = Column(String, name="name", index=True)
    is_deleted = Column(Boolean(), name="is_deleted", default=False)
    name_eng = Column(String, name="name_eng")
    federal_district_id = Column(Integer,
                                 ForeignKey('catalogs.federal_districts.id'),
                                 nullable=True)
    region_id = Column(Integer, ForeignKey('catalogs.regions.id'), nullable=True)
    type_id = Column(Integer, ForeignKey('catalogs.city_types.id'), nullable=True)
    geo_lat = Column(String, name="geo_lat")
    geo_lon = Column(String, name="geo_lon")

    district = relationship("Federal_Districts",
                            foreign_keys="Cities.federal_district_id",
                            back_populates="cities")
    region = relationship("Regions",
                          foreign_keys="Cities.region_id",
                          back_populates="city_region")
    type = relationship("City_types",
                        foreign_keys="Cities.type_id",
                        back_populates="city_type")
    club_addresses = relationship("Club_addresses", back_populates="cities")


