from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Replace_pedigree_header_requests(Base):
    """
    Модель для таблицы requests.replace_pedigree_header_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Replace_pedigree_header_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Replace_pedigree_header_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    date_create = Column('date_create', TIMESTAMP, nullable=False)
    date_change = Column('date_change', TIMESTAMP, nullable=False)
    express = Column('express', Boolean, default=False)
    barcode = Column('barcode', VARCHAR, nullable=True)
    is_deleted = Column('is_deleted', Boolean, default=False)
    paid_earlier = Column('paid_earlier', Boolean, default=False)
    is_pedigree_link_show = Column('is_pedigree_link_show', Boolean, default=False)

    federation_id = Column(
        'federation_id',
        Integer,
        ForeignKey('catalogs.federations.id'),
        nullable=False)
    federations = relationship(
        "Federations",
        foreign_keys="Replace_pedigree_header_requests.federation_id",
        back_populates="replace_pedigree_header_requests")

    declarant_id = Column(
        'declarant_id',
        Integer,
        ForeignKey('declarants.declarants.id'),
        nullable=False)
    declarants = relationship(
        "Declarants",
        foreign_keys="Replace_pedigree_header_requests.declarant_id",
        back_populates="replace_pedigree_header_requests")

    status_id = Column(
        'status_id',
        Integer,
        ForeignKey('requests.replace_pedigree_request_statuses.id'),
        nullable=False)
    replace_pedigree_request_statuses = relationship(
        "Replace_pedigree_request_statuses",
        foreign_keys=[status_id],
        back_populates="replace_pedigree_header_requests")

    prev_status_id = Column(
        'prev_status_id',
        Integer,
        ForeignKey('requests.replace_pedigree_request_statuses.id'),
        nullable=True)
    prev_replace_pedigree_request_statuses = relationship(
        "Replace_pedigree_request_statuses",
        foreign_keys=[prev_status_id],
        back_populates="replace_pedigree_header_requests")

    type_id = Column(
        'type_id',
        Integer,
        ForeignKey('requests.replace_pedigree_request_types.id'),
        nullable=False)
    replace_pedigree_request_types = relationship(
        "Replace_pedigree_request_types",
        foreign_keys="Replace_pedigree_header_requests.type_id",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_export_old_requests = relationship(
        "Replace_pedigree_export_old_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_old_requests = relationship(
        "Replace_pedigree_old_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_duplicate_requests = relationship(
        "Replace_pedigree_duplicate_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_owner_requests = relationship(
        "Replace_pedigree_owner_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_declarant_error_requests = relationship(
        "Replace_pedigree_declarant_error_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_out_rkf_fci_requests = relationship(
        "Replace_pedigree_out_rkf_fci_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_foreign_requests = relationship(
        "Replace_pedigree_foreign_requests",
        back_populates="replace_pedigree_header_requests")

    replace_pedigree_requests_histories = relationship(
        "Replace_pedigree_requests_histories",
        back_populates="replace_pedigree_header_requests")
