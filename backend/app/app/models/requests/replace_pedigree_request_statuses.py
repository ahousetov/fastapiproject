from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Replace_pedigree_request_statuses(Base):
    """
    Модель для таблицы requests.replace_pedigree_request_statuses.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Replace_pedigree_request_statuses в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Replace_pedigree_request_statuses"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    name = Column('name', VARCHAR, nullable=False)
    description = Column('description', VARCHAR, nullable=True)
    is_deleted = Column('is_deleted', Boolean, nullable=False, default=False)

    replace_pedigree_header_requests = relationship("Replace_pedigree_header_requests",
                                                    foreign_keys="Replace_pedigree_header_requests."
                                                                 "status_id",
                                                    back_populates="replace_pedigree_request_statuses")

    prev_replace_pedigree_header_requests = relationship("Replace_pedigree_header_requests",
                                                         foreign_keys="Replace_pedigree_header_requests."
                                                                      "prev_status_id",
                                                         back_populates="prev_replace_pedigree_request_statuses")

    replace_pedigree_requests_histories = relationship(
        "Replace_pedigree_requests_histories",
        back_populates="replace_pedigree_request_statuses")
