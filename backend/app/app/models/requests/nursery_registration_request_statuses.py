from typing import TYPE_CHECKING
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Boolean, Table
from app.db.base_class import Base


class Nursery_registration_request_statuses(Base):
    """
    Модель для таблицы requests.nursery_registration_request_statuses.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Nursery_registration_request_statuses в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Статусы запросов о регистрации питомников"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column("id", Integer, primary_key=True, index=True)
    name = Column("name", String, nullable=True)
    description = Column("description", String, nullable=True)
    is_deleted = Column("is_deleted", Boolean(), nullable=False, default=False)

    nursery_registration_requests = relationship("Nursery_registration_requests",
                                                 back_populates="nursery_registration_requests_status_id")