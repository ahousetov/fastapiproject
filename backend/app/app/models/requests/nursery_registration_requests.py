import uuid
import datetime
from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from app.db.base_class import Base


class Nursery_registration_requests(Base):
    """
    Модель для таблицы requests.nursery_registration_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Nursery_registration_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Запросы на регистрацию питомника"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}
    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        index=True)
    date_create = Column(TIMESTAMP,
                         name="date_create",
                         nullable=False,
                         default=datetime.datetime.now())
    date_update = Column(TIMESTAMP, name="date_update", nullable=False)
    status_id = Column(Integer,
                       ForeignKey('requests.nursery_registration_request_statuses.id'),
                       name="status_id",
                       nullable=False)
    phone = Column(String, name="phone", nullable=False)
    owner_specialist_rkf = Column(Boolean(),
                                  name="owner_specialist_rkf",
                                  default=False,
                                  nullable=False)
    owner_place_speciality = Column(String,
                                    name="owner_place_speciality",
                                    nullable=True)
    owner_ranks = Column(String, name="owner_ranks", nullable=True)
    dogs_ranks = Column(String, name="dogs_ranks", nullable=True)
    registration_date = Column(TIMESTAMP, name="registration_date", nullable=False)
    certificate_rkf_number = Column(String, name="certificate_rkf_number", nullable=False)
    experience_dog_breeding = Column(TIMESTAMP, name="experience_dog_breeding", nullable=True)
    puppies_total_count = Column(Integer, name="puppies_totla_count", nullable=True)
    prefix = Column(Boolean(), name="prefix", default=False, nullable=False)
    suffix = Column(Boolean(), name="suffix", default=False, nullable=False)
    certificate_registration_nursery_id = Column(Integer,
                                                 ForeignKey('documents.documents.id'),
                                                 name="certificate_registration_nursery_id",
                                                 nullable=False)
    certificate_registration_in_rkf_id = Column(Integer,
                                                ForeignKey('documents.documents.id'),
                                                name="certificate_registration_in_rkf_id",
                                                nullable=False)
    certificate_special_education_id = Column(Integer,
                                              ForeignKey('documents.documents.id'),
                                              name="certificate_special_education_id",
                                              nullable=True)
    certificate_specialist_rkf_id = Column(Integer,
                                           ForeignKey('documents.documents.id'),
                                           name="certificate_specialist_rkf_id",
                                           nullable=True)
    certificate_honorary_title_id = Column(Integer,
                                           ForeignKey('documents.documents.id'),
                                           name="certificate_honorary_title_id",
                                           nullable=True)
    postcode = Column(String, name="postcode", nullable=False)
    street_type_id = Column(Integer,
                            ForeignKey('catalogs.street_types.id'),
                            name="street_type_id",
                            nullable=False)
    street_name = Column(String, name="street_name", nullable=False)
    house_type_id = Column(Integer,
                           ForeignKey('catalogs.house_types.id'),
                           name="house_type_id",
                           nullable=False)
    house_name = Column(String, name="house_name", nullable=False)
    flat_type_id = Column(Integer,
                          ForeignKey('catalogs.flat_types.id'),
                          name="flat_type_id",
                          nullable=True)
    flat_name = Column(String, name="flat_name", nullable=True)
    is_deleted = Column(Boolean(), name="is_deleted", default=False, nullable=False)
    personal_office_access = Column(Boolean(),
                                    name="personal_office_access",
                                    default=False, nullable=False)
    open_access_to_all_requests = Column(Boolean(),
                                        name="open_access_to_all_requests",
                                        default=False, nullable=False)

    # nursery_registration_request_statuses_certificate_registration_nursery_id = relationship("Documents",
    #                          primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_registration_nursery_id)",
    #                          foreign_keys="Nursery_registration_requests.certificate_registration_nursery_id",
    #                          back_populates="certificate_registration_nursery_id")
    nursery_registration_request_statuses_certificate_registration_nursery_id = relationship("Documents",
                             primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_registration_nursery_id)",
                             foreign_keys="Nursery_registration_requests.certificate_registration_nursery_id")
    # nursery_registration_request_statuses_certificate_registration_nursery_id = relationship("Documents",
    #                          foreign_keys="Nursery_registration_requests.certificate_registration_nursery_id")

    ###########################################################################################################################
    # nursery_registration_request_statuses_certificate_registration_in_rkf_id = relationship("Documents",
    #                          primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_registration_in_rkf_id)",
    #                          foreign_keys="Nursery_registration_requests.certificate_registration_in_rkf_id",
    #                          back_populates="certificate_registration_in_rkf_id")
    nursery_registration_request_statuses_certificate_registration_in_rkf_id = relationship("Documents",
                             primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_registration_in_rkf_id)",
                             foreign_keys="Nursery_registration_requests.certificate_registration_in_rkf_id")
    # nursery_registration_request_statuses_certificate_special_education_id = relationship("Documents",
    #                          primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_special_education_id)",
    #                          foreign_keys="Nursery_registration_requests.certificate_special_education_id",
    #                          back_populates="certificate_special_education_id")
    nursery_registration_request_statuses_certificate_special_education_id = relationship("Documents",
                             primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_special_education_id)",
                             foreign_keys="Nursery_registration_requests.certificate_special_education_id")

    # nursery_registration_request_statuses_certificate_specialist_rkf_id = relationship("Documents",
    #                          primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_specialist_rkf_id)",
    #                          foreign_keys="Nursery_registration_requests.certificate_specialist_rkf_id",
    #                          back_populates="certificate_specialist_rkf_id")
    nursery_registration_request_statuses_certificate_specialist_rkf_id = relationship("Documents",
                             primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_specialist_rkf_id)",
                             foreign_keys="Nursery_registration_requests.certificate_specialist_rkf_id")
    # nursery_registration_request_statuses_certificate_honorary_title_id = relationship("Documents",
    #                          primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_honorary_title_id)",
    #                          foreign_keys="Nursery_registration_requests.certificate_honorary_title_id",
    #                          back_populates="certificate_honorary_title_id")
    nursery_registration_request_statuses_certificate_honorary_title_id = relationship("Documents",
                             primaryjoin="and_(Documents.id==Nursery_registration_requests.certificate_honorary_title_id)",
                             foreign_keys="Nursery_registration_requests.certificate_honorary_title_id")

    nursery_registration_requests_status_id = relationship("Nursery_registration_request_statuses",
                                        primaryjoin="and_(Nursery_registration_request_statuses.id==Nursery_registration_requests.status_id)",
                                        foreign_keys="Nursery_registration_requests.status_id",
                                        back_populates="nursery_registration_requests")

    nursery_registration_requests_flat_type = relationship("Flat_types",
                                        primaryjoin="and_(Flat_types.id==Nursery_registration_requests.flat_type_id)",
                                        foreign_keys="Nursery_registration_requests.flat_type_id",
                                        back_populates="nursery_registration_requests")

    nursery_registration_requests_street_type = relationship("Street_types",
                                        primaryjoin="and_(Street_types.id==Nursery_registration_requests.street_type_id)",
                                        foreign_keys="Nursery_registration_requests.street_type_id",
                                        back_populates="nursery_registration_requests")

    nursery_registration_requests_house_type = relationship("House_types",
                                        primaryjoin="and_(House_types.id==Nursery_registration_requests.house_type_id)",
                                        foreign_keys="Nursery_registration_requests.house_type_id",
                                        back_populates="nursery_registration_requests")

    nursery_registration_requests_profiles = relationship("Profiles",
                                        primaryjoin="and_(Profiles.id==Nursery_registration_requests.profile_id)",
                                        foreign_keys="Nursery_registration_requests.profile_id",
                                        back_populates="nursery_registration_requests")