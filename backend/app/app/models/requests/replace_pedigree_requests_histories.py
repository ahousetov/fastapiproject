from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Replace_pedigree_requests_histories(Base):
    """
    Модель для таблицы requests.replace_pedigree_requests_histories.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Replace_pedigree_requests_histories в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Replace_pedigree_requests_histories"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    date_create = Column('date_create', TIMESTAMP, nullable=False)
    comment = Column('comment', VARCHAR, nullable=True)
    is_deleted = Column('is_deleted', Boolean, nullable=False, default=False)
    document_id = Column('document_id', Integer, nullable=True)
    date_ready = Column('date_ready', TIMESTAMP, nullable=True)

    replace_pedigree_header_requests_id = Column('replace_pedigree_header_requests_id',
                                                 Integer,
                                                 ForeignKey('requests.replace_pedigree_header_requests.id'),
                                                 nullable=False)
    replace_pedigree_header_requests = relationship(
        "Replace_pedigree_header_requests",
        foreign_keys="Replace_pedigree_requests_histories.replace_pedigree_header_requests_id",
        back_populates="replace_pedigree_requests_histories")

    status_id = Column('status_id',
                       Integer,
                       ForeignKey('requests.replace_pedigree_request_statuses.id'),
                       nullable=False)
    replace_pedigree_request_statuses = relationship(
        "Replace_pedigree_request_statuses",
        foreign_keys="Replace_pedigree_requests_histories.status_id",
        back_populates="replace_pedigree_requests_histories")
