from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Litter_header_declarant_requests(Base):
    """
    Модель для таблицы requests.litter_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Litter_header_declarant_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Litter_header_declarant_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    declarant_uid = Column('declarant_uid', UUID(as_uuid=True), nullable=False)
    barcode = Column('barcode', VARCHAR, nullable=False)
    date_create = Column('date_create', TIMESTAMP, nullable=False)
    date_change = Column('date_change', TIMESTAMP, nullable=False)
    is_deleted = Column('is_deleted', Boolean, default=False)

    litter_request_id = Column('litter_request_id', Integer, ForeignKey("requests.litter_requests.id"), nullable=False)
    litter_requests = relationship("Litter_requests",
                                   foreign_keys="Litter_header_declarant_requests.litter_request_id",
                                   back_populates="litter_header_declarant_requests")

    status_id = Column('status_id',
                       Integer,
                       ForeignKey('requests.request_statuses.id'),
                       nullable=False)
    request_statuses = relationship("Request_statuses",
                                    foreign_keys=[status_id],
                                    back_populates="litter_header_declarant_requests"
                                    )

    prev_status_id = Column('prev_status_id',
                            Integer,
                            ForeignKey('requests.request_statuses.id'),
                            nullable=True)
    prev_request_statuses = relationship("Request_statuses",
                                         foreign_keys=[prev_status_id],
                                         back_populates="litter_header_declarant_requests"
                                         )

    litter_declarant_requests = relationship("Litter_declarant_requests",
                                             back_populates="litter_header_declarant_requests")

    litter_declarant_requests_histories = relationship("Litter_declarant_requests_histories",
                                                       back_populates="litter_header_declarant_requests")

    litter_declarant_documents = relationship("Litter_declarant_documents",
                                              back_populates="litter_header_declarant_requests")
