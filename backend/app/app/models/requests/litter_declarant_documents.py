from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Litter_declarant_documents(Base):
    """
    Модель для таблицы requests.litter_declarant_documents.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Litter_declarant_documents в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Litter_declarant_documents"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    document_id = Column("document_id", Integer, nullable=False)
    document_type_id = Column("document_type_id", Integer, nullable=False)
    accept = Column("accept", Boolean, default=False)
    is_deleted = Column("is_deleted", Boolean, default=False)

    litter_header_declarant_request_id = Column("litter_header_declarant_request_id",
                                                Integer,
                                                ForeignKey("requests.litter_header_declarant_requests.id"),
                                                nullable=False)
    litter_header_declarant_requests = relationship("Litter_header_declarant_requests",
                                                    foreign_keys="Litter_declarant_documents."
                                                                 "litter_header_declarant_request_id",
                                                    back_populates="litter_declarant_documents")
