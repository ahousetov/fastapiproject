from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Pedigree_declarant_requests(Base):
    """
    Модель для таблицы requests.pedigree_declarant_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Pedigree_declarant_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Pedigree_declarant_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    email = Column("email", VARCHAR, nullable=True)
    biometric_card_document_id = Column("biometric_card_document_id", Integer, nullable=False)
    personal_data_document_id = Column("personal_data_document_id", Integer, nullable=False)
    biometric_card_document_accept = Column("biometric_card_document_accept", Boolean, default=False)
    personal_data_document_accept = Column("personal_data_document_accept", Boolean, default=False)
    chip = Column("chip", Boolean, nullable=False)
    chip_number = Column("chip_number", VARCHAR, nullable=True)
    number = Column("number", VARCHAR, nullable=True)
    dog_name = Column("dog_name", VARCHAR, nullable=False)
    dog_name_lat = Column("dog_name_lat", VARCHAR, nullable=True)
    dog_birth_date = Column("dog_birth_date", TIMESTAMP, nullable=False)
    dog_sex_type = Column("dog_sex_type", Integer, nullable=False)
    stamp_number = Column("stamp_number", VARCHAR, nullable=False)
    color = Column("color", VARCHAR, nullable=False)
    father_name = Column("father_name", VARCHAR, nullable=False)
    father_pedigree_number = Column("father_pedigree_number", VARCHAR, nullable=False)
    mother_name = Column("mother_name", VARCHAR, nullable=False)
    mother_pedigree_number = Column("mother_pedigree_number", VARCHAR, nullable=False)
    breeder_first_name = Column("breeder_first_name", VARCHAR, nullable=False)
    breeder_last_name = Column("breeder_last_name", VARCHAR, nullable=False)
    breeder_second_name = Column("breeder_second_name", VARCHAR, nullable=True)
    breeder_address = Column("breeder_address", VARCHAR, nullable=False)
    owner_first_name = Column("owner_first_name", VARCHAR, nullable=False)
    owner_last_name = Column("owner_last_name", VARCHAR, nullable=False)
    owner_second_name = Column("owner_second_name", VARCHAR, nullable=True)
    owner_address = Column("owner_address", VARCHAR, nullable=False)
    owner_first_name_lat = Column("owner_first_name_lat", VARCHAR, nullable=False)
    owner_last_name_lat = Column("owner_last_name_lat", VARCHAR, nullable=False)
    owner_address_lat = Column("owner_address_lat", VARCHAR, nullable=False)
    was_reviewed = Column("was_reviewed", Boolean, nullable=False)
    litter_or_request_number = Column("litter_or_request_number", VARCHAR, nullable=True)
    mother_foreign = Column("mother_foreign", Boolean, nullable=False)
    father_foreign = Column("father_foreign", Boolean, nullable=False)
    mother_pedigree_document_id = Column("mother_pedigree_document_id", Integer, nullable=True)
    father_pedigree_document_id = Column("father_pedigree_document_id", Integer, nullable=True)
    express = Column("express", Boolean, nullable=False)
    pedigree_link = Column("pedigree_link", VARCHAR, nullable=True)
    one_generation = Column("one_generation", Boolean, nullable=False)
    two_generation = Column("two_generation", Boolean, nullable=False)
    foreign_owner = Column("foreign_owner", Boolean, nullable=False)
    paid_earlier = Column("paid_earlier", Boolean, default=False)
    co_owner_first_name = Column("co_owner_first_name", VARCHAR, nullable=True)
    co_owner_last_name = Column("co_owner_last_name", VARCHAR, nullable=True)
    co_owner_second_name = Column("co_owner_second_name", VARCHAR, nullable=True)
    co_owner_mail = Column("co_owner_mail", VARCHAR, nullable=True)
    is_pedigree_link_show = Column("is_pedigree_link_show", Boolean, default=False)

    breed_id = Column("breed_id", Integer, ForeignKey("dogs.breeds.id"), nullable=False)
    breeds = relationship("Breeds",
                          foreign_keys="Pedigree_declarant_requests.breed_id",
                          back_populates="pedigree_declarant_requests")

    stamp_code_id = Column("stamp_code_id", Integer, ForeignKey("public.stamp_codes.id"), nullable=True)
    stamp_codes = relationship("Stamp_codes",
                               foreign_keys="Pedigree_declarant_requests.stamp_code_id",
                               back_populates="pedigree_declarant_requests")

    pedigree_header_declarant_request_id = Column("pedigree_header_declarant_request_id",
                                                  Integer,
                                                  ForeignKey("requests.pedigree_header_declarant_requests.id"),
                                                  nullable=False)
    pedigree_header_declarant_requests = relationship("Pedigree_header_declarant_requests",
                                                      foreign_keys="Pedigree_declarant_requests."
                                                                   "pedigree_header_declarant_request_id",
                                                      back_populates="pedigree_declarant_requests")
