from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Pedigree_declarant_requests_histories(Base):
    """
    Модель для таблицы requests.pedigree_declarant_requests_histories.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Pedigree_declarant_requests_histories в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Pedigree_declarant_requests_histories"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)
    date_create = Column("date_create", TIMESTAMP, nullable=False)
    comment = Column("comment", VARCHAR, nullable=True)
    is_deleted = Column("is_deleted", Boolean, default=False)
    status_id = Column("status_id", Integer, nullable=True)
    document_id = Column("document_id", Integer, nullable=True)
    date_ready = Column("date_ready", TIMESTAMP, nullable=True)

    pedigree_header_declarant_request_id = Column("pedigree_header_declarant_request_id",
                                                  Integer,
                                                  ForeignKey("requests.pedigree_header_declarant_requests.id"),
                                                  nullable=False)
    pedigree_header_declarant_requests = relationship("Pedigree_header_declarant_requests",
                                                      foreign_keys="Pedigree_declarant_requests_histories."
                                                                   "pedigree_header_declarant_request_id",
                                                      back_populates="pedigree_declarant_requests_histories")



