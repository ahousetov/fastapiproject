from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Replace_pedigree_out_rkf_fci_requests(Base):
    """
    Модель для таблицы requests.replace_pedigree_out_rkf_fci_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Replace_pedigree_out_rkf_fci_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Replace_pedigree_out_rkf_fci_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    copy_pedigree_document_id = Column('copy_pedigree_document_id', Integer, nullable=False)
    personal_data_document_id = Column('personal_data_document_id', Integer, nullable=False)
    truncated_pedigree_application_document_id = Column('truncated_pedigree_application_document_id',
                                                        Integer,
                                                        nullable=False)
    payment_document_id = Column('payment_document_id', Integer, nullable=False)
    payment_date = Column('payment_date', TIMESTAMP, nullable=False)
    payment_number = Column('payment_number', VARCHAR, nullable=False)
    payment_name = Column('payment_name', VARCHAR, nullable=False)
    inn = Column('inn', VARCHAR, nullable=True)
    pedigree_link = Column('pedigree_link', VARCHAR, nullable=True)
    payment_document_accept = Column('payment_document_accept', Boolean, nullable=False, default=False)
    dog_name = Column('dog_name', VARCHAR, nullable=True)
    owner_name = Column('owner_name', VARCHAR, nullable=True)
    stamp_code = Column('stamp_code', VARCHAR, nullable=True)
    stamp_number = Column('stamp_number', VARCHAR, nullable=True)
    chip_number = Column('chip_number', VARCHAR, nullable=True)
    rkf_payment_date = Column('rkf_payment_date', TIMESTAMP, nullable=True)
    rkf_payment_number = Column('rkf_payment_number', VARCHAR, nullable=True)

    replace_pedigree_header_requests_id = Column(
        'replace_pedigree_header_requests_id',
        Integer,
        ForeignKey('requests.replace_pedigree_header_requests.id'),
        nullable=False)
    replace_pedigree_header_requests = relationship(
        "Replace_pedigree_header_requests",
        foreign_keys="Replace_pedigree_out_rkf_fci_requests."
                     "replace_pedigree_header_requests_id",
        back_populates="replace_pedigree_out_rkf_fci_requests")

    breed_id = Column(
        'breed_id',
        Integer,
        ForeignKey('dogs.breeds.id'),
        nullable=True)
    breeds = relationship(
        "Breeds",
        foreign_keys="Replace_pedigree_out_rkf_fci_requests.breed_id",
        back_populates="replace_pedigree_out_rkf_fci_requests")
