from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Litter_declarant_requests(Base):
    """
    Модель для таблицы requests.litter_declarant_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Litter_declarant_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Litter_declarant_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    email = Column('email', VARCHAR, nullable=True)
    application_document_id = Column('application_document_id', Integer, nullable=True)
    application_document_accept = Column('application_document_accept', Boolean, default=False)
    litter_diagnostic_id = Column('litter_diagnostic_id', Integer, nullable=False)
    litter_diagnostic_accept = Column('litter_diagnostic_accept', Boolean, default=False)
    dog_mating_act_id = Column('dog_mating_act_id', Integer, nullable=False)
    dog_mating_act_accept = Column('dog_mating_act_accept', Boolean, default=False)
    father_pedigree_document_id = Column('father_pedigree_document_id', Integer, nullable=True)
    parent_certificate_2_id = Column('parent_certificate_2_id', Integer, nullable=True)
    parent_certificate_2_accept = Column('parent_certificate_2_accept', Boolean, default=False)
    first_name = Column('first_name', VARCHAR, nullable=False)
    last_name = Column('last_name', VARCHAR, nullable=False)
    second_name = Column('second_name', VARCHAR, nullable=True)
    first_name_lat = Column('first_name_lat', VARCHAR, nullable=False)
    last_name_lat = Column('last_name_lat', VARCHAR, nullable=False)
    address = Column('address', VARCHAR, nullable=False)
    address_lat = Column('address_lat', VARCHAR, nullable=False)
    breed_id = Column('breed_id', Integer, nullable=False)
    mother_name = Column('mother_name', VARCHAR, nullable=False)
    mother_pedigree_number = Column('mother_pedigree_number', VARCHAR, nullable=False)
    father_name = Column('father_name', VARCHAR, nullable=False)
    father_pedigree_number = Column('father_pedigree_number', VARCHAR, nullable=False)
    date_of_birth_litter = Column('date_of_birth_litter', TIMESTAMP, nullable=False)
    litters_accept = Column('litters_accept', Boolean, default=False)
    nursery_name = Column('nursery_name', VARCHAR, nullable=True)
    personal_data_document_id = Column('personal_data_document_id', Integer, nullable=False)
    personal_data_document_accept = Column('personal_data_document_accept', Boolean, default=False)
    instructor_nursery_owner_first_name = Column('instructor_nursery_owner_first_name', VARCHAR, nullable=True)
    instructor_nursery_owner_last_name = Column('instructor_nursery_owner_last_name', VARCHAR, nullable=True)
    instructor_nursery_owner_second_name = Column('instructor_nursery_owner_second_name', VARCHAR, nullable=True)
    hallmark_first_name = Column('hallmark_first_name', VARCHAR, nullable=False)
    hallmark_last_name = Column('hallmark_last_name', VARCHAR, nullable=False)
    hallmark_second_name = Column('hallmark_second_name', VARCHAR, nullable=True)
    mother_foreign = Column('mother_foreign', Boolean, default=False)
    father_foreign = Column('father_foreign', Boolean, default=False)
    suffix = Column('suffix', Boolean, default=False)
    prefix = Column('prefix', Boolean, default=False)
    paid_earlier = Column('paid_earlier', Boolean, default=False)
    express = Column('express', Boolean, default=False)
    decision_breeding_commission_document_id = Column(
        'decision_breeding_commission_document_id',
        Integer, nullable=True)
    decision_breeding_commission_document_accept = Column(
        'decision_breeding_commission_document_accept',
        Boolean, default=False)
    receipt_payment_fee_violated_breeding_document_id = Column(
        'receipt_payment_fee_violated_breeding_document_id',
        Integer, nullable=True)
    receipt_payment_fee_violated_breeding_document_accept = Column(
        'receipt_payment_fee_violated_breeding_document_accept',
        Boolean, default=False)

    litter_header_declarant_request_id = Column('litter_header_declarant_request_id',
                                                Integer,
                                                ForeignKey("requests.litter_header_declarant_requests.id"),
                                                nullable=False)
    litter_header_declarant_requests = relationship("Litter_header_declarant_requests",
                                                    foreign_keys="Litter_declarant_requests."
                                                                 "litter_header_declarant_request_id",
                                                    back_populates="litter_declarant_requests")

    stamp_code_id = Column('stamp_code_id',
                           Integer,
                           ForeignKey("public.stamp_codes.id"),
                           nullable=True)
    stamp_codes = relationship("Stamp_codes",
                               foreign_keys="Litter_declarant_requests.stamp_code_id",
                               back_populates="litter_declarant_requests")