from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Pedigree_header_declarant_requests(Base):
    """
    Модель для таблицы requests.pedigree_header_declarant_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Pedigree_header_declarant_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Pedigree_header_declarant_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)
    declarant_uid = Column("declarant_uid", UUID(as_uuid=True), nullable=False)
    # default=uuid.uuid4()
    # primary_key=True
    barcode = Column("barcode", VARCHAR, nullable=False)
    date_create = Column("date_create", TIMESTAMP, nullable=False)
    date_change = Column("date_change", TIMESTAMP, nullable=False)
    is_deleted = Column("is_deleted", Boolean, default=False)

    pedigree_request_id = Column("pedigree_request_id",
                                 Integer,
                                 ForeignKey('requests.pedigree_requests.id'),
                                 nullable=False)
    pedigree_requests = relationship("Pedigree_requests",
                                     foreign_keys="Pedigree_header_declarant_requests.pedigree_request_id",
                                     back_populates="pedigree_header_declarant_requests")

    status_id = Column("status_id",
                       Integer,
                       ForeignKey('requests.pedigree_request_statuses.id'),
                       nullable=False)
    pedigree_request_statuses = relationship("Pedigree_request_statuses",
                                             foreign_keys=[status_id],
                                             back_populates="pedigree_header_declarant_requests"
                                             )

    prev_status_id = Column("prev_status_id",
                            Integer,
                            ForeignKey('requests.pedigree_request_statuses.id'),
                            nullable=False)
    prev_pedigree_request_statuses = relationship("Pedigree_request_statuses",
                                                  foreign_keys=[prev_status_id],
                                                  back_populates="pedigree_header_declarant_requests"
                                                  )

    pedigree_declarant_requests_histories = relationship("Pedigree_declarant_requests_histories",
                                                         back_populates="pedigree_header_declarant_requests")
    pedigree_declarant_documents = relationship("Pedigree_declarant_documents",
                                                back_populates="pedigree_header_declarant_requests")
    pedigree_declarant_requests = relationship("Pedigree_declarant_requests",
                                               back_populates="pedigree_header_declarant_requests")
