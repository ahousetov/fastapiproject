from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Litter_requests(Base):
    """
    Модель для таблицы requests.litter_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('requests')
    - 'extend_existing' - если для таблицы с указанным именем (Litter_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Litter_requests"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'requests'}

    id = Column("id", Integer, primary_key=True, index=True)

    federation_accept = Column('federation_accept', Boolean, default=False)
    payment_document_id = Column('payment_document_id', Integer, nullable=True)
    payment_document_accept = Column('payment_document_accept', Boolean, default=False)
    payment_date = Column('payment_date', TIMESTAMP, nullable=True)
    payment_date_accept = Column('payment_date_accept', Boolean, default=False)
    payment_number = Column('payment_number', VARCHAR, nullable=True)
    payment_number_accept = Column('payment_number_accept', Boolean, default=False)
    date_create = Column('date_create', TIMESTAMP, nullable=False)
    date_change = Column('date_change', TIMESTAMP, nullable=False)
    is_deleted = Column('is_deleted', Boolean, default=False)
    request_check_code = Column('request_check_code', UUID(as_uuid=True), nullable=False)
    folder_number = Column('folder_number', VARCHAR, nullable=True)
    inn = Column('inn', VARCHAR, nullable=True)
    payment_name = Column('payment_name', VARCHAR, nullable=True)
    barcode = Column('barcode', VARCHAR, nullable=True)
    cash_payment = Column('cash_payment', Boolean, nullable=False)
    cash_payment_accept = Column('cash_payment_accept', Boolean, default=False)
    rkf_buh_accept = Column('rkf_buh_accept', Boolean, default=False)
    rkf_payment_date = Column('rkf_payment_date', TIMESTAMP, nullable=True)
    rkf_payment_number = Column('rkf_payment_number', VARCHAR, nullable=True)
    date_archive = Column('date_archive', TIMESTAMP, nullable=True)

    federation_id = Column('federation_id', Integer, ForeignKey("catalogs.federations.id"), nullable=False)
    federations = relationship("Federations",
                               foreign_keys="Litter_requests.federation_id",
                               back_populates="litter_requests")

    status_id = Column('status_id', Integer, ForeignKey("requests.request_statuses.id"), nullable=False)
    request_statuses = relationship("Request_statuses",
                                    foreign_keys="Litter_requests.status_id",
                                    back_populates="litter_requests")

    declarant_id = Column('declarant_id', Integer, ForeignKey("declarants.declarants.id"), nullable=False)
    declarants = relationship("Declarants",
                              foreign_keys="Litter_requests.declarant_id",
                              back_populates="litter_requests")

    litter_header_declarant_requests = relationship("Litter_header_declarant_requests",
                                                    back_populates="litter_requests")
