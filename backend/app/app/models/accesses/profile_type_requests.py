from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Profile_type_requests(Base):
    """
    Модель для таблицы accesses.profile_type_requests.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('accesses')
    - 'extend_existing' - если для таблицы с указанным именем (profile_type_requests в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Связь тип профиля - тип запроса."
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'accesses'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_type_id = Column(Integer,
                             ForeignKey('public.profile_types.id'),
                             name="profile_type_id",
                             nullable=True)
    request_type_id = Column(Integer,
                             ForeignKey('accesses.request_types.id'),
                             name="request_type_id",
                             nullable=True)
    is_default = Column(Boolean(), name="is_default", nullable=False)

    profile_type = relationship("Profile_types",
                                primaryjoin="and_(Profile_types.id==Profile_type_requests.profile_type_id)",
                                foreign_keys="Profile_type_requests.profile_type_id",
                                back_populates="profile_type_requests")

    request_types = relationship("Request_types",
                                 primaryjoin="and_(Request_types.id==Profile_type_requests.request_type_id)",
                                 foreign_keys="Profile_type_requests.request_type_id",
                                 back_populates="profile_type_requests")