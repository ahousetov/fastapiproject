from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Request_types(Base):
    """
    Модель для таблицы accesses.request_types.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('accesses')
    - 'extend_existing' - если для таблицы с указанным именем (request_types в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Типы запросов"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'accesses'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}

    id = Column(Integer, name="id", primary_key=True, index=True)
    name = Column(String, name="name")
    description = Column(String, name="description")
    is_deleted = Column(Boolean(), name="is_deleted", default=False)

    user_accesses = relationship("User_accesses", back_populates="request_types")
    profile_type_requests = relationship("Profile_type_requests", back_populates="request_types")
