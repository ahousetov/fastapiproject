import datetime
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Nurseries(Base):
    """
    Модель для таблицы nurseries.nurseries.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Nurseries в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Питомники"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'nurseries'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}
    id = Column(Integer, name="id", primary_key=True, index=True)
    create_date = Column(TIMESTAMP, name="create_date",
                         nullable=False, default=datetime.datetime.now())
    profile_id = Column(Integer, ForeignKey('public.profiles.id'),
                        name="profile_id", nullable=False,
                        index=True)
    owner_specialist_rkf = Column(Boolean(), name="owner_specialist_rkf",
                                  default=False, nullable=False)
    owner_speciality = Column(String, name="owner_speciality",
                              nullable=True)
    owner_special_education = Column(String, name="owner_special_education",
                                     nullable=True)
    owner_date_speciality = Column(TIMESTAMP, name="owner_date_speciality",
                                   nullable=True)
    owner_place_speciality = Column(String, name="owner_place_speciality",
                                    nullable=True)
    owner_ranks = Column(String, name="owner_ranks",
                         nullable=True)
    dogs_ranks = Column(String, name="dogs_ranks",
                        nullable=True)
    registration_date = Column(TIMESTAMP, name="registration_date",
                               nullable=False, default=datetime.datetime.now())
    certificate_rkf_number = Column(String, name="certificate_rkf_number",
                                    nullable=True)
    experience_dog_breeding = Column(TIMESTAMP, name="experience_dog_breeding",
                                     nullable=True)
    puppies_total_count = Column(Integer, name="puppies_total_count",
                                 index=True, nullable=True)
    prefix = Column(Boolean(), name="prefix", nullable=True, default=False)
    suffix = Column(Boolean(), name="suffix", nullable=True, default=False)
    certificate_registration_nursery_id = Column(Integer, name="certificate_registration_nursery_id",
                                                 nullable=False)
    certificate_registration_in_rkf_id = Column(Integer, name="certificate_registration_in_rkf_id",
                                                 nullable=False)
    certificate_special_education_id = Column(Integer, name="certificate_special_education_id",
                                              nullable=True)
    certificate_specialist_rkf_id = Column(Integer, name="certificate_specialist_rkf_id",
                                           nullable=True)
    certificate_honorary_title_id = Column(Integer, name="certificate_honorary_title_id",
                                           nullable=True)
    is_deleted = Column(Boolean(), name="is_deleted", default=False, nullable=False)
    name_lat = Column(String, name="name_lat", nullable=True)
    co_owner_first_name = Column(String, name="co_owner_first_name", nullable=True)
    co_owner_last_name = Column(String, name="co_owner_last_name", nullable=True)
    co_owner_second_name = Column(String, name="co_owner_second_name", nullable=True)
    co_owner_mail = Column(String, name="co_owner_mail", nullable=True)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Nurseries.profile_id)",
                            foreign_keys="Nurseries.profile_id",
                            back_populates="nurseries")

    rel_nurseries_breeding_breeds = relationship("Rel_nurseries_breeding_breeds",
                                                 back_populates="nurseries")