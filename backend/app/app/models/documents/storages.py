from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Storages(Base):
    """
    Модель для таблицы storages.storages.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('storages')
    - 'extend_existing' - если для таблицы с указанным именем (Storages в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Storages"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'storages'}
    id = Column(Integer, name="id", primary_key=True, index=True)
    host = Column(String, name="host", nullable=False)
    path = Column(String, name="path", nullable=False)
    public_path = Column(String, name="public_path", nullable=False)
    description = Column(String, name="description", nullable=False)

    #documents = relationship("Documents",  back_populates="storage")