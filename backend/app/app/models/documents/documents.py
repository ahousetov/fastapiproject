from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Documents(Base):
    """
    Модель для таблицы documents.documents.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('documents')
    - 'extend_existing' - если для таблицы с указанным именем (Documents в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Documents"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'documents'}
    id = Column(Integer, name="id", primary_key=True, index=True)
    phisical_location = Column(String, name="phisical_location", nullable=False)
    size = Column(Integer, name="size", index=True)
    load_date = Column(TIMESTAMP, name="load_date", nullable=False)
    format = Column(String, name="format", nullable=False)
    name = Column(String, name="name", nullable=False)
    is_public = Column(Boolean(), name="is_public", default=False, nullable=False)
    storage_id = Column(Integer,
                        ForeignKey('documents.storages.id'),
                        name="storage_id",
                        nullable=True)
    date_archive = Column(TIMESTAMP, name="date_archive", nullable=False)

    storage = relationship("Storages",
                           primaryjoin="and_(Storages.id==Documents.storage_id)",
                           foreign_keys="Documents.storage_id")

    # Комментирую эти ключи, т.к. аж 5 из них ссылаются на поле Documents.id и поэтому
    # certificate_registration_nursery_id = relationship("Nursery_registration_requests",
    #                                                    back_populates="nursery_registration_request_statuses_certificate_registration_nursery_id")
    # certificate_registration_in_rkf_id = relationship("Nursery_registration_requests",
    #                                                    cascade="all,delete",
    #                                                    back_populates="nursery_registration_request_statuses_certificate_registration_in_rkf_id")
    # certificate_special_education_id = relationship("Nursery_registration_requests",
    #                                                 cascade="all,delete",
    #                                                 back_populates="nursery_registration_request_statuses_certificate_special_education_id")
    # certificate_specialist_rkf_id = relationship("Nursery_registration_requests",
    #                                              cascade="all,delete",
    #                                              back_populates="nursery_registration_request_statuses_certificate_specialist_rkf_id")
    # certificate_honorary_title_id = relationship("Nursery_registration_requests",
    #                                              cascade="all,delete",
    #                                              back_populates="nursery_registration_request_statuses_certificate_honorary_title_id")

