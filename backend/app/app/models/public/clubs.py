from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base

# select
# p.id as id,
# u.id as user_id,
# li.id as legal_information_id,
# li."name" as full_name,
# li.short_name as short_name,
# li.owner_position as head_position,
# li.owner_name as head_name,
# os.id as org_status_id,
# os."name" as org_status_name,
# li.registration_date as date_registered,
# li.liquidate_date as date_liquidation,
# f.short_name as federation_name,
# li.folder_number,
# li.ogrn as ogrn,
# li.inn as inn,
# li.kpp as kpp,
# li.okpo as okpo,
# li.okved as okved,
# bd.bank_name as bank_name,
# bd.bic_number as bik,
# bd.rs_number as payment_account_number,
# contact1.value as phone,
# contact2.value as email,
# contact3.value as site_address,
# ca1.postcode as legal_index,
# ct1."name" as legal_city_type,
# c1."name" as legal_city_name,
# st1."name" || ' ' || ca1.street_name as legal_street_full,
# ht1."name" || ' ' || ca1.house_name as legal_house_full,
# ft1."name" as legal_flat_type,
# ca1.flat_name as legal_flat,
# ca1.geo_lat as legal_geo_lat,
# ca1.geo_lon as legal_geo_lon,
# ca2.postcode as local_index,
# ct2."name" as local_city_type,
# c2."name" as local_city_name,
# st2."name" || ' ' || ca2.street_name as local_street_full,
# ht2."name" || ' ' || ca2.house_name as local_house_full,
# ft2."name" as local_flat_type,
# ca2.flat_name as local_flat,
# ca2.geo_lat as local_geo_lat,
# ca2.geo_lon as local_geo_lon,
# li.is_enable_web
# from public.profiles p
# inner join public.profile_types pt on pt.id =p.profile_type_id
# inner join relations.rel_users_profiles up on up.profile_id =p.id
# inner join public.legal_informations li on li.profile_id = p.id
# inner join public.users u on u.id =up.user_id
# left join catalogs.organization_statuses os on os.id =li.organization_status_id
# left join relations.rel_profiles_federations pf on pf.profile_id =p.id
# left join catalogs.federations f on f.id = pf.federation_id
# left join public.bank_data bd on bd.profile_id = p.id
# left join (select
#             rpc.profile_id,
#             c.value
#             from relations.rel_profiles_contacts rpc
#             inner join contacts c on c.id =rpc.contact_id
#             where c.type_id =1 and c.is_main = true and c.is_deleted = false) as contact1 on contact1.profile_id = p.id
# left join (select
#             rpc.profile_id,
#             c.value from relations.rel_profiles_contacts rpc
#             inner join contacts c on c.id =rpc.contact_id
#             where c.type_id =2 and c.is_main = true and c.is_deleted = false) as contact2 on contact2.profile_id = p.id
# left join (select
#             rpc.profile_id, c.value from relations.rel_profiles_contacts rpc
#             inner join contacts c on c.id =rpc.contact_id
#             where c.type_id =3 and c.is_main = true and c.is_deleted = false) as contact3 on contact3.profile_id = p.id
# left join public.club_addresses ca1 on ca1.profile_id =p.id and ca1.is_deleted = false and ca1.address_type_id = 2
# left join catalogs.cities c1 on c1.id =ca1.city_id
# left join catalogs.city_types ct1 on ct1.id =c1.type_id
# left join catalogs.street_types st1 on st1.id =ca1.street_type_id
# left join catalogs.house_types ht1 on ht1.id = ca1.house_type_id
# left join catalogs.flat_types ft1 on ft1.id = ca1.flat_type_id
# left join public.club_addresses ca2 on ca2.profile_id = p.id and ca2.is_deleted = false and ca2.address_type_id = 1
# left join catalogs.cities c2 on c2.id = ca2.city_id
# left join catalogs.city_types ct2 on ct2.id =c2.type_id
# left join catalogs.street_types st2 on st2.id = ca2.street_type_id
# left join catalogs.house_types ht2 on ht2.id = ca2.house_type_id
# left join catalogs.flat_types ft2 on ft2.id = ca2.flat_type_id
# where p.is_deleted = false and li.is_deleted = false and pt."name" ='Club'
# order by full_name asc


class Clubs(Base):
    """
    Модель для таблицы public.clubs.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Клубы"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}


