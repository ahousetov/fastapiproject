import uuid
import datetime
from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from app.db.base_class import Base


class Nursery_stamp_codes(Base):
    """
    Модель для таблицы public.nursery_stamp_codes.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Nursery_nursery_stamp_codes в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Штампы питомников"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}

    id = Column("id", Integer, primary_key=True, index=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        index=True)
    stamp_code_id = Column(Integer,
                           ForeignKey('public.stamp_codes.id'),
                           name="stamp_code_id",
                           nullable=False)

    is_default = Column("is_default", Boolean, nullable=False)
    is_deleted = Column("is_deleted", Boolean, nullable=False)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Nursery_stamp_codes.profile_id)",
                            foreign_keys="Nursery_stamp_codes.profile_id",
                            back_populates="nursery_stamp_codes")

    stamp_codes = relationship("Stamp_codes",
                               primaryjoin="and_(Stamp_codes.id==Nursery_stamp_codes.stamp_code_id)",
                               foreign_keys="Nursery_stamp_codes.stamp_code_id",
                               back_populates="nursery_stamp_codes")