from sqlalchemy import Column, Integer, Boolean, ForeignKey, String
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Social_networks(Base):
    """
    Модель для таблицы public.social_networks.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('clubs')
    - 'extend_existing' - если для таблицы с указанным именем (federation_clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}

    # region Колонки
    id = Column("id", Integer, primary_key=True, index=True)
    site = Column("site", String, nullable=False)
    description = Column("description", String, nullable=True)
    is_deleted = Column("is_deleted", Boolean, default=False, nullable=False)
    type_id = Column("type_id", Integer,
                     ForeignKey('catalogs.social_networks_types.id'), nullable=False)
    # endregion Колонки

    # region Отношения
    social_networks_types = relationship(
        "Social_networks_types",
        primaryjoin="and_(Social_networks_types.id==Social_networks.type_id)",
        foreign_keys="Social_networks.type_id",
        back_populates="social_networks",
    )
    rel_national_breed_clubs_social_networks = relationship(
        "Rel_national_breed_clubs_social_networks",
        back_populates="social_networks",
    )
    # endregion Отношения
