from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Contacts(Base):
    """
    Модель для таблицы public.contacts.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Contacts в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Контакты"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}

    id = Column(Integer, name="id", primary_key=True, index=True)
    value = Column(String, name="value", nullable=False)
    description = Column(String, name="description", nullable=True)
    is_main = Column(Boolean(), name="is_main", default=False, nullable=False)
    is_deleted = Column(Boolean(), name="is_deleted", default=False, nullable=False)
    type_id = Column(Integer,
                     ForeignKey('catalogs.contact_types.id'),
                     name="type_id",
                     nullable=True)
    is_hidden = Column(Boolean(), name="is_hidden", default=False, nullable=False)

    # region Отношения
    rel = relationship(
        "Rel_profiles_contacts",
        cascade="all,delete",
        back_populates="contacts",
    )
    type = relationship(
        "Contact_types",
        foreign_keys="Contacts.type_id",
        back_populates="contact_type",
    )
    rel_national_breed_clubs_contacts = relationship(
        "Rel_national_breed_clubs_contacts",
        back_populates="contacts",
    )
    # endregion Отношения
