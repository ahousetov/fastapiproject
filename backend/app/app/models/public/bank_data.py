from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Bank_data(Base):
    """
    Модель для таблицы public.bank_data.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Bank_data в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Банковские данные"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    rs_number = Column(String, name="rs_number")
    bank_name = Column(String, name="bank_name")
    is_deleted = Column(Boolean(), name="is_deleted", default=False)
    bic_number = Column(String, name="bic_number")
    profile_id = Column(Integer,
                        ForeignKey("public.profiles.id"),
                        name="profile_id",
                        nullable=False)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Bank_data.profile_id)",
                            foreign_keys="Bank_data.profile_id",
                            back_populates="bank_data")

    # rel_profiles = relationship("Rel_profiles_users",
    #                             cascade="all,delete",
    #                             back_populates="users")
    #
    # # rel_profiles = relationship("Rel_profiles_users",
    # #                             cascade="all,delete",
    # #                             back_populates="users")
    #
    # rel_federation_clubs = relationship("Federation_clubs",
    #                                     cascade="all,delete",
    #                                     back_populates="users")