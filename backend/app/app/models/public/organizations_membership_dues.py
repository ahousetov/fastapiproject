import datetime
from app.models.public import Profiles
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Organizations_membership_dues(Base):
    """
    Модель для таблицы public.organizations_history.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "История деятельности"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}
    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        index=True)
    is_deleted = Column(Boolean(), name="is_deleted",
                                  default=False,
                                  nullable=True)
    year_payment = Column(Integer, name="year_payment", default=1900)
    date_payment = Column(TIMESTAMP,
                          name="date_payment",
                          default=datetime.datetime(1989, 2, 8))
    is_paid = Column(Boolean(), name="is_paid", default=False, nullable=True)
    comments = Column(String, name="comments", nullable=True)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Organizations_membership_dues.profile_id)",
                            foreign_keys="Organizations_membership_dues.profile_id",
                            back_populates="organizations_membership_dues")