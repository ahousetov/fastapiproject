import datetime
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP, ForeignKey
from app.db.base_class import Base


class Legal_informations(Base):
    """
    Модель для таблицы public.legal_informations.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('relations')
    - 'extend_existing' - если для таблицы с указанным именем (rel_breeds_sections_groups в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Официальная информация"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    name = Column(String, name="name", nullable=True)
    owner_name = Column(String, name="owner_name", nullable=True)
    address = Column(String, name="address", nullable=True)
    inn = Column(String, name="inn", nullable=True)
    kpp = Column(String, name="kpp", nullable=True)
    ogrn = Column(String, name="ogrn", nullable=True)
    okpo = Column(String, name="okpo", nullable=True)
    registration_number = Column(String, name="registration_number", nullable=True)
    registration_date = Column(TIMESTAMP, name="registration_date", default=datetime.datetime.now())
    is_public = Column(Boolean(), name="is_public", default=False, nullable=False)
    is_deleted = Column(Boolean(), name="is_deleted", default=False, nullable=False)
    # confident_legal_person = Column(String, name="confident_legal_person", nullable=True)
    owner_position = Column(String, name="owner_position", nullable=True)
    description = Column(String, name="description", nullable=True)
    short_name = Column(String, name="short_name", nullable=True)
    liquidate_date = Column(TIMESTAMP, name="liquidate_date")
    okved = Column(String, name="okved", nullable=True)
    is_enable_web = Column(Boolean(), name="is_enable_web", default=False)
    organization_status_id = Column(Integer,
                                    ForeignKey('catalogs.organization_statuses.id'),
                                    name="organization_status_id",
                                    nullable=True)
    organization_type_id = Column(Integer, name="organization_type_id", nullable=True)
    profile_id = Column(Integer, ForeignKey('public.profiles.id'), name="profile_id", nullable=True)
    city_id = Column(Integer, name="city_id", nullable=True)
    active_member = Column(Boolean(), name="active_member", default=False, nullable=False)
    folder_number = Column(String, name="folder_number", nullable=True)
    without_stamp_code = Column(Boolean(), name="without_stamp_code", default=False, nullable=False)
    active_rkf_user = Column(Boolean(), name="active_rkf_user", default=False, nullable=False)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Legal_informations.profile_id)",
                            foreign_keys="Legal_informations.profile_id",
                            back_populates="legal_informations")

    org_status = relationship("Organization_statuses",
                              primaryjoin="and_(Organization_statuses.id==Legal_informations.organization_status_id )",
                              foreign_keys="Legal_informations.organization_status_id ",
                              back_populates="legal_informations")

    # rel_profiles_federations = relationship("Rel_profiles_federations",
    #                                         cascade="all,delete",
    #                                         back_populates="legal_informations")
