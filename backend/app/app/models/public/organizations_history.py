import datetime
from app.models.public import Profiles
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Organizations_history(Base):
    """
    Модель для таблицы public.organizations_history.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Clubs в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "История деятельности"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}
    id = Column(Integer, name="id", primary_key=True, index=True)
    is_deleted = Column(Boolean(), name="is_deleted",
                                  default=False,
                                  nullable=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        nullable=True)
    tribal_reviews_declared = Column(Integer,
                                     name="tribal_reviews_declared",
                                     nullable=True)
    tribal_reviews_completed = Column(Integer,
                                      name="tribal_reviews_completed",
                                      nullable=True)
    first_exhibition_date = Column(TIMESTAMP,
                                   name="first_exhibition_date",
                                   default=datetime.datetime.now())
    last_exhibition_date = Column(TIMESTAMP,
                                  name="last_exhibition_date",
                                  default=datetime.datetime.now())
    tests_over_past_year = Column(Boolean(),
                                  name="tests_over_past_year",
                                  default=False,
                                  nullable=True)
    availability_premises = Column(Boolean(), name="availability_premises",
                                  default=False,
                                  nullable=True)
    presentation = Column(Boolean(), name="presentation", default=False,
                          nullable=True)
    exceptions_in_reports = Column(String, name="exceptions_in_reports",
                                   nullable=True)
    succession_of_exhibition_activities = Column(String,
                                                 name="succession_of_exhibition_activities",
                                                 nullable=True)
    exhibition_activities_past_periods = Column(String,
                                                name="exhibition_activities_past_periods",
                                                nullable=True)
    sanctions = Column(String, name="sanctions", nullable=True)

    profiles = relationship("Profiles",
                            primaryjoin="and_(Profiles.id==Organizations_history.profile_id)",
                            foreign_keys="Organizations_history.profile_id",
                            back_populates="organizations_history")

