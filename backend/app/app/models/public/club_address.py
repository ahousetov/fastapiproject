import datetime
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Club_addresses(Base):
    """
    Модель для таблицы public.city_types.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (city_typess в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Адресные данные клубов"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}

    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_id = Column(Integer,
                        ForeignKey('public.profiles.id'),
                        name="profile_id",
                        nullable=True)
    postcode = Column(String, name="postcode")
    city_id = Column(Integer,
                     ForeignKey('catalogs.cities.id'),
                     name="city_id",
                     nullable=True)
    street_type_id = Column(Integer,
                            ForeignKey('catalogs.street_types.id'),
                            name="street_type_id",
                            nullable=True)
    street_name = Column(String, name="street_name")
    house_type_id = Column(Integer,
                           ForeignKey('catalogs.house_types.id'),
                           name="house_type_id",
                           nullable=True)
    house_name = Column(String, name="house_name")
    flat_type_id = Column(Integer,
                          ForeignKey('catalogs.flat_types.id'),
                          name="flat_type_id",
                          nullable=True)
    flat_name = Column(String, name="flat_name")
    geo_lat = Column(String, name="geo_lat")
    geo_lon = Column(String, name="geo_lon")
    create_date = Column(TIMESTAMP,
                         name="create_date",
                         default=datetime.datetime.now())
    is_deleted = Column(Boolean(), name="is_deleted", default=False)
    address_type_id = Column(Integer,
                             ForeignKey('catalogs.address_types.id'),
                             name="address_type_id",
                             nullable=True)
    building_name = Column(String, name="building_name")

    type = relationship("Address_types",
                        foreign_keys="Club_addresses.address_type_id",
                        back_populates="club_addresses")
    flat_type = relationship("Flat_types",
                             foreign_keys="Club_addresses.flat_type_id",
                             back_populates="club_addresses")
    house_type = relationship("House_types",
                              foreign_keys="Club_addresses.house_type_id",
                              back_populates="club_addresses")
    street_type = relationship("Street_types",
                               foreign_keys="Club_addresses.street_type_id",
                               back_populates="club_addresses")
    cities = relationship("Cities",
                          foreign_keys="Club_addresses.city_id",
                          back_populates="club_addresses")
    profiles = relationship("Profiles",
                            foreign_keys="Club_addresses.profile_id",
                            back_populates="club_addresses")

    # profiles = relationship("Profiles",
    #                         primaryjoin="and_(Profiles.id==Club_addresses.profile_id)",
    #                         foreign_keys="Club_addresses.profile_id",
    #                         back_populates="club_addresses")