import datetime
from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Profiles(Base):
    """
    Модель для таблицы public.profiles.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Profiles в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Профили"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}

    id = Column(Integer, name="id", primary_key=True, index=True)
    profile_type_id = Column(Integer,
                             ForeignKey('public.profile_types.id'),
                             name="profile_type_id",
                             nullable=True)
    client_id = Column(Integer, name="client_id", nullable=True)
    is_deleted = Column(Boolean(), name="is_deleted", default=False, nullable=False)
    is_active = Column(Boolean(), name="is_active", default=False, nullable=False)
    date_activate = Column(TIMESTAMP, name="date_activate", default=datetime.datetime.now())
    helpdesk_api_key = Column(String, name="helpdesk_api_key")
    alias_id = Column(Integer, name="alias_id")

    profile_type = relationship("Profile_types",
                                primaryjoin="and_(Profile_types.id==Profiles.profile_type_id)",
                                foreign_keys="Profiles.profile_type_id",
                                back_populates="profiles")

    rel = relationship("Rel_profiles_contacts",
                       cascade="all,delete",
                       back_populates="profiles")
    club_addresses = relationship("Club_addresses",
                                  cascade="all,delete",
                                  back_populates="profiles")
    rel_users_profiles = relationship("Rel_users_profiles",
                                      cascade="all,delete",
                                      back_populates="profiles")
    rel_profiles_federations = relationship("Rel_profiles_federations",
                                            cascade="all,delete",
                                            back_populates="profiles")
    legal_informations = relationship("Legal_informations",
                                      cascade="all,delete",
                                      back_populates="profiles")
    bank_data = relationship("Bank_data",
                             cascade="all,delete",
                             back_populates="profiles")
    federation_clubs = relationship("Federation_clubs",
                                    cascade="all,delete",
                                    back_populates="profiles")
    organizations_history = relationship("Organizations_history",
                                        cascade="all,delete",
                                        back_populates="profiles")
    organizations_membership_dues = relationship("Organizations_membership_dues",
                                                 cascade="all,delete",
                                                 back_populates="profiles")
    declarants = relationship("Declarants",
                              cascade="all,delete",
                              back_populates="profiles")
    nurseries = relationship("Nurseries",
                             cascade="all,delete",
                             back_populates="profiles")
    nursery_stamp_codes = relationship("Nursery_stamp_codes",
                                       cascade="all,delete",
                                       back_populates="profiles")
    nursery_registration_requests = relationship("Nursery_registration_requests",
                                                 cascade="all,delete",
                                                 back_populates="nursery_registration_requests_profiles")