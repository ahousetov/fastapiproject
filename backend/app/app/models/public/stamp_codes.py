from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Stamp_codes(Base):
    """
    Модель для таблицы public.stamp_codes.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Stamp_codes в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Stamp_codes"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}

    id = Column("id", Integer, primary_key=True, index=True)
    profile_id = Column("profile_id", Integer, nullable=True)
    stamp_code = Column("stamp_code", VARCHAR, nullable=False)
    is_default = Column("is_default", Boolean, nullable=False)
    is_deleted = Column("is_deleted", Boolean, nullable=False)

    pedigree_declarant_requests = relationship(
        "Pedigree_declarant_requests",
        back_populates="stamp_codes")

    litter_declarant_requests = relationship(
        "Litter_declarant_requests",
        back_populates="stamp_codes")

    replace_pedigree_export_old_requests = relationship(
        "Replace_pedigree_export_old_requests",
        back_populates="stamp_codes")

    replace_pedigree_old_requests = relationship(
        "Replace_pedigree_old_requests",
        back_populates="stamp_codes")

    replace_pedigree_duplicate_requests = relationship(
        "Replace_pedigree_duplicate_requests",
        back_populates="stamp_codes")

    replace_pedigree_owner_requests = relationship(
        "Replace_pedigree_owner_requests",
        back_populates="stamp_codes")

    replace_pedigree_declarant_error_requests = relationship(
        "Replace_pedigree_declarant_error_requests",
        back_populates="stamp_codes")

    nursery_stamp_codes = relationship(
        "Nursery_stamp_codes",
        back_populates="stamp_codes")