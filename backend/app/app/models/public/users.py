from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class Users(Base):
    """
    Модель для таблицы public.users.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('public')
    - 'extend_existing' - если для таблицы с указанным именем (Users в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Пользователи"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'public'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    login = Column(String, name="login")
    password = Column(String, name="password")
    mail = Column(String, name="mail")
    phone = Column(String, name="phone")
    isActive = Column(Boolean(), name="isActive", default=False)
    user_type = Column(Integer, name="user_type")
    client_id = Column(Integer, name="client_id")

    rel_users_profiles = relationship("Rel_users_profiles",
                                      cascade="all,delete",
                                      back_populates="users")

    user_accesses = relationship("User_accesses",
                                 cascade="all,delete",
                                 back_populates="users")