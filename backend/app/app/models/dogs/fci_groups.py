from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class FCI_groups(Base):
    """
    Модель для таблицы dogs.FCI_groups.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('dogs')
    - 'extend_existing' - если для таблицы с указанным именем (FCI_groups в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Группы пород в соответствии с классификацией FCI"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'dogs'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    #TODO: почитать про __mapper_args__
    # __mapper_args__ = {'always_refresh': True}

    id = Column(Integer, name="id", primary_key=True, index=True)
    number = Column(Integer, name="number", index=True)
    name = Column(String, name="name", index=True)
    name_en = Column(String, name="name_en", index=True)
    is_deleted = Column(Boolean(), name="is_deleted", default=False)

    #Связь через rel_breeds_sections_groups-таблицу осуществляется простой декларацией
    # отношения (relationship).
    rel_breeds_sections_groups = relationship("Rel_breeds_sections_groups", back_populates="groups")