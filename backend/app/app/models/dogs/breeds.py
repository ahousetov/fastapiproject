from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base_class import Base
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups


class Breeds(Base):
    """
    Модель для таблицы dogs.breeds.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('dogs')
    - 'extend_existing' - если для таблицы с указанным именем (Breeds в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Классификатор пород собак"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'dogs'}
    # Комментирую на будущее.
    # __mapper_args__ - набор параметров для мапера, который позволить
    # изменять текст SQL запросов, генерируемых ORM. В будущем это пригодиться
    # TODO: почитать про __mapper_args__
    # __mapper_args__ = {'column_prefix': '_!-'}

    id = Column(Integer, name="id", primary_key=True, index=True)
    name_rus = Column(String, name="name_rus", index=True)
    name_en = Column(String, name="name_en")
    name_original = Column(String, name="name_original")
    is_cacib = Column(Boolean(), name="is_cacib", default=False)
    is_fci = Column(Boolean(), name="is_fci", default=False)
    is_specific = Column(Boolean(), name="is_specific", default=False)
    is_deleted = Column(Boolean(), name="is_deleted", default=False)
    for_dog_handler = Column(Boolean(), name="for_dog_handler", default=False)
    fci_status_id = Column(Integer,
                           ForeignKey('dogs.fci_breed_statuses.id'),
                           nullable=True)
    fci_number = Column(Integer, name="fci_number", nullable=True)

    # region Отношения
    status = relationship("FCI_breed_statuses",
                          foreign_keys="Breeds.fci_status_id",
                          back_populates="fci_breed_statuses")
    rel = relationship("Rel_breeds_sections_groups",
                       cascade="all,delete",
                       back_populates="breeds")
    rel_nurseries_breeding_breeds = relationship(
        "Rel_nurseries_breeding_breeds",
        cascade="all,delete",
        back_populates="breeds"
    )
    national_breed_clubs_breeds = relationship(
        "National_breed_clubs_breeds",
        back_populates="breeds"
    )
    # endregion Отношения

    pedigree_declarant_requests = relationship(
        "Pedigree_declarant_requests",
        back_populates="breeds")

    replace_pedigree_export_old_requests = relationship(
        "Replace_pedigree_export_old_requests",
        back_populates="breeds")

    replace_pedigree_old_requests = relationship(
        "Replace_pedigree_old_requests",
        back_populates="breeds")

    replace_pedigree_duplicate_requests = relationship(
        "Replace_pedigree_duplicate_requests",
        back_populates="breeds")

    replace_pedigree_owner_requests = relationship(
        "Replace_pedigree_owner_requests",
        back_populates="breeds")

    replace_pedigree_declarant_error_requests = relationship(
        "Replace_pedigree_declarant_error_requests",
        back_populates="breeds")

    replace_pedigree_out_rkf_fci_requests = relationship(
        "Replace_pedigree_out_rkf_fci_requests",
        back_populates="breeds")

    replace_pedigree_foreign_requests = relationship(
        "Replace_pedigree_foreign_requests",
        back_populates="breeds")
