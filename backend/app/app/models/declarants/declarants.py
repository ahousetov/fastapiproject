from typing import TYPE_CHECKING
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey, VARCHAR, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
from app.db.base_class import Base


class Declarants(Base):
    """
    Модель для таблицы declarants.declarants.

    __table_args__ - параметры для SQLAlchemy.

    - 'quote' - названия полей таблиц в запросах заключается в кавычки (False)
    - 'schema' - название схемы для обращения к таблице ('declarants')
    - 'extend_existing' - если для таблицы с указанным именем (Declarants в данном случае),
    указываются новые поля для конструктора или прочие изменения, то все они применяются
    к УЖЕ СУЩЕСТВУЮЩЕЙ таблице.

    Если флаги 'extend_existing', 'keep_existing' не выставлены, при доп.изменениях
    таблиц уже объявленных в Metadata будет вызвана ошибка.
    """
    __title = "Declarants"
    __table_args__ = {'quote': False,
                      'extend_existing': False,
                      'schema': 'declarants'}

    id = Column("id", Integer, primary_key=True, index=True)
    first_name = Column("first_name", VARCHAR, nullable=True)
    last_name = Column("last_name", VARCHAR, nullable=True)
    second_name = Column("second_name", VARCHAR, nullable=True)
    index = Column("index", VARCHAR, nullable=True)
    city_id = Column("city_id", Integer, nullable=True)
    street = Column("street", VARCHAR, nullable=True)
    house = Column("house", VARCHAR, nullable=True)
    building = Column("building", VARCHAR, nullable=True)
    flat = Column("flat", VARCHAR, nullable=True)
    email = Column("email", VARCHAR, nullable=True)
    subscriber_mail = Column("subscriber_mail", VARCHAR, nullable=True)
    phone = Column("phone", VARCHAR, nullable=True)
    is_default = Column("is_default", Boolean, default=False)
    is_deleted = Column("is_deleted", Boolean, default=False)
    is_personal = Column("is_personal", Boolean, default=False)

    club_id = Column("club_id",
                     Integer,
                     ForeignKey("public.profiles.id"),
                     nullable=False)
    profiles = relationship("Profiles",
                            foreign_keys="Declarants.club_id",
                            back_populates="declarants")

    pedigree_requests = relationship("Pedigree_requests",
                                     back_populates="declarants")

    litter_requests = relationship("Litter_requests",
                                   back_populates="declarants")

    replace_pedigree_header_requests = relationship("Replace_pedigree_header_requests",
                                                    back_populates="declarants")

