import logging
import pprint

from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed
from app.core.config import settings
from app.db.session import SessionLocal

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 5  # 5 minutes
wait_seconds = 1




@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    try:
        db = SessionLocal()
        # Try to create session to check if DB is awake
        db.execute("SELECT 1")
        result = db.execute("SELECT * FROM information_schema.tables;")
        # result = db.execute("SELECT catalogs.federal_districts.id,"
        #                     "catalogs.federal_districts.name, "
        #                     "catalogs.federal_districts.is_deleted "
        #                     "FROM catalogs.federal_districts;")
        result = db.execute("SELECT catalogs.federal_districts.id,"
                            "catalogs.federal_districts.name, "
                            "catalogs.federal_districts.is_deleted "
                            "FROM catalogs.federal_districts;")
        d, a = {}, []
        for rowproxy in result:
            # rowproxy.items() returns an array like [(key0, value0), (key1, value1)]
            # for column, value in rowproxy.items():
            #     # build up the dictionary
            #     d = {**d, **{column: value}}
            # a.append(d)
            index, name, is_deleted = rowproxy
            a.append({index: {name: is_deleted}})
        print(f"d : {d}, a : {a}")
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Initializing service")
    #print(f"settings.SQLALCHEMY_DATABASE_URI : {settings.SQLALCHEMY_DATABASE_URI}")
    logger.info(f"settings.SQLALCHEMY_DATABASE_URI : {settings.SQLALCHEMY_DATABASE_URI}")
    init()
    logger.info("Service finished initializing")


if __name__ == "__main__":
    main()
