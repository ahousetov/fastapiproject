from typing import Optional, List, Type, Any, TypeVar
from app.schemas.common import *

from pydantic import BaseModel, Field


from app.schemas.common import RKF_office_base_model


class National_breed_clubs_base(RKF_office_base_model):
    """
    Общий шаблон national_breed_clubs.
    """
    id: Optional[int]
    nbc_name: Optional[str]
    owner_name: Optional[str]
    is_deleted: Optional[str]
    owner_position: Optional[str]
    comment: Optional[str] = Field(alias="comments")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class National_breed_clubs_create(RKF_office_base_model):
    """
    """
    nbc_name: str
    owner_name: str


class National_breed_clubs_update(RKF_office_base_model):
    """
    """
    id: int


class National_breed_clubs_delete(RKF_office_base_model):
    """
    """
    id: int


# region Схемы для api
# class OutputNBCOwner(BaseModel):
#     """
#     Владелец
#     """
#     owner_position: Optional[str]
#     nbc_name: Optional[str]


class OutputNBC(BaseModel):
    id: Optional[int]
    nbc_name: Optional[str]
    # owner: Optional[OutputNBCOwner]
    owner_name: Optional[str]
    owner_position: Optional[str]
    presidium_members: Optional[str]
    breeds: Optional[str]
    phones: Optional[str]
    emails: Optional[str]
    sites: Optional[str]
    comment: Optional[str] = Field(alias="comments")

    class Config:
        allow_population_by_field_name = True


class OutputDetailNBC(RKF_office_base_model):
    title: str = 'Национальные клубы пород'
    title_item: str = ''
    blocks: List[Any]


class MainBlockView(National_breed_clubs_base):
    block_title: str = 'Основная информация'
    block_icon: str = 'info_outline'


class ContactInfoBlockView(RKF_office_base_model):
    id: Optional[int]
    type_name: Optional[str]
    type_id: Optional[int]
    value: Optional[str]
    description: Optional[str]
    is_main: Optional[bool]


class ContactsBlocksView(RKF_office_base_model):
    block_title: str = 'Контактные данные'
    block_icon: str = 'call'
    contacts: Optional[List[ContactInfoBlockView]]


class MemberInfoBlockView(RKF_office_base_model):
    id: Optional[int]
    name: Optional[str]
    position: Optional[str]


class MembersBlocksView(RKF_office_base_model):
    block_title: str = 'Члены президиума'
    block_icon: str = 'group'
    presidium_members: Optional[List[MemberInfoBlockView]]


class BreedsInfoBlockView(RKF_office_base_model):
    id: Optional[int]
    fci_number: Optional[int]
    # group_number: Optional[int]
    breed_name: Optional[str]

    number: Optional[int] = Field(alias="group_number")

    class Config:
        allow_population_by_field_name = True

class BreedsBlocksView(RKF_office_base_model):
    block_title: str = 'Породы'
    block_icon: str = 'pets'
    breeds: Optional[List[BreedsInfoBlockView]]


# class InputCreateNBCMainInfo(BaseModel):
#     name: Optional[str]
#     owner_position: Optional[str]
#     owner_name: Optional[str]
#     comment: Optional[str]
#
#
# class InputCreateNBCPresidiumMembers(BaseModel):
#     pass
#
#
# class InputCreateNBC(BaseModel):
#     """
#     Набросок схемы входных данных для создания НКП
#     """
#     main_info: InputCreateNBCMainInfo
#     presidium_members: InputCreateNBCPresidiumMembers

# endregion Схемы для api


# region API. Создание
# class InputCreateNBCMainInfo(BaseModel):
#     """
#     Для таблицы clubs.national_breed_clubs
#     """
#     id: Optional[int]  #
#     nbc_name: str  #
#     owner_position: str  #
#     owner_name: str  #
#     comment: Optional[str]  #
#     is_deleted: Optional[bool] = False  #


# class InputCreateNBCPresidiumMember(BaseModel):
#     """
#     Для таблицы clubs.national_breed_clubs_presidium_members
#     """
#     id: Optional[int]  #
#     national_breed_club_id: int  #
#     name: str  #
#     position: str  #
#     is_deleted: Optional[bool] = False  #


# class InputCreateNBCBreed(BaseModel):
#     """
#     Для таблицы dogs.breeds
#     """
#     id: Optional[int]  #
#     name_rus: Optional[str]  #
#     name_en: Optional[str]  #
#     name_original: Optional[str]  #
#     is_cacib: Optional[bool] = False  #
#     is_fci: Optional[bool] = False  #
#     is_specific: Optional[bool] = False  #
#     is_deleted: Optional[bool] = False  #
#     for_dog_handler: Optional[bool] = False  #
#     fci_status_id: Optional[int]  #
#     fci_number: Optional[int]  #


# class InputCreateNBCContact(BaseModel):
#     """
#     Для таблицы public.contacts
#     """
#     id: Optional[int]  #
#     value: str  #
#     description: Optional[str]  #
#     is_main: Optional[bool] = False  #
#     is_deleted: Optional[bool] = False  #
#
#     # Таблица catalogs.contact_types
#     type_id: Optional[int]  #
#
#     is_hidden: Optional[bool] = False  #


# class InputCreateNBCSocialNetwork(BaseModel):
#     """
#     Для таблицы public.social_networks
#     """
#     id: Optional[int]  #
#     site: Optional[str]  #
#     description: Optional[str]  #
#     is_deleted: Optional[bool]  #
#
#     # Таблица catalogs.social_networks_types
#     type_id: Optional[int]  #


# class InputCreateNBC(BaseModel):
#     """
#     Набросок схемы входных данных для создания НКП
#     """
#     nbc_name: str
#     owner_name: str
#     owner_position: Optional[str]
#     comment: Optional[str]
#
#     # Прямая связь
#     presidium_members: Optional[List[InputCreateNBCPresidiumMember]]
#
#     # Через таблицу clubs.national_breed_clubs_breeds
#     breeds: Optional[List[InputCreateNBCBreed]]
#
#     # Через таблицу relations.rel_national_breed_clubs_contacts
#     contacts: Optional[List[InputCreateNBCContact]]
#
#     # # Через таблицу relations.rel_national_breed_clubs_social_networks
#     # social_networks: Optional[List[InputCreateNBCSocialNetwork]]

# endregion API. Создание


class ContactsInput(RKF_office_base_model):
    id: Optional[int] = None
    type_id: Optional[int] = None
    value: Optional[str] = None
    description: Optional[str]
    is_main: Optional[bool] = None


class ContactsUpdate(RKF_office_base_model):
    id: int
    type_id: Optional[int]
    value: Optional[str]
    description: Optional[str]
    is_main: Optional[bool]


class ContactsCreate(RKF_office_base_model):
    type_id: int
    value: str
    description: Optional[str]
    is_main: Optional[bool] = False


class MembersInput(RKF_office_base_model):
    id: Optional[int]
    name: Optional[str]
    position: Optional[str]


class MembersUpdate(RKF_office_base_model):
    id: int
    name: Optional[str]
    position: Optional[str]


class MembersCreate(RKF_office_base_model):
    name: Optional[str]
    position: Optional[str]


class BreedsRelInput(RKF_office_base_model):
    id: Optional[int]           # Breed_id в national_breed_clubs_breeds по сути


class BreedsRelCreate(RKF_office_base_model):
    id: int


class InputUpdateNBC(BaseModel):
    id: Optional[int]
    name: Optional[str] = Field(alias="nbc_name")
    owner_name: Optional[str]
    owner_position: Optional[str]
    comment: Optional[str] = Field(alias="comments")
    presidium_members: Optional[List[MembersInput]]
    breeds: Optional[List[BreedsRelInput]]
    contacts: Optional[List[ContactsInput]]
    class Config:
        allow_population_by_field_name = True


class InputNBCMainInfo(BaseModel):
    """
    Для таблицы clubs.national_breed_clubs
    """
    name: Optional[str] = Field(alias="nbc_name")
    owner_position: Optional[str]  #
    owner_name: Optional[str]  #
    comment: Optional[str] = Field(alias="comments")
    is_deleted: Optional[bool] = False  #

    class Config:
        allow_population_by_field_name = True


class OutputEditedNBC(BaseModel):
    id: Optional[int]
    nbc_name: str = None
    owner_name: str = None
    owner_position: str = None
    comment: Optional[str] = Field(alias="comments")
    presidium_members: List[MemberInfoBlockView] = None
    breeds: List[BreedsInfoBlockView] = None
    contacts: List[ContactInfoBlockView] = None

    class Config:
        allow_population_by_field_name = True



class InputCreateNBC(BaseModel):
    """
    Набросок схемы входных данных для создания НКП
    """
    name: str = Field(alias="nbc_name")
    # nbc_name: str
    owner_name: str
    owner_position: Optional[str]
    comment: Optional[str] = Field(alias="comments")
    presidium_members: Optional[List[MembersInput]]
    breeds: Optional[List[BreedsRelInput]]
    contacts: Optional[List[ContactsInput]]

    class Config:
        allow_population_by_field_name = True
