from app.schemas.common import *


class Federation_clubs_base(RKF_office_base_model):
    """
    Общий шаблон отношений federation_clubs.
    """
    id: Optional[int]
    profile_id: Optional[int] = None
    federation_id: Optional[int] = None

    class Config:
        orm_mode = True


class Federation_clubs_create(Federation_clubs_base):
    """
    Создаём новый federation_clubs.
    """
    pass


class Federation_clubs_update(Federation_clubs_base):
    """
    Изменяем секцию federation_clubs.
    """
    pass


class Federation_clubs_delete(Federation_clubs_base):
    """
    Удаляем секцию federation_clubs. (помечаем удалённым, не удаляем из БД)
    """
    id: int


class Federation_clubs_DB(Federation_clubs_base):
    """
    Модель-предсталвения данных в БД.
    """

    pass


class Federation_clubs(Federation_clubs_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Federation_clubs_DB, если не переопределим чего-то
    в наследнике.
    """
    pass