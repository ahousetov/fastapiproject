from typing import Optional

from app.schemas.common import RKF_office_base_model


class National_breed_clubs_breeds_base(RKF_office_base_model):
    """
    Общий шаблон national_breed_clubs_breeds.
    """
    id: Optional[int]
    national_breed_club_id: Optional[int]
    breed_id: Optional[int]
    is_deleted: Optional[bool]

    class Config:
        orm_mode = True


class National_breed_clubs_breeds_create(National_breed_clubs_breeds_base):
    """
    """
    national_breed_club_id: int
    breed_id: int


class National_breed_clubs_breeds_update(National_breed_clubs_breeds_base):
    """
    """
    id: int


class National_breed_clubs_breeds_delete(National_breed_clubs_breeds_base):
    """
    """
    id: int
    is_deleted: bool
