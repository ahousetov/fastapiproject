from typing import Optional

from app.schemas.common import RKF_office_base_model


class National_breed_clubs_presidium_members_base(RKF_office_base_model):
    """
    Общий шаблон national_breed_clubs_presidium_members.
    """
    id: Optional[int]
    national_breed_club_id: Optional[int]
    position: Optional[str]
    name: Optional[str]
    is_deleted: Optional[bool]

    class Config:
        orm_mode = True


class National_breed_clubs_presidium_members_create(National_breed_clubs_presidium_members_base):
    """
    """
    national_breed_club_id: int
    position: str
    name: str


class National_breed_clubs_presidium_members_update(National_breed_clubs_presidium_members_base):
    """
    """
    id: int


class National_breed_clubs_presidium_members_delete(National_breed_clubs_presidium_members_base):
    """
    """
    id: int
    is_deleted: bool
