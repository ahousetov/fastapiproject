from app.schemas.common import *


class Declarants_base(RKF_office_base_model):
    """
    Базовая схема для 'Declarants'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Declarants_create(Declarants_base):
    """
    Создание новой записи Declarants.

    Все поля наследуем от Declarants_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """


class Declarants_update(Declarants_base):
    """
    Редактирование данных Declarants.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Declarants_base ничего
    не добавляем.
    """


class Declarants_delete(Declarants_base):
    """
    Наследуемся от Declarants_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """