from typing import Optional, List, Any, Union, Dict
from pydantic import BaseModel, constr
from enum import Enum

#TODO: Оптимизировать содержание и вывод из Enum'ов путём
# выставления соответствия между значением поля Enum'а
# и тем значением условия для filter которое должно быть в запросе.


class Filter_types(Enum):
    name = "filterType"
    text = "text"
    number = "number"


class Filter_type_base(Enum):
    name = "type"


class Text_filter_type(Enum):
    name = "type"
    not_contains = "notContains"
    contains = "contains"
    equals = "equals"
    not_equals = "notEqual"
    starts_with = "startsWith"
    ends_with = "endsWith"


class Number_filter_type(Enum):
    name = "type"
    equals = "equals"
    less_than = "lessThan"
    greater_than = "greaterThan"


class Operators(Enum):
    name = "operator"
    _a = "AND"
    _o = "OR"


class Condition(BaseModel):
    filterType: Optional[str] = None
    type: Optional[str] = None
    filter: Optional[Union[constr(min_length=3), int]] = None
    operator: Optional[str] = None

    def is_empty(self):
        return self.filterType == None \
               and self.type == None \
               and self.filter == None \
               and self.operator == None


class Filters_field(Condition):
    class Config:
        extra = 'allow'


# Старая структура фильтров
# class Filters_field(Condition):
#     conditions: Optional[List[Condition]]


class Filters_model(BaseModel):
    filterModel: Optional[Dict[str, Filters_field]] = None
    sortModel: Optional[Dict[Any, str]] = None
    skip: Optional[int] = None
    limit: Optional[int] = None