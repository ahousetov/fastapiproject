from app.schemas.common import *


class Users_base(RKF_office_base_model):
    """
    Базовая схема для 'Населённых пунктов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    id: Optional[int]
    password: Optional[str]
    isActive: Optional[bool]
    user_type: Optional[int]
    client_id: Optional[int]

    id - обязательный параметр, поэтому указываем
    их в наследниках явно.
    """
    id: Optional[int]
    login: str
    password: Optional[str]
    mail: str
    phone: str
    isActive: Optional[bool]
    user_type: Optional[int]
    client_id: Optional[int]

    # @property
    # def main(self):
    #     return self.is_main

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Users_create(Users_base):
    """
    Создание нового пользователя.

    Все поля наследуем от Users_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    pass


class Users_update(Users_base):
    """
    Редактирование данных пользователя.
    """
    login: Optional[str]
    mail: Optional[str]
    phone: Optional[str]


class Users_delete(Users_base):
    """
    Наследуемся от Users_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    id: int


class Users_DB(Users_base):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Users_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    pass


class Users(Users_DB):
    """
    Схема-наследник Users_DB для вывода данных для
    одиночной записи Users, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Users_multi(Users_base):
    """
    Схема вывода списка данных по населённым пунктам.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    pass


class Users_block(Block, Users_base):
    pass


class Users_blocks(Blocks_list):
    blocks: Optional[List[Users_block]] = None