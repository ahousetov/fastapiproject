from app.schemas.common import *


class Profiles_base(RKF_office_base_model):
    """
    Базовая схема для 'Населённых пунктов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    id: Optional[int]
    profile_type_id: Optional[int]
    client_id: Optional[int]
    is_deleted: bool
    is_active: bool
    date_activate: Optional[datetime]
    helpdesk_api_key: str
    alias_id: Optional[int]

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    id: Optional[int]
    profile_type_id: Optional[int]
    client_id: Optional[int]
    is_deleted: bool
    is_active: bool
    date_activate: Optional[datetime]
    helpdesk_api_key: str
    alias_id: Optional[int]


    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Profiles_create(Profiles_base):
    """
    Создание нового населённого пункта.

    Все поля наследуем от Profiles_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    pass


class Profiles_update(Profiles_base):
    """
    Редактирование данных населённого пункта.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Profiles_base и Profiles_foreign_id ничего
    не добавляем.
    """
    is_deleted: Optional[bool] = None
    is_active: Optional[bool] = None
    helpdesk_api_key: Optional[str] = None


class Profiles_delete(Profiles_base):
    """
    Наследуемся от Profiles_base.

    id - обязательный параментр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    pass


class Profiles_DB(Profiles_base):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Profiles_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    pass


class Profiles(Profiles_DB):
    """
    Схема-наследник Profiles_DB для вывода данных для
    одиночной записи Profiles, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Profiles_multi(Profiles_base):
    """
    Схема вывода списка данных по населённым пунктам.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    pass
