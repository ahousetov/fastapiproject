from app.schemas.common import *
from app.schemas.public.organizations_history import Organization_history_item
from app.schemas.public.organizations_membership_dues import Organizations_membership_dues_item
# catalogs blocks
from app.schemas.catalogs.organization_statuses import Organization_statuses_block, Organization_statuses
from app.schemas.catalogs.federations import Federation_info_block
from app.schemas.catalogs.contact_types import Contact_types
from app.schemas.catalogs.address_types import Address_types


# p   - public_profile
# li  - public_legal_information
# pt  - public_profile_types
# up  - relations_rel_users_profiles
# u   - public_users
# os  - catalogs_organization_statuses
# pf  - relations_rel_profiles_federations
# f   - catalogs_federations
# bd  - public_bank_data
# rpc - relations_rel_profiles_contacts
# c   - public_contacts
# ca1 - public_club_addresses
# c1  - catalogs_cities
# ct1 - catalogs_city_types
# st1 - catalogs_street_types
# ht1 - catalogs_house_types
# ft1 - catalogs_flat_types
# ca2 - public_club_addresses
# c2  - catalogs_cities
# ct2 - catalogs_city_types
# st2 - catalogs_street_types
# ht2 - catalogs_house_types
# ft2 - catalogs_flat_types

# 'public_profiles_id',
# 'public_users_id',
# 'public_legal_informations_name',
# 'public_legal_informations_short_name',
# 'catalogs_federations_short_name',
# 'public_legal_informations_folder_number',
# 'public_legal_informations_owner_position',
# 'public_legal_informations_owner_name',
# 'catalogs_organization_statuses_name',
# 'public_legal_informations_ogrn',
# 'public_legal_informations_kpp',
# 'public_legal_informations_inn',
# 'public_legal_informations_okpo',
# 'public_legal_informations_okved',
# 'public_bank_data_bank_name',
# 'public_bank_data_bic_number',
# 'public_bank_data_rs_number',
# 'legal_address',
# 'legal_address_coordinates',
# 'fact_address',
# 'fact_address_coordinates',
# 'Phones',
# 'EMail',
# 'public_legal_informations_is_enable_web')

# class Blocks(RKF_office_base_model, ABC):
#     _icons: Dict[str, type] = {}
#     _matching: Dict[str, str]
#     name: str
#
#     def __init__subclass__(cls, block_icon: Optional[str] = None):
#         cls._matching = {
#             'main_block': 'info_outline',
#             'contacts_blocks': 'call',
#             'address_blocks': 'home',
#             '': ''
#         }
#         cls._icons[block_icon or cls.__name__.lower()] = cls
#
#     @classmethod
#     def __get_validators__(cls):
#         yield cls.validate
#
#     @classmethod
#     def validate(cls, value: Dict[str, Any]) -> 'Blocks':
#         try:
#             pet_type = value.pop('type')
#             return cls._types[pet_type](**value)
#         except:
#             raise ValueError('...')


class Clubs_base(RKF_office_base_model):
    """
    Базовая схема для 'клубов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    """
    # kennel_full_name: Optional[str]   # public_legal_informations_name: Optional[str]
    club_full_name: Optional[str]   # public_legal_informations_name: Optional[str]
    club_short_name: Optional[str]  # public_legal_informations_short_name: Optional[str]
    folder_number: Optional[str]    # public_legal_informations_folder_number: Optional[str]
    owner_position: Optional[str]   # public_legal_informations_owner_position: Optional[str]
    owner_name: Optional[str]       # public_legal_informations_owner_name: Optional[str]
    ogrn: Optional[str]             # public_legal_informations_ogrn: Optional[str]
    inn: Optional[str]              # public_legal_informations_inn: Optional[str]
    kpp: Optional[str]              # public_legal_informations_kpp: Optional[str]
    okpo: Optional[str]             # public_legal_informations_okpo: Optional[str]
    okved: Optional[str]            # public_legal_informations_okved: Optional[str]
    bank_name: Optional[str]        # public_bank_data_bank_name: Optional[str]
    bic_number: Optional[str]       # public_bank_data_bic_number: Optional[str]
    bank_rs: Optional[str]          # public_bank_data_rs_number: Optional[str]
    is_active_user: Optional[bool] # public_legal_informations_is_enable_web: Optional[bool]

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Main_block_view(Clubs_base):
    block_title: str = 'Сведения о кинологической организации'
    block_icon: str = 'info_outline'
    legal_registration_date: Optional[datetime]
    legal_liquidation_date: Optional[datetime]
    # legal_status_id: Optional[int]
    legal_status_name: Optional[str]  # в редактировании нет
    federation_name: Optional[str]
    is_public: Optional[bool] #public_legal_informations_active_member


class Main_block_edit(Clubs_base):
    block_title: str = 'Сведения о кинологической организации'
    block_icon: str = 'info_outline'
    legal_registration_date: Optional[datetime]
    legal_liquidation_date: Optional[datetime]
    legal_status_id: Optional[int]
    #legal_status_name: Optional[str]
    federation_id: Optional[int]
    is_public: Optional[bool]


class Contact_info_block_view(RKF_office_base_model):
    id: Optional[int]
    type_name: Optional[str]
    value: Optional[str]
    description: Optional[str]
    is_main: Optional[bool]


class Contact_info_block_edit(RKF_office_base_model):
    id: Optional[int] = None
    type_id: Optional[int] = None
    value: Optional[str] = None
    description: Optional[str]
    is_main: Optional[bool] = None


class Contacts_blocks_view(RKF_office_base_model):
    block_title: str = 'Контактные данные'
    block_icon: str = 'call'
    contacts: Optional[List[Contact_info_block_view]]


class Contacts_blocks_edit(RKF_office_base_model):
    block_title: str = 'Контактные данные'
    block_icon: str = 'call'
    contacts: Optional[List[Contact_info_block_edit]]


class Address_info_block_view(RKF_office_base_model):
    id: Optional[int] = None
    type_name: Optional[str] = ''
    address: Optional[str]


class Address_info_block_edit(RKF_office_base_model):
    id: Optional[int] = None
    # address_type_id: Optional[int]  # type_id: Optional[int]
    address_type_id: Optional[int] = Field(alias='type_id')  # type_id: Optional[int]

    public_club_addresses_postcode: Optional[str] = Field(alias='postcode')
    public_club_addresses_city_id: Optional[int] = Field(alias='city_id')
    public_club_addresses_street_type_id: Optional[int] = Field(alias='street_type_id')
    public_club_addresses_street_name: Optional[str] = Field(alias='street_name')
    public_club_addresses_house_type_id: Optional[int] = Field(alias='house_type_id')
    public_club_addresses_house_name: Optional[str] = Field(alias='house_name')
    public_club_addresses_flat_type_id: Optional[int] = Field(alias='flat_type_id')
    public_club_addresses_flat_name: Optional[str] = Field(alias='flat_name')
    public_club_addresses_geo_lat: Optional[str] = Field(alias='geo_lat')
    public_club_addresses_geo_lon: Optional[str] = Field(alias='geo_lon')

    class Config:
        allow_population_by_field_name = True

    @validator("public_club_addresses_postcode", pre=True)
    def int_to_str(cls, int_value: Any):
        # try:
        if not int_value:
            return None
        return str(int_value)


class AddressInfoBlockEditInput(RKF_office_base_model):
    id: Optional[int] = None
    # address_type_id: Optional[int]  # type_id: Optional[int]
    type_id: Optional[int] = Field(alias='address_type_id')  # type_id: Optional[int]

    postcode: Optional[str] = Field(alias='postcode')
    city_id: Optional[int] = Field(alias='city_id')
    street_type_id: Optional[int] = Field(alias='street_type_id')
    street_name: Optional[str] = Field(alias='street_name')
    house_type_id: Optional[int] = Field(alias='house_type_id')
    house_name: Optional[str] = Field(alias='house_name')
    flat_type_id: Optional[int] = Field(alias='flat_type_id')
    flat_name: Optional[str] = Field(alias='flat_name')
    geo_lat: Optional[str] = Field(alias='geo_lat')
    geo_lon: Optional[str] = Field(alias='geo_lon')

    class Config:
        allow_population_by_field_name = True

    @validator("postcode", pre=True)
    def int_to_str(cls, int_value: Any):
        # try:
        if not int_value:
            return None
        return str(int_value)


class LegalInfoEdit(RKF_office_base_model):
    registration_date: Optional[datetime] = Field(None, alias='legal_registration_date')
    liquidate_date: Optional[datetime] = Field(None, alias='legal_liquidation_date')
    organization_status_id: Optional[int] = Field(None, alias='legal_status_id')
    name: Optional[str] = Field(None, alias='club_full_name')
    short_name: Optional[str] = Field(None, alias='club_short_name')
    owner_position: Optional[str] = Field(None, alias='owner_position')
    owner_name: Optional[str] = Field(None, alias='owner_name')
    ogrn: Optional[str] = Field(None, alias='ogrn')
    inn: Optional[str] = Field(None, alias='inn')
    kpp: Optional[str] = Field(None, alias='kpp')
    okpo: Optional[str] = Field(None, alias='okpo')
    okved: Optional[str] = Field(None, alias='okved')
    active_member: Optional[bool] = Field(None, alias='is_public')
    is_enable_web: Optional[bool] = Field(None, alias='is_active_user')
    folder_number: Optional[str] = Field(None, alias='folder_number')

    # legal_registration_date: Optional[datetime] = Field(alias='registration_date')
    # legal_liquidation_date: Optional[datetime] = Field(alias='liquidation_date')
    # legal_status_id: Optional[int] = Field(alias='organization_status_id')
    # club_full_name: Optional[str] = Field(alias='name')
    # club_short_name: Optional[str] = Field(alias='short_name')
    # owner_position: Optional[str] = Field(alias='owner_position')
    # owner_name: Optional[str] = Field(alias='owner_name')
    # ogrn: Optional[str] = Field(alias='ogrn')
    # inn: Optional[str] = Field(alias='inn')
    # kpp: Optional[str] = Field(alias='kpp')
    # okpo: Optional[str] = Field(alias='okpo')
    # okved: Optional[str] = Field(alias='okved')
    # is_public: Optional[bool] = Field(alias='active_member')
    # is_active_user: Optional[bool] = Field(alias='is_enable_web')
    # folder_number: Optional[str] = Field(alias='folder_number')

    class Config:
        allow_population_by_field_name = True


class BankDataEdit(RKF_office_base_model):
    """
    Редактирование дан
    ных пользователя.
    """
    rs_number: Optional[str] = Field(None, alias='bank_rs')
    bank_name: Optional[str] = Field(None, alias='bank_name')
    bic_number: Optional[str] = Field(None, alias='bic_number')
    profile_id: Optional[int]
    is_delete: Optional[bool]
    login: Optional[str]
    mail: Optional[str]
    phone: Optional[str]

    class Config:
        allow_population_by_field_name = True


class ProfilesFederationsEdit(RKF_office_base_model):
    federation_id: Optional[int] = Field(None, alias='federation_id')


class Address_blocks_view(RKF_office_base_model):
    block_title: str = 'Адресные данные'
    block_icon: str = 'home'
    addresses: Optional[List[Address_info_block_view]]


class Address_blocks_edit(RKF_office_base_model):
    block_title: str = 'Адресные данные'
    block_icon: str = 'home'
    addresses: Optional[List[Address_info_block_edit]]


class Payment_blocks(RKF_office_base_model):
    block_title: str = 'Оплата членских взносов в Федерацию'
    block_icon: str = 'payment'
    payments: Optional[List[Organizations_membership_dues_item]]


class History_blocks(RKF_office_base_model):
    block_title: str = 'История деятельности'
    block_icon: str = 'history'
    first_exhibition_date: Optional[datetime] = None
    last_exhibition_date: Optional[datetime] = None
    tests_over_past_year: Optional[bool] = False            # is_tests_availability: Optional[bool]
    availability_premises: Optional[bool] = False           # is_premise_availability: Optional[bool]
    presentation: Optional[bool] = False                    # is_presentation_availability: Optional[bool]
    tribal_reviews_declared: Optional[int] = None             # declared_inspection_count: Optional[str]
    tribal_reviews_completed: Optional[int] = None            # conducted_inspection_count: Optional[str]
    succession_of_exhibition_activities: Optional[str] = '' # succession_exhibition_activities: Optional[str]
    exhibition_activities_past_periods: Optional[str] = ''  # reporting_errors: Optional[str]
    exceptions_in_reports: Optional[str] = ''               # past_exhibition_activities: Optional[str]
    sanctions: Optional[str] = ''
    # blocks: Optional[List[Organization_history_item]]


class Access_block(RKF_office_base_model):
    access_name: str
    value: bool


class Access_blocks_view(RKF_office_base_model):
    block_title: str = 'Доступ'
    block_icon: str = 'admin_panel_settings'
    access: Optional[List[Access_block]]


class Access_blocks_edit(RKF_office_base_model):
    block_title: str = 'Доступ'
    block_icon: str = 'admin_panel_settings'
    access: Optional[List[Any]]


class Clubs_by_id_catalogs(RKF_office_base_model):
    organization_statuses: Optional[List[Organization_statuses]] = None
    federations: Optional[Federation_info_block] = None
    contact_types: Optional[List[Contact_types]] = None
    address_types: Optional[List[Address_types]] = None


class Clubs_db(Clubs_base):
    """
    Схема для вывода списка клубов.
    """
    id: Optional[int]
    federation_name: Optional[str]
    legal_address: Optional[str]
    legal_address_coordinates: Optional[str]
    legal_status_name: Optional[str]
    fact_address: Optional[str]
    fact_address_coordinates: Optional[str]
    phones: Optional[str]
    emails: Optional[str]
    sites: Optional[str]
    is_public: Optional[bool]


class Clubs_by_id(RKF_office_base_model):
    title: str = 'Клубы'
    title_item: str = ''
    blocks: List[Any]


class Clubs_create(Clubs_base):
    """
    Схема для создания нового клуба
    """
    pass


class Clubs_update(Clubs_base):
    """
    Схема для редактирования данных для клуба
    """
    # {
    #     legal_registration_date: '',
    #     legal_liquidation_date: '',
    #     legal_status_id: '',
    #     club_full_name: '',
    #     club_short_name: '',
    #     owner_position: '',
    #     owner_name: '',
    #     federation_id: '',
    #     folder_number: '',
    #     is_active_user: false,
    #     ogrn: '',
    #     inn: '',
    #     kpp: '',
    #     okpo: '',
    #     okved: '',
    #     bank_name: '',
    #     bank_bic: '',
    #     bank_rs: '',
    #     is_public: false,
    #     contacts: [{
    #         id: null,
    #         type_id: '',
    #         value: '',
    #         description: '',
    #         is_main: false
    #     }],
    #     addresses: [{
    #         id: null,
    #         type_id: '', # address_type_id
    #         postcode: '',
    #         city_id: '',
    #         street_type_id: '',
    #         street_name: '',
    #         house_type_id: '',
    #         house_name: '',
    #         flat_type_id: '',
    #         flat_name: '',
    #         geo_lat: '',
    #         geo_lon: ''
    #     }],
    #     payments: [{
    #         id: null,
    #         payment_date: '',
    #         payment_year: '',
    #         is_paid: false,
    #         comment: ''
    #     }],
    #     first_exhibition_date: '',
    #     last_exhibition_date: '',
    #     is_tests_availability: false,
    #     is_premise_availability: false,
    #     is_presentation_availability: false,
    #     declared_inspection_count: '',
    #     conducted_inspection_count: '',
    #     succession_exhibition_activities: '',
    #     reporting_errors: '',
    #     past_exhibition_activities: '',
    #     sanctions: '',
    #     access: []
    # }
    legal_registration_date: Optional[datetime] = None
    legal_liquidation_date: Optional[datetime] = None
    legal_status_id: Optional[int] = None
    federation_id: Optional[int] = None
    is_public: Optional[bool] = None
    contacts: Optional[List[Contact_info_block_edit]] = None
    addresses: Optional[List[AddressInfoBlockEditInput]] = None
    payments: Optional[List[Organizations_membership_dues_item]] = None
    first_exhibition_date: Optional[datetime] = None
    last_exhibition_date: Optional[datetime] = None
    tests_over_past_year: Optional[bool] = False            # is_tests_availability: Optional[bool]
    availability_premises: Optional[bool] = False           # is_premise_availability: Optional[bool]
    presentation: Optional[bool] = False                    # is_presentation_availability: Optional[bool]
    tribal_reviews_declared: Optional[int] = None             # declared_inspection_count: Optional[str]
    tribal_reviews_completed: Optional[int] = None            # conducted_inspection_count: Optional[str]
    succession_of_exhibition_activities: Optional[str] = None # succession_exhibition_activities: Optional[str]
    exhibition_activities_past_periods: Optional[str] = None  # reporting_errors: Optional[str]
    exceptions_in_reports: Optional[str] = None               # past_exhibition_activities: Optional[str]
    sanctions: Optional[str] = None

    @validator("legal_registration_date", "legal_liquidation_date", "first_exhibition_date", "last_exhibition_date", pre=True)
    def str_to_date(cls, datetime_value: Any):
        # try:
        if not datetime_value:
            return None
        # if isinstance(datetime_value, date):
        #     # d_date = datetime.strptime(datetime_value, '%Y-%M-%d').timestamp()
        if isinstance(datetime_value, str):
            if 'T' in datetime_value:
                datetime_value = datetime_value.split('T')[0]
            d_date = datetime.strptime(datetime_value, '%Y-%m-%d').timestamp()
            return d_date

        # except Exception as e:
        #     raise ValidationError(
        #         f"Organization_history_item: first_exhibition_date or last_exhibition_date:{datetime_value}")

    @validator("tribal_reviews_declared", "tribal_reviews_completed", pre=True)
    def str_to_date2(cls, int_value: Any):
        # try:
        if not int_value:
            return None
        if isinstance(int_value, str) and int_value.isdigit():
            return int(int_value)
        elif isinstance(int_value, int):
            return int_value

        # except Exception as e:
        #     raise ValidationError(
        #         f"Organization_history_item: first_exhibition_date or last_exhibition_date:{datetime_value}")
