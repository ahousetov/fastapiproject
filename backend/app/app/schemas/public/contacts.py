from app.schemas.common import *


class Contacts_base(RKF_office_base_model):
    """
    Базовая схема для 'Населённых пунктов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    id: int
    value: str
    description: Optional[str]
    is_main: bool
    is_deleted: bool
    type_id: Optional[int]
    is_hidden: bool

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    id: Optional[int]
    value: str
    description: Optional[str]
    is_main: bool

    # @property
    # def main(self):
    #     return self.is_main

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Contacts_DB(Contacts_base):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Contacts_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    pass


class Contacts(Contacts_DB):
    """
    Схема-наследник Contacts_DB для вывода данных для
    одиночной записи Contacts, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    type_id: Optional[int]
    profile_id: Optional[int]


class Contacts_create(Contacts):
    """
    Создание нового населённого пункта.

    Все поля наследуем от Contacts, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    pass


class Contacts_update(Contacts_base):
    """
    Редактирование данных населённого пункта.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Contacts_base ничего
    не добавляем.
    """
    value: Optional[str]
    is_main: Optional[bool]
    type_id: Optional[int]
    profile_id: Optional[int]


class ContactsInput(RKF_office_base_model):
    value: Optional[str]
    description: Optional[str]
    is_main: Optional[bool]
    type_id: Optional[int]


class RelProfilesContactsInput(RKF_office_base_model):
    profile_id: Optional[int]
    contact_id: Optional[int]


class Contacts_extra_data(Contacts_update):
    type_name: Optional[str]


class Contacts_delete(Contacts):
    """
    Наследуемся от Contacts.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    id: int
    is_deleted: bool


class Contacts_multi(Contacts):
    """
    Схема вывода списка данных по населённым пунктам.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    pass


class Contact_HTTP_error(RKF_office_base_model):
    detail: str

    class Config:
        schema_extra = {
            "example": {"detail": "Ошибка HTTP имеет место быть."}
        }


# class Contact_for_block(RKF_office_base_model):
#     """
#     Схема для 'Федераций'.
#
#     Опциональные параметры:
#     id: int
#     value: str
#     description: Optional[str]
#     is_main: bool
#
#     id и is_deleted - обязательные параметры, поэтому указываем
#     их в наследниках явно.
#     """
#     id: int
#     value: str
#     description: str
#     is_main: bool


class Contact_block(Block):
    contacts: List[Contacts_base] = None


class Contact_blocks(Blocks_list):
    title: Optional[str] = "Контакты"
    blocks: Optional[List[Contact_block]] = None