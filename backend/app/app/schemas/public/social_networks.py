from typing import Optional

from app.schemas.common import RKF_office_base_model


class Social_networks_base(RKF_office_base_model):
    """
    Общий шаблон public.social_networks.
    """
    id: Optional[int]
    site: Optional[str]
    description: Optional[str]
    is_deleted: Optional[bool]
    type_id: Optional[int]

    class Config:
        orm_mode = True


class Social_networks_create(Social_networks_base):
    """
    """
    site: str
    type_id: int


class Social_networks_update(Social_networks_base):
    """
    """
    id: int


class Social_networks_delete(Social_networks_base):
    """
    """
    id: int
    is_deleted: bool
