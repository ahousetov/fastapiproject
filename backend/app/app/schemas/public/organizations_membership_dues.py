import time

from app.schemas.common import *


class Organizations_membership_dues_base(RKF_office_base_model):
    """
    Базовая схема для 'Членские взносы'. Использую как основу
    для остальных схем. Является элементом списка.

    Опциональные параметры:
    id: Optional[int]
    date_payment: Optional[datetime]
    year_payment: Optional[int]
    is_paid: Optional[bool]
    comments: Optional[str]
    """
    id: Optional[int] = Field(None, alias="id")
    date_payment: Optional[datetime] = Field(None, alias="date_payment")
    year_payment: Optional[int] = Field(None, alias="year_payment")
    is_paid: Optional[bool] = Field(None, alias="is_paid")
    comments: Optional[str] = Field(None, alias="comments")

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Organizations_membership_dues_item(Organizations_membership_dues_base):
    date_payment: Optional[datetime] = Field(None, alias="date_payment")
    year_payment: Optional[int] = Field(None, alias="year_payment")

    @validator("date_payment", pre=True)
    def str_to_date(cls, datetime_value: Any):
        try:
            if not datetime_value:
                return None
            if isinstance(datetime_value, str):
                if 'T' in datetime_value:
                    datetime_value = datetime_value.split('T')[0]
                try:
                    d_date = datetime.strptime(datetime_value, '%Y-%M-%d').timestamp()
                    return d_date
                except Exception as e:
                    try:
                        dt_obj = datetime.fromtimestamp(datetime_value)
                        d_date = dt_obj.timestamp()
                        return d_date
                    except Exception as e:
                        return None
            if isinstance(datetime_value, datetime):
                d_date = datetime_value.timestamp()
                return d_date
            # if isinstance(datetime_value, date):
            #     dtt = datetime_value.timetuple()
            #     dtt_tuple = tuple([int(d) for d in dtt])
            #     d_date = time.mktime(dtt_tuple)
            #     #cur_time = time.mktime()
            #     return d_date
        except Exception as e:
            raise ValidationError(f"Organizations_membership_dues_item: date_payment:{datetime_value}")

    @validator("year_payment", pre=True)
    def str_to_int(cls, str_int: str):
        try:
            if not str_int:
                return None
            i_int = int(str_int)
            return i_int
        except Exception as e:
            err_msg = f"Organizations_membership_dues_item: year_payment:{str_int}"
            raise ValidationError(err_msg)