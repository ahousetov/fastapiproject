from app.schemas.common import *


class Club_address_base(RKF_office_base_model):
    """
    Базовая схема для 'Адреса клубов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    id: Optional[int] = None
    profile_id: Optional[int] = None
    address_type_id: Optional[int] = None
    street_name: Optional[str] = None
    house_name: Optional[str] = None
    flat_name: Optional[str] = None
    postcode: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None
    building_name: Optional[str] = None
    catalogs_address_types_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None
    catalogs_cities_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_street_types_name: Optional[str] = None
    catalogs_house_types_name: Optional[str] = None
    catalogs_flat_types_name: Optional[str] = None

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    id: Optional[int] = None
    profile_id: Optional[int] = None
    address_type_id: Optional[int] = None
    street_name: Optional[str] = None
    house_name: Optional[str] = None
    flat_name: Optional[str] = None
    postcode: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None
    building_name: Optional[str] = None
    catalogs_address_types_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None
    catalogs_cities_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_street_types_name: Optional[str] = None
    catalogs_house_types_name: Optional[str] = None
    catalogs_flat_types_name: Optional[str] = None

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Club_address_create(Club_address_base):
    """
    Создание нового адрес клуба

    Все поля наследуем от Club_address_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.

    Опциональные параметры:
    id: Optional[int] = None
    profile_id: Optional[int] = None
    address_type_id: Optional[int] = None
    street_name: Optional[str] = None
    house_name: Optional[str] = None
    flat_name: Optional[str] = None
    postcode: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None
    building_name: Optional[str] = None
    catalogs_address_types_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None
    catalogs_cities_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_street_types_name: Optional[str] = None
    catalogs_house_types_name: Optional[str] = None
    catalogs_flat_types_name: Optional[str] = None


    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    id: Optional[int] = None
    profile_id: Optional[int] = None
    address_type_id: Optional[int] = None
    street_name: Optional[str] = None
    house_name: Optional[str] = None
    flat_name: Optional[str] = None
    postcode: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None
    building_name: Optional[str] = None
    catalogs_address_types_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None
    catalogs_cities_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_street_types_name: Optional[str] = None
    catalogs_house_types_name: Optional[str] = None
    catalogs_flat_types_name: Optional[str] = None


class Club_address_update(Club_address_base):
    """
    Редактирование адрес клуба.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Club_address_base и Club_address_foreign_id ничего
    не добавляем.
    """
    profile_id: Optional[int] = None
    address_type_id: Optional[int] = None
    street_name: Optional[str] = None
    house_name: Optional[str] = None
    flat_name: Optional[str] = None
    postcode: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None
    building_name: Optional[str] = None
    catalogs_address_types_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None
    catalogs_cities_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_street_types_name: Optional[str] = None
    catalogs_house_types_name: Optional[str] = None
    catalogs_flat_types_name: Optional[str] = None


class Club_address_update_nurseries(RKF_office_base_model):
    address_type_id: Optional[int] = Field(None, alias="address_type_id")
    profile_id: Optional[int] = Field(None, alias="profile_id")
    postcode: Optional[str] = Field(None, alias="postcode")
    city_id: Optional[int] = Field(None, alias="city_id")
    street_type_id: Optional[int] = Field(None, alias="street_type_id")
    street_name: Optional[str] = Field(None, alias="street_name")
    house_type_id: Optional[int] = Field(None, alias="house_type_id")
    house_types_name: Optional[str] = Field(None, alias="catalogs_house_types_name")
    flat_type_id: Optional[int] = Field(None, alias="flat_type_id")
    flat_types_name: Optional[str] = Field(None, alias="catalogs_flat_types_name")
    geo_lat: Optional[str] = Field(None, alias="geo_lat")
    geo_lon: Optional[str] = Field(None, alias="geo_lon")


class Club_address_delete(Club_address_base):
    """
    Наследуемся от Club_address_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    id: int = None


class Club_address_DB(Club_address_base):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Club_address_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    pass


class Club_address(Club_address_DB):
    """
    Схема-наследник Club_address_DB для вывода данных для
    одиночной записи Club_address, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Club_address_multi(Club_address_base):
    """
    Схема вывода списка данных по населённым пунктам.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    pass


class Club_address_block(Block, Club_address_base):
    block_icon: str = 'info_outline'


class Club_addresses_blocks(RKF_office_base_model):
    title: str = 'Адресная информация клуба'
    blocks: Optional[List[Club_address_block]]
