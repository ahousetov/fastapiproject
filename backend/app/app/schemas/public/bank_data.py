from app.schemas.common import *


class Bank_data_base(RKF_office_base_model):
    """
    Базовая схема для 'банковский данных'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    rs_number: Optional[str]
    bank_name: Optional[str]
    bic_number: Optional[str]

    id - обязательный параметр, поэтому указываем
    их в наследниках явно.
    """
    # id: int
    rs_number: Optional[str]
    bank_name: Optional[str]
    is_delete: bool = False
    bic_number: Optional[str]
    profile_id: int

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Bank_data_create(Bank_data_base):
    """
    Создание нового пользователя.

    Все поля наследуем от Bank_data_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    pass


class Bank_data_update(Bank_data_base):
    """
    Редактирование данных пользователя.
    """
    id: int
    is_delete: Optional[bool]
    login: Optional[str]
    mail: Optional[str]
    phone: Optional[str]


class Bank_data_delete(Bank_data_base):
    """
    Наследуемся от Bank_data_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    id: int


class Bank_data_DB(Bank_data_base):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Bank_data_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    pass


class Bank_data(Bank_data_DB):
    """
    Схема-наследник Bank_data_DB для вывода данных для
    одиночной записи Bank_data, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Bank_data_multi(Bank_data_base):
    """
    Схема вывода списка данных по банковским данным.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    pass


class Bank_data_block(Block, Bank_data_base):
    pass


class Bank_data_blocks(Blocks_list):
    blocks: Optional[List[Bank_data_block]] = None