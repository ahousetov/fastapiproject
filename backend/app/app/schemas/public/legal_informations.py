from app.schemas.common import *


class Legal_informations_base(RKF_office_base_model):
    """
    Базовая схема для 'Населённых пунктов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    id: int
    is_public: bool
    is_deleted: bool
    active_member: bool
    without_stamp_code: bool
    active_rkf_user: bool

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    id: Optional[int]
    is_deleted: bool = False
    is_public: bool = False
    active_member: bool = False
    without_stamp_code: bool = False
    active_rkf_user: bool = False
    name: Optional[str] = None
    owner_name: Optional[str] = None
    address: Optional[str] = None
    inn: Optional[str] = None
    kpp: Optional[str] = None
    ogrn: Optional[str] = None
    okpo: Optional[str] = None
    okved: Optional[str] = None
    registration_number: Optional[str] = None
    registration_date: Optional[datetime] = None
    confident_legal_person: Optional[str] = None
    owner_position: Optional[str] = None
    description: Optional[str] = None
    short_name: Optional[str] = None
    liquidate_date: Optional[datetime] = None
    is_enable_web: Optional[bool] = None
    organization_status_id: Optional[int] = None
    organization_type_id: Optional[int] = None
    profile_id: Optional[int] = None
    city_id: Optional[int] = None
    folder_number: Optional[str] = None

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Legal_informations_create(Legal_informations_base):
    """
    Создание нового населённого пункта.

    Все поля наследуем от Legal_informations_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    pass


class Legal_informations_update(Legal_informations_base):
    """
    Редактирование данных населённого пункта.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Legal_informations_base и Legal_informations_foreign_id ничего
    не добавляем.
    """
    pass


class Legal_informations_delete(Legal_informations_base):
    """
    Наследуемся от Legal_informations_base.

    id - обязательный параментр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    pass


class Legal_informations_DB(Legal_informations_base):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Legal_informations_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    pass


class Legal_informations(Legal_informations_DB):
    """
    Схема-наследник Legal_informations_DB для вывода данных для
    одиночной записи Legal_informations, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Legal_informations_multi(Legal_informations_base):
    """
    Схема вывода списка данных по населённым пунктам.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    pass
