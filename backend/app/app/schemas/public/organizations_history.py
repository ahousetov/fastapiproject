from app.schemas.common import *


class Organization_history_base(RKF_office_base_model):
    """
    Базовая схема для 'Истории деятельности'. Использую как основу
    для остальных схем. Является элементом списка.

    Опциональные параметры:
    id: int
    profile_id: int
    first_exhibition_date: datetime
    last_exhibition_date: datetime
    tests_over_past_year: bool
    availability_premises: bool
    presentation: bool
    tribal_reviews_declared: int
    tribal_reviews_completed: int
    succession_of_exhibition_activities: str
    exhibition_activities_past_periods: str
    exceptions_in_reports: str
    sanctions: str
    """
    id: Optional[int] = Field(None, alias="id")
    profile_id: int = Field(None, alias="profile_id")
    first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
    last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
    tests_over_past_year: Optional[bool] = Field(None, alias="tests_over_past_year")
    availability_premises: Optional[bool] = Field(None, alias="availability_premises")
    presentation: Optional[bool] = Field(None, alias="presentation")
    tribal_reviews_declared: Optional[int] = Field(None, alias="tribal_reviews_declared")
    tribal_reviews_completed: Optional[int] = Field(None, alias="tribal_reviews_completed")
    succession_of_exhibition_activities: Optional[str] = Field(None, alias="succession_of_exhibition_activities")
    exhibition_activities_past_periods: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
    exceptions_in_reports: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
    sanctions: Optional[str] = Field(None, alias="sanctions")

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Organization_history_item(Organization_history_base):
    # first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
    # last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
    first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
    last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
    tribal_reviews_declared: Optional[int] = Field(None, alias="tribal_reviews_declared")
    tribal_reviews_completed: Optional[int] = Field(None, alias="tribal_reviews_completed")

    @validator("first_exhibition_date", "last_exhibition_date", pre=True)
    def str_to_date(cls, str_date: str):
        try:
            if not str_date:
                return None
            if isinstance(str_date, str):
                if 'T' in str_date:
                    str_date = str_date.split('T')[0]
                try:
                    d_date = datetime.strptime(str_date, '%Y-%M-%d').timestamp()
                    return d_date
                except Exception as e:
                    try:
                        dt_obj = datetime.fromtimestamp(str_date)
                        d_date = dt_obj.timestamp()
                        return d_date
                    except Exception as e:
                        return None
            if isinstance(str_date, datetime):
                d_date = str_date.timestamp()
                return d_date
                # d_date = datetime.strptime(str_date, '%Y-%M-%d').timestamp()
            #return d_date

        except Exception as e:
            raise ValidationError(f"Organization_history_item: first_exhibition_date or last_exhibition_date:{str_date}")

    @validator("tribal_reviews_declared", "tribal_reviews_completed", pre=True)
    def str_to_int(cls, str_int: str):
        try:
            if not str_int:
                return None
            i_int = int(str_int)
            return i_int
        except Exception as e:
            err_msg = f"Organization_history_item: tribal_reviews_declared tribal_reviews_completed:{str_int}"
            raise ValidationError(err_msg)

