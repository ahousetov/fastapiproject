from app.schemas.common import *
from alphabet_detector import AlphabetDetector

ad = AlphabetDetector()


class Stamp_codes_base(RKF_office_base_model):
    """
    Базовая схема для 'Stamp_codes'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    profile_id: Optional[int] = Field(None)
    stamp_code: Optional[str] = Field(None)
    is_default: Optional[bool] = Field(False)
    is_deleted: Optional[bool] = Field(False)

    @validator("stamp_code", pre=True)
    def stamp_code_check(cls, stamp: str):
        if not (stamp.isupper() and len(stamp) == 3 and ad.is_latin(stamp)):
            raise AttributeError(f"{stamp} - не валидный код клейма. Должен быть три латинских, заглавных символа.")
        return stamp

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Stamp_codes_create(Stamp_codes_base):
    """
    Создание нового Stamp_code.

    Все поля наследуем от Stamp_codes_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    profile_id: int = Field(None, alias='profile_id')
    stamp_code: str = Field(None, alias='stamp_code')


class Stamp_codes_update(RKF_office_base_model):
    """
    Редактирование данных Stamp_codes.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Stamp_codes_base ничего
    не добавляем.
    """
    stamp_code: str = Field(None)

    # @validator("stamp_code")
    # def stamp_code_check(cls, stamp: str, pre=True):
    #     if not (stamp.isupper() and len(stamp) == 3 and ad.is_latin(stamp)):
    #         raise AttributeError(f"{stamp} - не валидный код клейма. Должен быть три латинских, заглавных символа.")
    #     return stamp


class Stamp_codes_delete(Stamp_codes_base):
    """
    Наследуемся от Stamp_codes_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """