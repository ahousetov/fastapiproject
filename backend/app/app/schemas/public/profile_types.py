from app.schemas.common import *


class Profile_types_base(RKF_office_base_model):
    """
    Общий шаблон профилей типов.
    """
    name: Optional[str] = None


class Profile_types_create(Profile_types_base):
    """
    Создаём новый профиль типов.
    """
    pass


class Profile_types_update(Profile_types_base):
    """
    Изменяем профиль типов.
    """
    pass


class Profile_types_delete(Profile_types_base):
    """
    Удаляем профиль типов (помечаем удалённым, а не удаляем из БД)
    """
    id: int


class Profile_types_DB(Profile_types_base):
    """
    Модель - представление данных в БД.
    """

    id: int
    name: str

    class Config:
        orm_mode = True


class Profile_types_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class Profile_types(Profile_types_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Profile_types_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class Profile_types_block(Block):
    id: Optional[int] = None


class Profile_types_blocks(Blocks_list):
    blocks: Optional[List[Profile_types_block]]