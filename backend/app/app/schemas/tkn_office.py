from typing import Optional

from app.schemas.common import *


# class Tkn_office(RKF_office_base_model):
#     access_token: str
#     token_type: str

class Tkn_office(RKF_office_base_model):
    access_token: str
    token_type: str

# class Tkn_office_payload(RKF_office_base_model):
#     sub: Optional[int] = None


class Tkn_office_payload(RKF_office_base_model):
    sub: Optional[int] = None
