from app.schemas.common import *
from app.schemas.public.organizations_history import Organization_history_item
from app.schemas.public.organizations_membership_dues import Organizations_membership_dues_item
from app.schemas.catalogs.organization_statuses import Organization_statuses_block, Organization_statuses
from app.schemas.catalogs.federations import Federation_info_block
from app.schemas.catalogs.contact_types import Contact_types
from app.schemas.catalogs.address_types import Address_types
from alphabet_detector import AlphabetDetector

ad = AlphabetDetector()


class Nurseries_base(RKF_office_base_model):
	id: int = Field(None, alias="id")
	name: Optional[str] = Field(None, alias="kennel_full_name")
	short_name: Optional[str] = Field(None, alias="kennel_short_name")
	federation_name: Optional[str] = Field(None, alias="federation_name")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	stamp_code: Optional[str] = Field('', alias="brand_code")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	address: Optional[str] = Field('', alias="address")
	address_coordinates: Optional[str] = Field('', alias="address_coordinates")
	phones: Optional[str] = Field(None, alias="phones")
	emails: Optional[str] = Field(None, alias="emails")
	sites: Optional[str] = Field(None, alias="sites")
	legal_informations_is_public: Optional[bool] = Field(False, alias="is_public")


class Nurseries(Nurseries_base):
	id: Optional[int] = Field(None, alias="id")


class Nurseries_breeding_breeds(RKF_office_base_model):
	id: Optional[int] = Field(None, alias="id")
	fci_number: Optional[int] = Field(None, alias="fci_number")
	group_number: Optional[int] = Field(None, alias="group_number")
	breed_name: Optional[str] = Field(None, alias="breed_name")

	@validator("fci_number", "group_number", pre=True)
	def str_to_int(cls, str_int: str):
		try:
			if not str_int:
				return None
			i_int = int(str_int)
			return i_int
		except Exception as e:
			err_msg = f"Nurseries_breeding_breeds: fci_number group_number:{str_int}"
			raise ValidationError(err_msg)


class Nurseries_history(RKF_office_base_model):
	first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
	last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
	tests_over_past_year: Optional[bool] = Field(False, alias="tests_over_past_year")
	availability_premises: Optional[bool] = Field(False, alias="availability_premises")
	presentation: Optional[bool] = Field(False, alias="presentation")
	tribal_reviews_declared: Optional[int] = Field(None, alias="tribal_reviews_declared")
	tribal_reviews_completed: Optional[int] = Field(None, alias="tribal_reviews_completed")
	succession_of_exhibition_activities: Optional[str] = Field(None, alias="succession_of_exhibition_activities")
	exceptions_in_reports: Optional[str] = Field(None, alias="exceptions_in_reports")
	exhibition_activities_past_periods: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
	sanctions: Optional[str] = Field(None, alias="sanctions")


class Nurseries_main_without_alias(RKF_office_base_model):
	id: int = Field(None, alias="id")
	rel_users_profiles_user_id: Optional[int] = Field(None, alias="rel_users_profiles_user_id")
	legal_informations_id: Optional[int] = Field(None, alias="legal_informations_id")
	legal_informations_organization_status_id: Optional[int] = Field(None, alias="organization_status_id")
	legal_informations_city_id: Optional[int] = Field(None, alias="city_id")
	name: Optional[str] = Field(None, alias="name")
	short_name: Optional[str] = Field(None, alias="short_name")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	owner_position: Optional[str] = Field(None, alias="owner_position")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	active_member: Optional[bool] = Field(False, alias="is_active_user")
	is_enable_web: Optional[bool] = Field(False, alias="is_enable_web")
	kennel_id: Optional[int] = Field(None, alias="kennel_id")
	federation_id: Optional[int] = Field(None, alias="federation_id")
	stamp_code: Optional[str] = Field(None, alias="stamp_code")
	open_access_to_all_requests: Optional[bool] = Field(False, alias="open_access_to_all_requests")


class Nurseries_main_block_view(RKF_office_base_model):
	block_title: str = 'Основная информация'
	block_icon: str = 'info_outline'
	name: Optional[str] = Field(None, alias="kennel_full_name")
	short_name: Optional[str] = Field(None, alias="kennel_short_name")
	owner_position: Optional[str] = Field(None, alias="owner_position")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	federation_name: Optional[str] = Field(None, alias="federation_name")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	stamp_code: Optional[str] = Field('', alias="brand_code")
	is_active_user: Optional[bool] = Field(None, alias="is_active_user")
	is_enable_web: Optional[bool] = Field(False, alias="is_enable_web")
	is_public: Optional[bool] = Field(False, alias="is_public")


class Nurseries_main_block_editinfo(RKF_office_base_model):
	block_title: str = 'Основная информация'
	block_icon: str = 'info_outline'
	organization_status_id: Optional[int] = Field(0, alias="status_id")
	name: Optional[str] = Field(None, alias="kennel_full_name")
	short_name: Optional[str] = Field(None, alias="kennel_short_name")
	owner_position: Optional[str] = Field(None, alias="owner_position")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	federation_id: Optional[int] = Field(None, alias="federation_id")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	stamp_code: Optional[str] = Field('', alias="brand_code")
	active_member: Optional[bool] = Field(False, alias="is_active_user")
	is_enable_web: Optional[bool] = Field(False, alias="is_enable_web")
	is_public: Optional[bool] = Field(False, alias="is_public")


class Nurseries_main_block_edit(RKF_office_base_model):
	status_id: Optional[int] = Field(None, alias="status_id")
	name: Optional[str] = Field(None, alias="kennel_full_name")
	short_name: Optional[str] = Field(None, alias="kennel_short_name")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	owner_position: Optional[str] = Field(None, alias="owner_position")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	kennel_id: Optional[int] = Field(None, alias="kennel_id")
	federation_id: Optional[int] = Field(None, alias="federation_id")
	stamp_code: Optional[str] = Field(None, alias="brand_code")
	active_member: Optional[bool] = Field(False, alias="is_active_user")
	is_enable_web: Optional[bool] = Field(False, alias="is_enable_web")
	is_public: Optional[bool] = Field(False, alias="is_public")


class Nurseries_breeding_breeds_block(RKF_office_base_model):
	block_title: str = 'Породы'
	block_icon: str = 'pets'
	breeds: List[RKF_office_base_model] = List[Nurseries_breeding_breeds]


class Nurseries_history_block_view(RKF_office_base_model):
	block_title: str = 'История деятельности'
	block_icon: str = 'history'
	first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
	last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
	tests_over_past_year: Optional[bool] = Field(False, alias="tests_over_past_year")
	availability_premises: Optional[bool] = Field(False, alias="availability_premises")
	presentation: Optional[bool] = Field(False, alias="presentation")
	tribal_reviews_declared: Optional[int] = Field(None, alias="tribal_reviews_declared")
	tribal_reviews_completed: Optional[int] = Field(None, alias="tribal_reviews_completed")
	succession_of_exhibition_activities: Optional[str] = Field(None, alias="succession_of_exhibition_activities")
	exceptions_in_reports: Optional[str] = Field(None, alias="exceptions_in_reports")
	exhibition_activities_past_periods: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
	sanctions: Optional[str] = Field(None, alias="sanctions")


class Nurseries_history_block_edit(RKF_office_base_model):
	block_title: str = 'История деятельности'
	block_icon: str = 'history'
	first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
	last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
	tests_over_past_year: Optional[bool] = Field(False, alias="tests_over_past_year")
	availability_premises: Optional[bool] = Field(False, alias="availability_premises")
	presentation: Optional[bool] = Field(False, alias="presentation")
	tribal_reviews_declared: Optional[int] = Field(None, alias="tribal_reviews_declared")
	tribal_reviews_completed: Optional[int] = Field(None, alias="tribal_reviews_completed")
	succession_of_exhibition_activities: Optional[str] = Field(None, alias="succession_of_exhibition_activities")
	exceptions_in_reports: Optional[str] = Field(None, alias="exceptions_in_reports")
	exhibition_activities_past_periods: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
	sanctions: Optional[str] = Field(None, alias="sanctions")


class Nurseries_additional(RKF_office_base_model):
	certificate_registration_nursery_id: Optional[int] = Field(None, alias="rkf_certificate")
	registration_date: Optional[datetime] = Field(datetime.now(), alias="registration_date")
	prefix: Optional[bool] = Field(False, alias="prefix")
	suffix: Optional[bool] = Field(False, alias="suffix")
	name_lat: Optional[str] = Field(None, alias="name_lat")
	owner_specialist_rkf: Optional[bool] = Field(False, alias="is_rkf_specialist")
	owner_date_speciality: Optional[datetime] = Field(datetime.now(), alias="specialty_obtaining_date")
	owner_special_education: Optional[str] = Field(None, alias="special_education")
	owner_place_speciality: Optional[str] = Field(None, alias="specialty_obtaining_place")
	owner_speciality: Optional[str] = Field(None, alias="specialization")
	experience_dog_breeding: Optional[datetime] = Field(datetime.now(), alias="dog_breeding_experience_date")
	puppies_total_count: Optional[int] = Field(None, alias="total_puppies_count")
	owner_ranks: Optional[str] = Field(None, alias="owner_ranks")
	dogs_ranks: Optional[str] = Field(None, alias="dogs_ranks")


class Nurseries_additional_block_view(Nurseries_additional):
	block_title: str = 'Дополнительная информация'
	block_icon: str = 'info_outline'


class Nurseries_payments_block(RKF_office_base_model):
	block_title: str = 'Оплата членских взносов в Федерацию'
	block_icon: str = 'payment'
	payments: List[Organizations_membership_dues_item] = Field(None, alias="payments")


class Nurseries_by_id(RKF_office_base_model):
	blocks: List[Any]


class Access_blocks_edit(RKF_office_base_model):
	block_title: str = 'Доступ'
	block_icon: str = 'admin_panel_settings'
	access: Optional[List[Any]] = Field(None, alias="access")


class Contact_info_block_view(RKF_office_base_model):
	id: Optional[int] = Field(None, alias="id")
	type_name: Optional[str] = Field(None, alias="type_name")
	value: Optional[str] = Field(None, alias="value")
	description: Optional[str] = Field(None, alias="description")
	is_main: Optional[bool] = Field(None, alias="is_main")


class Contact_info_block_edit(RKF_office_base_model):
	id: Optional[int] = Field(None, alias="id")
	type_id: Optional[int] = Field(None, alias="type_id")
	value: Optional[str] = Field(None, alias="value")
	description: Optional[str] = Field(None, alias="description")
	is_main: Optional[bool] = Field(False, alias="is_main")


class Contacts_blocks_view(RKF_office_base_model):
	block_title: str = Field('Контактные данные', alias="block_title")
	block_icon: str = Field('call', alias="block_icon")
	contacts: Optional[List[Contact_info_block_view]] = Field(None, alias="contacts")


class Contacts_blocks_edit(RKF_office_base_model):
	block_title: str = Field('Контактные данные', alias="block_title")
	block_icon: str = Field('call', alias="block_icon")
	contacts: Optional[List[Contact_info_block_edit]] = Field(None, alias="contacts")


class Address_info_block_view(RKF_office_base_model):
	id: Optional[int] = Field(None, alias="id")
	type_name: Optional[str] = Field(None, alias="type_name")
	address: Optional[str] = Field(None, alias="address")


class Address_info_block_edit_info(RKF_office_base_model):
	id: Optional[int] = Field(None, alias="id")
	type_id: Optional[int] = Field(None, alias="type_id")
	postcode: Optional[str] = Field(None, alias="postcode")
	city_id: Optional[int] = Field(None, alias="city_id")
	street_type_id: Optional[int] = Field(None, alias="street_type_id")
	street_name: Optional[str] = Field(None, alias="street_name")
	house_type_id: Optional[int] = Field(None, alias="house_type_id")
	catalogs_house_types_name: Optional[str] = Field(None, alias="house_name")
	flat_type_id: Optional[int] = Field(None, alias="flat_type_id")
	catalogs_flat_types_name: Optional[str] = Field(None, alias="flat_name")
	geo_lat: Optional[str] = Field(None, alias="geo_lat")
	geo_lon: Optional[str] = Field(None, alias="geo_lon")


class Address_info_block_edit(RKF_office_base_model):
	id: Optional[int] = Field(None, alias="id")
	#address_type_id: Optional[int] = Field(None, alias="address_type_id")
	address_type_id: Optional[int] = Field(None, alias="type_id")
	postcode: Optional[str] = Field(None, alias="postcode")
	city_id: Optional[int] = Field(None, alias="city_id")
	street_type_id: Optional[int] = Field(None, alias="street_type_id")
	street_name: Optional[str] = Field(None, alias="street_name")
	house_type_id: Optional[int] = Field(None, alias="house_type_id")
	house_name: Optional[str] = Field(None, alias="catalogs_house_types_name")
	flat_type_id: Optional[int] = Field(None, alias="flat_type_id")
	flat_name: Optional[str] = Field(None, alias="catalogs_flat_types_name")
	geo_lat: Optional[str] = Field(None, alias="geo_lat")
	geo_lon: Optional[str] = Field(None, alias="geo_lon")


class Address_blocks_view(RKF_office_base_model):
	block_title: str = Field('Адресные данные', alias="block_title")
	block_icon: str = Field('home', alias="block_icon")
	addresses: Optional[List[Address_info_block_view]] = Field(None, alias="addresses")


class Address_blocks_edit(RKF_office_base_model):
	block_title: str = Field('Адресные данные', alias="block_title")
	block_icon: str = Field('home', alias="block_icon")
	addresses: Optional[List[Address_info_block_edit]] = Field(None, alias="addresses")


class Nurseries_update(RKF_office_base_model):
	"""
	Схема для редактирования данных для клуба
	"""
	status_id: Optional[int] = Field(None, alias="status_id")
	name: Optional[str] = Field(None, alias="kennel_full_name")
	short_name: Optional[str] = Field(None, alias="kennel_short_name")
	owner_position: Optional[str] = Field(None, alias="owner_position")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	federation_id: Optional[int] = Field(None, alias="federation_id")
	stamp_code: Optional[str] = Field('', alias="brand_code")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	active_member: Optional[bool] = Field(False, alias="is_active_user")
	is_enable_web: Optional[bool] = Field(False, alias="is_enable_web")
	is_public: Optional[bool] = Field(False, alias="is_public")
	contacts: Optional[List[Contact_info_block_edit]] = None
	addresses: Optional[List[Address_info_block_edit]] = None
	payments: Optional[List[Organizations_membership_dues_item]] = None
	breeds: Optional[List[Nurseries_breeding_breeds]] = None
	first_exhibition_date: Optional[str] = Field(None, alias="first_exhibition_date")
	last_exhibition_date: Optional[str] = Field(None, alias="last_exhibition_date")
	tests_over_past_year: Optional[bool] = Field(False, alias="tests_over_past_year")
	availability_premises: Optional[bool] = Field(False, alias="availability_premises")
	presentation: Optional[bool] = Field(False, alias="presentation")
	tribal_reviews_declared: Optional[str] = Field(None, alias="tribal_reviews_declared")
	tribal_reviews_completed: Optional[str] = Field(None, alias="tribal_reviews_completed")
	succession_of_exhibition_activities: Optional[str] = Field(None, alias="succession_of_exhibition_activities")
	exceptions_in_reports: Optional[str] = Field(None, alias="exceptions_in_reports")
	exhibition_activities_past_periods: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
	sanctions: Optional[str] = Field(None, alias="sanctions")
	certificate_registration_nursery_id: Optional[str] = Field(None, alias="rkf_certificate")
	registration_date: Optional[datetime] = Field(datetime.now(), alias="registration_date")
	prefix: Optional[bool] = Field(False, alias="prefix")
	suffix: Optional[bool] = Field(False, alias="suffix")
	name_lat: Optional[str] = Field(None, alias="name_lat")
	owner_specialist_rkf: Optional[bool] = Field(False, alias="is_rkf_specialist")
	owner_date_speciality: Optional[datetime] = Field(datetime.now(), alias="specialty_obtaining_date")
	owner_special_education: Optional[str] = Field(None, alias="special_education")
	owner_place_speciality: Optional[str] = Field(None, alias="specialty_obtaining_place")
	owner_speciality: Optional[str] = Field(None, alias="specialization")
	experience_dog_breeding: Optional[datetime] = Field(datetime.now(), alias="dog_breeding_experience_date")
	puppies_total_count: Optional[str] = Field(None, alias="total_puppies_count")
	owner_ranks: Optional[str] = Field(None, alias="owner_ranks")
	dogs_ranks: Optional[str] = Field(None, alias="dogs_ranks")
    # Пока не понятно куда эти поля девать
    #certifi#cate_registration_nursery_id: Optional[int] = Field(None, alias="#certifi#cate_registration_nursery_id")
	#certifi#cate_registration_in_rkf_id: Optional[int] = Field(None, alias="#certifi#cate_registration_in_rkf_id")
	#certifi#cate_spe#cial_edu#cation_id: Optional[int] = Field(None, alias="#certifi#cate_spe#cial_edu#cation_id")
	#certifi#cate_spe#cialist_rkf_id: Optional[int] = Field(None, alias="#certifi#cate_spe#cialist_rkf_id")
	#certifi#cate_honorary_title_id: Optional[int] = Field(None, alias="#certifi#cate_honorary_title_id")
	#co_owner_last_name: Optional[str] = Field(None, alias="#co_owner_last_name")
	#co_owner_first_name: Optional[str] = Field(None, alias="#co_owner_first_name")
	#co_owner_se#cond_name: Optional[str] = Field(None, alias="#co_owner_se#cond_name")
	#co_owner_mail: Optional[str] = Field(None, alias="#co_owner_mail")

	@validator("first_exhibition_date", "last_exhibition_date", pre=True)
	def str_to_date(cls, datetime_value: Any):
		try:
			if not datetime_value:
				return None
			if isinstance(datetime_value, str):
				if "T" in datetime_value:
					datetime_value = datetime_value.split('T')[0]
				d_date = datetime.strptime(datetime_value, '%Y-%M-%d').timestamp()
			if isinstance(datetime_value, datetime):
				# d_date = datetime_value.timestamp()
				d_date = datetime_value.strftime('%Y-%M-%d')
			return d_date

		except Exception as e:
			raise ValidationError(
				f"Organization_history_item: first_exhibition_date or last_exhibition_date:{str_date}")

	@validator("stamp_code", pre=True)
	def stamp_code_check(cls, stamp: str):
		if not stamp:
			return None
		elif not (stamp.isupper() and len(stamp) == 3 and ad.is_latin(stamp)):
			raise ValidationError(f"{stamp} - не валидный код клейма. Должен быть три латинских, заглавных символа.")
		return stamp
	# @validator("year_payment", pre=True)
	# def str_to_int(cls, str_int: str):
	# 	try:
	# 		if not str_int:
	# 			return None
	# 		i_int = int(str_int)
	# 		return i_int
	# 	except Exception as e:
	# 		err_msg = f"Organization_history_item: tribal_reviews_declared tribal_reviews_completed:{str_int}"
	# 		raise ValidationError(err_msg)


class Nurseries_update_response(RKF_office_base_model):
	"""
	Схема для редактирования данных для клуба
	"""
	status_id: Optional[int] = Field(None, alias="status_id")
	kennel_full_name: Optional[str] = Field(None, alias="name")
	kennel_short_name: Optional[str] = Field(None, alias="short_name")
	owner_position: Optional[str] = Field(None, alias="owner_position")
	owner_name: Optional[str] = Field(None, alias="owner_name")
	federation_id: Optional[int] = Field(None, alias="federation_id")
	brand_code: Optional[str] = Field('', alias="stamp_code")
	folder_number: Optional[str] = Field(None, alias="folder_number")
	active_member: Optional[bool] = Field(False, alias="is_active_user")
	is_enable_web: Optional[bool] = Field(False, alias="is_enable_web")
	is_public: Optional[bool] = Field(False, alias="is_public")
	contacts: Optional[List[Contact_info_block_edit]] = None
	addresses: Optional[List[Address_info_block_edit]] = None
	payments: Optional[List[Organizations_membership_dues_item]] = None
	breeds: Optional[List[Nurseries_breeding_breeds]] = None
	first_exhibition_date: Optional[datetime] = Field(None, alias="first_exhibition_date")
	last_exhibition_date: Optional[datetime] = Field(None, alias="last_exhibition_date")
	tests_over_past_year: Optional[bool] = Field(False, alias="tests_over_past_year")
	availability_premises: Optional[bool] = Field(False, alias="availability_premises")
	presentation: Optional[bool] = Field(False, alias="presentation")
	tribal_reviews_declared: Optional[str] = Field(None, alias="tribal_reviews_declared")
	tribal_reviews_completed: Optional[str] = Field(None, alias="tribal_reviews_completed")
	succession_of_exhibition_activities: Optional[str] = Field(None, alias="succession_of_exhibition_activities")
	exceptions_in_reports: Optional[str] = Field(None, alias="exceptions_in_reports")
	exhibition_activities_past_periods: Optional[str] = Field(None, alias="exhibition_activities_past_periods")
	sanctions: Optional[str] = Field(None, alias="sanctions")
	rkf_certificate: Optional[str] = Field(None, alias="certificate_registration_nursery_id")
	registration_date: Optional[datetime] = Field(datetime.now(), alias="registration_date")
	prefix: Optional[bool] = Field(False, alias="prefix")
	suffix: Optional[bool] = Field(False, alias="suffix")
	name_lat: Optional[str] = Field(None, alias="name_lat")
	is_rkf_specialist: Optional[bool] = Field(False, alias="owner_specialist_rkf")
	specialty_obtaining_date: Optional[datetime] = Field(datetime.now(), alias="owner_date_speciality")
	special_education: Optional[str] = Field(None, alias="owner_special_education")
	specialty_obtaining_place: Optional[str] = Field(None, alias="owner_place_speciality")
	specialization: Optional[str] = Field(None, alias="owner_speciality")
	dog_breeding_experience_date: Optional[datetime] = Field(datetime.now(), alias="experience_dog_breeding")
	total_puppies_count: Optional[str] = Field(None, alias="puppies_total_count")
	owner_ranks: Optional[str] = Field(None, alias="owner_ranks")
	dogs_ranks: Optional[str] = Field(None, alias="dogs_ranks")


class Nurseries_by_id(RKF_office_base_model):
	title: str = 'Питомники'
	blocks: List[Any]


class Access_block(RKF_office_base_model):
    access_name: str
    value: bool


class Access_blocks_view(RKF_office_base_model):
    block_title: str = 'Доступ'
    block_icon: str = 'admin_panel_settings'
    access: Optional[List[Access_block]]


class Access_blocks_edit(RKF_office_base_model):
    block_title: str = 'Доступ'
    block_icon: str = 'admin_panel_settings'
    access: Optional[List[Any]]
