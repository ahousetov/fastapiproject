from abc import ABC
from typing import Optional, List, Any, Union, Dict
from pydantic import BaseModel, Field, ValidationError, validator
from datetime import datetime, date, time
from typing_extensions import Annotated


block_icon_value = "info_outline"
block_title_value = "Основная информация"


class RKF_office_base_model(BaseModel):
    pass


class Block(BaseModel):
    block_title: Optional[str] = block_title_value
    block_icon: Optional[str] = block_icon_value

    class Config:
        arbitrary_types_allowed = True


class Blocks_list(BaseModel):
    title: Optional[str]
    item_title: Optional[str]
    blocks: Optional[List[Block]]

    class Config:
        arbitrary_types_allowed = True