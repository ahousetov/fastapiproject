from app.schemas.common import *


class Pedigree_declarant_requests_histories_base(RKF_office_base_model):
    """
    Базовая схема для 'Pedigree_declarant_requests_histories'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Pedigree_declarant_requests_histories_create(Pedigree_declarant_requests_histories_base):
    """
    Создание новой записи Pedigree_declarant_requests_histories.

    Все поля наследуем от Pedigree_declarant_requests_histories_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """


class Pedigree_declarant_requests_histories_update(Pedigree_declarant_requests_histories_base):
    """
    Редактирование данных Pedigree_declarant_requests_histories.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Pedigree_declarant_requests_histories_base ничего
    не добавляем.
    """


class Pedigree_declarant_requests_histories_delete(Pedigree_declarant_requests_histories_base):
    """
    Наследуемся от Pedigree_declarant_requests_histories_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """