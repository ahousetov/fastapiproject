from app.schemas.common import *


class Replace_pedigree_request_statuses_base(RKF_office_base_model):
    """
    Базовая схема для 'Replace_pedigree_request_statuses'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Replace_pedigree_request_statuses_create(Replace_pedigree_request_statuses_base):
    """
    Создание новой записи Replace_pedigree_request_statuses.

    Все поля наследуем от Replace_pedigree_request_statuses_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """


class Replace_pedigree_request_statuses_update(Replace_pedigree_request_statuses_base):
    """
    Редактирование данных Replace_pedigree_request_statuses.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Replace_pedigree_request_statuses_base ничего
    не добавляем.
    """


class Replace_pedigree_request_statuses_delete(Replace_pedigree_request_statuses_base):
    """
    Наследуемся от Replace_pedigree_request_statuses_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """