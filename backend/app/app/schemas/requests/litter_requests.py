from app.schemas.common import *


class Litter_requests_base(RKF_office_base_model):
    """
    Базовая схема для 'Litter_requests'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Litter_requests_create(Litter_requests_base):
    """
    Создание новой записи Litter_requests.

    Все поля наследуем от Litter_requests_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """


class Litter_requests_update(Litter_requests_base):
    """
    Редактирование данных Litter_requests.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Litter_requests_base ничего
    не добавляем.
    """


class Litter_requests_delete(Litter_requests_base):
    """
    Наследуемся от Litter_requests_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """