from app.schemas.common import *


class FCI_breed_statuses_base(RKF_office_base_model):
    """
    Общий шаблон секции статус пород собак.
    """
    name: Optional[str] = None


class FCI_breed_statuses_create(FCI_breed_statuses_base):
    """
    Создаём новый секцию статус пород собак.
    """
    pass


class FCI_breed_statuses_update(FCI_breed_statuses_base):
    """
    Изменяем секцию статус пород собак.
    """
    pass


class FCI_breed_statuses_delete(FCI_breed_statuses_base):
    """
    Удаляем секцию статус пород собак (помечаем удалённым, не удаляем из БД)
    """
    id: int


class FCI_breed_statuses_DB(FCI_breed_statuses_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    name: str

    class Config:
        orm_mode = True


class FCI_breed_statuses_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class FCI_breed_statuses(FCI_breed_statuses_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из FCI_breed_statuses_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class FCI_breed_statuses_block(Block):
    id: Optional[int] = None
    name: Optional[str] = None


class FCI_breed_statuses_blocks(Blocks_list):
    blocks: Optional[List[FCI_breed_statuses_block]]