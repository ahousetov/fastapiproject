from app.schemas.common import *


class Breeds_foreign_id(RKF_office_base_model):
    """
    Отдельная модель, для хранения значений id
    для полей foreign_key в Breeds.

    Смысл отдельной модели - опциональное приминение
    этих полей.

    Опциональные параметры:
    dogs_fci_groups_number: Optional[int] = Field(None, alias="dogs_fci_groups_number")
    dogs_fci_groups_name: Optional[str] = Field(None, alias="dogs_fci_groups_name")
    dogs_fci_sections_number: Optional[str] = Field(None, alias="dogs_fci_sections_number")
    dogs_fci_sections_name: Optional[str] = Field(None, alias="dogs_fci_sections_name")
    dogs_fci_breed_statuses_name: Optional[str] = Field(None, alias="dogs_fci_breed_statuses_name")
    """
    dogs_fci_groups_number: Optional[int] = Field(None, alias="dogs_fci_groups_number")
    dogs_fci_groups_name: Optional[str] = Field(None, alias="dogs_fci_groups_name")
    dogs_fci_sections_number: Optional[str] = Field(None, alias="dogs_fci_sections_number")
    dogs_fci_sections_name: Optional[str] = Field(None, alias="dogs_fci_sections_name")
    dogs_fci_breed_statuses_name: Optional[str] = Field(None, alias="dogs_fci_breed_statuses_name")


class Breeds_base(RKF_office_base_model):
    """
    Базовая схема для 'пород собак'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    name_rus: Optional[str] = Field(None, alias="name_rus")
    name_en: Optional[str] = Field(None, alias="name_en")
    name_original: Optional[str] = Field(None, alias="name_original")
    fci_number: Optional[int] = Field(None, alias="fci_number")
    fci_status_id: Optional[int] = Field(None, alias="fci_status_id")
    is_cacib: Optional[bool] = Field(False, alias="is_cacib")
    is_fci: Optional[bool] = Field(False, alias="is_fci")
    is_specific: Optional[bool] = Field(False, alias="is_specific")
    for_dog_handler: Optional[bool] = Field(False, alias="for_dog_handler")

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Breeds_create(Breeds_base):
    """
    Создание новой породы собак.

    Все поля наследуем от Breeds_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    id: Optional[int] = Field(None, alias="id")
    relations_rel_breeds_sections_groups_group_id: Optional[int] = Field(None, alias="relations_rel_breeds_sections_groups_group_id")
    relations_rel_breeds_sections_groups_section_id: Optional[int] = Field(None, alias="relations_rel_breeds_sections_groups_section_id")


class Breeds_update(Breeds_base):
    """
    Редактирование данных населённого пункта.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Breeds_base и Breeds_foreign_id ничего
    не добавляем.
    """
    id: Optional[int] = Field(None, alias="id")
    relations_rel_breeds_sections_groups_group_id: Optional[int] = Field(None, alias="relations_rel_breeds_sections_groups_group_id")
    relations_rel_breeds_sections_groups_section_id: Optional[int] = Field(None, alias="relations_rel_breeds_sections_groups_section_id")


class Breeds_delete(Breeds_base):
    """
    Наследуемся от Breeds_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    id: Optional[int] = Field(None, alias="id")
    is_deleted: bool = Field(False, alias="is_deleted")


class Breeds_DB(Breeds_base, Breeds_foreign_id):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Breeds_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    id: int = Field(None, alias="id")
    name_rus: str = Field(None, alias="name_rus")
    name_en: str = Field(None, alias="name_en")
    name_original: str = Field(None, alias="name_original")
    dogs_fci_breed_statuses_name: Optional[str] = Field(None, alias="dogs_fci_breed_statuses_name")


class Breeds(Breeds_DB):
    """
    Схема-наследник Breeds_DB для вывода данных для
    одиночной записи Breeds, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Breeds_block(Block, Breeds_base, Breeds_foreign_id):
    relations_rel_breeds_sections_groups_group_id: Optional[int] = Field(None, alias="relations_rel_breeds_sections_groups_group_id")
    relations_rel_breeds_sections_groups_section_id: Optional[int] = Field(None, alias="relations_rel_breeds_sections_groups_section_id")
    dogs_fci_breed_statuses_name: Optional[str] = Field(None, alias="dogs_fci_breed_statuses_name")


class Breeds_blocks(Blocks_list):
    title: str = Field("Породы", alias='title')
    item_title: Optional[str] = Field('', alias='item_title')
    blocks: Optional[List[Breeds_block]] = Field(None, alias='blocks')
