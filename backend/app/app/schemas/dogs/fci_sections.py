from app.schemas.common import *


class FCI_section_base(RKF_office_base_model):
    """
    Общий шаблон секции пород собак.
    """
    number: Optional[int] = None
    name: Optional[str] = None
    name_en: Optional[str] = None


class FCI_section_create(FCI_section_base):
    """
    Создаём новый секцию пород собак.
    """
    pass


class FCI_section_update(FCI_section_base):
    """
    Изменяем секцию пород собак.
    """
    pass


class FCI_section_delete(FCI_section_base):
    """
    Удаляем секцию пород собак (помечаем удалённым, не удаляем из БД)
    """
    id: int


class FCI_section_DB(FCI_section_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    number: int
    name: str
    name_en: str

    class Config:
        orm_mode = True


class FCI_section_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class FCI_group_valuelist(FCI_section_base):
    """
    Список value-значений
    """
    value: int
    number: int
    name: str
    name_en: str


class FCI_section(FCI_section_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из FCI_section_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


#class FCI_sections_block(RKF_office_base_model):
class FCI_sections_block(Block):
    number: Optional[int] = None
    name: Optional[str] = None
    name_en: Optional[str] = None


#class FCI_sections_blocks(RKF_office_base_model):
class FCI_sections_blocks(Blocks_list):
    blocks: Optional[List[FCI_sections_block]]