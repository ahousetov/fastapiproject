from app.schemas.common import *


class FCI_group_base(RKF_office_base_model):
    """
    Общий шаблон типа здания.
    """
    number: Optional[int] = None
    name: Optional[str] = None
    name_en: Optional[str] = None




class FCI_group_create(FCI_group_base):
    """
    Создаём новый тип здания.
    """
    pass


class FCI_group_update(FCI_group_base):
    """
    Изменяем тип здания.
    """
    pass


class FCI_group_delete(FCI_group_base):
    """
    Удаляем тип здания (помечаем удалённым, не удаляем из БД)
    """
    id: int


    # Для того, чтобы не возникало ошибки при ответе от API,
    # конфигурацию orm_mode надо добавлять в класс схемы.
    # class Config:
    #    orm_mode = True

class FCI_group_DB(FCI_group_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    number: int
    name: str
    name_en: str

    class Config:
        orm_mode = True


class FCI_group_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class FCI_group(FCI_group_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из FCI_group_DB, если не переопределим чего-то
    в наследнике.
    {
        title: "Группы пород в соответствии с классификацией FCI" (название справочника например),
        item_title: "Пастушьи и скотогонные собаки, кроме швейцарских скотогонных собак" (заголовок в карточке),
        blocks: [
            {
                    block_title: "Основная информация",
                    number: 1,
                    name: "Пастушьи и скотогонные собаки, кроме швейцарских скотогонных собак",
                    name_en: "Sheepdogs and Cattledogs (except Swiss Cattledogs)"
                },
                {Следующие блоки в других запросах...}
        ]
    }
    """
    pass


class FCI_groups_block(Block):
    number: Optional[int] = None
    name: Optional[str] = None
    name_en: Optional[str] = None


class FCI_groups_blocks(Blocks_list):
    blocks: Optional[List[FCI_groups_block]]

