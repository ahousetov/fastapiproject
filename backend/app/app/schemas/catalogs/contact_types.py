from app.schemas.common import *


# Общий шаблон типа данных контакта
class Contact_types_base(RKF_office_base_model):
    name: Optional[str] = None


# Создаём новый тип данных контакта
class Contact_types_create(Contact_types_base):
    pass


# Изменяем тип данных контакта
class Contact_types_update(Contact_types_base):
    name: Optional[str] = None


#  Удаляем тип данных контакта
class Contact_types_delete(Contact_types_base):
    pass


# Properties shared by models stored in DB
#
class Contact_types_in_DBBase(Contact_types_base):
    id: int
    name: str

    class Config:
        orm_mode = True


class Contact_types_value_list(Contact_types_base):
    value: int
    name: str

# Properties to return to client
class Contact_types(Contact_types_in_DBBase):
    pass


class Contact_types_block(Block, Contact_types_base):
    id: int


class Contact_types_blocks(Blocks_list):
    blocks: Optional[List[Contact_types_block]]