from app.schemas.common import *


# Общий шаблон федерального района
class Federal_district_base(RKF_office_base_model):
    name: Optional[str] = None


# Создаём новый район
class Federal_district_create(Federal_district_base):
    pass


# Изменяем федеральный район
class Federal_district_update(Federal_district_base):
    name: Optional[str] = None


#  федеральный район
class Federal_district_delete(Federal_district_base):
    pass


# Properties shared by models stored in DB
#
class Federal_district_db(Federal_district_base):
    id: int = Field(None, alias="id")
    name: str = Field(None, alias="name")

    class Config:
        orm_mode = True


# Properties to return to client
class Federal_district(Federal_district_db):
    pass


class Federal_district_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class Federal_district_block(Block, Federal_district_base):
    id: int


class Federal_district_blocks(Blocks_list):
    blocks: Optional[List[Federal_district_block]]