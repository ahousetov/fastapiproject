from app.schemas.common import *


class Street_type_base(RKF_office_base_model):
    """
    Общий шаблон типа улицы.
    """
    name: Optional[str] = None


class Street_type_create(Street_type_base):
    """
    Создаём новый тип улицы.
    """
    pass


class Street_type_update(Street_type_base):
    """
    Изменяем тип улицы.
    """
    name: Optional[str] = None


class Street_type_delete(Street_type_base):
    """
    Удаляем тип улицы (помечаем удалённым, не удаляем из БД)
    """
    pass


class Street_type_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class Street_type_DB(Street_type_base):
    """
    Модель-предсталвения данных в БД.
    """
    id: int
    name: str

    class Config:
        orm_mode = True


class Street_type(Street_type_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Street_type_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class Street_type_block(Block):
    id: int
    name: str


class Street_type_blocks(Blocks_list):
    title: str = 'Типы улицы'
    item_title: Optional[str] = Field('', alias='item_title')
    blocks: Optional[List[Street_type_block]] = Field(None, alias='blocks')
