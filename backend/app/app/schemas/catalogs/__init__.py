from .cities import Cities, \
    Cities_multi, \
    Cities_create, \
    Cities_update, \
    Cities_delete, \
    Cities_blocks, \
    Cities_block, \
    Cities_valuelist, \
    Cities_multi_patch
from .city_types import City_type, \
    City_types_create, \
    City_types_update, \
    City_types_delete, \
    City_types_blocks, \
    City_types_block, \
    City_types_valuelist
from .federal_district import Federal_district, \
    Federal_district_create, \
    Federal_district_update, \
    Federal_district_delete, \
    Federal_district_blocks, \
    Federal_district_block, \
    Federal_district_valuelist
from .flat_types import Flat_type, \
    Flat_type_create, \
    Flat_type_update, \
    Flat_type_delete, \
    Flat_type_blocks, \
    Flat_type_block, \
    Flat_type_valuelist
from .house_types import House_type, \
    House_type_create, \
    House_type_update, \
    House_type_delete, \
    House_type_blocks, \
    House_type_block, \
    House_type_valuelist
from .regions import Region, \
    Region_create, \
    Region_update, \
    Region_delete, \
    Region_blocks, \
    Region_block, \
    Region_valuelist
from .street_types import Street_type, \
    Street_type_create, \
    Street_type_update, \
    Street_type_block, \
    Street_type_blocks, \
    Street_type_valuelist
from .federations import Federations, \
    Federations_create, \
    Federations_update, \
    Federations_delete, \
    Federations_blocks, \
    Federations_block, \
    Federation_info_block, \
    Federation_info_value_list
from .address_types import Address_types, \
    Address_types_create, \
    Address_types_update, \
    Address_types_delete, \
    Address_types_blocks, \
    Address_types_block, \
    Address_types_value_list
from .organization_statuses import Organization_statuses, \
    Organization_statuses_create, \
    Organization_statuses_update, \
    Organization_statuses_blocks, \
    Organization_statuses_block, \
    Organization_statuses_value_list
from .weekends_list import Weekends_list_base, \
    Weekends_list_create, \
    Weekends_list_update, \
    Weekends_list_delete
from .contact_types import Contact_types_block, \
    Contact_types_value_list
