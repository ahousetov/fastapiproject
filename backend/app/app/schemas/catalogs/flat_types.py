from app.schemas.common import *


class Flat_type_base(RKF_office_base_model):
    """
    Общий шаблон типа помещения.
    """
    name: Optional[str] = None


class Flat_type_create(Flat_type_base):
    """
    Создаём новый тип помещения.
    """
    pass


class Flat_type_update(Flat_type_base):
    """
    Изменяем тип помещения.
    """
    name: Optional[str] = None


class Flat_type_delete(Flat_type_base):
    """
    Удаляем тип помещения (помечаем удалённым, не удаляем из БД)
    """
    pass


class Flat_type_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class Flat_type_DB(Flat_type_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    name: str

    class Config:
        orm_mode = True


class Flat_type(Flat_type_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Flat_type_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class Flat_type_block(Block):
    id: int
    name: str


class Flat_type_blocks(Blocks_list):
    title: str = 'Тип помещения'
    blocks: Optional[List[Flat_type_block]]