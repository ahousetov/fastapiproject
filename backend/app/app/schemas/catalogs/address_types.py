from app.schemas.common import *


# Общий шаблон типа адреса
class Address_types_base(RKF_office_base_model):
    name: Optional[str] = None


# Создаём новый тип адреса
class Address_types_create(Address_types_base):
    pass


# Изменяем тип адреса
class Address_types_update(Address_types_base):
    name: Optional[str] = None


#  тип адреса
class Address_types_delete(Address_types_base):
    pass

# Properties shared by models stored in DB
#
class Address_types_in_DBBase(Address_types_base):
    id: int
    name: str

    class Config:
        orm_mode = True


class Address_types_value_list(RKF_office_base_model):
    value: Optional[int] = None
    name: Optional[str] = None


# Properties to return to client
class Address_types(Address_types_in_DBBase):
    pass


class Address_types_block(Block, Address_types_base):
    id: int


class Address_types_blocks(Blocks_list):
    blocks: Optional[List[Address_types_block]] = Optional[List[Address_types_block]]
