from app.schemas.common import *


class Region_base(RKF_office_base_model):
    """
    Общий шаблон региона
    """
    name: Optional[str] = None


class Region_create(Region_base):
    """
    Создаём новый регион
    """
    pass


class Region_update(Region_base):
    """
    Изменяем регион
    """
    name: Optional[str] = None


class Region_delete(Region_base):
    """
    Удаляем регион (помечаем удалённым, не удаляем из БД)
    """
    pass


class Region_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class Region_base_db(Region_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    name: str

    class Config:
        orm_mode = True


class Region(Region_base_db):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Region_base_db, если не переопределим чего-то
    в наследнике.
    """
    pass


class Region_block(Block):
    id: int
    name: str


class Region_blocks(Blocks_list):
    title = "Регионы"
    blocks: Optional[List[Region_block]]
