from app.schemas.common import *


class Weekends_list_base(RKF_office_base_model):
    """
    Базовая схема для 'Weekends_list'. Используем как шаблон
    для остальных схем.

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Weekends_list_create(Weekends_list_base):
    """
    Создание нового Stamp_code.

    Все поля наследуем от Weekends_list_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """


class Weekends_list_update(Weekends_list_base):
    """
    Редактирование данных Weekends_list.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Weekends_list_base ничего
    не добавляем.
    """


class Weekends_list_delete(Weekends_list_base):
    """
    Наследуемся от Weekends_list_base.

    id - обязательный параметр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """