from app.schemas.common import *


class Cities_foreign_id(RKF_office_base_model):
    """
    Отдельная модель, для хранения значений id
    для полей foreign_key в Cities.

    Смысл отдельной модели - опциональное приминение
    этих полей.

    Опциональные параметры:
    federal_district_id: Optional[int] = None
    region_id: Optional[int] = None
    type_id: Optional[int] = None
    """
    federal_district_id: Optional[int] = None
    region_id: Optional[int] = None
    type_id: Optional[int] = None


class Cities_base(RKF_office_base_model):
    """
    Базовая схема для 'Населённых пунктов'. Используем как шаблон
    для остальных схем.

    Опциональные параметры:
    name: str = None
    name_eng: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None

    id и is_deleted - обязательные параметры, поэтому указываем
    их в наследниках явно.
    """
    name: str = None
    name_eng: Optional[str] = None
    geo_lat: Optional[str] = None
    geo_lon: Optional[str] = None


class Cities_create(Cities_base, Cities_foreign_id):
    """
    Создание нового населённого пункта.

    Все поля наследуем от Cities_base, ничего не добавляем.
    id будет сгенерировать при сохранении, а is_deleted по
    умолчанию False. Остальные поля опциональны.
    """
    pass


class Cities_update(Cities_base, Cities_foreign_id):
    """
    Редактирование данных населённого пункта.

    Делаем name - опциональным полем, т.к. при сохранении
    новое значение может быть пустым или нулевым (т.е. без изменений).

    Остальные поля наследуем от Cities_base и Cities_foreign_id ничего
    не добавляем.
    """
    name: Optional[str] = None


class Cities_delete(Cities_base):
    """
    Наследуемся от Cities_base.

    id - обязательный параментр. Удаление происходит только
    при наличии id.

    Остальные параметры могут быть проигнорированы.
    """
    id: int

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Cities_DB(Cities_base, Cities_foreign_id):
    """
    Схема для ответа API данными из БД.

    Обязательны поля id, name и id_deleted. Остальные наследуем от
    Cities_base. Всё кроме вышеописанной тройки поле - опционально.
    """
    id: int
    name: str

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True


class Cities_valuelist(RKF_office_base_model):
    value: int
    name: str


class Cities(Cities_DB):
    """
    Схема-наследник Cities_DB для вывода данных для
    одиночной записи Cities, полученной по id.

    Выводим данные со значениями id внешних ключей.
    """
    pass


class Cities_multi(Cities_base):
    """
    Схема вывода списка данных по населённым пунктам.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    id: int
    name: str
    catalogs_federal_districts_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None

    class Config:
        """
        Важный момент. Для всех схем, что отвечают данными запроса к БД
        надо оснащать вот такой конструкцией:

        class Config:
            orm_mode = True
        """
        orm_mode = True
        arbitrary_types_allowed = True


class Cities_multi_patch(Cities_multi):
    """
    Схема вывода списка данных по населённым пунктам для метода PATCH.

    Вместо id-значений внешних ключей, выводим имена значений из
    справочников федеральных округов, регионов, типов населённых пунктов.
    """
    geo: Optional[str] = None


class Cities_block(Block, Cities_base, Cities_foreign_id):
    catalogs_federal_districts_name: Optional[str] = None
    catalogs_regions_name: Optional[str] = None
    catalogs_city_types_name: Optional[str] = None


class Cities_blocks(Blocks_list):
    title: str = "Населённые пункты"
    blocks: Optional[List[Any]] = None