from app.schemas.common import *
from app.schemas.public import Contacts_update, Contacts_extra_data


class Federations_base(RKF_office_base_model):
    """
    Общий шаблон федерация
    """
    # id: Optional[int] = None
    name: Optional[str] = None
    short_name: Optional[str] = None
    public_legal_informations_owner_name: Optional[str] = None
    public_legal_informations_owner_position: Optional[str] = None
    public_legal_informations_ogrn: Optional[str] = None
    public_legal_informations_inn: Optional[str] = None
    public_legal_informations_kpp: Optional[str] = None
    # sites: Optional[str] = None
    # phones: Optional[str] = None
    # emails: Optional[str] = None

    class Config:
        orm_mode = True


class Federations_create(Federations_base):
    """
    Создаём новый федерацию
    """
    pass


# class Federations_update(Federations_base):
#     """
#     Изменяем федерацию
#     """
#     pass


class Federations(Federations_base):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Federations_base_db, если не переопределим чего-то
    в наследнике.
    """
    id: Optional[int] = None
    sites: Optional[str] = None
    phones: Optional[str] = None
    emails: Optional[str] = None


class Federations_update(Federations_base):
    """
    Изменяем федерацию
    """
    id: Optional[int] = None
    contacts: Optional[List[Contacts_update]] = None


class LegalInformationsInput(RKF_office_base_model):
    owner_name: Optional[str] = Field(None, alias='public_legal_informations_owner_name')
    owner_position: Optional[str] = Field(None, alias='public_legal_informations_owner_position')
    ogrn: Optional[str] = Field(None, alias='public_legal_informations_ogrn')
    inn: Optional[str] = Field(None, alias='public_legal_informations_inn')
    kpp: Optional[str] = Field(None, alias='public_legal_informations_kpp')

    class Config:
        allow_population_by_field_name = True


class ContactsInput(RKF_office_base_model):
    id: Optional[int] = None
    type_id: Optional[int] = None
    value: Optional[str] = None
    description: Optional[str]
    is_main: Optional[bool] = None

class Federations_delete(Federations_base):
    """
    Удаляем федерацию (помечаем удалённым, не удаляем из БД)
    """
    pass


class Federations_base_db(Federations_base):
    """
    Модель-предсталвление данных в БД.
    """
    pass


class Federations_block(Block, Federations_base):
    block_title = "Основная информация"
    block_icon = "info_outline"


class Contact_block(Block):
    block_title = "Контактные данные"
    block_icon = "call"
    contacts: List[Contacts_extra_data] = None


class Federation_info_block(RKF_office_base_model):
    id: Optional[int]
    short_name: Optional[str]


class Federation_info_value_list(RKF_office_base_model):
    value: Optional[int]
    name: Optional[str]

class Federations_blocks(Blocks_list):
    blocks: Optional[List[Any]] = None


class ContactsUpdate(RKF_office_base_model):
    id: int
    type_id: Optional[int]
    value: Optional[str]
    description: Optional[str]
    is_main: Optional[bool]


class ContactsCreate(RKF_office_base_model):
    type_id: int
    value: str
    description: Optional[str]
    is_main: Optional[bool] = False
