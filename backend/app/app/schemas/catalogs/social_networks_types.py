from typing import Optional

from app.schemas.common import RKF_office_base_model


class Social_networks_types_base(RKF_office_base_model):
    """
    Общий шаблон social_networks_types.
    """
    id: Optional[int]
    name: Optional[str]
    is_deleted: Optional[bool]

    class Config:
        orm_mode = True


class Social_networks_types_create(Social_networks_types_base):
    """
    """
    name: str


class Social_networks_types_update(Social_networks_types_base):
    """
    """
    id: int


class Social_networks_types_delete(Social_networks_types_base):
    """
    """
    id: int
    is_deleted: bool
