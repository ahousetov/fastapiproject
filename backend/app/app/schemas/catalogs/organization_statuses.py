from app.schemas.common import *


class Organization_statuses_base(RKF_office_base_model):
    """
    Общий шаблон типа помещения.
    """
    name: Optional[str] = None


class Organization_statuses_create(Organization_statuses_base):
    """
    Создаём новый тип помещения.
    """
    pass


class Organization_statuses_update(Organization_statuses_base):
    """
    Изменяем тип помещения.
    """
    name: Optional[str] = None


class Organization_statuses_delete(Organization_statuses_base):
    """
    Удаляем тип помещения (помечаем удалённым, не удаляем из БД)
    """
    pass


class Organization_statuses_DB(Organization_statuses_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    name: str

    class Config:
        orm_mode = True


class Organization_statuses(Organization_statuses_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Organization_statuses_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class Organization_statuses_block(Block):
    id: int
    name: str


class Organization_statuses_value_list(RKF_office_base_model):
    value: int
    name: str


# class Organization_statuses_blocks(Blocks_list):
class Organization_statuses_blocks(RKF_office_base_model):
    block_title: str = "Статус организации"
    blocks: Optional[List[Organization_statuses_block]]