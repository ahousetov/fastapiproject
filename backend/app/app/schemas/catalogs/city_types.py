from app.schemas.common import *


# Общий шаблон федерального района
class City_types_base(RKF_office_base_model):
    name: Optional[str] = None


# Создаём новый район
class City_types_create(City_types_base):
    pass


# Изменяем федеральный район
class City_types_update(City_types_base):
    name: Optional[str] = None


#  федеральный район
class City_types_delete(City_types_base):
    pass

# Properties shared by models stored in DB
#
class City_types_in_DBBase(City_types_base):
    id: int
    name: str

    class Config:
        orm_mode = True


# Properties to return to client
class City_type(City_types_in_DBBase):
    pass


class City_types_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class City_types_block(Block, City_types_base):
    id: int


class City_types_blocks(Blocks_list):
    title: str = "Типы населённых пунктов"
    block_icon: str = 'info_outline'
    blocks: Optional[List[City_types_block]]
