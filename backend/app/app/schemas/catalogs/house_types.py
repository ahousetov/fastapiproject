from app.schemas.common import *


class House_type_base(RKF_office_base_model):
    """
    Общий шаблон типа здания.
    """
    name: Optional[str] = None


class House_type_create(House_type_base):
    """
    Создаём новый тип здания.
    """
    pass


class House_type_update(House_type_base):
    """
    Изменяем тип здания.
    """
    name: Optional[str] = None


class House_type_delete(House_type_base):
    """
    Удаляем тип здания (помечаем удалённым, не удаляем из БД)
    """
    pass


class House_type_valuelist(RKF_office_base_model):
    """
    Список value-значений
    """
    value: int
    name: str


class House_type_DB(House_type_base):
    """
    Модель-предсталвения данных в БД.
    """

    id: int
    name: str

    class Config:
        orm_mode = True


class House_type(House_type_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из House_type_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class House_type_block(Block):
    id: int
    name: str


class House_type_blocks(Blocks_list):
    blocks: Optional[List[House_type_block]]