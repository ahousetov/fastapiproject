from typing import Optional
from app.schemas.common import *


class Rel_users_profiles_base(RKF_office_base_model):
    """
    Общий шаблон отношений профиль-контакты.
    """
    id: Optional[int]
    profile_id: Optional[int] = None
    users_id: Optional[int] = None

    class Config:
        orm_mode = True


class Rel_users_profiles_create(Rel_users_profiles_base):
    """
    Создаём новый relation профиль-контакт.
    """
    pass


class Rel_users_profiles_update(Rel_users_profiles_base):
    """
    Изменяем секцию relation профиль-контакт.
    """
    pass


class Rel_users_profiles_delete(Rel_users_profiles_base):
    """
    Удаляем секцию relation профиль-контакт. (помечаем удалённым, не удаляем из БД)
    """
    id: int


class Rel_users_profiles_DB(Rel_users_profiles_base):
    """
    Модель-предсталвения данных в БД.
    """
    pass


class Rel_users_profiles(Rel_users_profiles_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Rel_users_profiles_DB, если не переопределим чего-то
    в наследнике.
    """
    pass
