from typing import Any, Optional, List
from app.schemas.common import *
from app.schemas.common import RKF_office_base_model


class Rel_national_breed_clubs_contacts_base(RKF_office_base_model):
    """
    Общий шаблон national_breed_clubs_presidium_members.
    """
    id: Optional[int]
    national_breed_club_id: Optional[int]
    contact_id: Optional[int]

    class Config:
        orm_mode = True


class Rel_national_breed_clubs_contacts_create(Rel_national_breed_clubs_contacts_base):
    """
    """
    national_breed_club_id: int
    contact_id: int


class Rel_national_breed_clubs_contacts_update(Rel_national_breed_clubs_contacts_base):
    """
    """
    id: int


class Rel_national_breed_clubs_contacts_delete(Rel_national_breed_clubs_contacts_base):
    """
    """
    id: int
