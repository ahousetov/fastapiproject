from typing import Optional
from app.schemas.common import *
from pydantic import Field


class Rel_breeds_sections_groups_base(RKF_office_base_model):
    """
    Общий шаблон отношений пород-секций-групп.
    """
    id: Optional[int]
    breed_id: Optional[int] = None
    section_id: Optional[int] = None
    group_id: Optional[int] = None

    class Config:
        orm_mode = True


class Rel_breeds_sections_groups_create(Rel_breeds_sections_groups_base):
    """
    Создаём новый relation пород-секций-групп.
    """
    pass


class Rel_breeds_sections_groups_update(Rel_breeds_sections_groups_base):
    """
    Изменяем секцию relation пород-секций-групп.
    """
    pass


class Rel_breeds_sections_groups_delete(Rel_breeds_sections_groups_base):
    """
    Удаляем секцию relation пород-секций-групп. (помечаем удалённым, не удаляем из БД)
    """
    id: int


class Rel_breeds_sections_groups_DB(Rel_breeds_sections_groups_base):
    """
    Модель-предсталвения данных в БД.
    """

    pass


class Rel_breeds_sections_groups(Rel_breeds_sections_groups_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Rel_breeds_sections_groups_DB, если не переопределим чего-то
    в наследнике.
    """
    pass


class Rel_breeds_sections_groups_aliased(RKF_office_base_model):
    """
    Общий шаблон отношений пород-секций-групп.
    """
    id: Optional[int]
    breed_id: Optional[int] = None
    # section_id: Optional[int] = None
    # group_id: Optional[int] = None
    group_id: Optional[int] = Field(alias="relations_rel_breeds_sections_groups_group_id")
    section_id: Optional[int] = Field(alias="relations_rel_breeds_sections_groups_section_id")

    class Config:
        orm_mode = True