from typing import Optional
from app.schemas.common import *
from app.schemas.common import RKF_office_base_model


class Rel_nurseries_breeding_breed(RKF_office_base_model):
    """
    Общий шаблон national_breed_clubs_presidium_members.
    """
    id: Optional[int] = Field(None, alias="id")
    breed_id: Optional[int] = Field(None, alias="breed_id")
    nursery_id: Optional[int] = Field(None, alias="nursery_id")

    class Config:
        orm_mode = True


class Rel_nurseries_breeding_breed_create(RKF_office_base_model):
    """
    Схема для создания связи в rel_nurseries_breeding_breed.
    Обязательные параметры:
        breed_id: int = Field(0, alias="breed_id")
        nursery_id: int = Field(0, alias="nursery_id")
    """
    breed_id: int = Field(0, alias="breed_id")
    nursery_id: int = Field(0, alias="nursery_id")


class Rel_nurseries_breeding_breed_update(Rel_nurseries_breeding_breed):
    """
    Схема для обновления связей в rel_nurseries_breeding_breed.
    Обязательные параметры:
        id: int = Field(0, alias="id")
    """
    id: int = Field(0, alias="id")


class Rel_nurseries_breeding_breed_delete(Rel_nurseries_breeding_breed):
    """
    Схема для удаления, к чертям собачьим, связи в rel_nurseries_breeding_breed.
    Обязательно знать лишь id:
            id: int = Field(0, alias="id")
    """
    id: int = Field(0, alias="id")
