from typing import Optional

from app.schemas.common import *


class Rel_profiles_contacts_base(RKF_office_base_model):
    """
    Общий шаблон отношений профиль-контакты.
    """
    id: Optional[int]
    profile_id: Optional[int] = None
    contact_id: Optional[int] = None

    class Config:
        orm_mode = True


class Rel_profiles_contacts_create(Rel_profiles_contacts_base):
    """
    Создаём новый relation профиль-контакт.
    """
    pass


class Rel_profiles_contacts_update(Rel_profiles_contacts_base):
    """
    Изменяем секцию relation профиль-контакт.
    """
    pass


class Rel_profiles_contacts_delete(Rel_profiles_contacts_base):
    """
    Удаляем секцию relation профиль-контакт. (помечаем удалённым, не удаляем из БД)
    """
    id: int


class Rel_profiles_contacts_DB(Rel_profiles_contacts_base):
    """
    Модель-предсталвения данных в БД.
    """

    pass


class Rel_profiles_contacts(Rel_profiles_contacts_DB):
    """
    Модель для ответа на запросы по API. Возвращаем
    набор данных из Rel_profiles_contacts_DB, если не переопределим чего-то
    в наследнике.
    """
    pass
