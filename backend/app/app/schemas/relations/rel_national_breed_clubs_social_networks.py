from typing import Optional

from app.schemas.common import RKF_office_base_model


class Rel_national_breed_clubs_social_networks_base(RKF_office_base_model):
    """
    Общий шаблон rel_national_breed_clubs_social_networks.
    """
    id: Optional[int]
    national_breed_club_id: Optional[int]
    social_networks_id: Optional[int]

    class Config:
        orm_mode = True


class Rel_national_breed_clubs_social_networks_create(Rel_national_breed_clubs_social_networks_base):
    """
    """
    national_breed_club_id: int
    social_networks_id: int


class Rel_national_breed_clubs_social_networks_update(Rel_national_breed_clubs_social_networks_base):
    """
    """
    id: int


class Rel_national_breed_clubs_social_networks_delete(Rel_national_breed_clubs_social_networks_base):
    """
    """
    id: int
