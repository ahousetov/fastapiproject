from .rel_breeds_sections_groups import Rel_breeds_sections_groups, \
    Rel_breeds_sections_groups_create, \
    Rel_breeds_sections_groups_update
from .rel_profiles_contacts import Rel_profiles_contacts_base, \
    Rel_profiles_contacts_create, \
    Rel_profiles_contacts_update
from .rel_profiles_federations import Rel_profiles_federations, \
    Rel_profiles_federations_create, \
    Rel_profiles_federations_update
from .rel_nurseries_breeding_breeds import Rel_nurseries_breeding_breed, \
    Rel_nurseries_breeding_breed_create, \
    Rel_nurseries_breeding_breed_update, \
    Rel_nurseries_breeding_breed_delete