import pytest
from app.crud.catalogs.crud_street_types import CRUDStreet_types
from app.models.catalogs.street_types import Street_types
from app.schemas.catalogs.street_types import Street_type_create, Street_type_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Street_type_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Street_type_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDStreet_types(model=Street_types)


@pytest.fixture()
def api_name():
    return 'street_types'


def test_create_street_types(test_create_by_api):
    assert test_create_by_api


def test_get_existing_street_types(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_street_types(test_delete_by_api):
    assert test_delete_by_api