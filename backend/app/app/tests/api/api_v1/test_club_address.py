import pytest
from app.crud.public.crud_club_address import CRUDClub_address
from app.models.public.club_address import Club_addresses
from app.crud.public.crud_club_address import club_address
from app.crud.catalogs.crud_cities import cities
from app.crud.catalogs.crud_street_types import street_types
from app.crud.catalogs.crud_flat_types import flat_types
from app.crud.catalogs.crud_house_types import house_types
from app.crud.catalogs.crud_address_types import address_types
from app.crud.public.crud_profiles import profiles
# from app.schemas.public.contacts import Contacts_create, Contacts_update
from app.schemas.public.club_address import Club_address_create, Club_address_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in(db):
    return Club_address_create(
        address_type_id=address_types.random_choose_id(db=db),
        profile_id=profiles.random_choose_id(db=db),
        city_id=cities.random_choose_id(db=db),
        postcode=Catalogs_tests.random_lower_string(),
        street_type_id=street_types.random_choose_id(db=db),
        street_name=Catalogs_tests.random_lower_string(),
        house_type_id=house_types.random_choose_id(db=db),
        house_name=Catalogs_tests.random_lower_string(),
        flat_type_id=flat_types.random_choose_id(db=db),
        flat_name=Catalogs_tests.random_lower_string(),
        geo_lat=Catalogs_tests.random_lower_string(),
        geo_lon=Catalogs_tests.random_lower_string(),
        # # не нашёл места для этих параметров.
        # profile_id=profiles.random_choose_id(db=db),
        # building_name=Catalogs_tests.random_lower_string()
    )


@pytest.fixture()
def update_schema_in(db):
    return Club_address_update(
        address_type_id=address_types.random_choose_id(db=db),
        profile_id=profiles.random_choose_id(db=db),
        city_id=cities.random_choose_id(db=db),
        postcode=Catalogs_tests.random_lower_string(),
        street_type_id=street_types.random_choose_id(db=db),
        street_name=Catalogs_tests.random_lower_string(),
        house_type_id=house_types.random_choose_id(db=db),
        house_name=Catalogs_tests.random_lower_string(),
        flat_type_id=flat_types.random_choose_id(db=db),
        flat_name=Catalogs_tests.random_lower_string(),
        geo_lat=Catalogs_tests.random_lower_string(),
        geo_lon=Catalogs_tests.random_lower_string(),
        # # не нашёл места для этих параметров.
        # profile_id=profiles.random_choose_id(db=db),
        # building_name=Catalogs_tests.random_lower_string()
    )


@pytest.fixture()
def crud():
    return club_address


# @pytest.fixture()
# def rel_crud():
#     return CRUDRel_profiles_contacts(model=Rel_profiles_contacts)


@pytest.fixture()
def api_name():
    return 'club_address'


# @pytest.fixture()
# def id_name_field_in_rel(name: str = 'contact_id') -> str:
#     return name


def test_create_club_address(test_create_by_api):
    assert test_create_by_api


def test_get_existing_club_address(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_club_address(test_delete_by_api):
    assert test_delete_by_api