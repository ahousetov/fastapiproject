import pytest
from app.crud.public.crud_contacts import CRUDContacts
from app.crud.relations.crud_profiles_contacts import CRUDRel_profiles_contacts
from app.models.public.contacts import Contacts
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.schemas.public.contacts import Contacts_create, Contacts_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Contacts_create(value=Catalogs_tests.random_lower_string(),
                           description=Catalogs_tests.random_lower_string(),
                           is_main=False,
                           type_id=1,
                           profile_id=1)


@pytest.fixture()
def update_schema_in():
    return Contacts_update(value=Catalogs_tests.random_lower_string(),
                           description=Catalogs_tests.random_lower_string(),
                           is_main=False,
                           type_id=2,
                           profile_id=2)


@pytest.fixture()
def crud():
    return CRUDContacts(model=Contacts)


@pytest.fixture()
def rel_crud():
    return CRUDRel_profiles_contacts(model=Rel_profiles_contacts)


@pytest.fixture()
def api_name():
    return 'contacts'


@pytest.fixture()
def id_name_field_in_rel(name: str = 'contact_id') -> str:
    return name


def test_create_contacts(test_get_existing_with_rel_by_api):
    assert test_get_existing_with_rel_by_api


def test_get_existing_contacts(test_get_existing_with_rel_by_api):
    assert test_get_existing_with_rel_by_api


def test_delete_contacts(test_delete_with_rel_by_api):
    assert test_delete_with_rel_by_api