import pytest
from app.crud.dogs.crud_fci_sections import CRUDFCI_section
from app.models.dogs.fci_sections import FCI_sections
from app.schemas.dogs.fci_sections import FCI_section_create, FCI_section_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return FCI_section_create(name=Catalogs_tests.random_lower_string(),
                              name_en=Catalogs_tests.random_lower_string(),
                              number=Catalogs_tests.random_int_number())


@pytest.fixture()
def update_schema_in():
    return FCI_section_update(name=Catalogs_tests.random_lower_string(),
                              name_en=Catalogs_tests.random_lower_string(),
                              number=Catalogs_tests.random_int_number())


@pytest.fixture()
def crud():
    return CRUDFCI_section(model=FCI_sections)


@pytest.fixture()
def api_name():
    return 'fci_sections'


def test_create_fci_sections(test_create_by_api):
    assert test_create_by_api


def test_get_existing_fci_sections(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_fci_sections(test_delete_by_api):
    assert test_delete_by_api