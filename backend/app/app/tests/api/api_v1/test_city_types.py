import pytest
from app.crud.catalogs.crud_city_types import CRUDCity_types
from app.models.catalogs.city_types import City_types
from app.schemas.catalogs.city_types import City_types_create, City_types_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return City_types_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return City_types_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDCity_types(model=City_types)


@pytest.fixture()
def api_name():
    return 'city_types'


def test_create_city_type(test_create_by_api):
    assert test_create_by_api


def test_get_existing_city_type(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_cities(test_delete_by_api):
    assert test_delete_by_api