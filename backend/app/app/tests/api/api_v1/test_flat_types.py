import pytest
from app.crud.catalogs.crud_flat_types import CRUDFlat_types
from app.models.catalogs.flat_types import Flat_types
from app.schemas.catalogs.flat_types import Flat_type_create, Flat_type_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Flat_type_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Flat_type_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFlat_types(model=Flat_types)


@pytest.fixture()
def api_name():
    return 'flat_types'


def test_create_flat_types(test_create_by_api):
    assert test_create_by_api


def test_get_existing_flat_types(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_flat_types(test_delete_by_api):
    assert test_delete_by_api