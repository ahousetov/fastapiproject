import pytest
import random
import string
from typing import Dict, List
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from app import crud
from app.main import app as fastapi_application
from app.core.config import settings
from app.exceptions import API_JSON_Exception
from app.schemas.catalogs.city_types import City_types_block, City_types_blocks
from app.tests.utils.utils import random_email, random_lower_string, random_int_number
from app.tests.utils.base_catalog_util import create_catalog_random_entry, \
    ModelType, \
    Create_schema_type, \
    Update_schema_type, \
    CRUDBase
from app import schemas


class Catalogs_tests:
    instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls.instance, cls):
            cls.instance = object.__new__(cls)
        return cls.instance

    def __init__(self,
                 client: TestClient = None,
                 db: Session = None,
                 api_name: str = '',
                 create_schema_in: Create_schema_type = None,
                 update_schema_in: Update_schema_type = None,
                 crud = None):
        self.client = client
        self.db = db
        self.api_name = api_name
        self.create_schema_in = create_schema_in
        self.update_schema_in = update_schema_in
        self.crud = crud

    @classmethod
    def random_lower_string(cls, get_names: bool = False, generated_names: List[str] = []) -> str:
        if get_names:
            result = ",".join(generated_names)
        else:
            result = "".join(random.choices(string.ascii_lowercase, k=32))
            generated_names.append(result)
        return result

    @classmethod
    def random_int_number(cls) -> int:
        result = random.choice(range(0, 999999, 1))
        return result

    def test_just_get(self) -> bool:
        headers = {"accept": "application/json"}
        url = f"{settings.API_V1_STR}/{self.api_name}/"
        r = self.client.get(url=url, headers=headers)
        assert 200 <= r.status_code < 300
        return True

    def test_create_by_api(self) -> bool:
        data = {k: v for k, v in self.create_schema_in if v is not None}
        headers = {
            "accept": "application/json",
            "Content-Type": "application/json"
        }
        url = f"{settings.API_V1_STR}/{self.api_name}/"
        if self.api_name == "club_address":
            print("Here!")
        try:
            r = self.client.post(url=url, headers=headers, json=data)
            assert 200 <= r.status_code < 300
            new_catalog_item = r.json()
            for k, v in data.items():
                assert data[k] == new_catalog_item[k]
        except API_JSON_Exception as api_exp:
            assert api_exp.status_code == 400
            print(api_exp.detail)
        except Exception as e:
            exp = e
            print(exp)
        return True

    def test_get_existing_by_api(self) -> bool:
        if self.api_name == "federations":
            # Т.к. пока методов добавления федераций нет, просто получаем
            # федерацию по ID и проверяем начиличие данных.
            test = self.crud.get(db=self.db, id=7)
            # headers = {"accept": "application/json"}
            # url = f"{settings.API_V1_STR}/{self.api_name}/{test.id}"
            # r = self.client.get(url=url, headers=headers)
            # assert 200 <= r.status_code < 300
            # catalog_item_api = r.json()
            # for k, v in catalog_item_api.items():
            #     if hasattr(test, k):
            #         test_attr = getattr(test, k)
            #         assert str(test_attr) == str(catalog_item_api[k])
        elif self.api_name == "club_address":
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in,
                                                ignore_check=True)

        elif self.api_name == "cities":
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in)
            test_id = test.id
            test = self.crud.get(db=self.db, id=test.id)
            headers = {"accept": "application/json"}
            url = f"{settings.API_V1_STR}/{self.api_name}/{test_id}"
            r = self.client.get(url=url, headers=headers)
            assert 200 <= r.status_code < 300
            catalog_item_api = r.json()
            for k, v in catalog_item_api['blocks'][0].items():
                test_attr = getattr(test.blocks[0], k)
                assert str(test_attr) == str(catalog_item_api['blocks'][0][k])


        elif self.api_name == "city_types":
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in)
            test = self.crud.get(db=self.db, id=test.id)
            test_id = test.id
            test = City_types_blocks(
                **{"title": crud.city_types.title,
                   "item_title": getattr(test, "name", ""),
                   "block_icon": 'info_outline',
                   "blocks": [City_types_block(**crud.city_types._orm_obj_dict(test))]})
            headers = {"accept": "application/json"}
            url = f"{settings.API_V1_STR}/{self.api_name}/{test_id}"
            r = self.client.get(url=url, headers=headers)
            assert 200 <= r.status_code < 300
            catalog_item_api = r.json()

            for k, v in test.dict().items():

                assert v == catalog_item_api[k]

        elif self.api_name == 'federal_districts':
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in)
            test_id = test.id
            test = self.crud.get(db=self.db, id=test.id)
            # Т.к. пока методов добавления федераций нет, просто получаем
            # федерацию по ID и проверяем начиличие данных.
            headers = {"accept": "application/json"}
            url = f"{settings.API_V1_STR}/{self.api_name}/{test_id}"
            r = self.client.get(url=url, headers=headers)
            assert 200 <= r.status_code < 300
            catalog_item_api = r.json()
            test = dict(test)
            catalog_item_api = catalog_item_api['blocks'][0]
            for k, v in test.items():
                test_attr = catalog_item_api[k]

                assert str(test_attr) == str(v)
        else:
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in)
            test_id = test.id
            test = self.crud.get(db=self.db, id=test.id)
            # Т.к. пока методов добавления федераций нет, просто получаем
            # федерацию по ID и проверяем начиличие данных.
            headers = {"accept": "application/json"}
            url = f"{settings.API_V1_STR}/{self.api_name}/{test_id}"
            r = self.client.get(url=url, headers=headers)
            assert 200 <= r.status_code < 300
            catalog_item_api = r.json()
            test = dict(test)
            if 'blocks' in test:
                for i, block in enumerate(test['blocks']):
                    test['blocks'][i] = dict(block)
            for k, v in catalog_item_api.items():
                test_attr = test[k]

                assert str(test_attr) == str(v)
        return True

    def test_set_is_deleted_by_api(self) -> bool:
        if self.api_name == "club_address":
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in,
                                                ignore_check=True)
            test = schemas.Club_address_delete(**crud.club_address._orm_obj_dict(test))
        else:
            test = self.crud.create_not_deleted(db=self.db,
                                                obj_in=self.create_schema_in)
        headers = {"accept": "application/json"}
        url = f"{settings.API_V1_STR}/{self.api_name}/{test.id}"
        r = self.client.delete(url=url, headers=headers)
        assert 200 <= r.status_code < 300
        catalog_item_api = r.json()
        for k, v in catalog_item_api.items():
            if hasattr(test, k) and hasattr(catalog_item_api, k):
                assert v if k == 'is_deleted' else str(getattr(test, k)) == str(catalog_item_api[k])
        return True

    def remove_api_tests_spawns(self) -> None:
        generated_names = self.random_lower_string(get_names=True)
        return self.crud.remove_by_names(db=self.db,
                                         generated_names=generated_names.split(","))


class Catalogs_tests_with_relation(Catalogs_tests):
    instance_rel = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls.instance_rel, cls):
            cls.instance_rel = object.__new__(cls)
        return cls.instance_rel

    def __init__(self,
                 client: TestClient = None,
                 db: Session = None,
                 api_name: str = '',
                 create_schema_in: Create_schema_type = None,
                 update_schema_in: Update_schema_type = None,
                 crud = None,
                 rel_create_schema_in: Create_schema_type = None,
                 rel_update_schema_in: Update_schema_type = None,
                 rel_crud = None,
                 rel_fields = None,
                 id_name_field_in_rel = None):
        self.client = client
        self.db = db
        self.api_name = api_name
        self.create_schema_in = create_schema_in
        self.update_schema_in = update_schema_in
        self.crud = crud
        self.rel_create_schema_in = rel_create_schema_in
        self.rel_update_schema_in = rel_update_schema_in
        self.rel_crud = rel_crud
        self.rel_fields = rel_fields
        self.id_name_field_in_rel = id_name_field_in_rel

    def test_get_existing_by_api(self) -> bool:
        # test = self.crud.create_not_deleted(db=self.db,
        #                                     obj_in=self.create_schema_in)
        #test = self.crud.get_with_groups_sections(db=self.db, id=test.id)
        if self.api_name == "breeds":
            print("Hey!")

        item_data, additional = self.crud.split_obj_in(obj_in=self.create_schema_in)
        if not additional:
            raise Exception(f"{self.id_name_field_in_rel} is missing.")
        test = self.crud.create_not_deleted(db=self.db, obj_in_encoded=item_data)
        headers = {"accept": "application/json"}
        url = f"{settings.API_V1_STR}/{self.api_name}/{test.id}"

        if self.api_name == "contacts":
            test_id = test.id
            additional[self.id_name_field_in_rel] = test.id
            rel = self.rel_crud.create_not_deleted(db=self.db, obj_in_encoded=additional)
            test = self.crud.get(db=self.db, id=test.id)
            if not isinstance(test, dict):
                test = test.dict()
        else:

            additional[self.id_name_field_in_rel] = test.id
            rel = self.rel_crud.create_not_deleted(db=self.db, obj_in_encoded=additional)
            test = self.crud.get(db=self.db, id=test.id)

        r = self.client.get(url=url, headers=headers)
        assert 200 <= r.status_code < 300
        catalog_item_api = r.json()
        for k, v in catalog_item_api.items():
            if hasattr(test, k):
                assert str(getattr(test, k)) == str(catalog_item_api[k])
        return True

    def test_set_is_deleted_by_api(self) -> bool:

        if self.api_name == 'contacts':
            item_data, additional = self.crud.split_obj_in(obj_in=self.create_schema_in)
            if not additional:
                raise Exception(f"{self.id_name_field_in_rel} is missing.")
            test = self.crud.create_not_deleted(db=self.db, obj_in_encoded=item_data)
            test_id = test.id
            additional[self.id_name_field_in_rel] = test.id
            rel = self.rel_crud.create_not_deleted(db=self.db, obj_in_encoded=additional)
            test = self.crud.get(db=self.db, id=test.id)
            if not isinstance(test, dict):
                from pydantic import BaseModel
                from app.db.base_class import Base
                if isinstance(test, BaseModel):
                    test = test.dict()
                elif isinstance(test, Base):
                    test = self.crud._orm_obj_dict(test)

            headers = {"accept": "application/json"}
            url = f"{settings.API_V1_STR}/{self.api_name}/{test_id}"
            r = self.client.delete(url=url, headers=headers)
            assert 200 <= r.status_code < 300
            catalog_item_api = r.json()
            for k, v in catalog_item_api.items():
                if hasattr(test, k) and hasattr(catalog_item_api, k):
                    assert v if k == 'is_deleted' else str(getattr(test, k)) == str(catalog_item_api[k])
            return True
        else:
            item_data, additional = self.crud.split_obj_in(obj_in=self.create_schema_in)
            if not additional:
                raise Exception(f"{self.id_name_field_in_rel} is missing.")
            test = self.crud.create_not_deleted(db=self.db, obj_in_encoded=item_data)
            additional[self.id_name_field_in_rel] = test.id
            rel = self.rel_crud.create_not_deleted(db=self.db, obj_in_encoded=additional)
            test = self.crud.get(db=self.db, id=test.id)
            headers = {"accept": "application/json"}
            url = f"{settings.API_V1_STR}/{self.api_name}/{test.id}"
            r = self.client.delete(url=url, headers=headers)
            assert 200 <= r.status_code < 300
            catalog_item_api = r.json()
            for k, v in catalog_item_api.items():
                if hasattr(test, k) and hasattr(catalog_item_api, k):
                    assert v if k == 'is_deleted' else str(getattr(test, k)) == str(catalog_item_api[k])
            return True

    def remove_api_tests_spawns(self) -> None:
        generated_names = self.random_lower_string(get_names=True)
        return self.crud.remove_by_names(db=self.db,
                                         rel_crud=self.rel_crud,
                                         generated_names=generated_names.split(","))