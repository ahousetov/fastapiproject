import pytest
from app.crud.catalogs.crud_federations import CRUDFederations
from app.models.catalogs.federations import Federations
from app.schemas.catalogs.federations import Federations_create, Federations_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Federations_create(name=Catalogs_tests.random_lower_string(),
                              short_name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Federations_update(name=Catalogs_tests.random_lower_string(),
                              short_name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFederations(model=Federations)


@pytest.fixture()
def api_name():
    return 'federations'


# def test_create_federations(test_create_by_api):
#     assert test_create_by_api


def test_get(test_just_get):
    assert test_just_get


def test_get_existing_federations(test_get_existing_by_api):
    assert test_get_existing_by_api


# def test_delete_federations(test_delete_by_api):
#     assert test_delete_by_api