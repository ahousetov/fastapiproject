import pytest
from typing import Dict, List

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud
from app.main import app as fastapi_application
from app.core.config import settings
from app.schemas.catalogs.regions import Region_create
# from app.schemas.catalogs.regions import Region_create
#from app.tests.utils.utils import random_email, random_lower_string, random_int_number
#from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests
from app.tests.utils.base_catalog_util import create_catalog_random_entry, \
    ModelType, \
    Create_schema_type, \
    Update_schema_type, \
    CRUDBase

from .templates.catalogs_api_tests import Catalogs_tests, Catalogs_tests_with_relation


@pytest.fixture()
def client():
    """
    Создаём экземпляр клиента для тестирования API
    """
    return TestClient(app=fastapi_application)


@pytest.fixture()
def api_name():
    """
    Имя API. Каждое API имеет свой адрес доступа:
    https://localhost:8000/api/regions
    https://localhost:8000/api/federal_district
    ...
    """
    return ''


@pytest.fixture()
def create_schema_in():
    """
    Схема для создания нового элемента. Как правило этот fixture будет возвращать
    Region_create или Federal_District_Create и т.д.

    Данный fixture переопределяется в тестах для каждого справочника.

    Пример кода:

    return Region_create(name=random_lower_string())
    """
    return None


@pytest.fixture()
def update_schema_in():
    """
    Схема для создания нового элемента. Как правило этот fixture будет возвращать
    Region_update или Federal_District_Update и т.д.

    Данный fixture переопределяется в тестах для каждого справочника.

    Пример кода:

    return Region_update(name=random_lower_string())
    """
    return None


@pytest.fixture()
def crud() -> CRUDBase:
    """
    Данный fixture для передачи в тест crud-методов определённого справочника.
    Возвращается объект, созданный конструктором, котором передана модель
    в качестве параметра.

    Пример кода:

    return CRUDRegions(model=Regions)
    """
    return None


@pytest.fixture()
def rel_crud()->CRUDBase:
    """
    Этот fixture по аналогии с 'crud' возвращает crud-объект для таблицы relations.
    Ничем не отличается от обычной, но самостоятельно не обрабатывается - только в связке
    с чем-то.

    Пример кода:

    return CRUDRegions(model=Regions)
    """
    return None


@pytest.fixture()
def rel_fields()->List:
    """
    Особый fixture для передачи
    """
    return None


@pytest.fixture()
def test_create_by_api(client, db, api_name, create_schema_in) -> bool:
    if api_name == 'club_address':
        print("Here!")
    catalogs_tests = Catalogs_tests(client=client,
                                    db=db,
                                    crud=crud,
                                    api_name=api_name,
                                    create_schema_in=create_schema_in)
    assert catalogs_tests.test_create_by_api()
    return True

@pytest.fixture()
def test_just_get(client,
                  db,
                  crud,
                  api_name,
                  create_schema_in) -> bool:
    catalogs_tests = Catalogs_tests(client=client,
                                    db=db,
                                    crud=crud,
                                    api_name=api_name,
                                    create_schema_in=create_schema_in)
    assert catalogs_tests.test_just_get()
    return True


@pytest.fixture()
def test_get_existing_by_api(client,
                             db,
                             crud,
                             api_name,
                             create_schema_in) -> bool:
    if api_name == "club_address":
        print("Here!")
    catalogs_tests = Catalogs_tests(client=client,
                                    db=db,
                                    crud=crud,
                                    api_name=api_name,
                                    create_schema_in=create_schema_in)
    assert catalogs_tests.test_get_existing_by_api()
    return True


@pytest.fixture()
def test_get_existing_with_rel_by_api(client,
                                      db,
                                      crud,
                                      rel_crud,
                                      api_name,
                                      create_schema_in,
                                      id_name_field_in_rel) -> bool:
    catalogs_tests_with_rel = Catalogs_tests_with_relation(client=client,
                                                           db=db,
                                                           crud=crud,
                                                           rel_crud=rel_crud,
                                                           api_name=api_name,
                                                           create_schema_in=create_schema_in,
                                                           id_name_field_in_rel=id_name_field_in_rel)
    assert catalogs_tests_with_rel.test_get_existing_by_api()
    return True


@pytest.fixture()
def test_delete_by_api(client,
                       db,
                       crud,
                       api_name,
                       create_schema_in) -> bool:
    catalogs_tests = Catalogs_tests(client=client,
                                    db=db,
                                    crud=crud,
                                    api_name=api_name,
                                    create_schema_in=create_schema_in,
                                    update_schema_in=update_schema_in)
    if api_name == 'club_address':
        print("Here!")
    assert catalogs_tests.test_set_is_deleted_by_api()
    assert catalogs_tests.remove_api_tests_spawns()
    return True

@pytest.fixture()
def test_delete_with_rel_by_api(client,
                                db,
                                crud,
                                rel_crud,
                                api_name,
                                create_schema_in,
                                id_name_field_in_rel) -> bool:
    # if api_name == "contacts":
    #     print("Here!")
    catalogs_tests = Catalogs_tests_with_relation(client=client,
                                                  db=db,
                                                  crud=crud,
                                                  rel_crud=rel_crud,
                                                  api_name=api_name,
                                                  create_schema_in=create_schema_in,
                                                  update_schema_in=update_schema_in,
                                                  id_name_field_in_rel=id_name_field_in_rel)
    assert catalogs_tests.test_set_is_deleted_by_api()
    assert catalogs_tests.remove_api_tests_spawns()
    return True