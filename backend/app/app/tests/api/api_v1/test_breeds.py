import pytest
from app.crud.dogs.crud_breeds import CRUDBreeds
from app.crud.relations.crud_rel_breeds_sections_groups import CRUDRel_breeds_sections_groups
from app.models.dogs.breeds import Breeds
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups
from app.schemas.dogs.breeds import Breeds_create, Breeds_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Breeds_create(name_rus=Catalogs_tests.random_lower_string(),
                         name_en=Catalogs_tests.random_lower_string(),
                         name_original=Catalogs_tests.random_lower_string(),
                         fci_number=1,
                         fci_status_id=2,
                         group_id=3,
                         section_id=4)


@pytest.fixture()
def update_schema_in():
    return Breeds_update(name_rus=Catalogs_tests.random_lower_string(),
                         name_en=Catalogs_tests.random_lower_string(),
                         name_original=Catalogs_tests.random_lower_string(),
                         fci_number=5,
                         fci_status_id=6,
                         group_id=7,
                         section_id=8)


@pytest.fixture()
def crud():
    return CRUDBreeds(model=Breeds)


@pytest.fixture()
def rel_crud():
    return CRUDRel_breeds_sections_groups(model=Rel_breeds_sections_groups)


@pytest.fixture()
def api_name():
    return 'breeds'


@pytest.fixture()
def id_name_field_in_rel(name: str = 'breed_id') -> str:
    return name


def test_create_breeds(test_create_by_api):
    assert test_create_by_api


def test_get_existing_breeds(test_get_existing_with_rel_by_api):
    assert test_get_existing_with_rel_by_api


def test_delete_breeds(test_delete_with_rel_by_api):
    assert test_delete_with_rel_by_api