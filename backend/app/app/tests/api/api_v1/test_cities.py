import pytest
from app.crud.catalogs.crud_cities import CRUDCities
from app.models.catalogs.cities import Cities
from app.schemas.catalogs.cities import Cities_create, Cities_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Cities_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Cities_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDCities(model=Cities)


@pytest.fixture()
def api_name():
    return 'cities'


def test_create_cities(test_create_by_api):
    assert test_create_by_api


def test_get_existing_cities(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_cities(test_delete_by_api):
    assert test_delete_by_api