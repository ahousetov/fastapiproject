import pytest
from app.crud.catalogs.crud_regions import CRUDRegions
from app.models.catalogs.regions import Regions
from app.schemas.catalogs.regions import Region_create, Region_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Region_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Region_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDRegions(model=Regions)


@pytest.fixture()
def api_name():
    return 'regions'


def test_create_region(test_create_by_api):
    assert test_create_by_api


def test_get_existing_region(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_regions(test_delete_by_api):
    assert test_delete_by_api
