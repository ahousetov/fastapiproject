import pytest
from app.crud.catalogs.crud_federal_district import CRUDFederalDistrict
from app.models.catalogs.federal_district import Federal_Districts
from app.schemas.catalogs.federal_district import \
    Federal_district_create, \
    Federal_district_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Federal_district_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Federal_district_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFederalDistrict(model=Federal_Districts)


@pytest.fixture()
def api_name():
    return 'federal_districts'


def test_create_federal_districts(test_create_by_api):
    assert test_create_by_api


def test_get_existing_federal_districts(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_federal_districts(test_delete_by_api):
    assert test_delete_by_api