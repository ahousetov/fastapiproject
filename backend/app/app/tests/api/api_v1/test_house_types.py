import pytest
from app.crud.catalogs.crud_house_types import CRUDHouse_types
from app.models.catalogs.house_types import House_types
from app.schemas.catalogs.house_types import House_type_create, House_type_update
from app.tests.api.api_v1.templates.catalogs_api_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return House_type_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return House_type_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDHouse_types(model=House_types)


@pytest.fixture()
def api_name():
    return 'house_types'


def test_create_house_types(test_create_by_api):
    assert test_create_by_api


def test_get_existing_house_types(test_get_existing_by_api):
    assert test_get_existing_by_api


def test_delete_house_types(test_delete_by_api):
    assert test_delete_by_api