from typing import Dict, Generator, Optional, TypeVar, Generic, Any

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from pydantic import BaseModel
from app.core.config import settings
from app.db.session import SessionLocal
from app.main import app
from app.db.base_class import Base
from app.tests.utils.user import authentication_token_from_email
from app.tests.utils.utils import get_superuser_token_headers
from app.tests.utils.base_catalog_util import create_catalog_random_entry


@pytest.fixture(scope="session")
def db() -> Generator:
    yield SessionLocal()


@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="module")
def superuser_token_headers(client: TestClient) -> Dict[str, str]:
    return get_superuser_token_headers(client)


@pytest.fixture(scope="module")
def normal_user_token_headers(client: TestClient, db: Session) -> Dict[str, str]:
    return authentication_token_from_email(
        client=client, email=settings.EMAIL_TEST_USER, db=db
    )


@pytest.fixture(params=[db, client])
def test_param_fixture(r):
    print(f"r.db : {r.db}")
    print(f"r.client : {r.client}")





# @pytest.fixture()
# def create_catalog_random_entry(db: Session,
#                                 create_schema_in: CreateSchemaType,
#                                 crud: CRUDBase):
#     name = random_lower_string()
#     catalog_obj_in = create_schema_in(name=name)
#     return crud.create(db=db, obj_in=catalog_obj_in)
#
#
# @pytest.fixture()
# def test_create_catalog_item(db: Session,
#                              obj_in: CreateSchemaType,
#                              crud: CRUDBase) -> None:
#     test = create_catalog_random_entry(db=db,
#                                        obj_in=obj_in,
#                                        crud=crud)
#     catalog_item_from_api = crud.get(db=db, id=test.id)
#     assert test.name == catalog_item_from_api.name
#     assert test.is_deleted == catalog_item_from_api.is_deleted
#
#
# @pytest.fixture()
# def test_get_catalog_item(db: Session,
#                           obj_in: CreateSchemaType,
#                           crud: CRUDBase) -> None:
#     test = create_catalog_random_entry(db=db,
#                                        obj_in=obj_in,
#                                        crud=crud)
#     catalog_item_from_api = crud.get(db=db, id=test.id)
#     assert test.name == catalog_item_from_api.name
#     assert test.is_deleted == catalog_item_from_api.is_deleted
#
#
# @pytest.fixture()
# def test_update_catalog_item(db: Session,
#                              obj_in: CreateSchemaType,
#                              upd_schema: Update_schema_type,
#                              crud: CRUDBase) -> None:
#     test = create_catalog_random_entry(db=db,
#                                        obj_in=obj_in,
#                                        crud=crud)
#     catalog_item_db = crud.get(db=db, id=test.id)
#     new_name = upd_schema(name="NewCatalogItem")
#     updated_region = crud.update(db=db,
#                                  db_obj=catalog_item_db,
#                                  obj_in=new_name)
#     assert catalog_item_db.id == updated_region.id
#     assert updated_region.name == "NewCatalogItem"
#
#
# @pytest.fixture()
# def test_delete_catalog_item(db: Session,
#                              obj_in: CreateSchemaType,
#                              upd_schema: Update_schema_type,
#                              crud: CRUDBase) -> None:
#     test = create_catalog_random_entry(db=db,
#                                        obj_in=obj_in,
#                                        crud=crud)
#     catalog_item_db = crud.get(db=db, id=test.id)
#     deleted_region = crud.is_deleted(db=db,
#                                      db_obj=catalog_item_db)
#     assert catalog_item_db.id == deleted_region.id
#     assert deleted_region.is_deleted == True


