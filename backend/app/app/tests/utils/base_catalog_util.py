from typing import Optional, TypeVar, Generic

from sqlalchemy.orm import Session
from pydantic import BaseModel
from app.tests.utils.utils import random_lower_string
from app.db.base_class import Base
from app.crud.base import CRUDBase

ModelType = TypeVar("ModelType", bound=Base)
Create_schema_type = TypeVar("CreateSchemaType", bound=BaseModel)
Update_schema_type = TypeVar("Update_schema_type", bound=BaseModel)

def create_catalog_random_entry(db: Session,
                                obj_in: Create_schema_type,
                                crud: CRUDBase):
    name = random_lower_string()
    catalog_obj_in = obj_in(name=name)
    return crud.create(db=db, obj_in=catalog_obj_in)