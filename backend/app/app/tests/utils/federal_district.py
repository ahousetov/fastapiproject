from typing import Optional

from sqlalchemy.orm import Session

from app import crud, models
from catalogs.federal_district import FederalDistrictCreate
from app.tests.utils.utils import random_lower_string


def create_random_region(db: Session) -> models.Federal_Districts:
    name = random_lower_string()
    federal_district_in = FederalDistrictCreate(name=name)
    return crud.federal_district.create(db=db, obj_in=federal_district_in)