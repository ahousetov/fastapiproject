from typing import Optional

from sqlalchemy.orm import Session

from app import crud, models
from app.schemas.catalogs.regions import Region_create
from app.tests.utils.utils import random_lower_string


def create_random_region(db: Session) -> models.Regions:
    name = random_lower_string()
    region_in = Region_create(name=name)
    return crud.regions.create(db=db, obj_in=region_in)