import pytest
from typing import List, Dict, Generator, Optional, TypeVar, Generic
from app.db.base_class import Base
from pydantic import BaseModel
from sqlalchemy.orm import Session
#from app.schemas.catalogs.regions import Region_create, Region_update

### Набор общих функций, для теста СRUD в справочниках
#from app.crud.crud_regions import CRUDRegions
#from app.models.regions import Regions
from app.tests.utils.utils import random_lower_string
from app.tests.utils.base_catalog_util import create_catalog_random_entry, \
    ModelType, \
    Create_schema_type, \
    Update_schema_type, \
    CRUDBase

from .templates.catalogs_crud_tests import \
    Catalogs_tests, \
    Catalogs_tests_with_relation

# Для передачи параметров в test_create_catalog_item определяем fixtures
# create_schema_in и crud.
# переопределяем их в каждом тесте, для своего справочник


@pytest.fixture()
def create_schema_in():
    """
    Схема для создания нового элемента. Как правило этот fixture будет возвращать
    Region_create или Federal_District_Create и т.д.

    Данный fixture переопределяется в тестах для каждого справочника.

    Пример кода:

    return Region_create(name=random_lower_string())
    """
    return None


@pytest.fixture()
def update_schema_in():
    """
    Схема для создания нового элемента. Как правило этот fixture будет возвращать
    Region_update или Federal_District_Update и т.д.

    Данный fixture переопределяется в тестах для каждого справочника.

    Пример кода:

    return Region_update(name=random_lower_string())
    """
    return None


@pytest.fixture()
def rel_create_schema_in():
    """
    Схема для создания нового элемента relation.
    relations - не самостоятельные элементы, поэтому этот fixture будет идти
    в связке с чем-то.
    """
    return None


@pytest.fixture()
def rel_update_schema_in():
    """
    Схема для изменения элемента relation.
    relations - не самостоятельные элементы, поэтому этот fixture будет идти
    в связке с чем-то.
    """
    return None


@pytest.fixture()
def crud()->CRUDBase:
    """
    Данный fixture для передачи в тест crud-методов определённого справочника.
    Возвращается объект, созданный конструктором, котором передана модель
    в качестве параметра.

    Пример кода:

    return CRUDRegions(model=Regions)
    """
    return None


@pytest.fixture()
def rel_crud()->CRUDBase:
    """
    Этот fixture по аналогии с 'crud' возвращает crud-объект для таблицы relations.
    Ничем не отличается от обычной, но самостоятельно не обрабатывается - только в связке
    с чем-то.

    Пример кода:

    return CRUDRegions(model=Regions)
    """
    return None


@pytest.fixture()
def rel_fields()->List:
    """
    Особый fixture для передачи
    """
    return None


@pytest.fixture()
def name_fields()->List:
    """
    Список полей-имён для чего бы то ни было
    """
    return ["name"]


@pytest.fixture()
def module_name(name: str = '') -> str:
    """
    Надо иметь возможность передать имя модуля, чей CRUD мы
    тестируем.
    """
    return name


@pytest.fixture()
def id_name_field_in_rel(name: str = '') -> str:
    """
    Надо иметь возможность передать имя модуля, чей CRUD мы
    тестируем.
    """
    return name

@pytest.fixture()
def test_create_catalog_item(db, create_schema_in, crud, module_name, name_fields) -> bool:
    module_name_str = module_name
    catalogs_tests = Catalogs_tests(db=db,
                                    create_schema_in=create_schema_in,
                                    update_schema_in=update_schema_in,
                                    crud=crud,
                                    name_fields=name_fields)
    assert catalogs_tests.create_item()
    return True


@pytest.fixture()
def test_get_catalog_item(db, create_schema_in, update_schema_in, crud, module_name, name_fields) -> bool:
    module_name_str = module_name
    catalogs_tests = Catalogs_tests(db=db,
                                    create_schema_in=create_schema_in,
                                    update_schema_in=update_schema_in,
                                    crud=crud,
                                    name_fields=name_fields)
    assert catalogs_tests.get_item()
    return True


@pytest.fixture()
def test_update_catalog_item(db, create_schema_in, update_schema_in, crud, module_name, name_fields) -> bool:
    module_name_str = module_name
    catalogs_tests = Catalogs_tests(db=db,
                                    create_schema_in=create_schema_in,
                                    update_schema_in=update_schema_in,
                                    crud=crud,
                                    name_fields=name_fields)
    assert catalogs_tests.upd_item()
    return True


@pytest.fixture()
def test_delete_catalog_item(db,
                             create_schema_in,
                             update_schema_in,
                             crud,
                             module_name,
                             name_fields) -> bool:
    module_name_str = module_name
    catalogs_tests = Catalogs_tests(db=db,
                                    create_schema_in=create_schema_in,
                                    update_schema_in=update_schema_in,
                                    crud=crud,
                                    name_fields=name_fields)
    assert catalogs_tests.del_item()
    assert catalogs_tests.remove_spawns()
    del catalogs_tests
    return True


############### 'with_rel' fixtures domain ###############

@pytest.fixture()
def test_get_catalog_item_with_rel(db,
                                   create_schema_in,
                                   update_schema_in,
                                   rel_create_schema_in,
                                   rel_update_schema_in,
                                   crud,
                                   rel_crud,
                                   rel_fields,
                                   module_name,
                                   name_fields,
                                   id_name_field_in_rel) -> bool:
    module_name_str = module_name
    # if module_name_str == "Contacts":
    #     print("Here!")
    # if module_name_str == "Breeds":
    #     print("Here!")
    catalogs_tests = Catalogs_tests_with_relation(db=db,
                                                  create_schema_in=create_schema_in,
                                                  update_schema_in=update_schema_in,
                                                  rel_create_schema_in=rel_create_schema_in,
                                                  rel_update_schema_in=rel_update_schema_in,
                                                  crud=crud,
                                                  rel_crud=rel_crud,
                                                  rel_fields=rel_fields,
                                                  name_fields=name_fields,
                                                  id_name_field_in_rel=id_name_field_in_rel)
    #assert catalogs_tests.test_get_catalog_item()
    assert catalogs_tests.get_item_rel()
    return True


@pytest.fixture()
def test_create_catalog_item_with_rel(db,
                                      create_schema_in,
                                      update_schema_in,
                                      rel_create_schema_in,
                                      rel_update_schema_in,
                                      crud,
                                      rel_crud,
                                      rel_fields,
                                      module_name,
                                      name_fields,
                                      id_name_field_in_rel) -> bool:
    module_name_str = module_name
    # if module_name_str == "Contacts":
    #     print("Here!")
    # if module_name_str == "Breeds":
    #     print("Here!")
    catalogs_tests = Catalogs_tests_with_relation(db=db,
                                                  create_schema_in=create_schema_in,
                                                  update_schema_in=update_schema_in,
                                                  rel_create_schema_in=rel_create_schema_in,
                                                  rel_update_schema_in=rel_update_schema_in,
                                                  crud=crud,
                                                  rel_crud=rel_crud,
                                                  rel_fields=rel_fields,
                                                  name_fields=name_fields,
                                                  id_name_field_in_rel=id_name_field_in_rel)
    assert catalogs_tests.create_item_rel()
    return True

@pytest.fixture()
def test_update_catalog_item_with_rel(db,
                                      create_schema_in,
                                      update_schema_in,
                                      rel_create_schema_in,
                                      rel_update_schema_in,
                                      crud,
                                      rel_crud,
                                      rel_fields,
                                      module_name,
                                      name_fields,
                                      id_name_field_in_rel) -> bool:
    module_name_str = module_name
    # if module_name_str == "Contacts":
    #     print("Here!")
    # if module_name_str == "Breeds":
    #     print("Here!")
    catalogs_tests = Catalogs_tests_with_relation(db=db,
                                                  create_schema_in=create_schema_in,
                                                  update_schema_in=update_schema_in,
                                                  rel_create_schema_in=rel_create_schema_in,
                                                  rel_update_schema_in=rel_update_schema_in,
                                                  crud=crud,
                                                  rel_crud=rel_crud,
                                                  rel_fields=rel_fields,
                                                  name_fields=name_fields,
                                                  id_name_field_in_rel=id_name_field_in_rel)
    assert catalogs_tests.upd_item_rel()
    return True

@pytest.fixture()
def test_delete_catalog_item_with_rel(db,
                                      create_schema_in,
                                      update_schema_in,
                                      rel_create_schema_in,
                                      rel_update_schema_in,
                                      crud,
                                      rel_crud,
                                      rel_fields,
                                      module_name,
                                      name_fields,
                                      id_name_field_in_rel) -> bool:
    module_name_str = module_name
    # if module_name_str == "Contacts":
    #     print("Here!")
    catalogs_tests = Catalogs_tests_with_relation(db=db,
                                                  create_schema_in=create_schema_in,
                                                  update_schema_in=update_schema_in,
                                                  rel_create_schema_in=rel_create_schema_in,
                                                  rel_update_schema_in=rel_update_schema_in,
                                                  crud=crud,
                                                  rel_crud=rel_crud,
                                                  rel_fields=rel_fields,
                                                  name_fields=name_fields,
                                                  id_name_field_in_rel=id_name_field_in_rel)
    assert catalogs_tests.del_item_rel()
    assert catalogs_tests.remove_spawns()
    del catalogs_tests
    return True
