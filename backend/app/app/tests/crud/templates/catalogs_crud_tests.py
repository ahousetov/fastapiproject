import random
import string
from typing import List, Union
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from app.tests.utils.base_catalog_util import \
    Create_schema_type, \
    Update_schema_type
from pydantic import BaseModel
from app.db.base_class import Base


class Catalogs_tests:
    instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls.instance, cls):
            cls.instance = object.__new__(cls)
        return cls.instance

    def __init__(self,
                 client: TestClient = None,
                 db: Session = None,
                 api_name: str = '',
                 create_schema_in: Create_schema_type = None,
                 update_schema_in: Update_schema_type = None,
                 crud=None,
                 name_fields: List[str] = None):
        self.client = client
        self.db = db
        self.api_name = api_name
        self.create_schema_in = create_schema_in
        self.update_schema_in = update_schema_in
        self.crud = crud
        self.name_fields = tuple(name_fields)

    @classmethod
    def random_lower_string(cls, get_names: bool = False, generated_names: List[str] = []) -> str:
        if get_names:
            result = ",".join(generated_names)
        else:
            result = "".join(random.choices(string.ascii_lowercase, k=32))
            generated_names.append(result)
        return result

    @classmethod
    def random_int_number(cls, minimum=0, maximum=99999, step=1) -> int:
        result = random.choice(range(minimum, maximum, step))
        return result

    def names_сheck(self, test_obj, catalog_obj):
        for name in self.name_fields:
            if hasattr(test_obj, name) and hasattr(catalog_obj, name):
                assert str(getattr(test_obj, name)) == str(getattr(catalog_obj, name))

    def names_check_in_dict(self, test_obj: dict, catalog_obj: dict):
        for name in self.name_fields:
            if name in test_obj and name in catalog_obj:
                assert str(test_obj[name]) == str(catalog_obj[name])

    def obj_into_dict(self,
                      obj: Union[dict, BaseModel, Base]):
        if isinstance(obj, BaseModel):
            obj = obj.dict()
        elif isinstance(obj, Base):
            obj = self.crud._orm_obj_dict(obj)
        if isinstance(obj, dict) and 'blocks' in obj:
            obj = obj['blocks'][0]

        return obj


    def create_item(self) -> bool:
        test = self.crud.create_not_deleted(db=self.db, obj_in=self.create_schema_in)
        catalog_item_from_api = self.crud.get(db=self.db, id=test.id)
        test = self.obj_into_dict(test)
        catalog_item_from_api = self.obj_into_dict(catalog_item_from_api)
        self.names_check_in_dict(test, catalog_item_from_api)

        # self.names_сheck(test, catalog_item_from_api)

        # for name in self.name_fields:
        #     if name in test and name in catalog_item_from_api:
        #         assert str(test[name]) == str(catalog_item_from_api[name])


        # assert test.is_deleted == catalog_item_from_api.is_deleted
        return True

    def get_item(self) -> bool:
        test = self.crud.create(db=self.db, obj_in=self.create_schema_in)
        catalog_item_from_api = self.crud.get(db=self.db, id=test.id)
        test = self.obj_into_dict(test)
        catalog_item_from_api = self.obj_into_dict(catalog_item_from_api)
        self.names_check_in_dict(test, catalog_item_from_api)
        return True

    def upd_item(self) -> bool:
        test = self.crud.create(db=self.db, obj_in=self.create_schema_in)
        # catalog_item_db = self.crud.get(db=self.db, id=test.id)
        catalog_item_db = self.db.query(self.crud.model).filter(self.crud.model.id == test.id).first()

        new_name = self.update_schema_in
        updated_catalog_item = self.crud.update(db=self.db,
                                                db_obj=catalog_item_db,
                                                obj_in=new_name)

        test = self.obj_into_dict(test)
        catalog_item_db = self.obj_into_dict(catalog_item_db)
        updated_catalog_item = self.obj_into_dict(updated_catalog_item)

        assert catalog_item_db['id'] == updated_catalog_item['id']
        self.names_check_in_dict(test, updated_catalog_item)
        return True

    def del_item(self) -> bool:
        test = self.crud.create(db=self.db, obj_in=self.create_schema_in)
        catalog_item_db = self.db.query(self.crud.model).filter(self.crud.model.id == test.id).first()
        deleted_catalog_item = self.crud.is_deleted(db=self.db,
                                                    db_obj=catalog_item_db)
        assert catalog_item_db.id == deleted_catalog_item.id
        assert deleted_catalog_item.is_deleted == True
        return True

    def remove_spawns(self) -> None:
        generated_names = self.random_lower_string(get_names=True)
        return self.crud.remove_by_names(db=self.db, generated_names=generated_names.split(","))


class Catalogs_tests_with_relation(Catalogs_tests):
    instance_rel = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls.instance_rel, cls):
            cls.instance_rel = object.__new__(cls)
        return cls.instance_rel

    def __init__(self,
                 client: TestClient = None,
                 db: Session = None,
                 api_name: str = '',
                 create_schema_in: Create_schema_type = None,
                 update_schema_in: Update_schema_type = None,
                 crud=None,
                 rel_create_schema_in: Create_schema_type = None,
                 rel_update_schema_in: Update_schema_type = None,
                 rel_crud=None,
                 rel_fields=None,
                 name_fields: List[str] = None,
                 id_name_field_in_rel: str = None):
        self.client = client
        self.db = db
        self.api_name = api_name
        self.create_schema_in = create_schema_in
        self.update_schema_in = update_schema_in
        self.crud = crud
        self.rel_create_schema_in = rel_create_schema_in
        self.rel_update_schema_in = rel_update_schema_in
        self.rel_crud = rel_crud
        self.rel_fields = rel_fields
        self.name_fields = tuple(name_fields)
        self.id_name_field_in_rel = id_name_field_in_rel

    def _prepare_data_for_rel(self, rel_data=None, field_name="name", value=None):
        if isinstance(rel_data, dict):
            rel_data.update({field_name: value})
            return rel_data

    # def test_create_catalog_item(self) -> bool:
    #     test = self.crud.create_not_deleted(db=self.db, obj_in=self.create_schema_in)
    #     catalog_item_from_api = self.crud.get(db=self.db, id=test.id, mode='for_put_post_scheme')
    #     self.names_сheck(test, catalog_item_from_api)
    #     assert test.is_deleted == catalog_item_from_api.is_deleted
    #     return True

    # def test_get_catalog_item(self) -> bool:
    #     test = self.crud.create_not_deleted(db=self.db, obj_in=self.create_schema_in)
    #     catalog_item_from_api = self.crud.get(db=self.db, id=test.id, mode='for_put_post_scheme')
    #     self.names_сheck(test, catalog_item_from_api)
    #     assert test.is_deleted == catalog_item_from_api.is_deleted
    #     return True

    # def test_update_catalog_item(self) -> bool:
    #     test = self.crud.create_not_deleted(db=self.db, obj_in=self.create_schema_in)
    #     catalog_item_db = self.crud.get(db=self.db, id=test.id, mode='for_put_post_scheme')
    #     new_name = self.update_schema_in
    #     updated_catalog_item = self.crud.update(db=self.db,
    #                                             db_obj=catalog_item_db,
    #                                             obj_in=new_name)
    #     assert catalog_item_db.id == updated_catalog_item.id
    #     self.names_сheck(test, updated_catalog_item)
    #     return True

    # def test_delete_catalog_item(self) -> bool:
    #     test = self.crud.create_not_deleted(db=self.db, obj_in=self.create_schema_in)
    #     catalog_item_db = self.crud.get(db=self.db, id=test.id, mode='for_put_post_scheme')
    #     deleted_catalog_item = self.crud.is_deleted(db=self.db,
    #                                                 db_obj=catalog_item_db)
    #     assert catalog_item_db.id == deleted_catalog_item.id
    #     assert deleted_catalog_item.is_deleted == True
    #     return True

    def _make_item_with_rel_(self):
        create_obj_in = self.create_schema_in
        create_rel_in = self.rel_create_schema_in
        item_data, rel_data = self.crud.split_obj_in(obj_in=create_obj_in, exclude_unset=True)
        test_item = self.crud.create_not_deleted(db=self.db, obj_in_encoded=item_data)
        new_rel_data = self._prepare_data_for_rel(rel_data=rel_data,
                                                  field_name=self.id_name_field_in_rel,
                                                  value=test_item.id)
        test_rel = self.rel_crud.create(db=self.db, obj_in_dict=new_rel_data)
        return test_item, test_rel, item_data, rel_data

    def get_item_rel(self) -> bool:
        test_item, test_rel, item_data, rel_data = self._make_item_with_rel_()
        item_from_crud = self.crud.get(db=self.db, id=test_item.id)
        rel_from_crud = self.rel_crud.get(db=self.db, id=test_rel.id)
        self.names_сheck(test_item, item_from_crud)
        item_from_crud = self.obj_into_dict(item_from_crud)
        for key, value in item_data.items():
            if hasattr(test_item, key):
                assert str(item_data[key]) == str(item_from_crud[key])
        return True

    def create_item_rel(self) -> bool:
        test_item, test_rel, item_data, rel_data = self._make_item_with_rel_()
        item_from_crud = self.crud.get(db=self.db, id=test_item.id)
        rel_from_crud = self.rel_crud.get(db=self.db, id=test_rel.id)
        self.names_сheck(test_item, item_from_crud)
        item_from_crud = self.obj_into_dict(item_from_crud)
        for key, value in item_data.items():
            if hasattr(test_item, key):
                assert str(item_data[key]) == str(item_from_crud[key])
        # Далее проводим проверки созданного relation
        for key, value in rel_data.items():
            if hasattr(test_rel, key):
                assert str(rel_data[key]) == str(getattr(rel_from_crud, key))
        return True

    def upd_item_rel(self) -> bool:
        # В данном случае мы применяем:
        #
        #       update_obj_in = self.update_schema_in
        #
        # поэтому использование self._make_item_with_rel_()
        # может давать неверные данные
        create_obj_in = self.create_schema_in
        update_obj_in = self.update_schema_in
        rel_in = self.rel_update_schema_in
        item_data, rel_data = self.crud.split_obj_in(obj_in=update_obj_in, exclude_unset=True)
        rel_data.update(rel_in.dict())
        test_item = self.crud.create_not_deleted(db=self.db, obj_in_encoded=item_data)
        new_rel_data = self._prepare_data_for_rel(rel_data=rel_data,
                                                  field_name=self.id_name_field_in_rel,
                                                  value=test_item.id)
        test_rel = self.rel_crud.create(db=self.db, obj_in_dict=new_rel_data)
        catalog_item_db = self.crud.update(db=self.db, item_obj=test_item, rel_obj=test_rel, obj_in=update_obj_in)
        for key, value in item_data.items():
            if hasattr(catalog_item_db, key):
                assert str(item_data[key]) == str(getattr(catalog_item_db, key))
        for key, value in rel_data.items():
            if key != 'id':
                if hasattr(catalog_item_db, key):
                    assert str(rel_data[key]) == str(getattr(catalog_item_db, key))
        return True

    def del_item_rel(self) -> bool:
        test_item, test_rel, item_data, rel_data = self._make_item_with_rel_()
        catalog_item_db = self.db.query(self.crud.model).filter(self.crud.model.id == test_item.id).first()
        # catalog_item_db = self.crud.get(db=self.db, id=test_item.id)
        deleted_catalog_item = self.crud.is_deleted(db=self.db,
                                                    db_obj=catalog_item_db)
        assert catalog_item_db.id == deleted_catalog_item.id
        assert deleted_catalog_item.is_deleted == True
        return True

    def remove_spawns(self) -> None:
        generated_names = self.random_lower_string(get_names=True)
        return self.crud.remove_by_names(db=self.db,
                                         rel_crud=self.rel_crud,
                                         generated_names=generated_names.split(","))