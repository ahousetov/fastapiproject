import pytest
from fastapi import HTTPException
from app.crud.public.crud_club_address import club_address
from app.crud.catalogs.crud_cities import cities
from app.crud.catalogs.crud_street_types import street_types
from app.crud.catalogs.crud_flat_types import flat_types
from app.crud.catalogs.crud_house_types import house_types
from app.crud.catalogs.crud_address_types import address_types
from app.crud.public.crud_profiles import profiles
from app.models.public.club_address import Club_addresses
from app.schemas.public.club_address import Club_address_create, Club_address_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in(db):
    return Club_address_create(
        profile_id=profiles.random_choose_id(db=db),
        postcode=Catalogs_tests.random_lower_string(),
        city_id=cities.random_choose_id(db=db),
        street_type_id=street_types.random_choose_id(db=db),
        street_name=Catalogs_tests.random_lower_string(),
        house_type_id=house_types.random_choose_id(db=db),
        house_name=Catalogs_tests.random_lower_string(),
        flat_type_id=flat_types.random_choose_id(db=db),
        flat_name=Catalogs_tests.random_lower_string(),
        geo_lat=Catalogs_tests.random_lower_string(),
        geo_lon=Catalogs_tests.random_lower_string(),
        address_type_id=address_types.random_choose_id(db=db),
        building_name=Catalogs_tests.random_lower_string()
    )


@pytest.fixture()
def update_schema_in(db):
    return Club_address_update(
        profile_id=profiles.random_choose_id(db=db),
        postcode=Catalogs_tests.random_lower_string(),
        city_id=cities.random_choose_id(db=db),
        street_type_id=street_types.random_choose_id(db=db),
        street_name=Catalogs_tests.random_lower_string(),
        house_type_id=house_types.random_choose_id(db=db),
        house_name=Catalogs_tests.random_lower_string(),
        flat_type_id=flat_types.random_choose_id(db=db),
        flat_name=Catalogs_tests.random_lower_string(),
        geo_lat=Catalogs_tests.random_lower_string(),
        geo_lon=Catalogs_tests.random_lower_string(),
        address_type_id=address_types.random_choose_id(db=db),
        building_name=Catalogs_tests.random_lower_string()
    )


@pytest.fixture()
def crud():
    return club_address
    #return CRUDClub_address(model=Club_addresses)


@pytest.fixture()
def module_name(name: str = 'Club_address') -> str:
    return name


class Catalogs_tests_club_address(Catalogs_tests):

    def create_item(self) -> bool:
        try:
            test = self.crud.create_not_deleted(db=self.db, obj_in=self.create_schema_in)
            catalog_item_from_api = self.db.query(self.crud.model).filter(self.crud.model.id == test.id).first()
            # catalog_item_from_api = self.crud.get(db=self.db, id=test.id)
            self.names_сheck(test, catalog_item_from_api)
            assert test.is_deleted == catalog_item_from_api.is_deleted
        except HTTPException as http_exp:
           assert http_exp.status_code == 400
        return True


@pytest.fixture()
def test_create_catalog_item(db, create_schema_in, crud, module_name, name_fields) -> bool:
    module_name_str = module_name
    catalogs_tests = Catalogs_tests_club_address(db=db,
                                                 create_schema_in=create_schema_in,
                                                 update_schema_in=update_schema_in,
                                                 crud=crud,
                                                 name_fields=name_fields)
    assert catalogs_tests.create_item()
    return True

@pytest.fixture()
def repeat_create_catalog_item(db, create_schema_in, crud, module_name, name_fields) -> bool:
    """
    Смысл подобного кульбита прост: контактные данные с указанным профилем УЖЕ созданы,
    значит исключение должно выпасть обязательно.
    """
    module_name_str = module_name
    catalogs_tests = Catalogs_tests_club_address(db=db,
                                                 create_schema_in=create_schema_in,
                                                 update_schema_in=update_schema_in,
                                                 crud=crud,
                                                 name_fields=name_fields)
    assert catalogs_tests.create_item()
    return True


def test_create_cities(test_create_catalog_item,
                       repeat_create_catalog_item):
    assert test_create_catalog_item
    assert repeat_create_catalog_item


def test_get_cities(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_cities(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_cities(test_delete_catalog_item):
    assert test_delete_catalog_item