import pytest
from app.crud.public.crud_contacts import CRUDContacts
from app.crud.relations.crud_profiles_contacts import CRUDRel_profiles_contacts
from app.models.public.contacts import Contacts
from app.models.relations.rel_profiles_contacts import Rel_profiles_contacts
from app.schemas.public.contacts import Contacts_create, Contacts_update
from app.schemas.relations.rel_profiles_contacts import \
    Rel_profiles_contacts_create, \
    Rel_profiles_contacts_update
from app.tests.crud.templates.catalogs_crud_tests import \
    Catalogs_tests, \
    Catalogs_tests_with_relation


@pytest.fixture()
def create_schema_in():
    return Contacts_create(
        is_main=False,
        is_deleted=False,
        value=Catalogs_tests.random_lower_string(),
        description=Catalogs_tests.random_lower_string(),
        is_hidden=False,
        type_id=1,
        profile_id=1
    )


@pytest.fixture()
def update_schema_in():
    return Contacts_update(
        is_main=False,
        is_deleted=False,
        value=Catalogs_tests.random_lower_string(),
        description=Catalogs_tests.random_lower_string(),
        is_hidden=True,
        type_id=1,
        profile_id=2
    )


@pytest.fixture()
def update_schema_in_for_flagtest():
    return Contacts_update(
        is_hidden=True,
        value=Catalogs_tests.random_lower_string(),
        description=Catalogs_tests.random_lower_string(),
        is_deleted=True,
        is_main=True,
        type_id=1,
        profile_id=2
    )

@pytest.fixture()
def rel_create_schema_in():
    return Rel_profiles_contacts_create(profile_id=3)


@pytest.fixture()
def rel_update_schema_in():
    return Rel_profiles_contacts_update(profile_id=4)


@pytest.fixture()
def crud():
    return CRUDContacts(model=Contacts)


@pytest.fixture()
def rel_crud():
    return CRUDRel_profiles_contacts(model=Rel_profiles_contacts)


@pytest.fixture()
def rel_fields():
    return ['profile_id']


@pytest.fixture()
def module_name(name: str = 'Contacts') -> str:
    return name


@pytest.fixture()
def id_name_field_in_rel(name: str = 'contact_id') -> str:
    return name


class Catalogs_tests_with_relation_test_flags(Catalogs_tests_with_relation):

    def upd_item_rel(self) -> bool:
        create_obj_in = self.create_schema_in
        update_obj_in = self.update_schema_in
        rel_in = self.rel_update_schema_in
        item_data, rel_data = self.crud.split_obj_in(obj_in=create_obj_in, exclude_unset=True)
        test_item = self.crud.create_not_deleted(db=self.db, obj_in_encoded=item_data)
        new_rel_data = self._prepare_data_for_rel(rel_data=rel_data,
                                                  field_name=self.id_name_field_in_rel,
                                                  value=test_item.id)
        test_rel = self.rel_crud.create(db=self.db, obj_in_dict=new_rel_data)
        current_main = self.rel_crud.get_all_contacts(db=self.db,
                                                      profile_id=new_rel_data['profile_id'],
                                                      except_contact_id=test_item.id)
        # dropped_main = self.crud.drop_main_flag(db=self.db, id=test_item.id)
        # current_main = [d for d in dropped_main if d.is_main]
        # assert not current_main
        try:
            # 'profile_id' в create_obj_in и update_obj_in различается
            # и это должно выбрасывать исключение, т.к. мы не можем
            # изменять 'profile_id' через API contact.
            catalog_item_db = self.crud.update(db=self.db,
                                               item_obj=test_item,
                                               rel_obj=test_rel,
                                               obj_in=update_obj_in)
            # Такое обновление проходить не должно. Если будет выброшено исключение -
            # тогда всё верно.
            assert False
        except Exception as e:
            assert True
            update_obj_in = Contacts_update(is_hidden=True,
                                            value=update_obj_in.value,
                                            description=update_obj_in.description,
                                            is_deleted=True,
                                            is_main=True,
                                            type_id=1,
                                            profile_id=1)
            catalog_item_db = self.crud.update(db=self.db,
                                               item_obj=test_item,
                                               rel_obj=test_rel,
                                               obj_in=update_obj_in)
        for key, value in item_data.items():
            if hasattr(catalog_item_db, key):
                assert str(item_data[key]) == str(getattr(catalog_item_db, key))
        # for key, value in rel_data.items():
        #     if hasattr(catalog_item_db, key):
        #         assert str(rel_data[key]) == str(getattr(catalog_item_db, key))
        return True


@pytest.fixture()
def test_update_catalog_item_with_rel_with_flags(db,
                                                 create_schema_in,
                                                 update_schema_in_for_flagtest,
                                                 rel_create_schema_in,
                                                 rel_update_schema_in,
                                                 crud,
                                                 rel_crud,
                                                 rel_fields,
                                                 module_name,
                                                 name_fields,
                                                 id_name_field_in_rel) -> str:
    catalogs_tests = Catalogs_tests_with_relation_test_flags(db=db,
                                                             create_schema_in=create_schema_in,
                                                             update_schema_in=update_schema_in_for_flagtest,
                                                             rel_create_schema_in=rel_create_schema_in,
                                                             rel_update_schema_in=rel_update_schema_in,
                                                             crud=crud,
                                                             rel_crud=rel_crud,
                                                             rel_fields=rel_fields,
                                                             name_fields=name_fields,
                                                             id_name_field_in_rel=id_name_field_in_rel)
    assert catalogs_tests.upd_item_rel()
    return True


def test_create_contact(test_create_catalog_item_with_rel):
    assert test_create_catalog_item_with_rel


def test_get_contact(test_get_catalog_item_with_rel):
    assert test_get_catalog_item_with_rel


def test_update_contact(test_update_catalog_item_with_rel,
                        test_update_catalog_item_with_rel_with_flags):
    assert test_update_catalog_item_with_rel
    assert test_update_catalog_item_with_rel_with_flags


def test_delete_contact(test_delete_catalog_item_with_rel):
    assert test_delete_catalog_item_with_rel
