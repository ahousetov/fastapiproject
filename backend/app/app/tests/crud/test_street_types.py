import pytest
from app.crud.catalogs.crud_street_types import CRUDStreet_types
from app.models.catalogs.street_types import Street_types
from app.schemas.catalogs.street_types import Street_type_create, Street_type_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Street_type_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Street_type_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDStreet_types(model=Street_types)


def test_create_region(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_region(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_region(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_region(test_delete_catalog_item):
    assert test_delete_catalog_item
