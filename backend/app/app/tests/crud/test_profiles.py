import pytest
import datetime
from app.crud.public.crud_profiles import CRUDProfiles
from app.models.public.profiles import Profiles
from app.schemas.public.profiles import Profiles_create, Profiles_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


# @pytest.fixture()
# def create_schema_in():
#     return Profiles_create(
#         profile_type_id=Catalogs_tests.random_int_number(),
#         client_id=Catalogs_tests.random_int_number(),
#         is_deleted=False,
#         is_active=True,
#         helpdesk_api_key=Catalogs_tests.random_lower_string(),
#         alias_id=Catalogs_tests.random_int_number(minimum=10, maximum=20)
#     )
#
#
# @pytest.fixture()
# def update_schema_in():
#     return Profiles_update(
#         profile_type_id=Catalogs_tests.random_int_number(),
#         client_id=Catalogs_tests.random_int_number(),
#         is_deleted=False,
#         is_active=False,
#         date_activate=datetime.datetime.now(),
#         helpdesk_api_key=Catalogs_tests.random_lower_string(),
#         alias_id=Catalogs_tests.random_int_number(minimum=21, maximum=30)
#     )
#
#
# @pytest.fixture()
# def crud():
#     return CRUDProfiles(model=Profiles)
#
#
# @pytest.fixture()
# def module_name(name: str = 'profiles') -> str:
#     return name


# def test_create_profiles(test_create_catalog_item):
#     assert test_create_catalog_item
#
#
# def test_get_profiles(test_get_catalog_item):
#     assert test_get_catalog_item
#
#
# def test_update_profiles(test_update_catalog_item):
#     assert test_update_catalog_item
#
#
# def test_delete_profiles(test_delete_catalog_item):
#     assert test_delete_catalog_item
