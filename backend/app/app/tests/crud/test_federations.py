import pytest
from app.crud.catalogs.crud_federations import CRUDFederations
from app.models.catalogs.federations import Federations
from app.schemas.catalogs.federations import Federations_create, Federations_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Federations_create(name=Catalogs_tests.random_lower_string(),
                              short_name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Federations_update(name=Catalogs_tests.random_lower_string(),
                              short_name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFederations(model=Federations)


def test_create_federation(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_federation(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_federation(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_federation(test_delete_catalog_item):
    assert test_delete_catalog_item