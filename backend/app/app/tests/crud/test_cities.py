import pytest
from app.crud.catalogs.crud_cities import CRUDCities
from app.models.catalogs.cities import Cities
from app.schemas.catalogs.cities import Cities_create, Cities_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Cities_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Cities_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDCities(model=Cities)


def test_create_cities(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_cities(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_cities(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_cities(test_delete_catalog_item):
    assert test_delete_catalog_item