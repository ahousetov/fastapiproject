import pytest
from app.crud.catalogs.crud_flat_types import CRUDFlat_types
from app.models.catalogs.flat_types import Flat_types
from app.schemas.catalogs.flat_types import Flat_type_create, Flat_type_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Flat_type_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Flat_type_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFlat_types(model=Flat_types)


def test_create_flat_type(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_flat_type(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_flat_type(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_flat_type(test_delete_catalog_item):
    assert test_delete_catalog_item
