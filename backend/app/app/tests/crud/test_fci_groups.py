import pytest
from app.crud.dogs.crud_fci_groups import CRUDFCI_group
from app.models.dogs.fci_groups import FCI_groups
from app.schemas.dogs.fci_groups import FCI_group_create, FCI_group_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return FCI_group_create(name=Catalogs_tests.random_lower_string(),
                            name_en=Catalogs_tests.random_lower_string(),
                            number=Catalogs_tests.random_int_number())


@pytest.fixture()
def update_schema_in():
    return FCI_group_update(name=Catalogs_tests.random_lower_string(),
                            name_en=Catalogs_tests.random_lower_string(),
                            number=Catalogs_tests.random_int_number())


@pytest.fixture()
def crud():
    return CRUDFCI_group(model=FCI_groups)


def test_create_fci_group(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_fci_group(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_fci_group(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_fci_group(test_delete_catalog_item):
    assert test_delete_catalog_item
