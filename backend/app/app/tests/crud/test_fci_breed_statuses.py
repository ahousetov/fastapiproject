import pytest
from app.crud.dogs.crud_fci_breed_statuses import CRUDFCI_breed_statuses
from app.models.dogs.fci_breed_statuses import FCI_breed_statuses
from app.schemas.dogs.fci_breed_statuses import FCI_breed_statuses_create, \
    FCI_breed_statuses_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return FCI_breed_statuses_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return FCI_breed_statuses_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFCI_breed_statuses(model=FCI_breed_statuses)


def test_create_fci_breed_statuses(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_fci_breed_statuses(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_fci_breed_statuses(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_fci_breed_statuses(test_delete_catalog_item):
    assert test_delete_catalog_item