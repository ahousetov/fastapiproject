import pytest
from app.crud.catalogs.crud_house_types import CRUDHouse_types
from app.models.catalogs.house_types import House_types
from app.schemas.catalogs.house_types import House_type_create, House_type_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return House_type_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return House_type_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDHouse_types(model=House_types)


def test_create_region(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_region(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_region(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_region(test_delete_catalog_item):
    assert test_delete_catalog_item
