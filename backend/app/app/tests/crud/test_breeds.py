import pytest
from app.crud.dogs.crud_breeds import CRUDBreeds
from app.crud.relations.crud_rel_breeds_sections_groups import CRUDRel_breeds_sections_groups
from app.models.dogs.breeds import Breeds
from app.models.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups
from app.schemas.dogs.breeds import Breeds_create, Breeds_update
from app.schemas.relations.rel_breeds_sections_groups import Rel_breeds_sections_groups_create, \
    Rel_breeds_sections_groups_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests, \
    Catalogs_tests_with_relation


@pytest.fixture()
def create_schema_in():
    return Breeds_create(name_rus=Catalogs_tests_with_relation.random_lower_string(),
                         name_en=Catalogs_tests_with_relation.random_lower_string(),
                         name_original=Catalogs_tests_with_relation.random_lower_string(),
                         fci_number=1,
                         fci_status_id=2,
                         group_id=3,
                         section_id=4)


@pytest.fixture()
def update_schema_in():
    return Breeds_update(name_rus=Catalogs_tests_with_relation.random_lower_string(),
                         name_en=Catalogs_tests_with_relation.random_lower_string(),
                         name_original=Catalogs_tests_with_relation.random_lower_string(),
                         fci_number=5,
                         fci_status_id=1,
                         group_id=7,
                         section_id=8)


@pytest.fixture()
def rel_create_schema_in():
    return Rel_breeds_sections_groups_create(group_id=7, section_id=8)


@pytest.fixture()
def rel_update_schema_in():
    return Rel_breeds_sections_groups_update(group_id=7, section_id=8)


@pytest.fixture()
def crud():
    return CRUDBreeds(model=Breeds)


@pytest.fixture()
def rel_crud():
    return CRUDRel_breeds_sections_groups(model=Rel_breeds_sections_groups)


@pytest.fixture()
def rel_fields():
    return ['breed_id', 'group_id', 'section_id']


@pytest.fixture()
def module_name(name: str = 'Breeds') -> str:
    return name


@pytest.fixture()
def name_fields():
    return ['name_rus', 'name_en', 'name_original']


@pytest.fixture()
def id_name_field_in_rel(name: str = 'breed_id') -> str:
    return name


def test_create_breeds(test_create_catalog_item_with_rel):
    assert test_create_catalog_item_with_rel


def test_get_breeds(test_get_catalog_item_with_rel):
    assert test_get_catalog_item_with_rel


def test_update_breeds(test_update_catalog_item_with_rel):
    assert test_update_catalog_item_with_rel


def test_delete_breeds(test_delete_catalog_item_with_rel):
    assert test_delete_catalog_item_with_rel