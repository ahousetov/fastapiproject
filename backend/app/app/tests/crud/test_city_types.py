import pytest
from app.crud.catalogs.crud_city_types import CRUDCity_types
from app.models.catalogs.city_types import City_types
from app.schemas.catalogs.city_types import City_types_create, City_types_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return City_types_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return City_types_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDCity_types(model=City_types)


def test_create_city_types(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_city_types(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_city_types(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_city_types(test_delete_catalog_item):
    assert test_delete_catalog_item
