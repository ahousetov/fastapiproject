import pytest
from app.crud.catalogs.crud_federal_district import CRUDFederalDistrict
from app.models.catalogs.federal_district import Federal_Districts
from app.schemas.catalogs.federal_district import Federal_district_create, Federal_district_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Federal_district_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Federal_district_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDFederalDistrict(model=Federal_Districts)


def test_create_region(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_region(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_region(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_region(test_delete_catalog_item):
    assert test_delete_catalog_item