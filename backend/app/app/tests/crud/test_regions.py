import pytest
from app.crud.catalogs.crud_regions import CRUDRegions
from app.models.catalogs.regions import Regions
from app.schemas.catalogs.regions import Region_create, Region_update
from app.tests.crud.templates.catalogs_crud_tests import Catalogs_tests


@pytest.fixture()
def create_schema_in():
    return Region_create(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def update_schema_in():
    return Region_update(name=Catalogs_tests.random_lower_string())


@pytest.fixture()
def crud():
    return CRUDRegions(model=Regions)


def test_create_region(test_create_catalog_item):
    assert test_create_catalog_item


def test_get_region(test_get_catalog_item):
    assert test_get_catalog_item


def test_update_region(test_update_catalog_item):
    assert test_update_catalog_item


def test_delete_region(test_delete_catalog_item):
    assert test_delete_catalog_item

