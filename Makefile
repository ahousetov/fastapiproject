lsc:
			@# lsc - (Win)(Lin)Текущие контейнеры.
			@echo "\033[1mТекущие контейнеры:\033[0m\n"
			@docker ps -a

lsi:
			@# lsi - (Win)(Lin)Текущие образы.
			@echo "\033[1mТекущие образы:\033[0m\n"
			@docker images

clearall:
			@# clearall - (Win)(Lin)удаление ВСЕХ контейнеров и образов.
			@echo "\033[1mУдаляю  все контейнеры...\n\033[0m\n"
			@- docker rm -f $$(docker ps -a -q)
			@- docker rmi $$(docker images -q)



venv:
			@# venv - (Lin)установка PIP, виртуального окружения (venv) и системные зависимости для установки.
			@echo "\033[1mУстанавливаю окружение и зависимости...\n\033[0m\n"
			@sudo apt install python3-pip curl python3-venv -y
			@python3 -m pip --version
			@python3 -m pip install --upgrade pip
			@python3 -m pip install virtualenv
		   	@python3 -m venv env38
			@(\
       			. ./env38/bin/activate; \
       			curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python - ; \
    			export PATH="${HOME}/.local/bin:${PATH}" && cd backend/app && poetry --version && poetry install \
			)

pre:
			@# pre - (Win)(Lin)Подготовка ДБ и самого проекта к работе
			@echo "\033[1mПодготовительные работы...\n\033[0m\n"
			@cd backend &&  \
			python3 app/app/backend_pre_start.py && \
			cd app &&  \
			alembic upgrade head && \
			cd .. &&  \
			python3 app/app/initial_data.py && \
			python3 app/app/celeryworker_pre_start.py && \
			cd ../..

run:
			@# run - (Win)(Lin)Запускаем backend локально
			@echo "\033[1mНачинаем работу приложения локально.\n\033[0m\n"
			@(\
       			. ./env38/bin/activate; \
				python3 ./backend/app/app/main.py \
			)


up:
			@# up - (Win)(Lin)"поднимаем" созданные контейнеры. Если контейнеры не созданы - она создадуться.
			# Помним, что по умолчанию docker-compose при запуске
			# требует root-привелегий, чтобы получить доступ к docker-daemon.
			# Чтобы избежать запуска из под root, создаём группу docker и добавляем
			# в неё пользователя.
			#            ifeq (${shell uname}, Linux)
			#				@echo Мы используем Linux!
			#            endif
			#            ifeq (${shell uname}, Winodws)
			#				@echo Мы используем Windows!
			#            endif
			@echo "\033[1mПоднимаем контейнеры...\n\033[0m\n"
			@docker-compose up


build:
			@# build - (Win)(Lin)сборка и настройка контейнеров.
			# Помним, что по умолчанию docker-compose при запуске
			# требует root-привелегий, чтобы получить доступ к docker-daemon.
			# Чтобы избежать запуска из под root, создаём группу docker и добавляем
			# в неё пользователя.
			#@if [ "uname" == "Linux" ]; then \
            #- sudo groupadd docker \
            #- sudo usermod -aG docker $$USER \
            #fi
			@echo "\033[1mСтроим контейнеры...\n\033[0m\n"
			@docker-compose build

tests:		
			@# tests - (Win)(Lin)Запускаем тесты для backend.
			@echo "\033[1mТестируем...\n\033[0m\n"
			@set -e && \
			python3 backend/app/app/tests_pre_start.py && \
			set -x && \
			pytest

help:
			@# help - (Lin)справка.
			@./MakefileHelp.sh
